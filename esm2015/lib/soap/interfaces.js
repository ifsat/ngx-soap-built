/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IXmlAttribute() { }
if (false) {
    /** @type {?} */
    IXmlAttribute.prototype.name;
    /** @type {?} */
    IXmlAttribute.prototype.value;
}
/**
 * @record
 */
export function IWsdlBaseOptions() { }
if (false) {
    /** @type {?|undefined} */
    IWsdlBaseOptions.prototype.attributesKey;
    /** @type {?|undefined} */
    IWsdlBaseOptions.prototype.valueKey;
    /** @type {?|undefined} */
    IWsdlBaseOptions.prototype.xmlKey;
    /** @type {?|undefined} */
    IWsdlBaseOptions.prototype.overrideRootElement;
    /** @type {?|undefined} */
    IWsdlBaseOptions.prototype.ignoredNamespaces;
    /** @type {?|undefined} */
    IWsdlBaseOptions.prototype.ignoreBaseNameSpaces;
    /** @type {?|undefined} */
    IWsdlBaseOptions.prototype.escapeXML;
    /** @type {?|undefined} */
    IWsdlBaseOptions.prototype.returnFault;
    /** @type {?|undefined} */
    IWsdlBaseOptions.prototype.handleNilAsNull;
    /** @type {?|undefined} */
    IWsdlBaseOptions.prototype.wsdl_headers;
    /** @type {?|undefined} */
    IWsdlBaseOptions.prototype.wsdl_options;
}
/**
 * @record
 */
export function Definitions() { }
if (false) {
    /** @type {?} */
    Definitions.prototype.descriptions;
    /** @type {?} */
    Definitions.prototype.ignoredNamespaces;
    /** @type {?} */
    Definitions.prototype.messages;
    /** @type {?} */
    Definitions.prototype.portTypes;
    /** @type {?} */
    Definitions.prototype.bindings;
    /** @type {?} */
    Definitions.prototype.services;
    /** @type {?} */
    Definitions.prototype.schemas;
    /** @type {?} */
    Definitions.prototype.valueKey;
    /** @type {?} */
    Definitions.prototype.xmlKey;
    /** @type {?} */
    Definitions.prototype.xmlns;
    /* Skipping unnamed member:
    '$targetNamespace': string;*/
    /* Skipping unnamed member:
    '$name': string;*/
}
/**
 * @record
 */
export function WsdlSchemas() { }
/**
 * @record
 */
export function WsdlSchema() { }
if (false) {
    /** @type {?} */
    WsdlSchema.prototype.children;
    /** @type {?|undefined} */
    WsdlSchema.prototype.complexTypes;
    /** @type {?|undefined} */
    WsdlSchema.prototype.elements;
    /** @type {?} */
    WsdlSchema.prototype.includes;
    /** @type {?} */
    WsdlSchema.prototype.name;
    /** @type {?} */
    WsdlSchema.prototype.nsName;
    /** @type {?} */
    WsdlSchema.prototype.prefix;
    /** @type {?|undefined} */
    WsdlSchema.prototype.types;
    /** @type {?} */
    WsdlSchema.prototype.xmlns;
}
/**
 * @record
 */
export function WsdlElements() { }
/**
 * @record
 */
export function WsdlXmlns() { }
if (false) {
    /** @type {?|undefined} */
    WsdlXmlns.prototype.wsu;
    /** @type {?|undefined} */
    WsdlXmlns.prototype.wsp;
    /** @type {?|undefined} */
    WsdlXmlns.prototype.wsam;
    /** @type {?|undefined} */
    WsdlXmlns.prototype.soap;
    /** @type {?|undefined} */
    WsdlXmlns.prototype.tns;
    /** @type {?|undefined} */
    WsdlXmlns.prototype.xsd;
    /** @type {?|undefined} */
    WsdlXmlns.prototype.__tns__;
    /* Skipping unhandled member: [prop: string]: string | void;*/
}
/**
 * @record
 */
export function XsdComplexType() { }
if (false) {
    /** @type {?} */
    XsdComplexType.prototype.children;
    /** @type {?} */
    XsdComplexType.prototype.name;
    /** @type {?} */
    XsdComplexType.prototype.nsName;
    /** @type {?} */
    XsdComplexType.prototype.prefix;
    /* Skipping unnamed member:
    '$name': string;*/
    /* Skipping unhandled member: [prop: string]: any;*/
}
/**
 * @record
 */
export function XsdElementType() { }
if (false) {
    /** @type {?} */
    XsdElementType.prototype.children;
    /** @type {?} */
    XsdElementType.prototype.name;
    /** @type {?} */
    XsdElementType.prototype.nsName;
    /** @type {?} */
    XsdElementType.prototype.prefix;
    /** @type {?} */
    XsdElementType.prototype.targetNSAlias;
    /** @type {?} */
    XsdElementType.prototype.targetNamespace;
    /* Skipping unnamed member:
    '$lookupType': string;*/
    /* Skipping unnamed member:
    '$lookupTypes': any[];*/
    /* Skipping unnamed member:
    '$name': string;*/
    /* Skipping unnamed member:
    '$type': string;*/
    /* Skipping unhandled member: [prop: string]: any;*/
}
/**
 * @record
 */
export function WsdlMessages() { }
/**
 * @record
 */
export function WsdlMessage() { }
if (false) {
    /** @type {?} */
    WsdlMessage.prototype.element;
    /** @type {?} */
    WsdlMessage.prototype.parts;
    /* Skipping unnamed member:
    '$name': string;*/
}
/**
 * @record
 */
export function WsdlPortTypes() { }
/**
 * @record
 */
export function WsdlPortType() { }
if (false) {
    /** @type {?} */
    WsdlPortType.prototype.methods;
}
/**
 * @record
 */
export function WsdlBindings() { }
/**
 * @record
 */
export function WsdlBinding() { }
if (false) {
    /** @type {?} */
    WsdlBinding.prototype.methods;
    /** @type {?} */
    WsdlBinding.prototype.style;
    /** @type {?} */
    WsdlBinding.prototype.transport;
    /** @type {?} */
    WsdlBinding.prototype.topElements;
}
/**
 * @record
 */
export function WsdlServices() { }
/**
 * @record
 */
export function WsdlService() { }
if (false) {
    /** @type {?} */
    WsdlService.prototype.ports;
}
/**
 * @record
 */
export function XsdTypeBase() { }
if (false) {
    /** @type {?} */
    XsdTypeBase.prototype.ignoredNamespaces;
    /** @type {?} */
    XsdTypeBase.prototype.valueKey;
    /** @type {?} */
    XsdTypeBase.prototype.xmlKey;
    /** @type {?|undefined} */
    XsdTypeBase.prototype.xmlns;
}
/**
 * @record
 */
export function IOptions() { }
if (false) {
    /** @type {?|undefined} */
    IOptions.prototype.disableCache;
    /** @type {?|undefined} */
    IOptions.prototype.endpoint;
    /** @type {?|undefined} */
    IOptions.prototype.envelopeKey;
    /** @type {?|undefined} */
    IOptions.prototype.httpClient;
    /** @type {?|undefined} */
    IOptions.prototype.stream;
    /** @type {?|undefined} */
    IOptions.prototype.forceSoap12Headers;
    /** @type {?|undefined} */
    IOptions.prototype.customDeserializer;
    /* Skipping unhandled member: [key: string]: any;*/
}
/**
 * @record
 */
export function WSDL() { }
if (false) {
    /** @type {?} */
    WSDL.prototype.ignoredNamespaces;
    /** @type {?} */
    WSDL.prototype.ignoreBaseNameSpaces;
    /** @type {?} */
    WSDL.prototype.valueKey;
    /** @type {?} */
    WSDL.prototype.xmlKey;
    /** @type {?} */
    WSDL.prototype.xmlnsInEnvelope;
    /** @type {?} */
    WSDL.prototype.uri;
    /** @type {?} */
    WSDL.prototype.definitions;
    /**
     * @param {?} definition
     * @param {?} uri
     * @param {?=} options
     * @return {?}
     */
    WSDL.prototype.constructor = function (definition, uri, options) { };
    /**
     * @param {?} callback
     * @return {?}
     */
    WSDL.prototype.onReady = function (callback) { };
    /**
     * @param {?} callback
     * @return {?}
     */
    WSDL.prototype.processIncludes = function (callback) { };
    /**
     * @return {?}
     */
    WSDL.prototype.describeServices = function () { };
    /**
     * @return {?}
     */
    WSDL.prototype.toXML = function () { };
    /**
     * @param {?} xml
     * @param {?=} callback
     * @return {?}
     */
    WSDL.prototype.xmlToObject = function (xml, callback) { };
    /**
     * @param {?} nsURI
     * @param {?} qname
     * @return {?}
     */
    WSDL.prototype.findSchemaObject = function (nsURI, qname) { };
    /**
     * @param {?} name
     * @param {?} params
     * @param {?=} nsPrefix
     * @param {?=} nsURI
     * @param {?=} type
     * @return {?}
     */
    WSDL.prototype.objectToDocumentXML = function (name, params, nsPrefix, nsURI, type) { };
    /**
     * @param {?} name
     * @param {?} params
     * @param {?=} nsPrefix
     * @param {?=} nsURI
     * @param {?=} isParts
     * @return {?}
     */
    WSDL.prototype.objectToRpcXML = function (name, params, nsPrefix, nsURI, isParts) { };
    /**
     * @param {?} ns
     * @return {?}
     */
    WSDL.prototype.isIgnoredNameSpace = function (ns) { };
    /**
     * @param {?} ns
     * @return {?}
     */
    WSDL.prototype.filterOutIgnoredNameSpace = function (ns) { };
    /**
     * @param {?} obj
     * @param {?} name
     * @param {?=} nsPrefix
     * @param {?=} nsURI
     * @param {?=} isFirst
     * @param {?=} xmlnsAttr
     * @param {?=} schemaObject
     * @param {?=} nsContext
     * @return {?}
     */
    WSDL.prototype.objectToXML = function (obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext) { };
    /**
     * @param {?} child
     * @param {?} nsContext
     * @return {?}
     */
    WSDL.prototype.processAttributes = function (child, nsContext) { };
    /**
     * @param {?} name
     * @param {?} nsURI
     * @return {?}
     */
    WSDL.prototype.findSchemaType = function (name, nsURI) { };
    /**
     * @param {?} parameterTypeObj
     * @param {?} childName
     * @param {?=} backtrace
     * @return {?}
     */
    WSDL.prototype.findChildSchemaObject = function (parameterTypeObj, childName, backtrace) { };
}
/**
 * @record
 */
export function Client() { }
if (false) {
    /** @type {?} */
    Client.prototype.wsdl;
    /* Skipping unhandled member: [method: string]: ISoapMethod | WSDL | Function;*/
    /**
     * @param {?} wsdl
     * @param {?=} endpoint
     * @param {?=} options
     * @return {?}
     */
    Client.prototype.constructor = function (wsdl, endpoint, options) { };
    /**
     * @param {?} bodyAttribute
     * @param {?=} name
     * @param {?=} namespace
     * @param {?=} xmlns
     * @return {?}
     */
    Client.prototype.addBodyAttribute = function (bodyAttribute, name, namespace, xmlns) { };
    /**
     * @param {?} name
     * @param {?} value
     * @return {?}
     */
    Client.prototype.addHttpHeader = function (name, value) { };
    /**
     * @param {?} soapHeader
     * @param {?=} name
     * @param {?=} namespace
     * @param {?=} xmlns
     * @return {?}
     */
    Client.prototype.addSoapHeader = function (soapHeader, name, namespace, xmlns) { };
    /**
     * @param {?} index
     * @param {?} soapHeader
     * @param {?=} name
     * @param {?=} namespace
     * @param {?=} xmlns
     * @return {?}
     */
    Client.prototype.changeSoapHeader = function (index, soapHeader, name, namespace, xmlns) { };
    /**
     * @return {?}
     */
    Client.prototype.clearBodyAttributes = function () { };
    /**
     * @return {?}
     */
    Client.prototype.clearHttpHeaders = function () { };
    /**
     * @return {?}
     */
    Client.prototype.clearSoapHeaders = function () { };
    /**
     * @return {?}
     */
    Client.prototype.describe = function () { };
    /**
     * @return {?}
     */
    Client.prototype.getBodyAttributes = function () { };
    /**
     * @return {?}
     */
    Client.prototype.getHttpHeaders = function () { };
    /**
     * @return {?}
     */
    Client.prototype.getSoapHeaders = function () { };
    /**
     * @param {?} endpoint
     * @return {?}
     */
    Client.prototype.setEndpoint = function (endpoint) { };
    /**
     * @param {?} action
     * @return {?}
     */
    Client.prototype.setSOAPAction = function (action) { };
    /**
     * @param {?} security
     * @return {?}
     */
    Client.prototype.setSecurity = function (security) { };
    /**
     * @param {?} method
     * @param {?} body
     * @param {?=} options
     * @param {?=} extraHeaders
     * @return {?}
     */
    Client.prototype.call = function (method, body, options, extraHeaders) { };
}
/**
 * @record
 */
export function ISoapMethod() { }
/**
 * @record
 */
export function ISoapMethodResponse() { }
if (false) {
    /** @type {?} */
    ISoapMethodResponse.prototype.err;
    /** @type {?} */
    ISoapMethodResponse.prototype.header;
    /** @type {?} */
    ISoapMethodResponse.prototype.responseBody;
    /** @type {?} */
    ISoapMethodResponse.prototype.xml;
    /** @type {?} */
    ISoapMethodResponse.prototype.result;
    /** @type {?} */
    ISoapMethodResponse.prototype.httpHeaders;
}
/**
 * @record
 */
export function ISecurity() { }
if (false) {
    /**
     * @param {?} options
     * @return {?}
     */
    ISecurity.prototype.addOptions = function (options) { };
    /**
     * @return {?}
     */
    ISecurity.prototype.toXML = function () { };
}
/**
 * @record
 */
export function BasicAuthSecurity() { }
if (false) {
    /**
     * @param {?} username
     * @param {?} password
     * @param {?=} defaults
     * @return {?}
     */
    BasicAuthSecurity.prototype.constructor = function (username, password, defaults) { };
    /**
     * @param {?} headers
     * @return {?}
     */
    BasicAuthSecurity.prototype.addHeaders = function (headers) { };
    /**
     * @param {?} options
     * @return {?}
     */
    BasicAuthSecurity.prototype.addOptions = function (options) { };
    /**
     * @return {?}
     */
    BasicAuthSecurity.prototype.toXML = function () { };
}
/**
 * @record
 */
export function BearerSecurity() { }
if (false) {
    /**
     * @param {?} token
     * @param {?=} defaults
     * @return {?}
     */
    BearerSecurity.prototype.constructor = function (token, defaults) { };
    /**
     * @param {?} headers
     * @return {?}
     */
    BearerSecurity.prototype.addHeaders = function (headers) { };
    /**
     * @param {?} options
     * @return {?}
     */
    BearerSecurity.prototype.addOptions = function (options) { };
    /**
     * @return {?}
     */
    BearerSecurity.prototype.toXML = function () { };
}
/**
 * @record
 */
export function WSSecurity() { }
if (false) {
    /**
     * @param {?} username
     * @param {?} password
     * @param {?=} options
     * @return {?}
     */
    WSSecurity.prototype.constructor = function (username, password, options) { };
    /**
     * @param {?} options
     * @return {?}
     */
    WSSecurity.prototype.addOptions = function (options) { };
    /**
     * @return {?}
     */
    WSSecurity.prototype.toXML = function () { };
}
/**
 * @record
 */
export function WSSecurityCert() { }
if (false) {
    /**
     * @param {?} privatePEM
     * @param {?} publicP12PEM
     * @param {?} password
     * @return {?}
     */
    WSSecurityCert.prototype.constructor = function (privatePEM, publicP12PEM, password) { };
    /**
     * @param {?} options
     * @return {?}
     */
    WSSecurityCert.prototype.addOptions = function (options) { };
    /**
     * @return {?}
     */
    WSSecurityCert.prototype.toXML = function () { };
}
/**
 * @record
 */
export function NTLMSecurity() { }
if (false) {
    /**
     * @param {?} username
     * @param {?} password
     * @param {?} domain
     * @param {?} workstation
     * @return {?}
     */
    NTLMSecurity.prototype.constructor = function (username, password, domain, workstation) { };
    /**
     * @param {?} headers
     * @return {?}
     */
    NTLMSecurity.prototype.addHeaders = function (headers) { };
    /**
     * @param {?} options
     * @return {?}
     */
    NTLMSecurity.prototype.addOptions = function (options) { };
    /**
     * @return {?}
     */
    NTLMSecurity.prototype.toXML = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1zb2FwLyIsInNvdXJjZXMiOlsibGliL3NvYXAvaW50ZXJmYWNlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBSUEsbUNBR0M7OztJQUZHLDZCQUFhOztJQUNiLDhCQUFjOzs7OztBQUdsQixzQ0FZQzs7O0lBWEcseUNBQXVCOztJQUN2QixvQ0FBa0I7O0lBQ2xCLGtDQUFnQjs7SUFDaEIsK0NBQWdGOztJQUNoRiw2Q0FBd0Y7O0lBQ3hGLGdEQUErQjs7SUFDL0IscUNBQW9COztJQUNwQix1Q0FBc0I7O0lBQ3RCLDJDQUEwQjs7SUFDMUIsd0NBQXNDOztJQUN0Qyx3Q0FBc0M7Ozs7O0FBRzFDLGlDQWFDOzs7SUFaRyxtQ0FBcUI7O0lBQ3JCLHdDQUE0Qjs7SUFDNUIsK0JBQXVCOztJQUN2QixnQ0FBeUI7O0lBQ3pCLCtCQUF1Qjs7SUFDdkIsK0JBQXVCOztJQUN2Qiw4QkFBcUI7O0lBQ3JCLCtCQUFpQjs7SUFDakIsNkJBQWU7O0lBQ2YsNEJBQWlCOzs7Ozs7Ozs7QUFNckIsaUNBRUM7Ozs7QUFDRCxnQ0FVQzs7O0lBVEcsOEJBQWdCOztJQUNoQixrQ0FBNEI7O0lBQzVCLDhCQUF3Qjs7SUFDeEIsOEJBQWdCOztJQUNoQiwwQkFBYTs7SUFDYiw0QkFBZTs7SUFDZiw0QkFBZTs7SUFDZiwyQkFBcUI7O0lBQ3JCLDJCQUFpQjs7Ozs7QUFFckIsa0NBRUM7Ozs7QUFHRCwrQkFTQzs7O0lBUkcsd0JBQWE7O0lBQ2Isd0JBQWE7O0lBQ2IseUJBQWM7O0lBQ2QseUJBQWM7O0lBQ2Qsd0JBQWE7O0lBQ2Isd0JBQWE7O0lBQ2IsNEJBQWlCOzs7Ozs7QUFJckIsb0NBT0M7OztJQU5HLGtDQUE4Qjs7SUFDOUIsOEJBQWE7O0lBQ2IsZ0NBQWU7O0lBQ2YsZ0NBQWU7Ozs7Ozs7O0FBS25CLG9DQVlDOzs7SUFYRyxrQ0FBOEI7O0lBQzlCLDhCQUFhOztJQUNiLGdDQUFlOztJQUNmLGdDQUFlOztJQUNmLHVDQUFzQjs7SUFDdEIseUNBQXdCOzs7Ozs7Ozs7Ozs7OztBQVE1QixrQ0FFQzs7OztBQUNELGlDQUlDOzs7SUFIRyw4QkFBb0I7O0lBQ3BCLDRCQUErQjs7Ozs7OztBQUluQyxtQ0FFQzs7OztBQUNELGtDQUVDOzs7SUFERywrQkFBdUM7Ozs7O0FBRzNDLGtDQUVDOzs7O0FBQ0QsaUNBS0M7OztJQUpHLDhCQUFzQjs7SUFDdEIsNEJBQWM7O0lBQ2QsZ0NBQWtCOztJQUNsQixrQ0FBbUM7Ozs7O0FBR3ZDLGtDQUVDOzs7O0FBQ0QsaUNBRUM7OztJQURHLDRCQUE2Qjs7Ozs7QUFHakMsaUNBS0M7OztJQUpHLHdDQUE0Qjs7SUFDNUIsK0JBQWlCOztJQUNqQiw2QkFBZTs7SUFDZiw0QkFBa0I7Ozs7O0FBR3RCLDhCQVdDOzs7SUFWRyxnQ0FBdUI7O0lBQ3ZCLDRCQUFrQjs7SUFDbEIsK0JBQXFCOztJQUNyQiw4QkFBd0I7O0lBRXhCLDBCQUFpQjs7SUFFakIsc0NBQTZCOztJQUM3QixzQ0FBeUI7Ozs7OztBQUk3QiwwQkF1QkM7OztJQXJCRyxpQ0FBNEI7O0lBQzVCLG9DQUE4Qjs7SUFDOUIsd0JBQWlCOztJQUNqQixzQkFBZTs7SUFDZiwrQkFBd0I7O0lBZXhCLG1CQUFZOztJQUNaLDJCQUF5Qjs7Ozs7OztJQXJCekIscUVBQThEOzs7OztJQU05RCxpREFBNkM7Ozs7O0lBQzdDLHlEQUFxRDs7OztJQUNyRCxrREFBeUM7Ozs7SUFDekMsdUNBQWdCOzs7Ozs7SUFDaEIsMERBQXVFOzs7Ozs7SUFDdkUsOERBQThFOzs7Ozs7Ozs7SUFDOUUsd0ZBQXNHOzs7Ozs7Ozs7SUFDdEcsc0ZBQW9HOzs7OztJQUNwRyxzREFBd0M7Ozs7O0lBQ3hDLDZEQUE4Qzs7Ozs7Ozs7Ozs7O0lBQzlDLG9IQUFxSjs7Ozs7O0lBQ3JKLG1FQUFzRDs7Ozs7O0lBQ3RELDJEQUEyQzs7Ozs7OztJQUMzQyw2RkFBbUY7Ozs7O0FBS3ZGLDRCQW1CQzs7O0lBSEcsc0JBQVc7Ozs7Ozs7O0lBZlgsc0VBQStEOzs7Ozs7OztJQUMvRCx5RkFBOEY7Ozs7OztJQUM5Riw0REFBOEM7Ozs7Ozs7O0lBQzlDLG1GQUF1Rjs7Ozs7Ozs7O0lBQ3ZGLDZGQUEwRzs7OztJQUMxRyx1REFBNEI7Ozs7SUFDNUIsb0RBQXlCOzs7O0lBQ3pCLG9EQUF5Qjs7OztJQUN6Qiw0Q0FBZ0I7Ozs7SUFDaEIscURBQTJCOzs7O0lBQzNCLGtEQUF5Qzs7OztJQUN6QyxrREFBMkI7Ozs7O0lBQzNCLHVEQUFvQzs7Ozs7SUFDcEMsdURBQW9DOzs7OztJQUNwQyx1REFBdUM7Ozs7Ozs7O0lBR3ZDLDJFQUFvRzs7Ozs7QUFHeEcsaUNBRUM7Ozs7QUFFRCx5Q0FPQzs7O0lBTkcsa0NBQVM7O0lBQ1QscUNBQVk7O0lBQ1osMkNBQXFCOztJQUNyQixrQ0FBWTs7SUFDWixxQ0FBWTs7SUFDWiwwQ0FBaUI7Ozs7O0FBR3JCLCtCQUdDOzs7Ozs7SUFGRyx3REFBK0I7Ozs7SUFDL0IsNENBQWdCOzs7OztBQUdwQix1Q0FLQzs7Ozs7Ozs7SUFKRyxzRkFBZ0U7Ozs7O0lBQ2hFLGdFQUErQjs7Ozs7SUFDL0IsZ0VBQStCOzs7O0lBQy9CLG9EQUFnQjs7Ozs7QUFHcEIsb0NBS0M7Ozs7Ozs7SUFKRyxzRUFBMkM7Ozs7O0lBQzNDLDZEQUErQjs7Ozs7SUFDL0IsNkRBQStCOzs7O0lBQy9CLGlEQUFnQjs7Ozs7QUFHcEIsZ0NBSUM7Ozs7Ozs7O0lBSEcsOEVBQStEOzs7OztJQUMvRCx5REFBK0I7Ozs7SUFDL0IsNkNBQWdCOzs7OztBQUdwQixvQ0FJQzs7Ozs7Ozs7SUFIRyx5RkFBK0Q7Ozs7O0lBQy9ELDZEQUErQjs7OztJQUMvQixpREFBZ0I7Ozs7O0FBR3BCLGtDQUtDOzs7Ozs7Ozs7SUFKRyw0RkFBNkU7Ozs7O0lBQzdFLDJEQUErQjs7Ozs7SUFDL0IsMkRBQStCOzs7O0lBQy9CLCtDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEV2ZW50RW1pdHRlciB9IGZyb20gJ2V2ZW50cyc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuXG5leHBvcnQgaW50ZXJmYWNlIElYbWxBdHRyaWJ1dGUge1xuICAgIG5hbWU6IHN0cmluZztcbiAgICB2YWx1ZTogc3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElXc2RsQmFzZU9wdGlvbnMge1xuICAgIGF0dHJpYnV0ZXNLZXk/OiBzdHJpbmc7XG4gICAgdmFsdWVLZXk/OiBzdHJpbmc7XG4gICAgeG1sS2V5Pzogc3RyaW5nO1xuICAgIG92ZXJyaWRlUm9vdEVsZW1lbnQ/OiB7IG5hbWVzcGFjZTogc3RyaW5nOyB4bWxuc0F0dHJpYnV0ZXM/OiBJWG1sQXR0cmlidXRlW107IH07XG4gICAgaWdub3JlZE5hbWVzcGFjZXM/OiBib29sZWFuIHwgc3RyaW5nW10gfCB7IG5hbWVzcGFjZXM/OiBzdHJpbmdbXTsgb3ZlcnJpZGU/OiBib29sZWFuOyB9O1xuICAgIGlnbm9yZUJhc2VOYW1lU3BhY2VzPzogYm9vbGVhbjtcbiAgICBlc2NhcGVYTUw/OiBib29sZWFuO1xuICAgIHJldHVybkZhdWx0PzogYm9vbGVhbjtcbiAgICBoYW5kbGVOaWxBc051bGw/OiBib29sZWFuO1xuICAgIHdzZGxfaGVhZGVycz86IHsgW2tleTogc3RyaW5nXTogYW55IH07XG4gICAgd3NkbF9vcHRpb25zPzogeyBba2V5OiBzdHJpbmddOiBhbnkgfTtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBEZWZpbml0aW9ucyB7XG4gICAgZGVzY3JpcHRpb25zOiBvYmplY3Q7XG4gICAgaWdub3JlZE5hbWVzcGFjZXM6IHN0cmluZ1tdO1xuICAgIG1lc3NhZ2VzOiBXc2RsTWVzc2FnZXM7XG4gICAgcG9ydFR5cGVzOiBXc2RsUG9ydFR5cGVzO1xuICAgIGJpbmRpbmdzOiBXc2RsQmluZGluZ3M7XG4gICAgc2VydmljZXM6IFdzZGxTZXJ2aWNlcztcbiAgICBzY2hlbWFzOiBXc2RsU2NoZW1hcztcbiAgICB2YWx1ZUtleTogc3RyaW5nO1xuICAgIHhtbEtleTogc3RyaW5nO1xuICAgIHhtbG5zOiBXc2RsWG1sbnM7XG4gICAgJyR0YXJnZXROYW1lc3BhY2UnOiBzdHJpbmc7XG4gICAgJyRuYW1lJzogc3RyaW5nO1xufVxuXG5cbmV4cG9ydCBpbnRlcmZhY2UgV3NkbFNjaGVtYXMge1xuICAgIFtwcm9wOiBzdHJpbmddOiBXc2RsU2NoZW1hO1xufVxuZXhwb3J0IGludGVyZmFjZSBXc2RsU2NoZW1hIGV4dGVuZHMgWHNkVHlwZUJhc2Uge1xuICAgIGNoaWxkcmVuOiBhbnlbXTtcbiAgICBjb21wbGV4VHlwZXM/OiBXc2RsRWxlbWVudHM7XG4gICAgZWxlbWVudHM/OiBXc2RsRWxlbWVudHM7XG4gICAgaW5jbHVkZXM6IGFueVtdO1xuICAgIG5hbWU6IHN0cmluZztcbiAgICBuc05hbWU6IHN0cmluZztcbiAgICBwcmVmaXg6IHN0cmluZztcbiAgICB0eXBlcz86IFdzZGxFbGVtZW50cztcbiAgICB4bWxuczogV3NkbFhtbG5zO1xufVxuZXhwb3J0IGludGVyZmFjZSBXc2RsRWxlbWVudHMge1xuICAgIFtwcm9wOiBzdHJpbmddOiBYc2RFbGVtZW50O1xufVxuZXhwb3J0IHR5cGUgWHNkRWxlbWVudCA9IFhzZEVsZW1lbnRUeXBlIHwgWHNkQ29tcGxleFR5cGU7XG5cbmV4cG9ydCBpbnRlcmZhY2UgV3NkbFhtbG5zIHtcbiAgICB3c3U/OiBzdHJpbmc7XG4gICAgd3NwPzogc3RyaW5nO1xuICAgIHdzYW0/OiBzdHJpbmc7XG4gICAgc29hcD86IHN0cmluZztcbiAgICB0bnM/OiBzdHJpbmc7XG4gICAgeHNkPzogc3RyaW5nO1xuICAgIF9fdG5zX18/OiBzdHJpbmc7XG4gICAgW3Byb3A6IHN0cmluZ106IHN0cmluZyB8IHZvaWQ7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgWHNkQ29tcGxleFR5cGUgZXh0ZW5kcyBYc2RUeXBlQmFzZSB7XG4gICAgY2hpbGRyZW46IFhzZEVsZW1lbnRbXSB8IHZvaWQ7XG4gICAgbmFtZTogc3RyaW5nO1xuICAgIG5zTmFtZTogc3RyaW5nO1xuICAgIHByZWZpeDogc3RyaW5nO1xuICAgICckbmFtZSc6IHN0cmluZztcbiAgICBbcHJvcDogc3RyaW5nXTogYW55O1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIFhzZEVsZW1lbnRUeXBlIGV4dGVuZHMgWHNkVHlwZUJhc2Uge1xuICAgIGNoaWxkcmVuOiBYc2RFbGVtZW50W10gfCB2b2lkO1xuICAgIG5hbWU6IHN0cmluZztcbiAgICBuc05hbWU6IHN0cmluZztcbiAgICBwcmVmaXg6IHN0cmluZztcbiAgICB0YXJnZXROU0FsaWFzOiBzdHJpbmc7XG4gICAgdGFyZ2V0TmFtZXNwYWNlOiBzdHJpbmc7XG4gICAgJyRsb29rdXBUeXBlJzogc3RyaW5nO1xuICAgICckbG9va3VwVHlwZXMnOiBhbnlbXTtcbiAgICAnJG5hbWUnOiBzdHJpbmc7XG4gICAgJyR0eXBlJzogc3RyaW5nO1xuICAgIFtwcm9wOiBzdHJpbmddOiBhbnk7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgV3NkbE1lc3NhZ2VzIHtcbiAgICBbcHJvcDogc3RyaW5nXTogV3NkbE1lc3NhZ2U7XG59XG5leHBvcnQgaW50ZXJmYWNlIFdzZGxNZXNzYWdlIGV4dGVuZHMgWHNkVHlwZUJhc2Uge1xuICAgIGVsZW1lbnQ6IFhzZEVsZW1lbnQ7XG4gICAgcGFydHM6IHsgW3Byb3A6IHN0cmluZ106IGFueSB9O1xuICAgICckbmFtZSc6IHN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBXc2RsUG9ydFR5cGVzIHtcbiAgICBbcHJvcDogc3RyaW5nXTogV3NkbFBvcnRUeXBlO1xufVxuZXhwb3J0IGludGVyZmFjZSBXc2RsUG9ydFR5cGUgZXh0ZW5kcyBYc2RUeXBlQmFzZSB7XG4gICAgbWV0aG9kczogeyBbcHJvcDogc3RyaW5nXTogWHNkRWxlbWVudCB9XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgV3NkbEJpbmRpbmdzIHtcbiAgICBbcHJvcDogc3RyaW5nXTogV3NkbEJpbmRpbmc7XG59XG5leHBvcnQgaW50ZXJmYWNlIFdzZGxCaW5kaW5nIGV4dGVuZHMgWHNkVHlwZUJhc2Uge1xuICAgIG1ldGhvZHM6IFdzZGxFbGVtZW50cztcbiAgICBzdHlsZTogc3RyaW5nO1xuICAgIHRyYW5zcG9ydDogc3RyaW5nO1xuICAgIHRvcEVsZW1lbnRzOiB7W3Byb3A6IHN0cmluZ106IGFueX07XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgV3NkbFNlcnZpY2VzIHtcbiAgICBbcHJvcDogc3RyaW5nXTogV3NkbFNlcnZpY2U7XG59XG5leHBvcnQgaW50ZXJmYWNlIFdzZGxTZXJ2aWNlIGV4dGVuZHMgWHNkVHlwZUJhc2Uge1xuICAgIHBvcnRzOiB7W3Byb3A6IHN0cmluZ106IGFueX07XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgWHNkVHlwZUJhc2Uge1xuICAgIGlnbm9yZWROYW1lc3BhY2VzOiBzdHJpbmdbXTtcbiAgICB2YWx1ZUtleTogc3RyaW5nO1xuICAgIHhtbEtleTogc3RyaW5nO1xuICAgIHhtbG5zPzogV3NkbFhtbG5zLFxufVxuXG5leHBvcnQgaW50ZXJmYWNlIElPcHRpb25zIGV4dGVuZHMgSVdzZGxCYXNlT3B0aW9ucyB7XG4gICAgZGlzYWJsZUNhY2hlPzogYm9vbGVhbjtcbiAgICBlbmRwb2ludD86IHN0cmluZztcbiAgICBlbnZlbG9wZUtleT86IHN0cmluZztcbiAgICBodHRwQ2xpZW50PzogSHR0cENsaWVudDtcbiAgICAvLyByZXF1ZXN0PzogKG9wdGlvbnM6IGFueSwgY2FsbGJhY2s/OiAoZXJyb3I6IGFueSwgcmVzOiBhbnksIGJvZHk6IGFueSkgPT4gdm9pZCkgPT4gdm9pZDtcbiAgICBzdHJlYW0/OiBib29sZWFuO1xuICAgIC8vIHdzZGwgb3B0aW9ucyB0aGF0IG9ubHkgd29yayBmb3IgY2xpZW50XG4gICAgZm9yY2VTb2FwMTJIZWFkZXJzPzogYm9vbGVhbjtcbiAgICBjdXN0b21EZXNlcmlhbGl6ZXI/OiBhbnk7XG4gICAgW2tleTogc3RyaW5nXTogYW55O1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIFdTREwge1xuICAgIGNvbnN0cnVjdG9yKGRlZmluaXRpb246IGFueSwgdXJpOiBzdHJpbmcsIG9wdGlvbnM/OiBJT3B0aW9ucyk7XG4gICAgaWdub3JlZE5hbWVzcGFjZXM6IHN0cmluZ1tdO1xuICAgIGlnbm9yZUJhc2VOYW1lU3BhY2VzOiBib29sZWFuO1xuICAgIHZhbHVlS2V5OiBzdHJpbmc7XG4gICAgeG1sS2V5OiBzdHJpbmc7XG4gICAgeG1sbnNJbkVudmVsb3BlOiBzdHJpbmc7XG4gICAgb25SZWFkeShjYWxsYmFjazogKGVycjpFcnJvcikgPT4gdm9pZCk6IHZvaWQ7XG4gICAgcHJvY2Vzc0luY2x1ZGVzKGNhbGxiYWNrOiAoZXJyOkVycm9yKSA9PiB2b2lkKTogdm9pZDtcbiAgICBkZXNjcmliZVNlcnZpY2VzKCk6IHsgW2s6IHN0cmluZ106IGFueSB9O1xuICAgIHRvWE1MKCk6IHN0cmluZztcbiAgICB4bWxUb09iamVjdCh4bWw6IGFueSwgY2FsbGJhY2s/OiAoZXJyOkVycm9yLCByZXN1bHQ6YW55KSA9PiB2b2lkKTogYW55O1xuICAgIGZpbmRTY2hlbWFPYmplY3QobnNVUkk6IHN0cmluZywgcW5hbWU6IHN0cmluZyk6IFhzZEVsZW1lbnQgfCBudWxsIHwgdW5kZWZpbmVkO1xuICAgIG9iamVjdFRvRG9jdW1lbnRYTUwobmFtZTogc3RyaW5nLCBwYXJhbXM6IGFueSwgbnNQcmVmaXg/OiBzdHJpbmcsIG5zVVJJPzogc3RyaW5nLCB0eXBlPzogc3RyaW5nKTogYW55O1xuICAgIG9iamVjdFRvUnBjWE1MKG5hbWU6IHN0cmluZywgcGFyYW1zOiBhbnksIG5zUHJlZml4Pzogc3RyaW5nLCBuc1VSST86IHN0cmluZywgaXNQYXJ0cz86IGFueSk6IHN0cmluZztcbiAgICBpc0lnbm9yZWROYW1lU3BhY2UobnM6IHN0cmluZyk6IGJvb2xlYW47XG4gICAgZmlsdGVyT3V0SWdub3JlZE5hbWVTcGFjZShuczogc3RyaW5nKTogc3RyaW5nO1xuICAgIG9iamVjdFRvWE1MKG9iajogYW55LCBuYW1lOiBzdHJpbmcsIG5zUHJlZml4PzogYW55LCBuc1VSST86IHN0cmluZywgaXNGaXJzdD86IGJvb2xlYW4sIHhtbG5zQXR0cj86IGFueSwgc2NoZW1hT2JqZWN0PzogYW55LCBuc0NvbnRleHQ/OiBhbnkpOiBzdHJpbmc7XG4gICAgcHJvY2Vzc0F0dHJpYnV0ZXMoY2hpbGQ6IGFueSwgbnNDb250ZXh0OiBhbnkpOiBzdHJpbmc7XG4gICAgZmluZFNjaGVtYVR5cGUobmFtZTogYW55LCBuc1VSSTogYW55KTogYW55O1xuICAgIGZpbmRDaGlsZFNjaGVtYU9iamVjdChwYXJhbWV0ZXJUeXBlT2JqOiBhbnksIGNoaWxkTmFtZTogYW55LCBiYWNrdHJhY2U/OiBhbnkpOiBhbnk7XG4gICAgdXJpOiBzdHJpbmc7XG4gICAgZGVmaW5pdGlvbnM6IERlZmluaXRpb25zO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIENsaWVudCBleHRlbmRzIEV2ZW50RW1pdHRlciB7XG4gICAgY29uc3RydWN0b3Iod3NkbDogV1NETCwgZW5kcG9pbnQ/OiBzdHJpbmcsIG9wdGlvbnM/OiBJT3B0aW9ucyk7XG4gICAgYWRkQm9keUF0dHJpYnV0ZShib2R5QXR0cmlidXRlOiBhbnksIG5hbWU/OiBzdHJpbmcsIG5hbWVzcGFjZT86IHN0cmluZywgeG1sbnM/OiBzdHJpbmcpOiB2b2lkO1xuICAgIGFkZEh0dHBIZWFkZXIobmFtZTogc3RyaW5nLCB2YWx1ZTogYW55KTogdm9pZDtcbiAgICBhZGRTb2FwSGVhZGVyKHNvYXBIZWFkZXI6IGFueSwgbmFtZT86IHN0cmluZywgbmFtZXNwYWNlPzogYW55LCB4bWxucz86IHN0cmluZyk6IG51bWJlcjtcbiAgICBjaGFuZ2VTb2FwSGVhZGVyKGluZGV4OiBudW1iZXIsIHNvYXBIZWFkZXI6IGFueSwgbmFtZT86IHN0cmluZywgbmFtZXNwYWNlPzogc3RyaW5nLCB4bWxucz86IHN0cmluZyk6IHZvaWQ7XG4gICAgY2xlYXJCb2R5QXR0cmlidXRlcygpOiB2b2lkO1xuICAgIGNsZWFySHR0cEhlYWRlcnMoKTogdm9pZDtcbiAgICBjbGVhclNvYXBIZWFkZXJzKCk6IHZvaWQ7XG4gICAgZGVzY3JpYmUoKTogYW55O1xuICAgIGdldEJvZHlBdHRyaWJ1dGVzKCk6IGFueVtdO1xuICAgIGdldEh0dHBIZWFkZXJzKCk6IHsgW2s6c3RyaW5nXTogc3RyaW5nIH07XG4gICAgZ2V0U29hcEhlYWRlcnMoKTogc3RyaW5nW107XG4gICAgc2V0RW5kcG9pbnQoZW5kcG9pbnQ6IHN0cmluZyk6IHZvaWQ7XG4gICAgc2V0U09BUEFjdGlvbihhY3Rpb246IHN0cmluZyk6IHZvaWQ7XG4gICAgc2V0U2VjdXJpdHkoc2VjdXJpdHk6IElTZWN1cml0eSk6IHZvaWQ7XG4gICAgd3NkbDogV1NETDtcbiAgICBbbWV0aG9kOiBzdHJpbmddOiBJU29hcE1ldGhvZCB8IFdTREwgfCBGdW5jdGlvbjtcbiAgICBjYWxsKG1ldGhvZDogc3RyaW5nLCBib2R5OiBhbnksIG9wdGlvbnM/OiBhbnksIGV4dHJhSGVhZGVycz86IGFueSk6IE9ic2VydmFibGU8SVNvYXBNZXRob2RSZXNwb25zZT47XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVNvYXBNZXRob2Qge1xuICAgIChhcmdzOiBhbnksIG9wdGlvbnM/OiBhbnksIGV4dHJhSGVhZGVycz86IGFueSk6IE9ic2VydmFibGU8SVNvYXBNZXRob2RSZXNwb25zZT47XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVNvYXBNZXRob2RSZXNwb25zZSB7XG4gICAgZXJyOiBhbnksXG4gICAgaGVhZGVyOiBhbnksXG4gICAgcmVzcG9uc2VCb2R5OiBzdHJpbmcsXG4gICAgeG1sOiBzdHJpbmc7XG4gICAgcmVzdWx0OiBhbnk7XG4gICAgaHR0cEhlYWRlcnM6IGFueTtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJU2VjdXJpdHkge1xuICAgIGFkZE9wdGlvbnMob3B0aW9uczogYW55KTogdm9pZDtcbiAgICB0b1hNTCgpOiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgQmFzaWNBdXRoU2VjdXJpdHkgZXh0ZW5kcyBJU2VjdXJpdHkge1xuICAgIGNvbnN0cnVjdG9yKHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkOiBzdHJpbmcsIGRlZmF1bHRzPzogYW55KTtcbiAgICBhZGRIZWFkZXJzKGhlYWRlcnM6IGFueSk6IHZvaWQ7XG4gICAgYWRkT3B0aW9ucyhvcHRpb25zOiBhbnkpOiB2b2lkO1xuICAgIHRvWE1MKCk6IHN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBCZWFyZXJTZWN1cml0eSBleHRlbmRzIElTZWN1cml0eSB7XG4gICAgY29uc3RydWN0b3IodG9rZW46IHN0cmluZywgZGVmYXVsdHM/OiBhbnkpO1xuICAgIGFkZEhlYWRlcnMoaGVhZGVyczogYW55KTogdm9pZDtcbiAgICBhZGRPcHRpb25zKG9wdGlvbnM6IGFueSk6IHZvaWQ7XG4gICAgdG9YTUwoKTogc3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIFdTU2VjdXJpdHkgZXh0ZW5kcyBJU2VjdXJpdHkge1xuICAgIGNvbnN0cnVjdG9yKHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkOiBzdHJpbmcsIG9wdGlvbnM/OiBhbnkpO1xuICAgIGFkZE9wdGlvbnMob3B0aW9uczogYW55KTogdm9pZDtcbiAgICB0b1hNTCgpOiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgV1NTZWN1cml0eUNlcnQgZXh0ZW5kcyBJU2VjdXJpdHkge1xuICAgIGNvbnN0cnVjdG9yKHByaXZhdGVQRU06IGFueSwgcHVibGljUDEyUEVNOiBhbnksIHBhc3N3b3JkOiBhbnkpO1xuICAgIGFkZE9wdGlvbnMob3B0aW9uczogYW55KTogdm9pZDtcbiAgICB0b1hNTCgpOiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgTlRMTVNlY3VyaXR5IGV4dGVuZHMgSVNlY3VyaXR5IHtcbiAgICBjb25zdHJ1Y3Rvcih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nLCBkb21haW46IHN0cmluZywgd29ya3N0YXRpb24pO1xuICAgIGFkZEhlYWRlcnMoaGVhZGVyczogYW55KTogdm9pZDtcbiAgICBhZGRPcHRpb25zKG9wdGlvbnM6IGFueSk6IHZvaWQ7XG4gICAgdG9YTUwoKTogc3RyaW5nO1xufVxuIl19