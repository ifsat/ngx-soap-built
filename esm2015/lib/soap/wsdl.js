/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*
 * Copyright (c) 2011 Vinay Pulim <vinay@milewise.com>
 * MIT Licensed
 *
 */
/*jshint proto:true*/
"use strict";
import * as sax from 'sax';
import { NamespaceContext } from './nscontext';
import * as url from 'url';
import { ok as assert } from 'assert';
// import stripBom from 'strip-bom';
/** @type {?} */
const stripBom = (x) => {
    // Catches EFBBBF (UTF-8 BOM) because the buffer-to-string
    // conversion translates it to FEFF (UTF-16 BOM)
    if (x.charCodeAt(0) === 0xFEFF) {
        return x.slice(1);
    }
    return x;
};
const ɵ0 = stripBom;
import * as _ from 'lodash';
import * as utils from './utils';
/** @type {?} */
let TNS_PREFIX = utils.TNS_PREFIX;
/** @type {?} */
let findPrefix = utils.findPrefix;
/** @type {?} */
let Primitives = {
    string: 1,
    boolean: 1,
    decimal: 1,
    float: 1,
    double: 1,
    anyType: 1,
    byte: 1,
    int: 1,
    long: 1,
    short: 1,
    negativeInteger: 1,
    nonNegativeInteger: 1,
    positiveInteger: 1,
    nonPositiveInteger: 1,
    unsignedByte: 1,
    unsignedInt: 1,
    unsignedLong: 1,
    unsignedShort: 1,
    duration: 0,
    dateTime: 0,
    time: 0,
    date: 0,
    gYearMonth: 0,
    gYear: 0,
    gMonthDay: 0,
    gDay: 0,
    gMonth: 0,
    hexBinary: 0,
    base64Binary: 0,
    anyURI: 0,
    QName: 0,
    NOTATION: 0
};
/**
 * @param {?} nsName
 * @return {?}
 */
function splitQName(nsName) {
    /** @type {?} */
    let i = typeof nsName === 'string' ? nsName.indexOf(':') : -1;
    return i < 0 ? { prefix: TNS_PREFIX, name: nsName } :
        { prefix: nsName.substring(0, i), name: nsName.substring(i + 1) };
}
/**
 * @param {?} obj
 * @return {?}
 */
function xmlEscape(obj) {
    if (typeof (obj) === 'string') {
        if (obj.substr(0, 9) === '<![CDATA[' && obj.substr(-3) === "]]>") {
            return obj;
        }
        return obj
            .replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&apos;');
    }
    return obj;
}
/** @type {?} */
let trimLeft = /^[\s\xA0]+/;
/** @type {?} */
let trimRight = /[\s\xA0]+$/;
/**
 * @param {?} text
 * @return {?}
 */
function trim(text) {
    return text.replace(trimLeft, '').replace(trimRight, '');
}
/**
 * @param {?} destination
 * @param {?} source
 * @return {?}
 */
function deepMerge(destination, source) {
    return _.mergeWith(destination || {}, source, function (a, b) {
        return _.isArray(a) ? a.concat(b) : undefined;
    });
}
/** @type {?} */
let Element = function (nsName, attrs, options) {
    /** @type {?} */
    let parts = splitQName(nsName);
    this.nsName = nsName;
    this.prefix = parts.prefix;
    this.name = parts.name;
    this.children = [];
    this.xmlns = {};
    this._initializeOptions(options);
    for (let key in attrs) {
        /** @type {?} */
        let match = /^xmlns:?(.*)$/.exec(key);
        if (match) {
            this.xmlns[match[1] ? match[1] : TNS_PREFIX] = attrs[key];
        }
        else {
            if (key === 'value') {
                this[this.valueKey] = attrs[key];
            }
            else {
                this['$' + key] = attrs[key];
            }
        }
    }
    if (this.$targetNamespace !== undefined) {
        // Add targetNamespace to the mapping
        this.xmlns[TNS_PREFIX] = this.$targetNamespace;
    }
};
const ɵ1 = Element;
Element.prototype._initializeOptions = function (options) {
    if (options) {
        this.valueKey = options.valueKey || '$value';
        this.xmlKey = options.xmlKey || '$xml';
        this.ignoredNamespaces = options.ignoredNamespaces || [];
    }
    else {
        this.valueKey = '$value';
        this.xmlKey = '$xml';
        this.ignoredNamespaces = [];
    }
};
Element.prototype.deleteFixedAttrs = function () {
    this.children && this.children.length === 0 && delete this.children;
    this.xmlns && Object.keys(this.xmlns).length === 0 && delete this.xmlns;
    delete this.nsName;
    delete this.prefix;
    delete this.name;
};
Element.prototype.allowedChildren = [];
Element.prototype.startElement = function (stack, nsName, attrs, options) {
    if (!this.allowedChildren) {
        return;
    }
    /** @type {?} */
    let ChildClass = this.allowedChildren[splitQName(nsName).name];
    /** @type {?} */
    let element = null;
    if (ChildClass) {
        stack.push(new ChildClass(nsName, attrs, options));
    }
    else {
        this.unexpected(nsName);
    }
};
Element.prototype.endElement = function (stack, nsName) {
    if (this.nsName === nsName) {
        if (stack.length < 2)
            return;
        /** @type {?} */
        let parent = stack[stack.length - 2];
        if (this !== stack[0]) {
            _.defaultsDeep(stack[0].xmlns, this.xmlns);
            // delete this.xmlns;
            parent.children.push(this);
            parent.addChild(this);
        }
        stack.pop();
    }
};
Element.prototype.addChild = function (child) {
    return;
};
Element.prototype.unexpected = function (name) {
    throw new Error('Found unexpected element (' + name + ') inside ' + this.nsName);
};
Element.prototype.description = function (definitions) {
    return this.$name || this.name;
};
Element.prototype.init = function () {
};
Element.createSubClass = function () {
    /** @type {?} */
    let root = this;
    /** @type {?} */
    let subElement = function () {
        root.apply(this, arguments);
        this.init();
    };
    // inherits(subElement, root);
    subElement.prototype.__proto__ = root.prototype;
    return subElement;
};
/** @type {?} */
let ElementElement = Element.createSubClass();
/** @type {?} */
let AnyElement = Element.createSubClass();
/** @type {?} */
let InputElement = Element.createSubClass();
/** @type {?} */
let OutputElement = Element.createSubClass();
/** @type {?} */
let SimpleTypeElement = Element.createSubClass();
/** @type {?} */
let RestrictionElement = Element.createSubClass();
/** @type {?} */
let ExtensionElement = Element.createSubClass();
/** @type {?} */
let ChoiceElement = Element.createSubClass();
/** @type {?} */
let EnumerationElement = Element.createSubClass();
/** @type {?} */
let ComplexTypeElement = Element.createSubClass();
/** @type {?} */
let ComplexContentElement = Element.createSubClass();
/** @type {?} */
let SimpleContentElement = Element.createSubClass();
/** @type {?} */
let SequenceElement = Element.createSubClass();
/** @type {?} */
let AllElement = Element.createSubClass();
/** @type {?} */
let MessageElement = Element.createSubClass();
/** @type {?} */
let DocumentationElement = Element.createSubClass();
/** @type {?} */
let SchemaElement = Element.createSubClass();
/** @type {?} */
let TypesElement = Element.createSubClass();
/** @type {?} */
let OperationElement = Element.createSubClass();
/** @type {?} */
let PortTypeElement = Element.createSubClass();
/** @type {?} */
let BindingElement = Element.createSubClass();
/** @type {?} */
let PortElement = Element.createSubClass();
/** @type {?} */
let ServiceElement = Element.createSubClass();
/** @type {?} */
let DefinitionsElement = Element.createSubClass();
/** @type {?} */
let ElementTypeMap = {
    types: [TypesElement, 'schema documentation'],
    schema: [SchemaElement, 'element complexType simpleType include import'],
    element: [ElementElement, 'annotation complexType'],
    any: [AnyElement, ''],
    simpleType: [SimpleTypeElement, 'restriction'],
    restriction: [RestrictionElement, 'enumeration all choice sequence'],
    extension: [ExtensionElement, 'all sequence choice'],
    choice: [ChoiceElement, 'element sequence choice any'],
    // group: [GroupElement, 'element group'],
    enumeration: [EnumerationElement, ''],
    complexType: [ComplexTypeElement, 'annotation sequence all complexContent simpleContent choice'],
    complexContent: [ComplexContentElement, 'extension'],
    simpleContent: [SimpleContentElement, 'extension'],
    sequence: [SequenceElement, 'element sequence choice any'],
    all: [AllElement, 'element choice'],
    service: [ServiceElement, 'port documentation'],
    port: [PortElement, 'address documentation'],
    binding: [BindingElement, '_binding SecuritySpec operation documentation'],
    portType: [PortTypeElement, 'operation documentation'],
    message: [MessageElement, 'part documentation'],
    operation: [OperationElement, 'documentation input output fault _operation'],
    input: [InputElement, 'body SecuritySpecRef documentation header'],
    output: [OutputElement, 'body SecuritySpecRef documentation header'],
    fault: [Element, '_fault documentation'],
    definitions: [DefinitionsElement, 'types message portType binding service import documentation'],
    documentation: [DocumentationElement, '']
};
/**
 * @param {?} types
 * @return {?}
 */
function mapElementTypes(types) {
    /** @type {?} */
    let rtn = {};
    types = types.split(' ');
    types.forEach(function (type) {
        rtn[type.replace(/^_/, '')] = (ElementTypeMap[type] || [Element])[0];
    });
    return rtn;
}
for (let n in ElementTypeMap) {
    /** @type {?} */
    let v = ElementTypeMap[n];
    v[0].prototype.allowedChildren = mapElementTypes(v[1]);
}
MessageElement.prototype.init = function () {
    this.element = null;
    this.parts = null;
};
SchemaElement.prototype.init = function () {
    this.complexTypes = {};
    this.types = {};
    this.elements = {};
    this.includes = [];
};
TypesElement.prototype.init = function () {
    this.schemas = {};
};
OperationElement.prototype.init = function () {
    this.input = null;
    this.output = null;
    this.inputSoap = null;
    this.outputSoap = null;
    this.style = '';
    this.soapAction = '';
};
PortTypeElement.prototype.init = function () {
    this.methods = {};
};
BindingElement.prototype.init = function () {
    this.transport = '';
    this.style = '';
    this.methods = {};
};
PortElement.prototype.init = function () {
    this.location = null;
};
ServiceElement.prototype.init = function () {
    this.ports = {};
};
DefinitionsElement.prototype.init = function () {
    if (this.name !== 'definitions')
        this.unexpected(this.nsName);
    this.messages = {};
    this.portTypes = {};
    this.bindings = {};
    this.services = {};
    this.schemas = {};
};
DocumentationElement.prototype.init = function () {
};
SchemaElement.prototype.merge = function (source) {
    assert(source instanceof SchemaElement);
    if (this.$targetNamespace === source.$targetNamespace) {
        _.merge(this.complexTypes, source.complexTypes);
        _.merge(this.types, source.types);
        _.merge(this.elements, source.elements);
        _.merge(this.xmlns, source.xmlns);
    }
    return this;
};
SchemaElement.prototype.addChild = function (child) {
    if (child.$name in Primitives)
        return;
    if (child.name === 'include' || child.name === 'import') {
        /** @type {?} */
        let location = child.$schemaLocation || child.$location;
        if (location) {
            this.includes.push({
                namespace: child.$namespace || child.$targetNamespace || this.$targetNamespace,
                location: location
            });
        }
    }
    else if (child.name === 'complexType') {
        this.complexTypes[child.$name] = child;
    }
    else if (child.name === 'element') {
        this.elements[child.$name] = child;
    }
    else if (child.$name) {
        this.types[child.$name] = child;
    }
    this.children.pop();
    // child.deleteFixedAttrs();
};
//fix#325
TypesElement.prototype.addChild = function (child) {
    assert(child instanceof SchemaElement);
    /** @type {?} */
    let targetNamespace = child.$targetNamespace;
    if (!this.schemas.hasOwnProperty(targetNamespace)) {
        this.schemas[targetNamespace] = child;
    }
    else {
        console.error('Target-Namespace "' + targetNamespace + '" already in use by another Schema!');
    }
};
InputElement.prototype.addChild = function (child) {
    if (child.name === 'body') {
        this.use = child.$use;
        if (this.use === 'encoded') {
            this.encodingStyle = child.$encodingStyle;
        }
        this.children.pop();
    }
};
OutputElement.prototype.addChild = function (child) {
    if (child.name === 'body') {
        this.use = child.$use;
        if (this.use === 'encoded') {
            this.encodingStyle = child.$encodingStyle;
        }
        this.children.pop();
    }
};
OperationElement.prototype.addChild = function (child) {
    if (child.name === 'operation') {
        this.soapAction = child.$soapAction || '';
        this.style = child.$style || '';
        this.children.pop();
    }
};
BindingElement.prototype.addChild = function (child) {
    if (child.name === 'binding') {
        this.transport = child.$transport;
        this.style = child.$style;
        this.children.pop();
    }
};
PortElement.prototype.addChild = function (child) {
    if (child.name === 'address' && typeof (child.$location) !== 'undefined') {
        this.location = child.$location;
    }
};
DefinitionsElement.prototype.addChild = function (child) {
    /** @type {?} */
    let self = this;
    if (child instanceof TypesElement) {
        // Merge types.schemas into definitions.schemas
        _.merge(self.schemas, child.schemas);
    }
    else if (child instanceof MessageElement) {
        self.messages[child.$name] = child;
    }
    else if (child.name === 'import') {
        self.schemas[child.$namespace] = new SchemaElement(child.$namespace, {});
        self.schemas[child.$namespace].addChild(child);
    }
    else if (child instanceof PortTypeElement) {
        self.portTypes[child.$name] = child;
    }
    else if (child instanceof BindingElement) {
        if (child.transport === 'http://schemas.xmlsoap.org/soap/http' ||
            child.transport === 'http://www.w3.org/2003/05/soap/bindings/HTTP/')
            self.bindings[child.$name] = child;
    }
    else if (child instanceof ServiceElement) {
        self.services[child.$name] = child;
    }
    else if (child instanceof DocumentationElement) {
    }
    this.children.pop();
};
MessageElement.prototype.postProcess = function (definitions) {
    /** @type {?} */
    let part = null;
    /** @type {?} */
    let child = undefined;
    /** @type {?} */
    let children = this.children || [];
    /** @type {?} */
    let ns = undefined;
    /** @type {?} */
    let nsName = undefined;
    /** @type {?} */
    let i = undefined;
    /** @type {?} */
    let type = undefined;
    for (i in children) {
        if ((child = children[i]).name === 'part') {
            part = child;
            break;
        }
    }
    if (!part) {
        return;
    }
    if (part.$element) {
        /** @type {?} */
        let lookupTypes = [];
        /** @type {?} */
        let elementChildren;
        delete this.parts;
        nsName = splitQName(part.$element);
        ns = nsName.prefix;
        /** @type {?} */
        let schema = definitions.schemas[definitions.xmlns[ns]];
        this.element = schema.elements[nsName.name];
        if (!this.element) {
            // debug(nsName.name + " is not present in wsdl and cannot be processed correctly.");
            return;
        }
        this.element.targetNSAlias = ns;
        this.element.targetNamespace = definitions.xmlns[ns];
        // set the optional $lookupType to be used within `client#_invoke()` when
        // calling `wsdl#objectToDocumentXML()
        this.element.$lookupType = part.$element;
        elementChildren = this.element.children;
        // get all nested lookup types (only complex types are followed)
        if (elementChildren.length > 0) {
            for (i = 0; i < elementChildren.length; i++) {
                lookupTypes.push(this._getNestedLookupTypeString(elementChildren[i]));
            }
        }
        // if nested lookup types where found, prepare them for furter usage
        if (lookupTypes.length > 0) {
            lookupTypes = lookupTypes.
                join('_').
                split('_').
                filter(function removeEmptyLookupTypes(type) {
                return type !== '^';
            });
            /** @type {?} */
            let schemaXmlns = definitions.schemas[this.element.targetNamespace].xmlns;
            for (i = 0; i < lookupTypes.length; i++) {
                lookupTypes[i] = this._createLookupTypeObject(lookupTypes[i], schemaXmlns);
            }
        }
        this.element.$lookupTypes = lookupTypes;
        if (this.element.$type) {
            type = splitQName(this.element.$type);
            /** @type {?} */
            let typeNs = schema.xmlns && schema.xmlns[type.prefix] || definitions.xmlns[type.prefix];
            if (typeNs) {
                if (type.name in Primitives) {
                    // this.element = this.element.$type;
                }
                else {
                    // first check local mapping of ns alias to namespace
                    schema = definitions.schemas[typeNs];
                    /** @type {?} */
                    let ctype = schema.complexTypes[type.name] || schema.types[type.name] || schema.elements[type.name];
                    if (ctype) {
                        this.parts = ctype.description(definitions, schema.xmlns);
                    }
                }
            }
        }
        else {
            /** @type {?} */
            let method = this.element.description(definitions, schema.xmlns);
            this.parts = method[nsName.name];
        }
        this.children.splice(0, 1);
    }
    else {
        // rpc encoding
        this.parts = {};
        delete this.element;
        for (i = 0; part = this.children[i]; i++) {
            if (part.name === 'documentation') {
                // <wsdl:documentation can be present under <wsdl:message>
                continue;
            }
            assert(part.name === 'part', 'Expected part element');
            nsName = splitQName(part.$type);
            ns = definitions.xmlns[nsName.prefix];
            type = nsName.name;
            /** @type {?} */
            let schemaDefinition = definitions.schemas[ns];
            if (typeof schemaDefinition !== 'undefined') {
                this.parts[part.$name] = definitions.schemas[ns].types[type] || definitions.schemas[ns].complexTypes[type];
            }
            else {
                this.parts[part.$name] = part.$type;
            }
            if (typeof this.parts[part.$name] === 'object') {
                this.parts[part.$name].prefix = nsName.prefix;
                this.parts[part.$name].xmlns = ns;
            }
            this.children.splice(i--, 1);
        }
    }
    this.deleteFixedAttrs();
};
/**
 * Takes a given namespaced String(for example: 'alias:property') and creates a lookupType
 * object for further use in as first (lookup) `parameterTypeObj` within the `objectToXML`
 * method and provides an entry point for the already existing code in `findChildSchemaObject`.
 *
 * @method _createLookupTypeObject
 * @param {String}            nsString          The NS String (for example "alias:type").
 * @param {Object}            xmlns       The fully parsed `wsdl` definitions object (including all schemas).
 * @returns {Object}
 * @private
 */
MessageElement.prototype._createLookupTypeObject = function (nsString, xmlns) {
    /** @type {?} */
    let splittedNSString = splitQName(nsString);
    /** @type {?} */
    let nsAlias = splittedNSString.prefix;
    /** @type {?} */
    let splittedName = splittedNSString.name.split('#');
    /** @type {?} */
    let type = splittedName[0];
    /** @type {?} */
    let name = splittedName[1];
    /** @type {?} */
    let lookupTypeObj = {};
    lookupTypeObj.$namespace = xmlns[nsAlias];
    lookupTypeObj.$type = nsAlias + ':' + type;
    lookupTypeObj.$name = name;
    return lookupTypeObj;
};
/**
 * Iterates through the element and every nested child to find any defined `$type`
 * property and returns it in a underscore ('_') separated String (using '^' as default
 * value if no `$type` property was found).
 *
 * @method _getNestedLookupTypeString
 * @param {Object}            element         The element which (probably) contains nested `$type` values.
 * @returns {String}
 * @private
 */
MessageElement.prototype._getNestedLookupTypeString = function (element) {
    /** @type {?} */
    let resolvedType = '^';
    /** @type {?} */
    let excluded = this.ignoredNamespaces.concat('xs');
    if (element.hasOwnProperty('$type') && typeof element.$type === 'string') {
        if (excluded.indexOf(element.$type.split(':')[0]) === -1) {
            resolvedType += ('_' + element.$type + '#' + element.$name);
        }
    }
    if (element.children.length > 0) {
        /** @type {?} */
        let self = this;
        element.children.forEach(function (child) {
            /** @type {?} */
            let resolvedChildType = self._getNestedLookupTypeString(child).replace(/\^_/, '');
            if (resolvedChildType && typeof resolvedChildType === 'string') {
                resolvedType += ('_' + resolvedChildType);
            }
        });
    }
    return resolvedType;
};
OperationElement.prototype.postProcess = function (definitions, tag) {
    /** @type {?} */
    let children = this.children;
    for (let i = 0, child; child = children[i]; i++) {
        if (child.name !== 'input' && child.name !== 'output')
            continue;
        if (tag === 'binding') {
            this[child.name] = child;
            children.splice(i--, 1);
            continue;
        }
        /** @type {?} */
        let messageName = splitQName(child.$message).name;
        /** @type {?} */
        let message = definitions.messages[messageName];
        message.postProcess(definitions);
        if (message.element) {
            definitions.messages[message.element.$name] = message;
            this[child.name] = message.element;
        }
        else {
            this[child.name] = message;
        }
        children.splice(i--, 1);
    }
    this.deleteFixedAttrs();
};
PortTypeElement.prototype.postProcess = function (definitions) {
    /** @type {?} */
    let children = this.children;
    if (typeof children === 'undefined')
        return;
    for (let i = 0, child; child = children[i]; i++) {
        if (child.name !== 'operation')
            continue;
        child.postProcess(definitions, 'portType');
        this.methods[child.$name] = child;
        children.splice(i--, 1);
    }
    delete this.$name;
    this.deleteFixedAttrs();
};
BindingElement.prototype.postProcess = function (definitions) {
    /** @type {?} */
    let type = splitQName(this.$type).name;
    /** @type {?} */
    let portType = definitions.portTypes[type];
    /** @type {?} */
    let style = this.style;
    /** @type {?} */
    let children = this.children;
    if (portType) {
        portType.postProcess(definitions);
        this.methods = portType.methods;
        for (let i = 0, child; child = children[i]; i++) {
            if (child.name !== 'operation')
                continue;
            child.postProcess(definitions, 'binding');
            children.splice(i--, 1);
            child.style || (child.style = style);
            /** @type {?} */
            let method = this.methods[child.$name];
            if (method) {
                method.style = child.style;
                method.soapAction = child.soapAction;
                method.inputSoap = child.input || null;
                method.outputSoap = child.output || null;
                method.inputSoap && method.inputSoap.deleteFixedAttrs();
                method.outputSoap && method.outputSoap.deleteFixedAttrs();
            }
        }
    }
    delete this.$name;
    delete this.$type;
    this.deleteFixedAttrs();
};
ServiceElement.prototype.postProcess = function (definitions) {
    /** @type {?} */
    let children = this.children;
    /** @type {?} */
    let bindings = definitions.bindings;
    if (children && children.length > 0) {
        for (let i = 0, child; child = children[i]; i++) {
            if (child.name !== 'port')
                continue;
            /** @type {?} */
            let bindingName = splitQName(child.$binding).name;
            /** @type {?} */
            let binding = bindings[bindingName];
            if (binding) {
                binding.postProcess(definitions);
                this.ports[child.$name] = {
                    location: child.location,
                    binding: binding
                };
                children.splice(i--, 1);
            }
        }
    }
    delete this.$name;
    this.deleteFixedAttrs();
};
SimpleTypeElement.prototype.description = function (definitions) {
    /** @type {?} */
    let children = this.children;
    for (let i = 0, child; child = children[i]; i++) {
        if (child instanceof RestrictionElement)
            return this.$name + "|" + child.description();
    }
    return {};
};
RestrictionElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    let children = this.children;
    /** @type {?} */
    let desc;
    for (let i = 0, child; child = children[i]; i++) {
        if (child instanceof SequenceElement ||
            child instanceof ChoiceElement) {
            desc = child.description(definitions, xmlns);
            break;
        }
    }
    if (desc && this.$base) {
        /** @type {?} */
        let type = splitQName(this.$base);
        /** @type {?} */
        let typeName = type.name;
        /** @type {?} */
        let ns = xmlns && xmlns[type.prefix] || definitions.xmlns[type.prefix];
        /** @type {?} */
        let schema = definitions.schemas[ns];
        /** @type {?} */
        let typeElement = schema && (schema.complexTypes[typeName] || schema.types[typeName] || schema.elements[typeName]);
        desc.getBase = function () {
            return typeElement.description(definitions, schema.xmlns);
        };
        return desc;
    }
    // then simple element
    /** @type {?} */
    let base = this.$base ? this.$base + "|" : "";
    return base + this.children.map(function (child) {
        return child.description();
    }).join(",");
};
ExtensionElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    let children = this.children;
    /** @type {?} */
    let desc = {};
    for (let i = 0, child; child = children[i]; i++) {
        if (child instanceof SequenceElement ||
            child instanceof ChoiceElement) {
            desc = child.description(definitions, xmlns);
        }
    }
    if (this.$base) {
        /** @type {?} */
        let type = splitQName(this.$base);
        /** @type {?} */
        let typeName = type.name;
        /** @type {?} */
        let ns = xmlns && xmlns[type.prefix] || definitions.xmlns[type.prefix];
        /** @type {?} */
        let schema = definitions.schemas[ns];
        if (typeName in Primitives) {
            return this.$base;
        }
        else {
            /** @type {?} */
            let typeElement = schema && (schema.complexTypes[typeName] ||
                schema.types[typeName] || schema.elements[typeName]);
            if (typeElement) {
                /** @type {?} */
                let base = typeElement.description(definitions, schema.xmlns);
                desc = _.defaultsDeep(base, desc);
            }
        }
    }
    return desc;
};
EnumerationElement.prototype.description = function () {
    return this[this.valueKey];
};
ComplexTypeElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    let children = this.children || [];
    for (let i = 0, child; child = children[i]; i++) {
        if (child instanceof ChoiceElement ||
            child instanceof SequenceElement ||
            child instanceof AllElement ||
            child instanceof SimpleContentElement ||
            child instanceof ComplexContentElement) {
            return child.description(definitions, xmlns);
        }
    }
    return {};
};
ComplexContentElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    let children = this.children;
    for (let i = 0, child; child = children[i]; i++) {
        if (child instanceof ExtensionElement) {
            return child.description(definitions, xmlns);
        }
    }
    return {};
};
SimpleContentElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    let children = this.children;
    for (let i = 0, child; child = children[i]; i++) {
        if (child instanceof ExtensionElement) {
            return child.description(definitions, xmlns);
        }
    }
    return {};
};
ElementElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    let element = {};
    /** @type {?} */
    let name = this.$name;
    /** @type {?} */
    let isMany = !this.$maxOccurs ? false : (isNaN(this.$maxOccurs) ? (this.$maxOccurs === 'unbounded') : (this.$maxOccurs > 1));
    if (this.$minOccurs !== this.$maxOccurs && isMany) {
        name += '[]';
    }
    if (xmlns && xmlns[TNS_PREFIX]) {
        this.$targetNamespace = xmlns[TNS_PREFIX];
    }
    /** @type {?} */
    let type = this.$type || this.$ref;
    if (type) {
        type = splitQName(type);
        /** @type {?} */
        let typeName = type.name;
        /** @type {?} */
        let ns = xmlns && xmlns[type.prefix] || definitions.xmlns[type.prefix];
        /** @type {?} */
        let schema = definitions.schemas[ns];
        /** @type {?} */
        let typeElement = schema && (this.$type ? schema.complexTypes[typeName] || schema.types[typeName] : schema.elements[typeName]);
        if (ns && definitions.schemas[ns]) {
            xmlns = definitions.schemas[ns].xmlns;
        }
        if (typeElement && !(typeName in Primitives)) {
            if (!(typeName in definitions.descriptions.types)) {
                /** @type {?} */
                let elem = {};
                definitions.descriptions.types[typeName] = elem;
                /** @type {?} */
                let description = typeElement.description(definitions, xmlns);
                if (typeof description === 'string') {
                    elem = description;
                }
                else {
                    Object.keys(description).forEach(function (key) {
                        elem[key] = description[key];
                    });
                }
                if (this.$ref) {
                    element = elem;
                }
                else {
                    element[name] = elem;
                }
                if (typeof elem === 'object') {
                    elem.targetNSAlias = type.prefix;
                    elem.targetNamespace = ns;
                }
                definitions.descriptions.types[typeName] = elem;
            }
            else {
                if (this.$ref) {
                    element = definitions.descriptions.types[typeName];
                }
                else {
                    element[name] = definitions.descriptions.types[typeName];
                }
            }
        }
        else {
            element[name] = this.$type;
        }
    }
    else {
        /** @type {?} */
        let children = this.children;
        element[name] = {};
        for (let i = 0, child; child = children[i]; i++) {
            if (child instanceof ComplexTypeElement) {
                element[name] = child.description(definitions, xmlns);
            }
        }
    }
    return element;
};
AllElement.prototype.description =
    SequenceElement.prototype.description = function (definitions, xmlns) {
        /** @type {?} */
        let children = this.children;
        /** @type {?} */
        let sequence = {};
        for (let i = 0, child; child = children[i]; i++) {
            if (child instanceof AnyElement) {
                continue;
            }
            /** @type {?} */
            let description = child.description(definitions, xmlns);
            for (let key in description) {
                sequence[key] = description[key];
            }
        }
        return sequence;
    };
ChoiceElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    let children = this.children;
    /** @type {?} */
    let choice = {};
    for (let i = 0, child; child = children[i]; i++) {
        /** @type {?} */
        let description = child.description(definitions, xmlns);
        for (let key in description) {
            choice[key] = description[key];
        }
    }
    return choice;
};
MessageElement.prototype.description = function (definitions) {
    if (this.element) {
        return this.element && this.element.description(definitions);
    }
    /** @type {?} */
    let desc = {};
    desc[this.$name] = this.parts;
    return desc;
};
PortTypeElement.prototype.description = function (definitions) {
    /** @type {?} */
    let methods = {};
    for (let name in this.methods) {
        /** @type {?} */
        let method = this.methods[name];
        methods[name] = method.description(definitions);
    }
    return methods;
};
OperationElement.prototype.description = function (definitions) {
    /** @type {?} */
    let inputDesc = this.input ? this.input.description(definitions) : null;
    /** @type {?} */
    let outputDesc = this.output ? this.output.description(definitions) : null;
    return {
        input: inputDesc && inputDesc[Object.keys(inputDesc)[0]],
        output: outputDesc && outputDesc[Object.keys(outputDesc)[0]]
    };
};
BindingElement.prototype.description = function (definitions) {
    /** @type {?} */
    let methods = {};
    for (let name in this.methods) {
        /** @type {?} */
        let method = this.methods[name];
        methods[name] = method.description(definitions);
    }
    return methods;
};
ServiceElement.prototype.description = function (definitions) {
    /** @type {?} */
    let ports = {};
    for (let name in this.ports) {
        /** @type {?} */
        let port = this.ports[name];
        ports[name] = port.binding.description(definitions);
    }
    return ports;
};
/** @type {?} */
export let WSDL = function (definition, uri, options) {
    /** @type {?} */
    let self = this;
    /** @type {?} */
    let fromFunc;
    this.uri = uri;
    this.callback = function () {
    };
    this._includesWsdl = [];
    // initialize WSDL cache
    this.WSDL_CACHE = (options || {}).WSDL_CACHE || {};
    this._initializeOptions(options);
    if (typeof definition === 'string') {
        definition = stripBom(definition);
        fromFunc = this._fromXML;
    }
    else if (typeof definition === 'object') {
        fromFunc = this._fromServices;
    }
    else {
        throw new Error('WSDL letructor takes either an XML string or service definition');
    }
    Promise.resolve(true).then(() => {
        try {
            fromFunc.call(self, definition);
        }
        catch (e) {
            return self.callback(e.message);
        }
        self.processIncludes().then(() => {
            self.definitions.deleteFixedAttrs();
            /** @type {?} */
            let services = self.services = self.definitions.services;
            if (services) {
                for (const name in services) {
                    services[name].postProcess(self.definitions);
                }
            }
            /** @type {?} */
            let complexTypes = self.definitions.complexTypes;
            if (complexTypes) {
                for (const name in complexTypes) {
                    complexTypes[name].deleteFixedAttrs();
                }
            }
            // for document style, for every binding, prepare input message element name to (methodName, output message element name) mapping
            /** @type {?} */
            let bindings = self.definitions.bindings;
            for (let bindingName in bindings) {
                /** @type {?} */
                let binding = bindings[bindingName];
                if (typeof binding.style === 'undefined') {
                    binding.style = 'document';
                }
                if (binding.style !== 'document')
                    continue;
                /** @type {?} */
                let methods = binding.methods;
                /** @type {?} */
                let topEls = binding.topElements = {};
                for (let methodName in methods) {
                    if (methods[methodName].input) {
                        /** @type {?} */
                        let inputName = methods[methodName].input.$name;
                        /** @type {?} */
                        let outputName = "";
                        if (methods[methodName].output)
                            outputName = methods[methodName].output.$name;
                        topEls[inputName] = { "methodName": methodName, "outputName": outputName };
                    }
                }
            }
            // prepare soap envelope xmlns definition string
            self.xmlnsInEnvelope = self._xmlnsMap();
            self.callback(null, self);
        }).catch(err => self.callback(err));
    });
    // process.nextTick(function() {
    //   try {
    //     fromFunc.call(self, definition);
    //   } catch (e) {
    //     return self.callback(e.message);
    //   }
    //   self.processIncludes(function(err) {
    //     let name;
    //     if (err) {
    //       return self.callback(err);
    //     }
    //     self.definitions.deleteFixedAttrs();
    //     let services = self.services = self.definitions.services;
    //     if (services) {
    //       for (name in services) {
    //         services[name].postProcess(self.definitions);
    //       }
    //     }
    //     let complexTypes = self.definitions.complexTypes;
    //     if (complexTypes) {
    //       for (name in complexTypes) {
    //         complexTypes[name].deleteFixedAttrs();
    //       }
    //     }
    //     // for document style, for every binding, prepare input message element name to (methodName, output message element name) mapping
    //     let bindings = self.definitions.bindings;
    //     for (let bindingName in bindings) {
    //       let binding = bindings[bindingName];
    //       if (typeof binding.style === 'undefined') {
    //         binding.style = 'document';
    //       }
    //       if (binding.style !== 'document')
    //         continue;
    //       let methods = binding.methods;
    //       let topEls = binding.topElements = {};
    //       for (let methodName in methods) {
    //         if (methods[methodName].input) {
    //           let inputName = methods[methodName].input.$name;
    //           let outputName="";
    //           if(methods[methodName].output )
    //             outputName = methods[methodName].output.$name;
    //           topEls[inputName] = {"methodName": methodName, "outputName": outputName};
    //         }
    //       }
    //     }
    //     // prepare soap envelope xmlns definition string
    //     self.xmlnsInEnvelope = self._xmlnsMap();
    //     self.callback(err, self);
    //   });
    // });
};
WSDL.prototype.ignoredNamespaces = ['tns', 'targetNamespace', 'typedNamespace'];
WSDL.prototype.ignoreBaseNameSpaces = false;
WSDL.prototype.valueKey = '$value';
WSDL.prototype.xmlKey = '$xml';
WSDL.prototype._initializeOptions = function (options) {
    this._originalIgnoredNamespaces = (options || {}).ignoredNamespaces;
    this.options = {};
    /** @type {?} */
    let ignoredNamespaces = options ? options.ignoredNamespaces : null;
    if (ignoredNamespaces &&
        (Array.isArray(ignoredNamespaces.namespaces) || typeof ignoredNamespaces.namespaces === 'string')) {
        if (ignoredNamespaces.override) {
            this.options.ignoredNamespaces = ignoredNamespaces.namespaces;
        }
        else {
            this.options.ignoredNamespaces = this.ignoredNamespaces.concat(ignoredNamespaces.namespaces);
        }
    }
    else {
        this.options.ignoredNamespaces = this.ignoredNamespaces;
    }
    this.options.valueKey = options.valueKey || this.valueKey;
    this.options.xmlKey = options.xmlKey || this.xmlKey;
    if (options.escapeXML !== undefined) {
        this.options.escapeXML = options.escapeXML;
    }
    else {
        this.options.escapeXML = true;
    }
    if (options.returnFault !== undefined) {
        this.options.returnFault = options.returnFault;
    }
    else {
        this.options.returnFault = false;
    }
    this.options.handleNilAsNull = !!options.handleNilAsNull;
    if (options.namespaceArrayElements !== undefined) {
        this.options.namespaceArrayElements = options.namespaceArrayElements;
    }
    else {
        this.options.namespaceArrayElements = true;
    }
    // Allow any request headers to keep passing through
    this.options.wsdl_headers = options.wsdl_headers;
    this.options.wsdl_options = options.wsdl_options;
    if (options.httpClient) {
        this.options.httpClient = options.httpClient;
    }
    // The supplied request-object should be passed through
    if (options.request) {
        this.options.request = options.request;
    }
    /** @type {?} */
    let ignoreBaseNameSpaces = options ? options.ignoreBaseNameSpaces : null;
    if (ignoreBaseNameSpaces !== null && typeof ignoreBaseNameSpaces !== 'undefined') {
        this.options.ignoreBaseNameSpaces = ignoreBaseNameSpaces;
    }
    else {
        this.options.ignoreBaseNameSpaces = this.ignoreBaseNameSpaces;
    }
    // Works only in client
    this.options.forceSoap12Headers = options.forceSoap12Headers;
    this.options.customDeserializer = options.customDeserializer;
    if (options.overrideRootElement !== undefined) {
        this.options.overrideRootElement = options.overrideRootElement;
    }
    this.options.useEmptyTag = !!options.useEmptyTag;
};
WSDL.prototype.onReady = function (callback) {
    if (callback)
        this.callback = callback;
};
WSDL.prototype._processNextInclude = function (includes) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        let self = this;
        /** @type {?} */
        let include = includes.shift();
        /** @type {?} */
        let options;
        if (!include)
            return; // callback();
        // callback();
        /** @type {?} */
        let includePath;
        if (!/^https?:/.test(self.uri) && !/^https?:/.test(include.location)) {
            // includePath = path.resolve(path.dirname(self.uri), include.location);
        }
        else {
            includePath = url.resolve(self.uri || '', include.location);
        }
        options = _.assign({}, this.options);
        // follow supplied ignoredNamespaces option
        options.ignoredNamespaces = this._originalIgnoredNamespaces || this.options.ignoredNamespaces;
        options.WSDL_CACHE = this.WSDL_CACHE;
        /** @type {?} */
        const wsdl = yield open_wsdl_recursive(includePath, options);
        self._includesWsdl.push(wsdl);
        if (wsdl.definitions instanceof DefinitionsElement) {
            _.mergeWith(self.definitions, wsdl.definitions, function (a, b) {
                return (a instanceof SchemaElement) ? a.merge(b) : undefined;
            });
        }
        else {
            self.definitions.schemas[include.namespace || wsdl.definitions.$targetNamespace] = deepMerge(self.definitions.schemas[include.namespace || wsdl.definitions.$targetNamespace], wsdl.definitions);
        }
        return self._processNextInclude(includes);
        // open_wsdl_recursive(includePath, options, function(err, wsdl) {
        //   if (err) {
        //     return callback(err);
        //   }
        //   self._includesWsdl.push(wsdl);
        //   if (wsdl.definitions instanceof DefinitionsElement) {
        //     _.mergeWith(self.definitions, wsdl.definitions, function(a,b) {
        //       return (a instanceof SchemaElement) ? a.merge(b) : undefined;
        //     });
        //   } else {
        //     self.definitions.schemas[include.namespace || wsdl.definitions.$targetNamespace] = deepMerge(self.definitions.schemas[include.namespace || wsdl.definitions.$targetNamespace], wsdl.definitions);
        //   }
        //   self._processNextInclude(includes, function(err) {
        //     callback(err);
        //   });
        // });
    });
};
WSDL.prototype.processIncludes = function () {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        let schemas = this.definitions.schemas;
        /** @type {?} */
        let includes = [];
        for (let ns in schemas) {
            /** @type {?} */
            let schema = schemas[ns];
            includes = includes.concat(schema.includes || []);
        }
        return this._processNextInclude(includes);
    });
};
WSDL.prototype.describeServices = function () {
    /** @type {?} */
    let services = {};
    for (let name in this.services) {
        /** @type {?} */
        let service = this.services[name];
        services[name] = service.description(this.definitions);
    }
    return services;
};
WSDL.prototype.toXML = function () {
    return this.xml || '';
};
WSDL.prototype.xmlToObject = function (xml, callback) {
    /** @type {?} */
    let self = this;
    /** @type {?} */
    let p = typeof callback === 'function' ? {} : sax.parser(true);
    /** @type {?} */
    let objectName = null;
    /** @type {?} */
    let root = {};
    /** @type {?} */
    let schema = {
        Envelope: {
            Header: {
                Security: {
                    UsernameToken: {
                        Username: 'string',
                        Password: 'string'
                    }
                }
            },
            Body: {
                Fault: {
                    faultcode: 'string',
                    faultstring: 'string',
                    detail: 'string'
                }
            }
        }
    };
    /** @type {?} */
    let stack = [{ name: null, object: root, schema: schema }];
    /** @type {?} */
    let xmlns = {};
    /** @type {?} */
    let refs = {};
    /** @type {?} */
    let id;
    p.onopentag = function (node) {
        /** @type {?} */
        let nsName = node.name;
        /** @type {?} */
        let attrs = node.attributes;
        /** @type {?} */
        let name = splitQName(nsName).name;
        /** @type {?} */
        let attributeName;
        /** @type {?} */
        let top = stack[stack.length - 1];
        /** @type {?} */
        let topSchema = top.schema;
        /** @type {?} */
        let elementAttributes = {};
        /** @type {?} */
        let hasNonXmlnsAttribute = false;
        /** @type {?} */
        let hasNilAttribute = false;
        /** @type {?} */
        let obj = {};
        /** @type {?} */
        let originalName = name;
        if (!objectName && top.name === 'Body' && name !== 'Fault') {
            /** @type {?} */
            let message = self.definitions.messages[name];
            // Support RPC/literal messages where response body contains one element named
            // after the operation + 'Response'. See http://www.w3.org/TR/wsdl#_names
            if (!message) {
                try {
                    // Determine if this is request or response
                    /** @type {?} */
                    let isInput = false;
                    /** @type {?} */
                    let isOutput = false;
                    if ((/Response$/).test(name)) {
                        isOutput = true;
                        name = name.replace(/Response$/, '');
                    }
                    else if ((/Request$/).test(name)) {
                        isInput = true;
                        name = name.replace(/Request$/, '');
                    }
                    else if ((/Solicit$/).test(name)) {
                        isInput = true;
                        name = name.replace(/Solicit$/, '');
                    }
                    // Look up the appropriate message as given in the portType's operations
                    /** @type {?} */
                    let portTypes = self.definitions.portTypes;
                    /** @type {?} */
                    let portTypeNames = Object.keys(portTypes);
                    // Currently this supports only one portType definition.
                    /** @type {?} */
                    let portType = portTypes[portTypeNames[0]];
                    if (isInput) {
                        name = portType.methods[name].input.$name;
                    }
                    else {
                        name = portType.methods[name].output.$name;
                    }
                    message = self.definitions.messages[name];
                    // 'cache' this alias to speed future lookups
                    self.definitions.messages[originalName] = self.definitions.messages[name];
                }
                catch (e) {
                    if (self.options.returnFault) {
                        p.onerror(e);
                    }
                }
            }
            topSchema = message.description(self.definitions);
            objectName = originalName;
        }
        if (attrs.href) {
            id = attrs.href.substr(1);
            if (!refs[id]) {
                refs[id] = { hrefs: [], obj: null };
            }
            refs[id].hrefs.push({ par: top.object, key: name, obj: obj });
        }
        if (id = attrs.id) {
            if (!refs[id]) {
                refs[id] = { hrefs: [], obj: null };
            }
        }
        //Handle element attributes
        for (attributeName in attrs) {
            if (/^xmlns:|^xmlns$/.test(attributeName)) {
                xmlns[splitQName(attributeName).name] = attrs[attributeName];
                continue;
            }
            hasNonXmlnsAttribute = true;
            elementAttributes[attributeName] = attrs[attributeName];
        }
        for (attributeName in elementAttributes) {
            /** @type {?} */
            let res = splitQName(attributeName);
            if (res.name === 'nil' && xmlns[res.prefix] === 'http://www.w3.org/2001/XMLSchema-instance' && elementAttributes[attributeName] &&
                (elementAttributes[attributeName].toLowerCase() === 'true' || elementAttributes[attributeName] === '1')) {
                hasNilAttribute = true;
                break;
            }
        }
        if (hasNonXmlnsAttribute) {
            obj[self.options.attributesKey] = elementAttributes;
        }
        // Pick up the schema for the type specified in element's xsi:type attribute.
        /** @type {?} */
        let xsiTypeSchema;
        /** @type {?} */
        let xsiType = elementAttributes['xsi:type'];
        if (xsiType) {
            /** @type {?} */
            let type = splitQName(xsiType);
            /** @type {?} */
            let typeURI;
            if (type.prefix === TNS_PREFIX) {
                // In case of xsi:type = "MyType"
                typeURI = xmlns[type.prefix] || xmlns.xmlns;
            }
            else {
                typeURI = xmlns[type.prefix];
            }
            /** @type {?} */
            let typeDef = self.findSchemaObject(typeURI, type.name);
            if (typeDef) {
                xsiTypeSchema = typeDef.description(self.definitions);
            }
        }
        if (topSchema && topSchema[name + '[]']) {
            name = name + '[]';
        }
        stack.push({
            name: originalName,
            object: obj,
            schema: (xsiTypeSchema || (topSchema && topSchema[name])),
            id: attrs.id,
            nil: hasNilAttribute
        });
    };
    p.onclosetag = function (nsName) {
        /** @type {?} */
        let cur = stack.pop();
        /** @type {?} */
        let obj = cur.object;
        /** @type {?} */
        let top = stack[stack.length - 1];
        /** @type {?} */
        let topObject = top.object;
        /** @type {?} */
        let topSchema = top.schema;
        /** @type {?} */
        let name = splitQName(nsName).name;
        if (typeof cur.schema === 'string' && (cur.schema === 'string' || ((/** @type {?} */ (cur.schema))).split(':')[1] === 'string')) {
            if (typeof obj === 'object' && Object.keys(obj).length === 0)
                obj = cur.object = '';
        }
        if (cur.nil === true) {
            if (self.options.handleNilAsNull) {
                obj = null;
            }
            else {
                return;
            }
        }
        if (_.isPlainObject(obj) && !Object.keys(obj).length) {
            obj = null;
        }
        if (topSchema && topSchema[name + '[]']) {
            if (!topObject[name]) {
                topObject[name] = [];
            }
            topObject[name].push(obj);
        }
        else if (name in topObject) {
            if (!Array.isArray(topObject[name])) {
                topObject[name] = [topObject[name]];
            }
            topObject[name].push(obj);
        }
        else {
            topObject[name] = obj;
        }
        if (cur.id) {
            refs[cur.id].obj = obj;
        }
    };
    p.oncdata = function (text) {
        /** @type {?} */
        let originalText = text;
        text = trim(text);
        if (!text.length) {
            return;
        }
        if (/<\?xml[\s\S]+\?>/.test(text)) {
            /** @type {?} */
            let top = stack[stack.length - 1];
            /** @type {?} */
            let value = self.xmlToObject(text);
            if (top.object[self.options.attributesKey]) {
                top.object[self.options.valueKey] = value;
            }
            else {
                top.object = value;
            }
        }
        else {
            p.ontext(originalText);
        }
    };
    p.onerror = function (e) {
        p.resume();
        throw {
            Fault: {
                faultcode: 500,
                faultstring: 'Invalid XML',
                detail: new Error(e).message,
                statusCode: 500
            }
        };
    };
    p.ontext = function (text) {
        /** @type {?} */
        let originalText = text;
        text = trim(text);
        if (!text.length) {
            return;
        }
        /** @type {?} */
        let top = stack[stack.length - 1];
        /** @type {?} */
        let name = splitQName(top.schema).name;
        /** @type {?} */
        let value;
        if (self.options && self.options.customDeserializer && self.options.customDeserializer[name]) {
            value = self.options.customDeserializer[name](text, top);
        }
        else {
            if (name === 'int' || name === 'integer') {
                value = parseInt(text, 10);
            }
            else if (name === 'bool' || name === 'boolean') {
                value = text.toLowerCase() === 'true' || text === '1';
            }
            else if (name === 'dateTime' || name === 'date') {
                value = new Date(text);
            }
            else {
                if (self.options.preserveWhitespace) {
                    text = originalText;
                }
                // handle string or other types
                if (typeof top.object !== 'string') {
                    value = text;
                }
                else {
                    value = top.object + text;
                }
            }
        }
        if (top.object[self.options.attributesKey]) {
            top.object[self.options.valueKey] = value;
        }
        else {
            top.object = value;
        }
    };
    if (typeof callback === 'function') {
        // we be streaming
        /** @type {?} */
        let saxStream = sax.createStream(true);
        saxStream.on('opentag', p.onopentag);
        saxStream.on('closetag', p.onclosetag);
        saxStream.on('cdata', p.oncdata);
        saxStream.on('text', p.ontext);
        xml.pipe(saxStream)
            .on('error', function (err) {
            callback(err);
        })
            .on('end', function () {
            /** @type {?} */
            let r;
            try {
                r = finish();
            }
            catch (e) {
                return callback(e);
            }
            callback(null, r);
        });
        return;
    }
    p.write(xml).close();
    return finish();
    /**
     * @return {?}
     */
    function finish() {
        // MultiRef support: merge objects instead of replacing
        for (let n in refs) {
            /** @type {?} */
            let ref = refs[n];
            for (let i = 0; i < ref.hrefs.length; i++) {
                _.assign(ref.hrefs[i].obj, ref.obj);
            }
        }
        if (root.Envelope) {
            /** @type {?} */
            let body = root.Envelope.Body;
            if (body && body.Fault) {
                /** @type {?} */
                let code = body.Fault.faultcode && body.Fault.faultcode.$value;
                /** @type {?} */
                let string = body.Fault.faultstring && body.Fault.faultstring.$value;
                /** @type {?} */
                let detail = body.Fault.detail && body.Fault.detail.$value;
                code = code || body.Fault.faultcode;
                string = string || body.Fault.faultstring;
                detail = detail || body.Fault.detail;
                /** @type {?} */
                let error = new Error(code + ': ' + string + (detail ? ': ' + detail : ''));
                error.root = root;
                throw error;
            }
            return root.Envelope;
        }
        return root;
    }
};
/**
 * Look up a XSD type or element by namespace URI and name
 * @param {String} nsURI Namespace URI
 * @param {String} qname Local or qualified name
 * @returns {*} The XSD type/element definition
 */
WSDL.prototype.findSchemaObject = function (nsURI, qname) {
    if (!nsURI || !qname) {
        return null;
    }
    /** @type {?} */
    let def = null;
    if (this.definitions.schemas) {
        /** @type {?} */
        let schema = this.definitions.schemas[nsURI];
        if (schema) {
            if (qname.indexOf(':') !== -1) {
                qname = qname.substring(qname.indexOf(':') + 1, qname.length);
            }
            // if the client passed an input element which has a `$lookupType` property instead of `$type`
            // the `def` is found in `schema.elements`.
            def = schema.complexTypes[qname] || schema.types[qname] || schema.elements[qname];
        }
    }
    return def;
};
/**
 * Create document style xml string from the parameters
 * @param {String} name
 * @param {*} params
 * @param {String} nsPrefix
 * @param {String} nsURI
 * @param {String} type
 */
WSDL.prototype.objectToDocumentXML = function (name, params, nsPrefix, nsURI, type) {
    //If user supplies XML already, just use that.  XML Declaration should not be present.
    if (params && params._xml) {
        return params._xml;
    }
    /** @type {?} */
    let args = {};
    args[name] = params;
    /** @type {?} */
    let parameterTypeObj = type ? this.findSchemaObject(nsURI, type) : null;
    return this.objectToXML(args, null, nsPrefix, nsURI, true, null, parameterTypeObj);
};
/**
 * Create RPC style xml string from the parameters
 * @param {String} name
 * @param {*} params
 * @param {String} nsPrefix
 * @param {String} nsURI
 * @returns {string}
 */
WSDL.prototype.objectToRpcXML = function (name, params, nsPrefix, nsURI, isParts) {
    /** @type {?} */
    let parts = [];
    /** @type {?} */
    let defs = this.definitions;
    /** @type {?} */
    let nsAttrName = '_xmlns';
    nsPrefix = nsPrefix || findPrefix(defs.xmlns, nsURI);
    nsURI = nsURI || defs.xmlns[nsPrefix];
    nsPrefix = nsPrefix === TNS_PREFIX ? '' : (nsPrefix + ':');
    parts.push(['<', nsPrefix, name, '>'].join(''));
    for (let key in params) {
        if (!params.hasOwnProperty(key)) {
            continue;
        }
        if (key !== nsAttrName) {
            /** @type {?} */
            let value = params[key];
            /** @type {?} */
            let prefixedKey = (isParts ? '' : nsPrefix) + key;
            /** @type {?} */
            let attributes = [];
            if (typeof value === 'object' && value.hasOwnProperty(this.options.attributesKey)) {
                /** @type {?} */
                let attrs = value[this.options.attributesKey];
                for (let n in attrs) {
                    attributes.push(' ' + n + '=' + '"' + attrs[n] + '"');
                }
            }
            parts.push(['<', prefixedKey].concat(attributes).concat('>').join(''));
            parts.push((typeof value === 'object') ? this.objectToXML(value, key, nsPrefix, nsURI) : xmlEscape(value));
            parts.push(['</', prefixedKey, '>'].join(''));
        }
    }
    parts.push(['</', nsPrefix, name, '>'].join(''));
    return parts.join('');
};
/**
 * @param {?} ns
 * @return {?}
 */
function appendColon(ns) {
    return (ns && ns.charAt(ns.length - 1) !== ':') ? ns + ':' : ns;
}
/**
 * @param {?} ns
 * @return {?}
 */
function noColonNameSpace(ns) {
    return (ns && ns.charAt(ns.length - 1) === ':') ? ns.substring(0, ns.length - 1) : ns;
}
WSDL.prototype.isIgnoredNameSpace = function (ns) {
    return this.options.ignoredNamespaces.indexOf(ns) > -1;
};
WSDL.prototype.filterOutIgnoredNameSpace = function (ns) {
    /** @type {?} */
    let namespace = noColonNameSpace(ns);
    return this.isIgnoredNameSpace(namespace) ? '' : namespace;
};
/**
 * Convert an object to XML.  This is a recursive method as it calls itself.
 *
 * @param {Object} obj the object to convert.
 * @param {String} name the name of the element (if the object being traversed is
 * an element).
 * @param {String} nsPrefix the namespace prefix of the object I.E. xsd.
 * @param {String} nsURI the full namespace of the object I.E. http://w3.org/schema.
 * @param {Boolean} isFirst whether or not this is the first item being traversed.
 * @param {?} xmlnsAttr
 * @param {?} parameterTypeObject
 * @param {NamespaceContext} nsContext Namespace context
 */
WSDL.prototype.objectToXML = function (obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext) {
    /** @type {?} */
    let self = this;
    /** @type {?} */
    let schema = this.definitions.schemas[nsURI];
    /** @type {?} */
    let parentNsPrefix = nsPrefix ? nsPrefix.parent : undefined;
    if (typeof parentNsPrefix !== 'undefined') {
        //we got the parentNsPrefix for our array. setting the namespace-letiable back to the current namespace string
        nsPrefix = nsPrefix.current;
    }
    parentNsPrefix = noColonNameSpace(parentNsPrefix);
    if (this.isIgnoredNameSpace(parentNsPrefix)) {
        parentNsPrefix = '';
    }
    /** @type {?} */
    let soapHeader = !schema;
    /** @type {?} */
    let qualified = schema && schema.$elementFormDefault === 'qualified';
    /** @type {?} */
    let parts = [];
    /** @type {?} */
    let prefixNamespace = (nsPrefix || qualified) && nsPrefix !== TNS_PREFIX;
    /** @type {?} */
    let xmlnsAttrib = '';
    if (nsURI && isFirst) {
        if (self.options.overrideRootElement && self.options.overrideRootElement.xmlnsAttributes) {
            self.options.overrideRootElement.xmlnsAttributes.forEach(function (attribute) {
                xmlnsAttrib += ' ' + attribute.name + '="' + attribute.value + '"';
            });
        }
        else {
            if (prefixNamespace && !this.isIgnoredNameSpace(nsPrefix)) {
                // resolve the prefix namespace
                xmlnsAttrib += ' xmlns:' + nsPrefix + '="' + nsURI + '"';
            }
            // only add default namespace if the schema elementFormDefault is qualified
            if (qualified || soapHeader)
                xmlnsAttrib += ' xmlns="' + nsURI + '"';
        }
    }
    if (!nsContext) {
        nsContext = new NamespaceContext();
        nsContext.declareNamespace(nsPrefix, nsURI);
    }
    else {
        nsContext.pushContext();
    }
    // explicitly use xmlns attribute if available
    if (xmlnsAttr && !(self.options.overrideRootElement && self.options.overrideRootElement.xmlnsAttributes)) {
        xmlnsAttrib = xmlnsAttr;
    }
    /** @type {?} */
    let ns = '';
    if (self.options.overrideRootElement && isFirst) {
        ns = self.options.overrideRootElement.namespace;
    }
    else if (prefixNamespace && (qualified || isFirst || soapHeader) && !this.isIgnoredNameSpace(nsPrefix)) {
        ns = nsPrefix;
    }
    /** @type {?} */
    let i;
    /** @type {?} */
    let n;
    // start building out XML string.
    if (Array.isArray(obj)) {
        for (i = 0, n = obj.length; i < n; i++) {
            /** @type {?} */
            let item = obj[i];
            /** @type {?} */
            let arrayAttr = self.processAttributes(item, nsContext);
            /** @type {?} */
            let correctOuterNsPrefix = parentNsPrefix || ns;
            //using the parent namespace prefix if given
            /** @type {?} */
            let body = self.objectToXML(item, name, nsPrefix, nsURI, false, null, schemaObject, nsContext);
            /** @type {?} */
            let openingTagParts = ['<', appendColon(correctOuterNsPrefix), name, arrayAttr, xmlnsAttrib];
            if (body === '' && self.options.useEmptyTag) {
                // Use empty (self-closing) tags if no contents
                openingTagParts.push(' />');
                parts.push(openingTagParts.join(''));
            }
            else {
                openingTagParts.push('>');
                if (self.options.namespaceArrayElements || i === 0) {
                    parts.push(openingTagParts.join(''));
                }
                parts.push(body);
                if (self.options.namespaceArrayElements || i === n - 1) {
                    parts.push(['</', appendColon(correctOuterNsPrefix), name, '>'].join(''));
                }
            }
        }
    }
    else if (typeof obj === 'object') {
        for (name in obj) {
            if (!obj.hasOwnProperty(name))
                continue;
            //don't process attributes as element
            if (name === self.options.attributesKey) {
                continue;
            }
            //Its the value of a xml object. Return it directly.
            if (name === self.options.xmlKey) {
                nsContext.popContext();
                return obj[name];
            }
            //Its the value of an item. Return it directly.
            if (name === self.options.valueKey) {
                nsContext.popContext();
                return xmlEscape(obj[name]);
            }
            /** @type {?} */
            let child = obj[name];
            if (typeof child === 'undefined') {
                continue;
            }
            /** @type {?} */
            let attr = self.processAttributes(child, nsContext);
            /** @type {?} */
            let value = '';
            /** @type {?} */
            let nonSubNameSpace = '';
            /** @type {?} */
            let emptyNonSubNameSpace = false;
            /** @type {?} */
            let nameWithNsRegex = /^([^:]+):([^:]+)$/.exec(name);
            if (nameWithNsRegex) {
                nonSubNameSpace = nameWithNsRegex[1] + ':';
                name = nameWithNsRegex[2];
            }
            else if (name[0] === ':') {
                emptyNonSubNameSpace = true;
                name = name.substr(1);
            }
            if (isFirst) {
                value = self.objectToXML(child, name, nsPrefix, nsURI, false, null, schemaObject, nsContext);
            }
            else {
                if (self.definitions.schemas) {
                    if (schema) {
                        /** @type {?} */
                        let childSchemaObject = self.findChildSchemaObject(schemaObject, name);
                        //find sub namespace if not a primitive
                        if (childSchemaObject &&
                            ((childSchemaObject.$type && (childSchemaObject.$type.indexOf('xsd:') === -1)) ||
                                childSchemaObject.$ref || childSchemaObject.$name)) {
                            /*if the base name space of the children is not in the ingoredSchemaNamspaces we use it.
                                           This is because in some services the child nodes do not need the baseNameSpace.
                                           */
                            /** @type {?} */
                            let childNsPrefix = '';
                            /** @type {?} */
                            let childName = '';
                            /** @type {?} */
                            let childNsURI;
                            /** @type {?} */
                            let childXmlnsAttrib = '';
                            /** @type {?} */
                            let elementQName = childSchemaObject.$ref || childSchemaObject.$name;
                            if (elementQName) {
                                elementQName = splitQName(elementQName);
                                childName = elementQName.name;
                                if (elementQName.prefix === TNS_PREFIX) {
                                    // Local element
                                    childNsURI = childSchemaObject.$targetNamespace;
                                    childNsPrefix = nsContext.registerNamespace(childNsURI);
                                    if (this.isIgnoredNameSpace(childNsPrefix)) {
                                        childNsPrefix = nsPrefix;
                                    }
                                }
                                else {
                                    childNsPrefix = elementQName.prefix;
                                    if (this.isIgnoredNameSpace(childNsPrefix)) {
                                        childNsPrefix = nsPrefix;
                                    }
                                    childNsURI = schema.xmlns[childNsPrefix] || self.definitions.xmlns[childNsPrefix];
                                }
                                /** @type {?} */
                                let unqualified = false;
                                // Check qualification form for local elements
                                if (childSchemaObject.$name && childSchemaObject.targetNamespace === undefined) {
                                    if (childSchemaObject.$form === 'unqualified') {
                                        unqualified = true;
                                    }
                                    else if (childSchemaObject.$form === 'qualified') {
                                        unqualified = false;
                                    }
                                    else {
                                        unqualified = schema.$elementFormDefault !== 'qualified';
                                    }
                                }
                                if (unqualified) {
                                    childNsPrefix = '';
                                }
                                if (childNsURI && childNsPrefix) {
                                    if (nsContext.declareNamespace(childNsPrefix, childNsURI)) {
                                        childXmlnsAttrib = ' xmlns:' + childNsPrefix + '="' + childNsURI + '"';
                                        xmlnsAttrib += childXmlnsAttrib;
                                    }
                                }
                            }
                            /** @type {?} */
                            let resolvedChildSchemaObject;
                            if (childSchemaObject.$type) {
                                /** @type {?} */
                                let typeQName = splitQName(childSchemaObject.$type);
                                /** @type {?} */
                                let typePrefix = typeQName.prefix;
                                /** @type {?} */
                                let typeURI = schema.xmlns[typePrefix] || self.definitions.xmlns[typePrefix];
                                childNsURI = typeURI;
                                if (typeURI !== 'http://www.w3.org/2001/XMLSchema' && typePrefix !== TNS_PREFIX) {
                                    // Add the prefix/namespace mapping, but not declare it
                                    nsContext.addNamespace(typePrefix, typeURI);
                                }
                                resolvedChildSchemaObject =
                                    self.findSchemaType(typeQName.name, typeURI) || childSchemaObject;
                            }
                            else {
                                resolvedChildSchemaObject =
                                    self.findSchemaObject(childNsURI, childName) || childSchemaObject;
                            }
                            if (childSchemaObject.$baseNameSpace && this.options.ignoreBaseNameSpaces) {
                                childNsPrefix = nsPrefix;
                                childNsURI = nsURI;
                            }
                            if (this.options.ignoreBaseNameSpaces) {
                                childNsPrefix = '';
                                childNsURI = '';
                            }
                            ns = childNsPrefix;
                            if (Array.isArray(child)) {
                                //for arrays, we need to remember the current namespace
                                childNsPrefix = {
                                    current: childNsPrefix,
                                    parent: ns
                                };
                            }
                            else {
                                //parent (array) already got the namespace
                                childXmlnsAttrib = null;
                            }
                            value = self.objectToXML(child, name, childNsPrefix, childNsURI, false, childXmlnsAttrib, resolvedChildSchemaObject, nsContext);
                        }
                        else if (obj[self.options.attributesKey] && obj[self.options.attributesKey].xsi_type) {
                            //if parent object has complex type defined and child not found in parent
                            /** @type {?} */
                            let completeChildParamTypeObject = self.findChildSchemaObject(obj[self.options.attributesKey].xsi_type.type, obj[self.options.attributesKey].xsi_type.xmlns);
                            nonSubNameSpace = obj[self.options.attributesKey].xsi_type.prefix;
                            nsContext.addNamespace(obj[self.options.attributesKey].xsi_type.prefix, obj[self.options.attributesKey].xsi_type.xmlns);
                            value = self.objectToXML(child, name, obj[self.options.attributesKey].xsi_type.prefix, obj[self.options.attributesKey].xsi_type.xmlns, false, null, null, nsContext);
                        }
                        else {
                            if (Array.isArray(child)) {
                                name = nonSubNameSpace + name;
                            }
                            value = self.objectToXML(child, name, nsPrefix, nsURI, false, null, null, nsContext);
                        }
                    }
                    else {
                        value = self.objectToXML(child, name, nsPrefix, nsURI, false, null, null, nsContext);
                    }
                }
            }
            ns = noColonNameSpace(ns);
            if (prefixNamespace && !qualified && isFirst && !self.options.overrideRootElement) {
                ns = nsPrefix;
            }
            else if (this.isIgnoredNameSpace(ns)) {
                ns = '';
            }
            /** @type {?} */
            let useEmptyTag = !value && self.options.useEmptyTag;
            if (!Array.isArray(child)) {
                // start tag
                parts.push(['<', emptyNonSubNameSpace ? '' : appendColon(nonSubNameSpace || ns), name, attr, xmlnsAttrib,
                    (child === null ? ' xsi:nil="true"' : ''),
                    useEmptyTag ? ' />' : '>'
                ].join(''));
            }
            if (!useEmptyTag) {
                parts.push(value);
                if (!Array.isArray(child)) {
                    // end tag
                    parts.push(['</', emptyNonSubNameSpace ? '' : appendColon(nonSubNameSpace || ns), name, '>'].join(''));
                }
            }
        }
    }
    else if (obj !== undefined) {
        parts.push((self.options.escapeXML) ? xmlEscape(obj) : obj);
    }
    nsContext.popContext();
    return parts.join('');
};
WSDL.prototype.processAttributes = function (child, nsContext) {
    /** @type {?} */
    let attr = '';
    if (child === null) {
        child = [];
    }
    /** @type {?} */
    let attrObj = child[this.options.attributesKey];
    if (attrObj && attrObj.xsi_type) {
        /** @type {?} */
        let xsiType = attrObj.xsi_type;
        /** @type {?} */
        let prefix = xsiType.prefix || xsiType.namespace;
        // Generate a new namespace for complex extension if one not provided
        if (!prefix) {
            prefix = nsContext.registerNamespace(xsiType.xmlns);
        }
        else {
            nsContext.declareNamespace(prefix, xsiType.xmlns);
        }
        xsiType.prefix = prefix;
    }
    if (attrObj) {
        for (let attrKey in attrObj) {
            //handle complex extension separately
            if (attrKey === 'xsi_type') {
                /** @type {?} */
                let attrValue = attrObj[attrKey];
                attr += ' xsi:type="' + attrValue.prefix + ':' + attrValue.type + '"';
                attr += ' xmlns:' + attrValue.prefix + '="' + attrValue.xmlns + '"';
                continue;
            }
            else {
                attr += ' ' + attrKey + '="' + xmlEscape(attrObj[attrKey]) + '"';
            }
        }
    }
    return attr;
};
/**
 * Look up a schema type definition
 * @param name
 * @param nsURI
 * @returns {*}
 */
WSDL.prototype.findSchemaType = function (name, nsURI) {
    if (!this.definitions.schemas || !name || !nsURI) {
        return null;
    }
    /** @type {?} */
    let schema = this.definitions.schemas[nsURI];
    if (!schema || !schema.complexTypes) {
        return null;
    }
    return schema.complexTypes[name];
};
WSDL.prototype.findChildSchemaObject = function (parameterTypeObj, childName, backtrace) {
    if (!parameterTypeObj || !childName) {
        return null;
    }
    if (!backtrace) {
        backtrace = [];
    }
    if (backtrace.indexOf(parameterTypeObj) >= 0) {
        // We've recursed back to ourselves; break.
        return null;
    }
    else {
        backtrace = backtrace.concat([parameterTypeObj]);
    }
    /** @type {?} */
    let found = null;
    /** @type {?} */
    let i = 0;
    /** @type {?} */
    let child;
    /** @type {?} */
    let ref;
    if (Array.isArray(parameterTypeObj.$lookupTypes) && parameterTypeObj.$lookupTypes.length) {
        /** @type {?} */
        let types = parameterTypeObj.$lookupTypes;
        for (i = 0; i < types.length; i++) {
            /** @type {?} */
            let typeObj = types[i];
            if (typeObj.$name === childName) {
                found = typeObj;
                break;
            }
        }
    }
    /** @type {?} */
    let object = parameterTypeObj;
    if (object.$name === childName && object.name === 'element') {
        return object;
    }
    if (object.$ref) {
        ref = splitQName(object.$ref);
        if (ref.name === childName) {
            return object;
        }
    }
    /** @type {?} */
    let childNsURI;
    // want to avoid unecessary recursion to improve performance
    if (object.$type && backtrace.length === 1) {
        /** @type {?} */
        let typeInfo = splitQName(object.$type);
        if (typeInfo.prefix === TNS_PREFIX) {
            childNsURI = parameterTypeObj.$targetNamespace;
        }
        else {
            childNsURI = this.definitions.xmlns[typeInfo.prefix];
        }
        /** @type {?} */
        let typeDef = this.findSchemaType(typeInfo.name, childNsURI);
        if (typeDef) {
            return this.findChildSchemaObject(typeDef, childName, backtrace);
        }
    }
    if (object.children) {
        for (i = 0, child; child = object.children[i]; i++) {
            found = this.findChildSchemaObject(child, childName, backtrace);
            if (found) {
                break;
            }
            if (child.$base) {
                /** @type {?} */
                let baseQName = splitQName(child.$base);
                /** @type {?} */
                let childNameSpace = baseQName.prefix === TNS_PREFIX ? '' : baseQName.prefix;
                childNsURI = child.xmlns[baseQName.prefix] || this.definitions.xmlns[baseQName.prefix];
                /** @type {?} */
                let foundBase = this.findSchemaType(baseQName.name, childNsURI);
                if (foundBase) {
                    found = this.findChildSchemaObject(foundBase, childName, backtrace);
                    if (found) {
                        found.$baseNameSpace = childNameSpace;
                        found.$type = childNameSpace + ':' + childName;
                        break;
                    }
                }
            }
        }
    }
    if (!found && object.$name === childName) {
        return object;
    }
    return found;
};
WSDL.prototype._parse = function (xml) {
    /** @type {?} */
    let self = this;
    /** @type {?} */
    let p = sax.parser(true);
    /** @type {?} */
    let stack = [];
    /** @type {?} */
    let root = null;
    /** @type {?} */
    let types = null;
    /** @type {?} */
    let schema = null;
    /** @type {?} */
    let options = self.options;
    p.onopentag = function (node) {
        /** @type {?} */
        let nsName = node.name;
        /** @type {?} */
        let attrs = node.attributes;
        /** @type {?} */
        let top = stack[stack.length - 1];
        /** @type {?} */
        let name;
        if (top) {
            try {
                top.startElement(stack, nsName, attrs, options);
            }
            catch (e) {
                if (self.options.strict) {
                    throw e;
                }
                else {
                    stack.push(new Element(nsName, attrs, options));
                }
            }
        }
        else {
            name = splitQName(nsName).name;
            if (name === 'definitions') {
                root = new DefinitionsElement(nsName, attrs, options);
                stack.push(root);
            }
            else if (name === 'schema') {
                // Shim a structure in here to allow the proper objects to be created when merging back.
                root = new DefinitionsElement('definitions', {}, {});
                types = new TypesElement('types', {}, {});
                schema = new SchemaElement(nsName, attrs, options);
                types.addChild(schema);
                root.addChild(types);
                stack.push(schema);
            }
            else {
                throw new Error('Unexpected root element of WSDL or include');
            }
        }
    };
    p.onclosetag = function (name) {
        /** @type {?} */
        let top = stack[stack.length - 1];
        assert(top, 'Unmatched close tag: ' + name);
        top.endElement(stack, name);
    };
    p.write(xml).close();
    return root;
};
WSDL.prototype._fromXML = function (xml) {
    this.definitions = this._parse(xml);
    this.definitions.descriptions = {
        types: {}
    };
    this.xml = xml;
};
WSDL.prototype._fromServices = function (services) {
};
WSDL.prototype._xmlnsMap = function () {
    /** @type {?} */
    let xmlns = this.definitions.xmlns;
    /** @type {?} */
    let str = '';
    for (let alias in xmlns) {
        if (alias === '' || alias === TNS_PREFIX) {
            continue;
        }
        /** @type {?} */
        let ns = xmlns[alias];
        switch (ns) {
            case "http://xml.apache.org/xml-soap": // apachesoap
            case "http://schemas.xmlsoap.org/wsdl/": // wsdl
            case "http://schemas.xmlsoap.org/wsdl/soap/": // wsdlsoap
            case "http://schemas.xmlsoap.org/wsdl/soap12/": // wsdlsoap12
            case "http://schemas.xmlsoap.org/soap/encoding/": // soapenc
            case "http://www.w3.org/2001/XMLSchema": // xsd
                continue;
        }
        if (~ns.indexOf('http://schemas.xmlsoap.org/')) {
            continue;
        }
        if (~ns.indexOf('http://www.w3.org/')) {
            continue;
        }
        if (~ns.indexOf('http://xml.apache.org/')) {
            continue;
        }
        str += ' xmlns:' + alias + '="' + ns + '"';
    }
    return str;
};
/*
 * Have another function to load previous WSDLs as we
 * don't want this to be invoked externally (expect for tests)
 * This will attempt to fix circular dependencies with XSD files,
 * Given
 * - file.wsdl
 *   - xs:import namespace="A" schemaLocation: A.xsd
 * - A.xsd
 *   - xs:import namespace="B" schemaLocation: B.xsd
 * - B.xsd
 *   - xs:import namespace="A" schemaLocation: A.xsd
 * file.wsdl will start loading, import A, then A will import B, which will then import A
 * Because A has already started to load previously it will be returned right away and
 * have an internal circular reference
 * B would then complete loading, then A, then file.wsdl
 * By the time file A starts processing its includes its definitions will be already loaded,
 * this is the only thing that B will depend on when "opening" A
 */
/**
 * @param {?} uri
 * @param {?} options
 * @return {?}
 */
function open_wsdl_recursive(uri, options) {
    /** @type {?} */
    let fromCache;
    /** @type {?} */
    let WSDL_CACHE;
    // if (typeof options === 'function') {
    //   callback = options;
    //   options = {};
    // }
    WSDL_CACHE = options.WSDL_CACHE;
    if (fromCache = WSDL_CACHE[uri]) {
        // return callback.call(fromCache, null, fromCache);
        return fromCache;
    }
    return open_wsdl(uri, options);
}
/**
 * @param {?} uri
 * @param {?} options
 * @return {?}
 */
export function open_wsdl(uri, options) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        // if (typeof options === 'function') {
        //   callback = options;
        //   options = {};
        // }
        // if (typeof options === 'function') {
        //   callback = options;
        //   options = {};
        // }
        // initialize cache when calling open_wsdl directly
        /** @type {?} */
        let WSDL_CACHE = options.WSDL_CACHE || {};
        /** @type {?} */
        let request_headers = options.wsdl_headers;
        /** @type {?} */
        let request_options = options.wsdl_options;
        // let wsdl;
        // if (!/^https?:/.test(uri)) {
        //   // debug('Reading file: %s', uri);
        //   // fs.readFile(uri, 'utf8', function(err, definition) {
        //   //   if (err) {
        //   //     callback(err);
        //   //   }
        //   //   else {
        //   //     wsdl = new WSDL(definition, uri, options);
        //   //     WSDL_CACHE[ uri ] = wsdl;
        //   //     wsdl.WSDL_CACHE = WSDL_CACHE;
        //   //     wsdl.onReady(callback);
        //   //   }
        //   // });
        // }
        // else {
        //   debug('Reading url: %s', uri);
        //   let httpClient = options.httpClient || new HttpClient(options);
        //   httpClient.request(uri, null /* options */, function(err, response, definition) {
        //     if (err) {
        //       callback(err);
        //     } else if (response && response.statusCode === 200) {
        //       wsdl = new WSDL(definition, uri, options);
        //       WSDL_CACHE[ uri ] = wsdl;
        //       wsdl.WSDL_CACHE = WSDL_CACHE;
        //       wsdl.onReady(callback);
        //     } else {
        //       callback(new Error('Invalid WSDL URL: ' + uri + "\n\n\r Code: " + response.statusCode + "\n\n\r Response Body: " + response.body));
        //     }
        //   }, request_headers, request_options);
        // }
        // return wsdl;
        // console.log('Reading url: %s', uri);
        /** @type {?} */
        const httpClient = options.httpClient;
        /** @type {?} */
        const wsdlDef = yield httpClient.get(uri, { responseType: 'text' }).toPromise();
        /** @type {?} */
        const wsdlObj = yield new Promise((resolve) => {
            /** @type {?} */
            const wsdl = new WSDL(wsdlDef, uri, options);
            WSDL_CACHE[uri] = wsdl;
            wsdl.WSDL_CACHE = WSDL_CACHE;
            wsdl.onReady(resolve(wsdl));
        });
        // console.log("wsdl", wsdlObj);
        return wsdlObj;
    });
}
export { ɵ0, ɵ1 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid3NkbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1zb2FwLyIsInNvdXJjZXMiOlsibGliL3NvYXAvd3NkbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQU9BLFlBQVksQ0FBQztBQUViLE9BQU8sS0FBSyxHQUFHLE1BQU0sS0FBSyxDQUFDO0FBRTNCLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFPLGFBQWEsQ0FBQztBQUVoRCxPQUFPLEtBQUssR0FBRyxNQUFNLEtBQUssQ0FBQztBQUMzQixPQUFPLEVBQUUsRUFBRSxJQUFJLE1BQU0sRUFBRSxNQUFNLFFBQVEsQ0FBQzs7O01BR2hDLFFBQVEsR0FBRyxDQUFDLENBQVMsRUFBVSxFQUFFO0lBQ3JDLDBEQUEwRDtJQUMxRCxnREFBZ0Q7SUFDaEQsSUFBSSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxLQUFLLE1BQU0sRUFBRTtRQUM5QixPQUFPLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7S0FDbkI7SUFFRCxPQUFPLENBQUMsQ0FBQztBQUNYLENBQUM7O0FBRUQsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxLQUFLLEtBQUssTUFBTSxTQUFTLENBQUM7O0lBRzdCLFVBQVUsR0FBRyxLQUFLLENBQUMsVUFBVTs7SUFDN0IsVUFBVSxHQUFHLEtBQUssQ0FBQyxVQUFVOztJQUU3QixVQUFVLEdBQUc7SUFDZixNQUFNLEVBQUUsQ0FBQztJQUNULE9BQU8sRUFBRSxDQUFDO0lBQ1YsT0FBTyxFQUFFLENBQUM7SUFDVixLQUFLLEVBQUUsQ0FBQztJQUNSLE1BQU0sRUFBRSxDQUFDO0lBQ1QsT0FBTyxFQUFFLENBQUM7SUFDVixJQUFJLEVBQUUsQ0FBQztJQUNQLEdBQUcsRUFBRSxDQUFDO0lBQ04sSUFBSSxFQUFFLENBQUM7SUFDUCxLQUFLLEVBQUUsQ0FBQztJQUNSLGVBQWUsRUFBRSxDQUFDO0lBQ2xCLGtCQUFrQixFQUFFLENBQUM7SUFDckIsZUFBZSxFQUFFLENBQUM7SUFDbEIsa0JBQWtCLEVBQUUsQ0FBQztJQUNyQixZQUFZLEVBQUUsQ0FBQztJQUNmLFdBQVcsRUFBRSxDQUFDO0lBQ2QsWUFBWSxFQUFFLENBQUM7SUFDZixhQUFhLEVBQUUsQ0FBQztJQUNoQixRQUFRLEVBQUUsQ0FBQztJQUNYLFFBQVEsRUFBRSxDQUFDO0lBQ1gsSUFBSSxFQUFFLENBQUM7SUFDUCxJQUFJLEVBQUUsQ0FBQztJQUNQLFVBQVUsRUFBRSxDQUFDO0lBQ2IsS0FBSyxFQUFFLENBQUM7SUFDUixTQUFTLEVBQUUsQ0FBQztJQUNaLElBQUksRUFBRSxDQUFDO0lBQ1AsTUFBTSxFQUFFLENBQUM7SUFDVCxTQUFTLEVBQUUsQ0FBQztJQUNaLFlBQVksRUFBRSxDQUFDO0lBQ2YsTUFBTSxFQUFFLENBQUM7SUFDVCxLQUFLLEVBQUUsQ0FBQztJQUNSLFFBQVEsRUFBRSxDQUFDO0NBQ1o7Ozs7O0FBRUQsU0FBUyxVQUFVLENBQUMsTUFBTTs7UUFDcEIsQ0FBQyxHQUFHLE9BQU8sTUFBTSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzdELE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ25ELEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDO0FBQ3RFLENBQUM7Ozs7O0FBRUQsU0FBUyxTQUFTLENBQUMsR0FBRztJQUNwQixJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxRQUFRLEVBQUU7UUFDN0IsSUFBSSxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxXQUFXLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssRUFBRTtZQUNoRSxPQUFPLEdBQUcsQ0FBQztTQUNaO1FBQ0QsT0FBTyxHQUFHO2FBQ1AsT0FBTyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUM7YUFDdEIsT0FBTyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUM7YUFDckIsT0FBTyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUM7YUFDckIsT0FBTyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUM7YUFDdkIsT0FBTyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztLQUM1QjtJQUVELE9BQU8sR0FBRyxDQUFDO0FBQ2IsQ0FBQzs7SUFFRyxRQUFRLEdBQUcsWUFBWTs7SUFDdkIsU0FBUyxHQUFHLFlBQVk7Ozs7O0FBRTVCLFNBQVMsSUFBSSxDQUFDLElBQUk7SUFDaEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQzNELENBQUM7Ozs7OztBQUVELFNBQVMsU0FBUyxDQUFDLFdBQVcsRUFBRSxNQUFNO0lBQ3BDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxXQUFXLElBQUksRUFBRSxFQUFFLE1BQU0sRUFBRSxVQUFVLENBQUMsRUFBRSxDQUFDO1FBQzFELE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO0lBQ2hELENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQzs7SUFFRyxPQUFPLEdBQVEsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLE9BQU87O1FBQzdDLEtBQUssR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDO0lBRTlCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO0lBQ3JCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUMzQixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUM7SUFDdkIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7SUFFaEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBRWpDLEtBQUssSUFBSSxHQUFHLElBQUksS0FBSyxFQUFFOztZQUNqQixLQUFLLEdBQUcsZUFBZSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDckMsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDM0Q7YUFDSTtZQUNILElBQUksR0FBRyxLQUFLLE9BQU8sRUFBRTtnQkFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDbEM7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDOUI7U0FDRjtLQUNGO0lBQ0QsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEtBQUssU0FBUyxFQUFFO1FBQ3ZDLHFDQUFxQztRQUNyQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztLQUNoRDtBQUNILENBQUM7O0FBRUQsT0FBTyxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsR0FBRyxVQUFVLE9BQU87SUFDdEQsSUFBSSxPQUFPLEVBQUU7UUFDWCxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxRQUFRLElBQUksUUFBUSxDQUFDO1FBQzdDLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUM7UUFDdkMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsSUFBSSxFQUFFLENBQUM7S0FDMUQ7U0FBTTtRQUNMLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7S0FDN0I7QUFDSCxDQUFDLENBQUM7QUFFRixPQUFPLENBQUMsU0FBUyxDQUFDLGdCQUFnQixHQUFHO0lBQ25DLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxJQUFJLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUNwRSxJQUFJLENBQUMsS0FBSyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3hFLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNuQixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDbkIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0FBQ25CLENBQUMsQ0FBQztBQUVGLE9BQU8sQ0FBQyxTQUFTLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQztBQUV2QyxPQUFPLENBQUMsU0FBUyxDQUFDLFlBQVksR0FBRyxVQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE9BQU87SUFDdEUsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUU7UUFDekIsT0FBTztLQUNSOztRQUVHLFVBQVUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUM7O1FBQzVELE9BQU8sR0FBRyxJQUFJO0lBRWhCLElBQUksVUFBVSxFQUFFO1FBQ2QsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLFVBQVUsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7S0FDcEQ7U0FDSTtRQUNILElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7S0FDekI7QUFFSCxDQUFDLENBQUM7QUFFRixPQUFPLENBQUMsU0FBUyxDQUFDLFVBQVUsR0FBRyxVQUFVLEtBQUssRUFBRSxNQUFNO0lBQ3BELElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxNQUFNLEVBQUU7UUFDMUIsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUM7WUFDbEIsT0FBTzs7WUFDTCxNQUFNLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ3BDLElBQUksSUFBSSxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNyQixDQUFDLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzNDLHFCQUFxQjtZQUNyQixNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMzQixNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3ZCO1FBQ0QsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO0tBQ2I7QUFDSCxDQUFDLENBQUM7QUFFRixPQUFPLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxVQUFVLEtBQUs7SUFDMUMsT0FBTztBQUNULENBQUMsQ0FBQztBQUVGLE9BQU8sQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFVBQVUsSUFBSTtJQUMzQyxNQUFNLElBQUksS0FBSyxDQUFDLDRCQUE0QixHQUFHLElBQUksR0FBRyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ25GLENBQUMsQ0FBQztBQUVGLE9BQU8sQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsV0FBVztJQUNuRCxPQUFPLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQztBQUNqQyxDQUFDLENBQUM7QUFFRixPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRztBQUN6QixDQUFDLENBQUM7QUFFRixPQUFPLENBQUMsY0FBYyxHQUFHOztRQUNuQixJQUFJLEdBQUcsSUFBSTs7UUFDWCxVQUFVLEdBQUc7UUFDZixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztRQUM1QixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDZCxDQUFDO0lBQ0QsOEJBQThCO0lBQzlCLFVBQVUsQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDaEQsT0FBTyxVQUFVLENBQUM7QUFDcEIsQ0FBQyxDQUFDOztJQUdFLGNBQWMsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUN6QyxVQUFVLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFDckMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUU7O0lBQ3ZDLGFBQWEsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUN4QyxpQkFBaUIsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUM1QyxrQkFBa0IsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUM3QyxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUMzQyxhQUFhLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFDeEMsa0JBQWtCLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFDN0Msa0JBQWtCLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFDN0MscUJBQXFCLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFDaEQsb0JBQW9CLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFDL0MsZUFBZSxHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUU7O0lBQzFDLFVBQVUsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUNyQyxjQUFjLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFDekMsb0JBQW9CLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFFL0MsYUFBYSxHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUU7O0lBQ3hDLFlBQVksR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUN2QyxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUMzQyxlQUFlLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFDMUMsY0FBYyxHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUU7O0lBQ3pDLFdBQVcsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUN0QyxjQUFjLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFDekMsa0JBQWtCLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFFN0MsY0FBYyxHQUFHO0lBQ25CLEtBQUssRUFBRSxDQUFDLFlBQVksRUFBRSxzQkFBc0IsQ0FBQztJQUM3QyxNQUFNLEVBQUUsQ0FBQyxhQUFhLEVBQUUsK0NBQStDLENBQUM7SUFDeEUsT0FBTyxFQUFFLENBQUMsY0FBYyxFQUFFLHdCQUF3QixDQUFDO0lBQ25ELEdBQUcsRUFBRSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUM7SUFDckIsVUFBVSxFQUFFLENBQUMsaUJBQWlCLEVBQUUsYUFBYSxDQUFDO0lBQzlDLFdBQVcsRUFBRSxDQUFDLGtCQUFrQixFQUFFLGlDQUFpQyxDQUFDO0lBQ3BFLFNBQVMsRUFBRSxDQUFDLGdCQUFnQixFQUFFLHFCQUFxQixDQUFDO0lBQ3BELE1BQU0sRUFBRSxDQUFDLGFBQWEsRUFBRSw2QkFBNkIsQ0FBQzs7SUFFdEQsV0FBVyxFQUFFLENBQUMsa0JBQWtCLEVBQUUsRUFBRSxDQUFDO0lBQ3JDLFdBQVcsRUFBRSxDQUFDLGtCQUFrQixFQUFFLDZEQUE2RCxDQUFDO0lBQ2hHLGNBQWMsRUFBRSxDQUFDLHFCQUFxQixFQUFFLFdBQVcsQ0FBQztJQUNwRCxhQUFhLEVBQUUsQ0FBQyxvQkFBb0IsRUFBRSxXQUFXLENBQUM7SUFDbEQsUUFBUSxFQUFFLENBQUMsZUFBZSxFQUFFLDZCQUE2QixDQUFDO0lBQzFELEdBQUcsRUFBRSxDQUFDLFVBQVUsRUFBRSxnQkFBZ0IsQ0FBQztJQUVuQyxPQUFPLEVBQUUsQ0FBQyxjQUFjLEVBQUUsb0JBQW9CLENBQUM7SUFDL0MsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLHVCQUF1QixDQUFDO0lBQzVDLE9BQU8sRUFBRSxDQUFDLGNBQWMsRUFBRSwrQ0FBK0MsQ0FBQztJQUMxRSxRQUFRLEVBQUUsQ0FBQyxlQUFlLEVBQUUseUJBQXlCLENBQUM7SUFDdEQsT0FBTyxFQUFFLENBQUMsY0FBYyxFQUFFLG9CQUFvQixDQUFDO0lBQy9DLFNBQVMsRUFBRSxDQUFDLGdCQUFnQixFQUFFLDZDQUE2QyxDQUFDO0lBQzVFLEtBQUssRUFBRSxDQUFDLFlBQVksRUFBRSwyQ0FBMkMsQ0FBQztJQUNsRSxNQUFNLEVBQUUsQ0FBQyxhQUFhLEVBQUUsMkNBQTJDLENBQUM7SUFDcEUsS0FBSyxFQUFFLENBQUMsT0FBTyxFQUFFLHNCQUFzQixDQUFDO0lBQ3hDLFdBQVcsRUFBRSxDQUFDLGtCQUFrQixFQUFFLDZEQUE2RCxDQUFDO0lBQ2hHLGFBQWEsRUFBRSxDQUFDLG9CQUFvQixFQUFFLEVBQUUsQ0FBQztDQUMxQzs7Ozs7QUFFRCxTQUFTLGVBQWUsQ0FBQyxLQUFLOztRQUN4QixHQUFHLEdBQUcsRUFBRTtJQUNaLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3pCLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJO1FBQzFCLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN2RSxDQUFDLENBQUMsQ0FBQztJQUNILE9BQU8sR0FBRyxDQUFDO0FBQ2IsQ0FBQztBQUVELEtBQUssSUFBSSxDQUFDLElBQUksY0FBYyxFQUFFOztRQUN4QixDQUFDLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQztJQUN6QixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Q0FDeEQ7QUFFRCxjQUFjLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRztJQUM5QixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztJQUNwQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztBQUNwQixDQUFDLENBQUM7QUFFRixhQUFhLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRztJQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztJQUN2QixJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztJQUNoQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztBQUNyQixDQUFDLENBQUM7QUFFRixZQUFZLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRztJQUM1QixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztBQUNwQixDQUFDLENBQUM7QUFFRixnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHO0lBQ2hDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0lBQ2xCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQ25CLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0lBQ3RCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO0lBQ3ZCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO0lBQ2hCLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO0FBQ3ZCLENBQUMsQ0FBQztBQUVGLGVBQWUsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHO0lBQy9CLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO0FBQ3BCLENBQUMsQ0FBQztBQUVGLGNBQWMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHO0lBQzlCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3BCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO0lBQ2hCLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO0FBQ3BCLENBQUMsQ0FBQztBQUVGLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHO0lBQzNCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0FBQ3ZCLENBQUMsQ0FBQztBQUVGLGNBQWMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHO0lBQzlCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO0FBQ2xCLENBQUMsQ0FBQztBQUVGLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUc7SUFDbEMsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLGFBQWE7UUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM5RCxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNuQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUNwQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNuQixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztBQUNwQixDQUFDLENBQUM7QUFFRixvQkFBb0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHO0FBQ3RDLENBQUMsQ0FBQztBQUVGLGFBQWEsQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHLFVBQVUsTUFBTTtJQUM5QyxNQUFNLENBQUMsTUFBTSxZQUFZLGFBQWEsQ0FBQyxDQUFDO0lBQ3hDLElBQUksSUFBSSxDQUFDLGdCQUFnQixLQUFLLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRTtRQUNyRCxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2hELENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN4QyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ25DO0lBQ0QsT0FBTyxJQUFJLENBQUM7QUFDZCxDQUFDLENBQUM7QUFHRixhQUFhLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxVQUFVLEtBQUs7SUFDaEQsSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLFVBQVU7UUFDM0IsT0FBTztJQUNULElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxTQUFTLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxRQUFRLEVBQUU7O1lBQ25ELFFBQVEsR0FBRyxLQUFLLENBQUMsZUFBZSxJQUFJLEtBQUssQ0FBQyxTQUFTO1FBQ3ZELElBQUksUUFBUSxFQUFFO1lBQ1osSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7Z0JBQ2pCLFNBQVMsRUFBRSxLQUFLLENBQUMsVUFBVSxJQUFJLEtBQUssQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCO2dCQUM5RSxRQUFRLEVBQUUsUUFBUTthQUNuQixDQUFDLENBQUM7U0FDSjtLQUNGO1NBQ0ksSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLGFBQWEsRUFBRTtRQUNyQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUM7S0FDeEM7U0FDSSxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO1FBQ2pDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQztLQUNwQztTQUNJLElBQUksS0FBSyxDQUFDLEtBQUssRUFBRTtRQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUM7S0FDakM7SUFDRCxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDO0lBQ3BCLDRCQUE0QjtBQUM5QixDQUFDLENBQUM7O0FBRUYsWUFBWSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsVUFBVSxLQUFLO0lBQy9DLE1BQU0sQ0FBQyxLQUFLLFlBQVksYUFBYSxDQUFDLENBQUM7O1FBRW5DLGVBQWUsR0FBRyxLQUFLLENBQUMsZ0JBQWdCO0lBRTVDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsRUFBRTtRQUNqRCxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxHQUFHLEtBQUssQ0FBQztLQUN2QztTQUFNO1FBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsR0FBRyxlQUFlLEdBQUcscUNBQXFDLENBQUMsQ0FBQztLQUMvRjtBQUNILENBQUMsQ0FBQztBQUVGLFlBQVksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsS0FBSztJQUMvQyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssTUFBTSxFQUFFO1FBQ3pCLElBQUksQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztRQUN0QixJQUFJLElBQUksQ0FBQyxHQUFHLEtBQUssU0FBUyxFQUFFO1lBQzFCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDLGNBQWMsQ0FBQztTQUMzQztRQUNELElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7S0FDckI7QUFDSCxDQUFDLENBQUM7QUFFRixhQUFhLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxVQUFVLEtBQUs7SUFDaEQsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLE1BQU0sRUFBRTtRQUN6QixJQUFJLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUM7UUFDdEIsSUFBSSxJQUFJLENBQUMsR0FBRyxLQUFLLFNBQVMsRUFBRTtZQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQyxjQUFjLENBQUM7U0FDM0M7UUFDRCxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDO0tBQ3JCO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxVQUFVLEtBQUs7SUFDbkQsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLFdBQVcsRUFBRTtRQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxXQUFXLElBQUksRUFBRSxDQUFDO1FBQzFDLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sSUFBSSxFQUFFLENBQUM7UUFDaEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztLQUNyQjtBQUNILENBQUMsQ0FBQztBQUVGLGNBQWMsQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsS0FBSztJQUNqRCxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO1FBQzVCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQztRQUNsQyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDMUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztLQUNyQjtBQUNILENBQUMsQ0FBQztBQUVGLFdBQVcsQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsS0FBSztJQUM5QyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssU0FBUyxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssV0FBVyxFQUFFO1FBQ3hFLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQztLQUNqQztBQUNILENBQUMsQ0FBQztBQUVGLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsVUFBVSxLQUFLOztRQUNqRCxJQUFJLEdBQUcsSUFBSTtJQUNmLElBQUksS0FBSyxZQUFZLFlBQVksRUFBRTtRQUNqQywrQ0FBK0M7UUFDL0MsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztLQUN0QztTQUNJLElBQUksS0FBSyxZQUFZLGNBQWMsRUFBRTtRQUN4QyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUM7S0FDcEM7U0FDSSxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssUUFBUSxFQUFFO1FBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksYUFBYSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ2hEO1NBQ0ksSUFBSSxLQUFLLFlBQVksZUFBZSxFQUFFO1FBQ3pDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQztLQUNyQztTQUNJLElBQUksS0FBSyxZQUFZLGNBQWMsRUFBRTtRQUN4QyxJQUFJLEtBQUssQ0FBQyxTQUFTLEtBQUssc0NBQXNDO1lBQzVELEtBQUssQ0FBQyxTQUFTLEtBQUssK0NBQStDO1lBQ25FLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQztLQUN0QztTQUNJLElBQUksS0FBSyxZQUFZLGNBQWMsRUFBRTtRQUN4QyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUM7S0FDcEM7U0FDSSxJQUFJLEtBQUssWUFBWSxvQkFBb0IsRUFBRTtLQUMvQztJQUNELElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7QUFDdEIsQ0FBQyxDQUFDO0FBRUYsY0FBYyxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXOztRQUN0RCxJQUFJLEdBQUcsSUFBSTs7UUFDWCxLQUFLLEdBQUcsU0FBUzs7UUFDakIsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLElBQUksRUFBRTs7UUFDOUIsRUFBRSxHQUFHLFNBQVM7O1FBQ2QsTUFBTSxHQUFHLFNBQVM7O1FBQ2xCLENBQUMsR0FBRyxTQUFTOztRQUNiLElBQUksR0FBRyxTQUFTO0lBRXBCLEtBQUssQ0FBQyxJQUFJLFFBQVEsRUFBRTtRQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxNQUFNLEVBQUU7WUFDekMsSUFBSSxHQUFHLEtBQUssQ0FBQztZQUNiLE1BQU07U0FDUDtLQUNGO0lBRUQsSUFBSSxDQUFDLElBQUksRUFBRTtRQUNULE9BQU87S0FDUjtJQUVELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTs7WUFDYixXQUFXLEdBQUcsRUFBRTs7WUFDbEIsZUFBZTtRQUVqQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7UUFFbEIsTUFBTSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbkMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7O1lBQ2YsTUFBTSxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2pCLHFGQUFxRjtZQUNyRixPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUVyRCx5RUFBeUU7UUFDekUsc0NBQXNDO1FBQ3RDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFFekMsZUFBZSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1FBRXhDLGdFQUFnRTtRQUNoRSxJQUFJLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzlCLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsZUFBZSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDM0MsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN2RTtTQUNGO1FBRUQsb0VBQW9FO1FBQ3BFLElBQUksV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDMUIsV0FBVyxHQUFHLFdBQVc7Z0JBQ3ZCLElBQUksQ0FBQyxHQUFHLENBQUM7Z0JBQ1QsS0FBSyxDQUFDLEdBQUcsQ0FBQztnQkFDVixNQUFNLENBQUMsU0FBUyxzQkFBc0IsQ0FBQyxJQUFJO2dCQUN6QyxPQUFPLElBQUksS0FBSyxHQUFHLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7O2dCQUVELFdBQVcsR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSztZQUV6RSxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3ZDLFdBQVcsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLFdBQVcsQ0FBQyxDQUFDO2FBQzVFO1NBQ0Y7UUFFRCxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksR0FBRyxXQUFXLENBQUM7UUFFeEMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRTtZQUN0QixJQUFJLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7O2dCQUNsQyxNQUFNLEdBQUcsTUFBTSxDQUFDLEtBQUssSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7WUFFeEYsSUFBSSxNQUFNLEVBQUU7Z0JBQ1YsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLFVBQVUsRUFBRTtvQkFDM0IscUNBQXFDO2lCQUN0QztxQkFDSTtvQkFDSCxxREFBcUQ7b0JBQ3JELE1BQU0sR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDOzt3QkFDakMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztvQkFHbkcsSUFBSSxLQUFLLEVBQUU7d0JBQ1QsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQzNEO2lCQUNGO2FBQ0Y7U0FDRjthQUNJOztnQkFDQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUM7WUFDaEUsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2xDO1FBR0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0tBQzVCO1NBQU07UUFDTCxlQUFlO1FBQ2YsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDaEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3BCLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4QyxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssZUFBZSxFQUFFO2dCQUNqQywwREFBMEQ7Z0JBQzFELFNBQVM7YUFDVjtZQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLE1BQU0sRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO1lBQ3RELE1BQU0sR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLEVBQUUsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN0QyxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQzs7Z0JBQ2YsZ0JBQWdCLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7WUFDOUMsSUFBSSxPQUFPLGdCQUFnQixLQUFLLFdBQVcsRUFBRTtnQkFDM0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDNUc7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQzthQUNyQztZQUVELElBQUksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxRQUFRLEVBQUU7Z0JBQzlDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO2dCQUM5QyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2FBQ25DO1lBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDOUI7S0FDRjtJQUNELElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0FBQzFCLENBQUMsQ0FBQzs7Ozs7Ozs7Ozs7O0FBYUYsY0FBYyxDQUFDLFNBQVMsQ0FBQyx1QkFBdUIsR0FBRyxVQUFVLFFBQVEsRUFBRSxLQUFLOztRQUN0RSxnQkFBZ0IsR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFDOztRQUN6QyxPQUFPLEdBQUcsZ0JBQWdCLENBQUMsTUFBTTs7UUFDakMsWUFBWSxHQUFHLGdCQUFnQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOztRQUMvQyxJQUFJLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQzs7UUFDdEIsSUFBSSxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUM7O1FBQ3RCLGFBQWEsR0FBUSxFQUFFO0lBRXpCLGFBQWEsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsT0FBTyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUM7SUFDM0MsYUFBYSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7SUFFM0IsT0FBTyxhQUFhLENBQUM7QUFDdkIsQ0FBQyxDQUFDOzs7Ozs7Ozs7OztBQVlGLGNBQWMsQ0FBQyxTQUFTLENBQUMsMEJBQTBCLEdBQUcsVUFBVSxPQUFPOztRQUNqRSxZQUFZLEdBQUcsR0FBRzs7UUFDcEIsUUFBUSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBRWhELElBQUksT0FBTyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxPQUFPLE9BQU8sQ0FBQyxLQUFLLEtBQUssUUFBUSxFQUFFO1FBQ3hFLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ3hELFlBQVksSUFBSSxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUMsS0FBSyxHQUFHLEdBQUcsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDN0Q7S0FDRjtJQUVELElBQUksT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztZQUMzQixJQUFJLEdBQUcsSUFBSTtRQUVmLE9BQU8sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQVUsS0FBSzs7Z0JBQ2xDLGlCQUFpQixHQUFHLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQztZQUVqRixJQUFJLGlCQUFpQixJQUFJLE9BQU8saUJBQWlCLEtBQUssUUFBUSxFQUFFO2dCQUM5RCxZQUFZLElBQUksQ0FBQyxHQUFHLEdBQUcsaUJBQWlCLENBQUMsQ0FBQzthQUMzQztRQUNILENBQUMsQ0FBQyxDQUFDO0tBQ0o7SUFFRCxPQUFPLFlBQVksQ0FBQztBQUN0QixDQUFDLENBQUM7QUFFRixnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsV0FBVyxFQUFFLEdBQUc7O1FBQzdELFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUTtJQUM1QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUMvQyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssT0FBTyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssUUFBUTtZQUNuRCxTQUFTO1FBQ1gsSUFBSSxHQUFHLEtBQUssU0FBUyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQ3pCLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDeEIsU0FBUztTQUNWOztZQUNHLFdBQVcsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUk7O1lBQzdDLE9BQU8sR0FBRyxXQUFXLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztRQUMvQyxPQUFPLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2pDLElBQUksT0FBTyxDQUFDLE9BQU8sRUFBRTtZQUNuQixXQUFXLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsT0FBTyxDQUFDO1lBQ3RELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQztTQUNwQzthQUNJO1lBQ0gsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUM7U0FDNUI7UUFDRCxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO0tBQ3pCO0lBQ0QsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7QUFDMUIsQ0FBQyxDQUFDO0FBRUYsZUFBZSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXOztRQUN2RCxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVE7SUFDNUIsSUFBSSxPQUFPLFFBQVEsS0FBSyxXQUFXO1FBQ2pDLE9BQU87SUFDVCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUMvQyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssV0FBVztZQUM1QixTQUFTO1FBQ1gsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBQ2xDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7S0FDekI7SUFDRCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDbEIsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7QUFDMUIsQ0FBQyxDQUFDO0FBRUYsY0FBYyxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXOztRQUN0RCxJQUFJLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJOztRQUNwQyxRQUFRLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7O1FBQ3RDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSzs7UUFDbEIsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRO0lBQzFCLElBQUksUUFBUSxFQUFFO1FBQ1osUUFBUSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7UUFFaEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDL0MsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLFdBQVc7Z0JBQzVCLFNBQVM7WUFDWCxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUMsQ0FBQztZQUMxQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLEtBQUssQ0FBQyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDOztnQkFDakMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUV0QyxJQUFJLE1BQU0sRUFBRTtnQkFDVixNQUFNLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7Z0JBQzNCLE1BQU0sQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQztnQkFDckMsTUFBTSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQztnQkFDdkMsTUFBTSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQztnQkFDekMsTUFBTSxDQUFDLFNBQVMsSUFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQ3hELE1BQU0sQ0FBQyxVQUFVLElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2FBQzNEO1NBQ0Y7S0FDRjtJQUNELE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNsQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDbEIsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7QUFDMUIsQ0FBQyxDQUFDO0FBRUYsY0FBYyxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXOztRQUN0RCxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVE7O1FBQzFCLFFBQVEsR0FBRyxXQUFXLENBQUMsUUFBUTtJQUNqQyxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtRQUNuQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMvQyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssTUFBTTtnQkFDdkIsU0FBUzs7Z0JBQ1AsV0FBVyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSTs7Z0JBQzdDLE9BQU8sR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDO1lBQ25DLElBQUksT0FBTyxFQUFFO2dCQUNYLE9BQU8sQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2pDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHO29CQUN4QixRQUFRLEVBQUUsS0FBSyxDQUFDLFFBQVE7b0JBQ3hCLE9BQU8sRUFBRSxPQUFPO2lCQUNqQixDQUFDO2dCQUNGLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDekI7U0FDRjtLQUNGO0lBQ0QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ2xCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0FBQzFCLENBQUMsQ0FBQztBQUdGLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXOztRQUN6RCxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVE7SUFDNUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7UUFDL0MsSUFBSSxLQUFLLFlBQVksa0JBQWtCO1lBQ3JDLE9BQU8sSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO0tBQ2pEO0lBQ0QsT0FBTyxFQUFFLENBQUM7QUFDWixDQUFDLENBQUM7QUFFRixrQkFBa0IsQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsV0FBVyxFQUFFLEtBQUs7O1FBQ2pFLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUTs7UUFDeEIsSUFBSTtJQUNSLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1FBQy9DLElBQUksS0FBSyxZQUFZLGVBQWU7WUFDbEMsS0FBSyxZQUFZLGFBQWEsRUFBRTtZQUNoQyxJQUFJLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDN0MsTUFBTTtTQUNQO0tBQ0Y7SUFDRCxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFOztZQUNsQixJQUFJLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7O1lBQy9CLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSTs7WUFDcEIsRUFBRSxHQUFHLEtBQUssSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQzs7WUFDbEUsTUFBTSxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDOztZQUNoQyxXQUFXLEdBQUcsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFaEgsSUFBSSxDQUFDLE9BQU8sR0FBRztZQUNiLE9BQU8sV0FBVyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzVELENBQUMsQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDO0tBQ2I7OztRQUdHLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRTtJQUM3QyxPQUFPLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEtBQUs7UUFDN0MsT0FBTyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDN0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ2YsQ0FBQyxDQUFDO0FBRUYsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxVQUFVLFdBQVcsRUFBRSxLQUFLOztRQUMvRCxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVE7O1FBQ3hCLElBQUksR0FBRyxFQUFFO0lBQ2IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7UUFDL0MsSUFBSSxLQUFLLFlBQVksZUFBZTtZQUNsQyxLQUFLLFlBQVksYUFBYSxFQUFFO1lBQ2hDLElBQUksR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUM5QztLQUNGO0lBQ0QsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFOztZQUNWLElBQUksR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQzs7WUFDL0IsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJOztZQUNwQixFQUFFLEdBQUcsS0FBSyxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDOztZQUNsRSxNQUFNLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7UUFFbEMsSUFBSSxRQUFRLElBQUksVUFBVSxFQUFFO1lBQzFCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztTQUNuQjthQUNJOztnQkFDQyxXQUFXLEdBQUcsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUM7Z0JBQ3hELE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUV0RCxJQUFJLFdBQVcsRUFBRTs7b0JBQ1gsSUFBSSxHQUFHLFdBQVcsQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUM7Z0JBQzdELElBQUksR0FBRyxDQUFDLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQzthQUNuQztTQUNGO0tBQ0Y7SUFDRCxPQUFPLElBQUksQ0FBQztBQUNkLENBQUMsQ0FBQztBQUVGLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUc7SUFDekMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQzdCLENBQUMsQ0FBQztBQUVGLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXLEVBQUUsS0FBSzs7UUFDakUsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLElBQUksRUFBRTtJQUNsQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUMvQyxJQUFJLEtBQUssWUFBWSxhQUFhO1lBQ2hDLEtBQUssWUFBWSxlQUFlO1lBQ2hDLEtBQUssWUFBWSxVQUFVO1lBQzNCLEtBQUssWUFBWSxvQkFBb0I7WUFDckMsS0FBSyxZQUFZLHFCQUFxQixFQUFFO1lBRXhDLE9BQU8sS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDOUM7S0FDRjtJQUNELE9BQU8sRUFBRSxDQUFDO0FBQ1osQ0FBQyxDQUFDO0FBRUYscUJBQXFCLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxVQUFVLFdBQVcsRUFBRSxLQUFLOztRQUNwRSxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVE7SUFDNUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7UUFDL0MsSUFBSSxLQUFLLFlBQVksZ0JBQWdCLEVBQUU7WUFDckMsT0FBTyxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUM5QztLQUNGO0lBQ0QsT0FBTyxFQUFFLENBQUM7QUFDWixDQUFDLENBQUM7QUFFRixvQkFBb0IsQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsV0FBVyxFQUFFLEtBQUs7O1FBQ25FLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUTtJQUM1QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUMvQyxJQUFJLEtBQUssWUFBWSxnQkFBZ0IsRUFBRTtZQUNyQyxPQUFPLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQzlDO0tBQ0Y7SUFDRCxPQUFPLEVBQUUsQ0FBQztBQUNaLENBQUMsQ0FBQztBQUVGLGNBQWMsQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsV0FBVyxFQUFFLEtBQUs7O1FBQzdELE9BQU8sR0FBRyxFQUFFOztRQUNkLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSzs7UUFDZixNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDNUgsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLElBQUksQ0FBQyxVQUFVLElBQUksTUFBTSxFQUFFO1FBQ2pELElBQUksSUFBSSxJQUFJLENBQUM7S0FDZDtJQUVELElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxVQUFVLENBQUMsRUFBRTtRQUM5QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0tBQzNDOztRQUNHLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxJQUFJO0lBQ2xDLElBQUksSUFBSSxFQUFFO1FBQ1IsSUFBSSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQzs7WUFDcEIsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJOztZQUN0QixFQUFFLEdBQUcsS0FBSyxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDOztZQUNsRSxNQUFNLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7O1lBQ2hDLFdBQVcsR0FBRyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFNUgsSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUNqQyxLQUFLLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUM7U0FDdkM7UUFFRCxJQUFJLFdBQVcsSUFBSSxDQUFDLENBQUMsUUFBUSxJQUFJLFVBQVUsQ0FBQyxFQUFFO1lBRTVDLElBQUksQ0FBQyxDQUFDLFFBQVEsSUFBSSxXQUFXLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFFOztvQkFFN0MsSUFBSSxHQUFRLEVBQUU7Z0JBQ2xCLFdBQVcsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQzs7b0JBQzVDLFdBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUM7Z0JBQzdELElBQUksT0FBTyxXQUFXLEtBQUssUUFBUSxFQUFFO29CQUNuQyxJQUFJLEdBQUcsV0FBVyxDQUFDO2lCQUNwQjtxQkFDSTtvQkFDSCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUc7d0JBQzVDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQy9CLENBQUMsQ0FBQyxDQUFDO2lCQUNKO2dCQUVELElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDYixPQUFPLEdBQUcsSUFBSSxDQUFDO2lCQUNoQjtxQkFDSTtvQkFDSCxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDO2lCQUN0QjtnQkFFRCxJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsRUFBRTtvQkFDNUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO29CQUNqQyxJQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQztpQkFDM0I7Z0JBRUQsV0FBVyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDO2FBQ2pEO2lCQUNJO2dCQUNILElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDYixPQUFPLEdBQUcsV0FBVyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ3BEO3FCQUNJO29CQUNILE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDMUQ7YUFDRjtTQUVGO2FBQ0k7WUFDSCxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztTQUM1QjtLQUNGO1NBQ0k7O1lBQ0MsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRO1FBQzVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDbkIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDL0MsSUFBSSxLQUFLLFlBQVksa0JBQWtCLEVBQUU7Z0JBQ3ZDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQzthQUN2RDtTQUNGO0tBQ0Y7SUFDRCxPQUFPLE9BQU8sQ0FBQztBQUNqQixDQUFDLENBQUM7QUFFRixVQUFVLENBQUMsU0FBUyxDQUFDLFdBQVc7SUFDOUIsZUFBZSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXLEVBQUUsS0FBSzs7WUFDOUQsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFROztZQUN4QixRQUFRLEdBQUcsRUFBRTtRQUNqQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMvQyxJQUFJLEtBQUssWUFBWSxVQUFVLEVBQUU7Z0JBQy9CLFNBQVM7YUFDVjs7Z0JBQ0csV0FBVyxHQUFHLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQztZQUN2RCxLQUFLLElBQUksR0FBRyxJQUFJLFdBQVcsRUFBRTtnQkFDM0IsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNsQztTQUNGO1FBQ0QsT0FBTyxRQUFRLENBQUM7SUFDbEIsQ0FBQyxDQUFDO0FBRUosYUFBYSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXLEVBQUUsS0FBSzs7UUFDNUQsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFROztRQUN4QixNQUFNLEdBQUcsRUFBRTtJQUNmLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFOztZQUMzQyxXQUFXLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDO1FBQ3ZELEtBQUssSUFBSSxHQUFHLElBQUksV0FBVyxFQUFFO1lBQzNCLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDaEM7S0FDRjtJQUNELE9BQU8sTUFBTSxDQUFDO0FBQ2hCLENBQUMsQ0FBQztBQUVGLGNBQWMsQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsV0FBVztJQUMxRCxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7UUFDaEIsT0FBTyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0tBQzlEOztRQUNHLElBQUksR0FBRyxFQUFFO0lBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQzlCLE9BQU8sSUFBSSxDQUFDO0FBQ2QsQ0FBQyxDQUFDO0FBRUYsZUFBZSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXOztRQUN2RCxPQUFPLEdBQUcsRUFBRTtJQUNoQixLQUFLLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7O1lBQ3pCLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztRQUMvQixPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztLQUNqRDtJQUNELE9BQU8sT0FBTyxDQUFDO0FBQ2pCLENBQUMsQ0FBQztBQUVGLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXOztRQUN4RCxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7O1FBQ25FLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtJQUMxRSxPQUFPO1FBQ0wsS0FBSyxFQUFFLFNBQVMsSUFBSSxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4RCxNQUFNLEVBQUUsVUFBVSxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQzdELENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixjQUFjLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxVQUFVLFdBQVc7O1FBQ3RELE9BQU8sR0FBRyxFQUFFO0lBQ2hCLEtBQUssSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTs7WUFDekIsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1FBQy9CLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0tBQ2pEO0lBQ0QsT0FBTyxPQUFPLENBQUM7QUFDakIsQ0FBQyxDQUFDO0FBRUYsY0FBYyxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXOztRQUN0RCxLQUFLLEdBQUcsRUFBRTtJQUNkLEtBQUssSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTs7WUFDdkIsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO1FBQzNCLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztLQUNyRDtJQUNELE9BQU8sS0FBSyxDQUFDO0FBQ2YsQ0FBQyxDQUFDOztBQUVGLE1BQU0sS0FBSyxJQUFJLEdBQUcsVUFBVSxVQUFVLEVBQUUsR0FBRyxFQUFFLE9BQU87O1FBQzlDLElBQUksR0FBRyxJQUFJOztRQUNiLFFBQVE7SUFFVixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztJQUNmLElBQUksQ0FBQyxRQUFRLEdBQUc7SUFDaEIsQ0FBQyxDQUFDO0lBQ0YsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7SUFFeEIsd0JBQXdCO0lBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDLENBQUMsVUFBVSxJQUFJLEVBQUUsQ0FBQztJQUVuRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLENBQUM7SUFFakMsSUFBSSxPQUFPLFVBQVUsS0FBSyxRQUFRLEVBQUU7UUFDbEMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNsQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztLQUMxQjtTQUNJLElBQUksT0FBTyxVQUFVLEtBQUssUUFBUSxFQUFFO1FBQ3ZDLFFBQVEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO0tBQy9CO1NBQ0k7UUFDSCxNQUFNLElBQUksS0FBSyxDQUFDLGlFQUFpRSxDQUFDLENBQUM7S0FDcEY7SUFFRCxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7UUFDOUIsSUFBSTtZQUNGLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1NBQ2pDO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDVixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ2pDO1FBRUQsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDOztnQkFDaEMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRO1lBQ3hELElBQUksUUFBUSxFQUFFO2dCQUNaLEtBQUssTUFBTSxJQUFJLElBQUksUUFBUSxFQUFFO29CQUMzQixRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztpQkFDOUM7YUFDRjs7Z0JBQ0csWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWTtZQUNoRCxJQUFJLFlBQVksRUFBRTtnQkFDaEIsS0FBSyxNQUFNLElBQUksSUFBSSxZQUFZLEVBQUU7b0JBQy9CLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2lCQUN2QzthQUNGOzs7Z0JBR0csUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUTtZQUN4QyxLQUFLLElBQUksV0FBVyxJQUFJLFFBQVEsRUFBRTs7b0JBQzVCLE9BQU8sR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDO2dCQUNuQyxJQUFJLE9BQU8sT0FBTyxDQUFDLEtBQUssS0FBSyxXQUFXLEVBQUU7b0JBQ3hDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDO2lCQUM1QjtnQkFDRCxJQUFJLE9BQU8sQ0FBQyxLQUFLLEtBQUssVUFBVTtvQkFDOUIsU0FBUzs7b0JBQ1AsT0FBTyxHQUFHLE9BQU8sQ0FBQyxPQUFPOztvQkFDekIsTUFBTSxHQUFHLE9BQU8sQ0FBQyxXQUFXLEdBQUcsRUFBRTtnQkFDckMsS0FBSyxJQUFJLFVBQVUsSUFBSSxPQUFPLEVBQUU7b0JBQzlCLElBQUksT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssRUFBRTs7NEJBQ3pCLFNBQVMsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUs7OzRCQUMzQyxVQUFVLEdBQUcsRUFBRTt3QkFDbkIsSUFBSSxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTTs0QkFDNUIsVUFBVSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO3dCQUNoRCxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxVQUFVLEVBQUUsQ0FBQztxQkFDNUU7aUJBQ0Y7YUFDRjtZQUVELGdEQUFnRDtZQUNoRCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUN4QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFFdEMsQ0FBQyxDQUFDLENBQUM7SUFFSCxnQ0FBZ0M7SUFDaEMsVUFBVTtJQUNWLHVDQUF1QztJQUN2QyxrQkFBa0I7SUFDbEIsdUNBQXVDO0lBQ3ZDLE1BQU07SUFFTix5Q0FBeUM7SUFDekMsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixtQ0FBbUM7SUFDbkMsUUFBUTtJQUVSLDJDQUEyQztJQUMzQyxnRUFBZ0U7SUFDaEUsc0JBQXNCO0lBQ3RCLGlDQUFpQztJQUNqQyx3REFBd0Q7SUFDeEQsVUFBVTtJQUNWLFFBQVE7SUFDUix3REFBd0Q7SUFDeEQsMEJBQTBCO0lBQzFCLHFDQUFxQztJQUNyQyxpREFBaUQ7SUFDakQsVUFBVTtJQUNWLFFBQVE7SUFFUix3SUFBd0k7SUFDeEksZ0RBQWdEO0lBQ2hELDBDQUEwQztJQUMxQyw2Q0FBNkM7SUFDN0Msb0RBQW9EO0lBQ3BELHNDQUFzQztJQUN0QyxVQUFVO0lBQ1YsMENBQTBDO0lBQzFDLG9CQUFvQjtJQUNwQix1Q0FBdUM7SUFDdkMsK0NBQStDO0lBQy9DLDBDQUEwQztJQUMxQywyQ0FBMkM7SUFDM0MsNkRBQTZEO0lBQzdELCtCQUErQjtJQUMvQiw0Q0FBNEM7SUFDNUMsNkRBQTZEO0lBQzdELHNGQUFzRjtJQUN0RixZQUFZO0lBQ1osVUFBVTtJQUNWLFFBQVE7SUFFUix1REFBdUQ7SUFDdkQsK0NBQStDO0lBRS9DLGdDQUFnQztJQUNoQyxRQUFRO0lBRVIsTUFBTTtBQUNSLENBQUM7QUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFpQixHQUFHLENBQUMsS0FBSyxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDLENBQUM7QUFFaEYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsR0FBRyxLQUFLLENBQUM7QUFFNUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO0FBQ25DLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztBQUUvQixJQUFJLENBQUMsU0FBUyxDQUFDLGtCQUFrQixHQUFHLFVBQVUsT0FBTztJQUNuRCxJQUFJLENBQUMsMEJBQTBCLEdBQUcsQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDLENBQUMsaUJBQWlCLENBQUM7SUFDcEUsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7O1FBRWQsaUJBQWlCLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLElBQUk7SUFFbEUsSUFBSSxpQkFBaUI7UUFDbkIsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxJQUFJLE9BQU8saUJBQWlCLENBQUMsVUFBVSxLQUFLLFFBQVEsQ0FBQyxFQUFFO1FBQ25HLElBQUksaUJBQWlCLENBQUMsUUFBUSxFQUFFO1lBQzlCLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEdBQUcsaUJBQWlCLENBQUMsVUFBVSxDQUFDO1NBQy9EO2FBQU07WUFDTCxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDOUY7S0FDRjtTQUFNO1FBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7S0FDekQ7SUFFRCxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDMUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3BELElBQUksT0FBTyxDQUFDLFNBQVMsS0FBSyxTQUFTLEVBQUU7UUFDbkMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQztLQUM1QztTQUFNO1FBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0tBQy9CO0lBQ0QsSUFBSSxPQUFPLENBQUMsV0FBVyxLQUFLLFNBQVMsRUFBRTtRQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDO0tBQ2hEO1NBQU07UUFDTCxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7S0FDbEM7SUFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQztJQUV6RCxJQUFJLE9BQU8sQ0FBQyxzQkFBc0IsS0FBSyxTQUFTLEVBQUU7UUFDaEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsR0FBRyxPQUFPLENBQUMsc0JBQXNCLENBQUM7S0FDdEU7U0FBTTtRQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDO0tBQzVDO0lBRUQsb0RBQW9EO0lBQ3BELElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUM7SUFDakQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQztJQUNqRCxJQUFJLE9BQU8sQ0FBQyxVQUFVLEVBQUU7UUFDdEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQztLQUM5QztJQUVELHVEQUF1RDtJQUN2RCxJQUFJLE9BQU8sQ0FBQyxPQUFPLEVBQUU7UUFDbkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQztLQUN4Qzs7UUFFRyxvQkFBb0IsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsSUFBSTtJQUN4RSxJQUFJLG9CQUFvQixLQUFLLElBQUksSUFBSSxPQUFPLG9CQUFvQixLQUFLLFdBQVcsRUFBRTtRQUNoRixJQUFJLENBQUMsT0FBTyxDQUFDLG9CQUFvQixHQUFHLG9CQUFvQixDQUFDO0tBQzFEO1NBQU07UUFDTCxJQUFJLENBQUMsT0FBTyxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQztLQUMvRDtJQUVELHVCQUF1QjtJQUN2QixJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixHQUFHLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQztJQUM3RCxJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixHQUFHLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQztJQUU3RCxJQUFJLE9BQU8sQ0FBQyxtQkFBbUIsS0FBSyxTQUFTLEVBQUU7UUFDN0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsR0FBRyxPQUFPLENBQUMsbUJBQW1CLENBQUM7S0FDaEU7SUFFRCxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztBQUNuRCxDQUFDLENBQUM7QUFFRixJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sR0FBRyxVQUFVLFFBQVE7SUFDekMsSUFBSSxRQUFRO1FBQ1YsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7QUFDN0IsQ0FBQyxDQUFDO0FBRUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsR0FBRyxVQUFnQixRQUFROzs7WUFDdkQsSUFBSSxHQUFHLElBQUk7O1lBQ2IsT0FBTyxHQUFHLFFBQVEsQ0FBQyxLQUFLLEVBQUU7O1lBQzFCLE9BQU87UUFFVCxJQUFJLENBQUMsT0FBTztZQUNWLE9BQU8sQ0FBQyxjQUFjOzs7WUFFcEIsV0FBVztRQUNmLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3BFLHdFQUF3RTtTQUN6RTthQUFNO1lBQ0wsV0FBVyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxFQUFFLEVBQUUsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQzdEO1FBRUQsT0FBTyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNyQywyQ0FBMkM7UUFDM0MsT0FBTyxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQywwQkFBMEIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDO1FBQzlGLE9BQU8sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQzs7Y0FFL0IsSUFBSSxHQUFHLE1BQU0sbUJBQW1CLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQztRQUM1RCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUU5QixJQUFJLElBQUksQ0FBQyxXQUFXLFlBQVksa0JBQWtCLEVBQUU7WUFDbEQsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsVUFBVSxDQUFDLEVBQUUsQ0FBQztnQkFDNUQsT0FBTyxDQUFDLENBQUMsWUFBWSxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO1lBQy9ELENBQUMsQ0FBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7U0FDbE07UUFFRCxPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUUxQyxrRUFBa0U7UUFDbEUsZUFBZTtRQUNmLDRCQUE0QjtRQUM1QixNQUFNO1FBRU4sbUNBQW1DO1FBRW5DLDBEQUEwRDtRQUMxRCxzRUFBc0U7UUFDdEUsc0VBQXNFO1FBQ3RFLFVBQVU7UUFDVixhQUFhO1FBQ2Isd01BQXdNO1FBQ3hNLE1BQU07UUFDTix1REFBdUQ7UUFDdkQscUJBQXFCO1FBQ3JCLFFBQVE7UUFDUixNQUFNO0lBQ1IsQ0FBQztDQUFBLENBQUM7QUFFRixJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsR0FBRzs7O1lBQzNCLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU87O1lBQ3BDLFFBQVEsR0FBRyxFQUFFO1FBRWYsS0FBSyxJQUFJLEVBQUUsSUFBSSxPQUFPLEVBQUU7O2dCQUNsQixNQUFNLEdBQUcsT0FBTyxDQUFDLEVBQUUsQ0FBQztZQUN4QixRQUFRLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1NBQ25EO1FBRUQsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDNUMsQ0FBQztDQUFBLENBQUM7QUFFRixJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixHQUFHOztRQUM1QixRQUFRLEdBQUcsRUFBRTtJQUNqQixLQUFLLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7O1lBQzFCLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztRQUNqQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7S0FDeEQ7SUFDRCxPQUFPLFFBQVEsQ0FBQztBQUNsQixDQUFDLENBQUM7QUFFRixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRztJQUNyQixPQUFPLElBQUksQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDO0FBQ3hCLENBQUMsQ0FBQztBQUVGLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsR0FBRyxFQUFFLFFBQVE7O1FBQzlDLElBQUksR0FBRyxJQUFJOztRQUNYLENBQUMsR0FBRyxPQUFPLFFBQVEsS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7O1FBQzFELFVBQVUsR0FBRyxJQUFJOztRQUNqQixJQUFJLEdBQVEsRUFBRTs7UUFDZCxNQUFNLEdBQUc7UUFDWCxRQUFRLEVBQUU7WUFDUixNQUFNLEVBQUU7Z0JBQ04sUUFBUSxFQUFFO29CQUNSLGFBQWEsRUFBRTt3QkFDYixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsUUFBUSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNGO2FBQ0Y7WUFDRCxJQUFJLEVBQUU7Z0JBQ0osS0FBSyxFQUFFO29CQUNMLFNBQVMsRUFBRSxRQUFRO29CQUNuQixXQUFXLEVBQUUsUUFBUTtvQkFDckIsTUFBTSxFQUFFLFFBQVE7aUJBQ2pCO2FBQ0Y7U0FDRjtLQUNGOztRQUNHLEtBQUssR0FBVSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQzs7UUFDN0QsS0FBSyxHQUFRLEVBQUU7O1FBRWYsSUFBSSxHQUFHLEVBQUU7O1FBQUUsRUFBRTtJQUVqQixDQUFDLENBQUMsU0FBUyxHQUFHLFVBQVUsSUFBSTs7WUFDdEIsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJOztZQUNsQixLQUFLLEdBQVEsSUFBSSxDQUFDLFVBQVU7O1lBQzVCLElBQUksR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSTs7WUFDaEMsYUFBYTs7WUFDYixHQUFHLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDOztZQUM3QixTQUFTLEdBQUcsR0FBRyxDQUFDLE1BQU07O1lBQ3RCLGlCQUFpQixHQUFHLEVBQUU7O1lBQ3RCLG9CQUFvQixHQUFHLEtBQUs7O1lBQzVCLGVBQWUsR0FBRyxLQUFLOztZQUN2QixHQUFHLEdBQUcsRUFBRTs7WUFDTixZQUFZLEdBQUcsSUFBSTtRQUV2QixJQUFJLENBQUMsVUFBVSxJQUFJLEdBQUcsQ0FBQyxJQUFJLEtBQUssTUFBTSxJQUFJLElBQUksS0FBSyxPQUFPLEVBQUU7O2dCQUN0RCxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBQzdDLDhFQUE4RTtZQUM5RSx5RUFBeUU7WUFDekUsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDWixJQUFJOzs7d0JBRUUsT0FBTyxHQUFHLEtBQUs7O3dCQUNmLFFBQVEsR0FBRyxLQUFLO29CQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUM1QixRQUFRLEdBQUcsSUFBSSxDQUFDO3dCQUNoQixJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQUM7cUJBQ3RDO3lCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7d0JBQ2xDLE9BQU8sR0FBRyxJQUFJLENBQUM7d0JBQ2YsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDO3FCQUNyQzt5QkFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUNsQyxPQUFPLEdBQUcsSUFBSSxDQUFDO3dCQUNmLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQztxQkFDckM7Ozt3QkFFRyxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTOzt3QkFDdEMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDOzs7d0JBRXRDLFFBQVEsR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMxQyxJQUFJLE9BQU8sRUFBRTt3QkFDWCxJQUFJLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO3FCQUMzQzt5QkFBTTt3QkFDTCxJQUFJLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO3FCQUM1QztvQkFDRCxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzFDLDZDQUE2QztvQkFDN0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQzNFO2dCQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNWLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUU7d0JBQzVCLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ2Q7aUJBQ0Y7YUFDRjtZQUVELFNBQVMsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNsRCxVQUFVLEdBQUcsWUFBWSxDQUFDO1NBQzNCO1FBRUQsSUFBSSxLQUFLLENBQUMsSUFBSSxFQUFFO1lBQ2QsRUFBRSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUM7YUFDckM7WUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7U0FDL0Q7UUFDRCxJQUFJLEVBQUUsR0FBRyxLQUFLLENBQUMsRUFBRSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUM7YUFDckM7U0FDRjtRQUVELDJCQUEyQjtRQUMzQixLQUFLLGFBQWEsSUFBSSxLQUFLLEVBQUU7WUFDM0IsSUFBSSxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUU7Z0JBQ3pDLEtBQUssQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUM3RCxTQUFTO2FBQ1Y7WUFDRCxvQkFBb0IsR0FBRyxJQUFJLENBQUM7WUFDNUIsaUJBQWlCLENBQUMsYUFBYSxDQUFDLEdBQUcsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQ3pEO1FBRUQsS0FBSyxhQUFhLElBQUksaUJBQWlCLEVBQUU7O2dCQUNuQyxHQUFHLEdBQUcsVUFBVSxDQUFDLGFBQWEsQ0FBQztZQUNuQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLEtBQUssS0FBSyxJQUFJLEtBQUssQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssMkNBQTJDLElBQUksaUJBQWlCLENBQUMsYUFBYSxDQUFDO2dCQUM3SCxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsRUFBRSxLQUFLLE1BQU0sSUFBSSxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLENBQUMsRUFDdkc7Z0JBQ0EsZUFBZSxHQUFHLElBQUksQ0FBQztnQkFDdkIsTUFBTTthQUNQO1NBQ0Y7UUFFRCxJQUFJLG9CQUFvQixFQUFFO1lBQ3hCLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLGlCQUFpQixDQUFDO1NBQ3JEOzs7WUFHRyxhQUFhOztZQUNiLE9BQU8sR0FBRyxpQkFBaUIsQ0FBQyxVQUFVLENBQUM7UUFDM0MsSUFBSSxPQUFPLEVBQUU7O2dCQUNQLElBQUksR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFDOztnQkFDMUIsT0FBTztZQUNYLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxVQUFVLEVBQUU7Z0JBQzlCLGlDQUFpQztnQkFDakMsT0FBTyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQzthQUM3QztpQkFBTTtnQkFDTCxPQUFPLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUM5Qjs7Z0JBQ0csT0FBTyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQztZQUN2RCxJQUFJLE9BQU8sRUFBRTtnQkFDWCxhQUFhLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDdkQ7U0FDRjtRQUVELElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUU7WUFDdkMsSUFBSSxHQUFHLElBQUksR0FBRyxJQUFJLENBQUM7U0FDcEI7UUFDRCxLQUFLLENBQUMsSUFBSSxDQUFDO1lBQ1QsSUFBSSxFQUFFLFlBQVk7WUFDbEIsTUFBTSxFQUFFLEdBQUc7WUFDWCxNQUFNLEVBQUUsQ0FBQyxhQUFhLElBQUksQ0FBQyxTQUFTLElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDekQsRUFBRSxFQUFFLEtBQUssQ0FBQyxFQUFFO1lBQ1osR0FBRyxFQUFFLGVBQWU7U0FDckIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQyxDQUFDO0lBRUYsQ0FBQyxDQUFDLFVBQVUsR0FBRyxVQUFVLE1BQU07O1lBQ3pCLEdBQUcsR0FBUSxLQUFLLENBQUMsR0FBRyxFQUFFOztZQUN4QixHQUFHLEdBQUcsR0FBRyxDQUFDLE1BQU07O1lBQ2hCLEdBQUcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7O1lBQzdCLFNBQVMsR0FBRyxHQUFHLENBQUMsTUFBTTs7WUFDdEIsU0FBUyxHQUFHLEdBQUcsQ0FBQyxNQUFNOztZQUN0QixJQUFJLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUk7UUFFaEMsSUFBSSxPQUFPLEdBQUcsQ0FBQyxNQUFNLEtBQUssUUFBUSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sS0FBSyxRQUFRLElBQUksQ0FBQyxtQkFBUSxHQUFHLENBQUMsTUFBTSxFQUFBLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssUUFBUSxDQUFDLEVBQUU7WUFDbEgsSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQztnQkFBRSxHQUFHLEdBQUcsR0FBRyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7U0FDckY7UUFFRCxJQUFJLEdBQUcsQ0FBQyxHQUFHLEtBQUssSUFBSSxFQUFFO1lBQ3BCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUU7Z0JBQ2hDLEdBQUcsR0FBRyxJQUFJLENBQUM7YUFDWjtpQkFBTTtnQkFDTCxPQUFPO2FBQ1I7U0FDRjtRQUVELElBQUksQ0FBQyxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxFQUFFO1lBQ3BELEdBQUcsR0FBRyxJQUFJLENBQUM7U0FDWjtRQUVELElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUU7WUFDdkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDcEIsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUN0QjtZQUNELFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDM0I7YUFBTSxJQUFJLElBQUksSUFBSSxTQUFTLEVBQUU7WUFDNUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUU7Z0JBQ25DLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQ3JDO1lBQ0QsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUMzQjthQUFNO1lBQ0wsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQztTQUN2QjtRQUVELElBQUksR0FBRyxDQUFDLEVBQUUsRUFBRTtZQUNWLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztTQUN4QjtJQUNILENBQUMsQ0FBQztJQUVGLENBQUMsQ0FBQyxPQUFPLEdBQUcsVUFBVSxJQUFJOztZQUNwQixZQUFZLEdBQUcsSUFBSTtRQUN2QixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2hCLE9BQU87U0FDUjtRQUVELElBQUksa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFOztnQkFDN0IsR0FBRyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzs7Z0JBQzdCLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztZQUNsQyxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRTtnQkFDMUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxHQUFHLEtBQUssQ0FBQzthQUMzQztpQkFBTTtnQkFDTCxHQUFHLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQzthQUNwQjtTQUNGO2FBQU07WUFDTCxDQUFDLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ3hCO0lBQ0gsQ0FBQyxDQUFDO0lBRUYsQ0FBQyxDQUFDLE9BQU8sR0FBRyxVQUFVLENBQUM7UUFDckIsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsTUFBTTtZQUNKLEtBQUssRUFBRTtnQkFDTCxTQUFTLEVBQUUsR0FBRztnQkFDZCxXQUFXLEVBQUUsYUFBYTtnQkFDMUIsTUFBTSxFQUFFLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU87Z0JBQzVCLFVBQVUsRUFBRSxHQUFHO2FBQ2hCO1NBQ0YsQ0FBQztJQUNKLENBQUMsQ0FBQztJQUVGLENBQUMsQ0FBQyxNQUFNLEdBQUcsVUFBVSxJQUFJOztZQUNuQixZQUFZLEdBQUcsSUFBSTtRQUN2QixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2hCLE9BQU87U0FDUjs7WUFFRyxHQUFHLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDOztZQUM3QixJQUFJLEdBQUcsVUFBVSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJOztZQUNwQyxLQUFLO1FBQ1AsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUM1RixLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDMUQ7YUFDSTtZQUNILElBQUksSUFBSSxLQUFLLEtBQUssSUFBSSxJQUFJLEtBQUssU0FBUyxFQUFFO2dCQUN4QyxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQzthQUM1QjtpQkFBTSxJQUFJLElBQUksS0FBSyxNQUFNLElBQUksSUFBSSxLQUFLLFNBQVMsRUFBRTtnQkFDaEQsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxNQUFNLElBQUksSUFBSSxLQUFLLEdBQUcsQ0FBQzthQUN2RDtpQkFBTSxJQUFJLElBQUksS0FBSyxVQUFVLElBQUksSUFBSSxLQUFLLE1BQU0sRUFBRTtnQkFDakQsS0FBSyxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3hCO2lCQUFNO2dCQUNMLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRTtvQkFDbkMsSUFBSSxHQUFHLFlBQVksQ0FBQztpQkFDckI7Z0JBQ0QsK0JBQStCO2dCQUMvQixJQUFJLE9BQU8sR0FBRyxDQUFDLE1BQU0sS0FBSyxRQUFRLEVBQUU7b0JBQ2xDLEtBQUssR0FBRyxJQUFJLENBQUM7aUJBQ2Q7cUJBQU07b0JBQ0wsS0FBSyxHQUFHLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2lCQUMzQjthQUNGO1NBQ0Y7UUFFRCxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUMxQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxDQUFDO1NBQzNDO2FBQU07WUFDTCxHQUFHLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUNwQjtJQUNILENBQUMsQ0FBQztJQUVGLElBQUksT0FBTyxRQUFRLEtBQUssVUFBVSxFQUFFOzs7WUFFOUIsU0FBUyxHQUFHLEdBQUcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO1FBQ3RDLFNBQVMsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNyQyxTQUFTLENBQUMsRUFBRSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdkMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2pDLFNBQVMsQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvQixHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQzthQUNoQixFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsR0FBRztZQUN4QixRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEIsQ0FBQyxDQUFDO2FBQ0QsRUFBRSxDQUFDLEtBQUssRUFBRTs7Z0JBQ0wsQ0FBQztZQUNMLElBQUk7Z0JBQ0YsQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDO2FBQ2Q7WUFBQyxPQUFPLENBQUMsRUFBRTtnQkFDVixPQUFPLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNwQjtZQUNELFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDcEIsQ0FBQyxDQUFDLENBQUM7UUFDTCxPQUFPO0tBQ1I7SUFDRCxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBRXJCLE9BQU8sTUFBTSxFQUFFLENBQUM7Ozs7SUFFaEIsU0FBUyxNQUFNO1FBQ2IsdURBQXVEO1FBQ3ZELEtBQUssSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFOztnQkFDZCxHQUFHLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNqQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3pDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ3JDO1NBQ0Y7UUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7O2dCQUNiLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUk7WUFDN0IsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTs7b0JBQ2xCLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNOztvQkFDMUQsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE1BQU07O29CQUNoRSxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTTtnQkFFMUQsSUFBSSxHQUFHLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDcEMsTUFBTSxHQUFHLE1BQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQztnQkFDMUMsTUFBTSxHQUFHLE1BQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQzs7b0JBRWpDLEtBQUssR0FBUSxJQUFJLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLE1BQU0sR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBRWhGLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUNsQixNQUFNLEtBQUssQ0FBQzthQUNiO1lBQ0QsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO1NBQ3RCO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0FBQ0gsQ0FBQyxDQUFDOzs7Ozs7O0FBUUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsR0FBRyxVQUFVLEtBQUssRUFBRSxLQUFLO0lBQ3RELElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxLQUFLLEVBQUU7UUFDcEIsT0FBTyxJQUFJLENBQUM7S0FDYjs7UUFFRyxHQUFHLEdBQUcsSUFBSTtJQUVkLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUU7O1lBQ3hCLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFDNUMsSUFBSSxNQUFNLEVBQUU7WUFDVixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQzdCLEtBQUssR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUMvRDtZQUVELDhGQUE4RjtZQUM5RiwyQ0FBMkM7WUFDM0MsR0FBRyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ25GO0tBQ0Y7SUFFRCxPQUFPLEdBQUcsQ0FBQztBQUNiLENBQUMsQ0FBQzs7Ozs7Ozs7O0FBVUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsR0FBRyxVQUFVLElBQUksRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxJQUFJO0lBQ2hGLHNGQUFzRjtJQUN0RixJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsSUFBSSxFQUFFO1FBQ3pCLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQztLQUNwQjs7UUFDRyxJQUFJLEdBQUcsRUFBRTtJQUNiLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUM7O1FBQ2hCLGdCQUFnQixHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtJQUN2RSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztBQUNyRixDQUFDLENBQUM7Ozs7Ozs7OztBQVVGLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxHQUFHLFVBQVUsSUFBSSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLE9BQU87O1FBQzFFLEtBQUssR0FBRyxFQUFFOztRQUNWLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVzs7UUFDdkIsVUFBVSxHQUFHLFFBQVE7SUFFekIsUUFBUSxHQUFHLFFBQVEsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztJQUVyRCxLQUFLLEdBQUcsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDdEMsUUFBUSxHQUFHLFFBQVEsS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLENBQUM7SUFFM0QsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBRWhELEtBQUssSUFBSSxHQUFHLElBQUksTUFBTSxFQUFFO1FBQ3RCLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQy9CLFNBQVM7U0FDVjtRQUNELElBQUksR0FBRyxLQUFLLFVBQVUsRUFBRTs7Z0JBQ2xCLEtBQUssR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDOztnQkFDbkIsV0FBVyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxHQUFHLEdBQUc7O2dCQUM3QyxVQUFVLEdBQUcsRUFBRTtZQUNuQixJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsSUFBSSxLQUFLLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUU7O29CQUM3RSxLQUFLLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDO2dCQUM3QyxLQUFLLElBQUksQ0FBQyxJQUFJLEtBQUssRUFBRTtvQkFDbkIsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO2lCQUN2RDthQUNGO1lBQ0QsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3ZFLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLEtBQUssS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDM0csS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxXQUFXLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDL0M7S0FDRjtJQUNELEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNqRCxPQUFPLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7QUFDeEIsQ0FBQyxDQUFDOzs7OztBQUdGLFNBQVMsV0FBVyxDQUFDLEVBQUU7SUFDckIsT0FBTyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztBQUNsRSxDQUFDOzs7OztBQUVELFNBQVMsZ0JBQWdCLENBQUMsRUFBRTtJQUMxQixPQUFPLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0FBQ3hGLENBQUM7QUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLGtCQUFrQixHQUFHLFVBQVUsRUFBRTtJQUM5QyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0FBQ3pELENBQUMsQ0FBQztBQUVGLElBQUksQ0FBQyxTQUFTLENBQUMseUJBQXlCLEdBQUcsVUFBVSxFQUFFOztRQUNqRCxTQUFTLEdBQUcsZ0JBQWdCLENBQUMsRUFBRSxDQUFDO0lBQ3BDLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztBQUM3RCxDQUFDLENBQUM7Ozs7Ozs7Ozs7Ozs7O0FBaUJGLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsR0FBRyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLFNBQVM7O1FBQ3hHLElBQUksR0FBRyxJQUFJOztRQUNYLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7O1FBRXhDLGNBQWMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFNBQVM7SUFDM0QsSUFBSSxPQUFPLGNBQWMsS0FBSyxXQUFXLEVBQUU7UUFDekMsOEdBQThHO1FBQzlHLFFBQVEsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDO0tBQzdCO0lBRUQsY0FBYyxHQUFHLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2xELElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxFQUFFO1FBQzNDLGNBQWMsR0FBRyxFQUFFLENBQUM7S0FDckI7O1FBRUcsVUFBVSxHQUFHLENBQUMsTUFBTTs7UUFDcEIsU0FBUyxHQUFHLE1BQU0sSUFBSSxNQUFNLENBQUMsbUJBQW1CLEtBQUssV0FBVzs7UUFDaEUsS0FBSyxHQUFHLEVBQUU7O1FBQ1YsZUFBZSxHQUFHLENBQUMsUUFBUSxJQUFJLFNBQVMsQ0FBQyxJQUFJLFFBQVEsS0FBSyxVQUFVOztRQUVwRSxXQUFXLEdBQUcsRUFBRTtJQUNwQixJQUFJLEtBQUssSUFBSSxPQUFPLEVBQUU7UUFDcEIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsZUFBZSxFQUFFO1lBQ3hGLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFVLFNBQVM7Z0JBQzFFLFdBQVcsSUFBSSxHQUFHLEdBQUcsU0FBUyxDQUFDLElBQUksR0FBRyxJQUFJLEdBQUcsU0FBUyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7WUFDckUsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3pELCtCQUErQjtnQkFDL0IsV0FBVyxJQUFJLFNBQVMsR0FBRyxRQUFRLEdBQUcsSUFBSSxHQUFHLEtBQUssR0FBRyxHQUFHLENBQUM7YUFDMUQ7WUFDRCwyRUFBMkU7WUFDM0UsSUFBSSxTQUFTLElBQUksVUFBVTtnQkFBRSxXQUFXLElBQUksVUFBVSxHQUFHLEtBQUssR0FBRyxHQUFHLENBQUM7U0FDdEU7S0FDRjtJQUVELElBQUksQ0FBQyxTQUFTLEVBQUU7UUFDZCxTQUFTLEdBQUcsSUFBSSxnQkFBZ0IsRUFBRSxDQUFDO1FBQ25DLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7S0FDN0M7U0FBTTtRQUNMLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztLQUN6QjtJQUVELDhDQUE4QztJQUM5QyxJQUFJLFNBQVMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLGVBQWUsQ0FBQyxFQUFFO1FBQ3hHLFdBQVcsR0FBRyxTQUFTLENBQUM7S0FDekI7O1FBRUcsRUFBRSxHQUFHLEVBQUU7SUFFWCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLElBQUksT0FBTyxFQUFFO1FBQy9DLEVBQUUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQztLQUNqRDtTQUFNLElBQUksZUFBZSxJQUFJLENBQUMsU0FBUyxJQUFJLE9BQU8sSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsRUFBRTtRQUN4RyxFQUFFLEdBQUcsUUFBUSxDQUFDO0tBQ2Y7O1FBRUcsQ0FBQzs7UUFBRSxDQUFDO0lBQ1IsaUNBQWlDO0lBQ2pDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtRQUN0QixLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTs7Z0JBQ2xDLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDOztnQkFDYixTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxTQUFTLENBQUM7O2dCQUNyRCxvQkFBb0IsR0FBRyxjQUFjLElBQUksRUFBRTs7O2dCQUV6QyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsU0FBUyxDQUFDOztnQkFFMUYsZUFBZSxHQUFHLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsV0FBVyxDQUFDO1lBRTVGLElBQUksSUFBSSxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRTtnQkFDM0MsK0NBQStDO2dCQUMvQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM1QixLQUFLLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUN0QztpQkFBTTtnQkFDTCxlQUFlLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUMxQixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDbEQsS0FBSyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQ3RDO2dCQUNELEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2pCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDdEQsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsb0JBQW9CLENBQUMsRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQzNFO2FBQ0Y7U0FDRjtLQUNGO1NBQU0sSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLEVBQUU7UUFDbEMsS0FBSyxJQUFJLElBQUksR0FBRyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQztnQkFBRSxTQUFTO1lBQ3hDLHFDQUFxQztZQUNyQyxJQUFJLElBQUksS0FBSyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRTtnQkFDdkMsU0FBUzthQUNWO1lBQ0Qsb0RBQW9EO1lBQ3BELElBQUksSUFBSSxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO2dCQUNoQyxTQUFTLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQ3ZCLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2xCO1lBQ0QsK0NBQStDO1lBQy9DLElBQUksSUFBSSxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFO2dCQUNsQyxTQUFTLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQ3ZCLE9BQU8sU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQzdCOztnQkFFRyxLQUFLLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQztZQUNyQixJQUFJLE9BQU8sS0FBSyxLQUFLLFdBQVcsRUFBRTtnQkFDaEMsU0FBUzthQUNWOztnQkFFRyxJQUFJLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxTQUFTLENBQUM7O2dCQUUvQyxLQUFLLEdBQUcsRUFBRTs7Z0JBQ1YsZUFBZSxHQUFHLEVBQUU7O2dCQUNwQixvQkFBb0IsR0FBRyxLQUFLOztnQkFFNUIsZUFBZSxHQUFHLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDcEQsSUFBSSxlQUFlLEVBQUU7Z0JBQ25CLGVBQWUsR0FBRyxlQUFlLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO2dCQUMzQyxJQUFJLEdBQUcsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzNCO2lCQUFNLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRTtnQkFDMUIsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO2dCQUM1QixJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN2QjtZQUVELElBQUksT0FBTyxFQUFFO2dCQUNYLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxTQUFTLENBQUMsQ0FBQzthQUM5RjtpQkFBTTtnQkFFTCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFO29CQUM1QixJQUFJLE1BQU0sRUFBRTs7NEJBQ04saUJBQWlCLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUM7d0JBQ3RFLHVDQUF1Qzt3QkFDdkMsSUFBSSxpQkFBaUI7NEJBQ25CLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBQzVFLGlCQUFpQixDQUFDLElBQUksSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsRUFBRTs7Ozs7Z0NBS2xELGFBQWEsR0FBUSxFQUFFOztnQ0FDdkIsU0FBUyxHQUFHLEVBQUU7O2dDQUNkLFVBQVU7O2dDQUNWLGdCQUFnQixHQUFHLEVBQUU7O2dDQUVyQixZQUFZLEdBQUcsaUJBQWlCLENBQUMsSUFBSSxJQUFJLGlCQUFpQixDQUFDLEtBQUs7NEJBQ3BFLElBQUksWUFBWSxFQUFFO2dDQUNoQixZQUFZLEdBQUcsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO2dDQUN4QyxTQUFTLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQztnQ0FDOUIsSUFBSSxZQUFZLENBQUMsTUFBTSxLQUFLLFVBQVUsRUFBRTtvQ0FDdEMsZ0JBQWdCO29DQUNoQixVQUFVLEdBQUcsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUM7b0NBQ2hELGFBQWEsR0FBRyxTQUFTLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUM7b0NBQ3hELElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxFQUFFO3dDQUMxQyxhQUFhLEdBQUcsUUFBUSxDQUFDO3FDQUMxQjtpQ0FDRjtxQ0FBTTtvQ0FDTCxhQUFhLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQztvQ0FDcEMsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLEVBQUU7d0NBQzFDLGFBQWEsR0FBRyxRQUFRLENBQUM7cUNBQzFCO29DQUNELFVBQVUsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2lDQUNuRjs7b0NBRUcsV0FBVyxHQUFHLEtBQUs7Z0NBQ3ZCLDhDQUE4QztnQ0FDOUMsSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLElBQUksaUJBQWlCLENBQUMsZUFBZSxLQUFLLFNBQVMsRUFBRTtvQ0FDOUUsSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLEtBQUssYUFBYSxFQUFFO3dDQUM3QyxXQUFXLEdBQUcsSUFBSSxDQUFDO3FDQUNwQjt5Q0FBTSxJQUFJLGlCQUFpQixDQUFDLEtBQUssS0FBSyxXQUFXLEVBQUU7d0NBQ2xELFdBQVcsR0FBRyxLQUFLLENBQUM7cUNBQ3JCO3lDQUFNO3dDQUNMLFdBQVcsR0FBRyxNQUFNLENBQUMsbUJBQW1CLEtBQUssV0FBVyxDQUFDO3FDQUMxRDtpQ0FDRjtnQ0FDRCxJQUFJLFdBQVcsRUFBRTtvQ0FDZixhQUFhLEdBQUcsRUFBRSxDQUFDO2lDQUNwQjtnQ0FFRCxJQUFJLFVBQVUsSUFBSSxhQUFhLEVBQUU7b0NBQy9CLElBQUksU0FBUyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsRUFBRSxVQUFVLENBQUMsRUFBRTt3Q0FDekQsZ0JBQWdCLEdBQUcsU0FBUyxHQUFHLGFBQWEsR0FBRyxJQUFJLEdBQUcsVUFBVSxHQUFHLEdBQUcsQ0FBQzt3Q0FDdkUsV0FBVyxJQUFJLGdCQUFnQixDQUFDO3FDQUNqQztpQ0FDRjs2QkFDRjs7Z0NBRUcseUJBQXlCOzRCQUM3QixJQUFJLGlCQUFpQixDQUFDLEtBQUssRUFBRTs7b0NBQ3ZCLFNBQVMsR0FBRyxVQUFVLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDOztvQ0FDL0MsVUFBVSxHQUFHLFNBQVMsQ0FBQyxNQUFNOztvQ0FDN0IsT0FBTyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDO2dDQUM1RSxVQUFVLEdBQUcsT0FBTyxDQUFDO2dDQUNyQixJQUFJLE9BQU8sS0FBSyxrQ0FBa0MsSUFBSSxVQUFVLEtBQUssVUFBVSxFQUFFO29DQUMvRSx1REFBdUQ7b0NBQ3ZELFNBQVMsQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2lDQUM3QztnQ0FDRCx5QkFBeUI7b0NBQ3ZCLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsSUFBSSxpQkFBaUIsQ0FBQzs2QkFDckU7aUNBQU07Z0NBQ0wseUJBQXlCO29DQUN2QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxJQUFJLGlCQUFpQixDQUFDOzZCQUNyRTs0QkFFRCxJQUFJLGlCQUFpQixDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLG9CQUFvQixFQUFFO2dDQUN6RSxhQUFhLEdBQUcsUUFBUSxDQUFDO2dDQUN6QixVQUFVLEdBQUcsS0FBSyxDQUFDOzZCQUNwQjs0QkFFRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsb0JBQW9CLEVBQUU7Z0NBQ3JDLGFBQWEsR0FBRyxFQUFFLENBQUM7Z0NBQ25CLFVBQVUsR0FBRyxFQUFFLENBQUM7NkJBQ2pCOzRCQUVELEVBQUUsR0FBRyxhQUFhLENBQUM7NEJBRW5CLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtnQ0FDeEIsdURBQXVEO2dDQUN2RCxhQUFhLEdBQUc7b0NBQ2QsT0FBTyxFQUFFLGFBQWE7b0NBQ3RCLE1BQU0sRUFBRSxFQUFFO2lDQUNYLENBQUM7NkJBQ0g7aUNBQU07Z0NBQ0wsMENBQTBDO2dDQUMxQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7NkJBQ3pCOzRCQUVELEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLFVBQVUsRUFDN0QsS0FBSyxFQUFFLGdCQUFnQixFQUFFLHlCQUF5QixFQUFFLFNBQVMsQ0FBQyxDQUFDO3lCQUNsRTs2QkFBTSxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLFFBQVEsRUFBRTs7O2dDQUVsRiw0QkFBNEIsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQzNELEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQzdDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7NEJBRWpELGVBQWUsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDOzRCQUNsRSxTQUFTLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQ3BFLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQzs0QkFDbEQsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUNuRixHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDO3lCQUNqRjs2QkFBTTs0QkFDTCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0NBQ3hCLElBQUksR0FBRyxlQUFlLEdBQUcsSUFBSSxDQUFDOzZCQUMvQjs0QkFFRCxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7eUJBQ3RGO3FCQUNGO3lCQUFNO3dCQUNMLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztxQkFDdEY7aUJBQ0Y7YUFDRjtZQUVELEVBQUUsR0FBRyxnQkFBZ0IsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUMxQixJQUFJLGVBQWUsSUFBSSxDQUFDLFNBQVMsSUFBSSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixFQUFFO2dCQUNqRixFQUFFLEdBQUcsUUFBUSxDQUFDO2FBQ2Y7aUJBQU0sSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsRUFBRSxDQUFDLEVBQUU7Z0JBQ3RDLEVBQUUsR0FBRyxFQUFFLENBQUM7YUFDVDs7Z0JBRUcsV0FBVyxHQUFHLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVztZQUNwRCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDekIsWUFBWTtnQkFDWixLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxlQUFlLElBQUksRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxXQUFXO29CQUN0RyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ3pDLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHO2lCQUMxQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQ2I7WUFFRCxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNoQixLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNsQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDekIsVUFBVTtvQkFDVixLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxFQUFFLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxlQUFlLElBQUksRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2lCQUN4RzthQUNGO1NBQ0Y7S0FDRjtTQUFNLElBQUksR0FBRyxLQUFLLFNBQVMsRUFBRTtRQUM1QixLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztLQUM3RDtJQUNELFNBQVMsQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUN2QixPQUFPLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7QUFDeEIsQ0FBQyxDQUFDO0FBRUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsR0FBRyxVQUFVLEtBQUssRUFBRSxTQUFTOztRQUN2RCxJQUFJLEdBQUcsRUFBRTtJQUViLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTtRQUNsQixLQUFLLEdBQUcsRUFBRSxDQUFDO0tBQ1o7O1FBRUcsT0FBTyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQztJQUMvQyxJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsUUFBUSxFQUFFOztZQUMzQixPQUFPLEdBQUcsT0FBTyxDQUFDLFFBQVE7O1lBRTFCLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxJQUFJLE9BQU8sQ0FBQyxTQUFTO1FBQ2hELHFFQUFxRTtRQUNyRSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ1gsTUFBTSxHQUFHLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDckQ7YUFBTTtZQUNMLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ25EO1FBQ0QsT0FBTyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7S0FDekI7SUFHRCxJQUFJLE9BQU8sRUFBRTtRQUNYLEtBQUssSUFBSSxPQUFPLElBQUksT0FBTyxFQUFFO1lBQzNCLHFDQUFxQztZQUNyQyxJQUFJLE9BQU8sS0FBSyxVQUFVLEVBQUU7O29CQUN0QixTQUFTLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQztnQkFDaEMsSUFBSSxJQUFJLGFBQWEsR0FBRyxTQUFTLENBQUMsTUFBTSxHQUFHLEdBQUcsR0FBRyxTQUFTLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztnQkFDdEUsSUFBSSxJQUFJLFNBQVMsR0FBRyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksR0FBRyxTQUFTLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztnQkFFcEUsU0FBUzthQUNWO2lCQUFNO2dCQUNMLElBQUksSUFBSSxHQUFHLEdBQUcsT0FBTyxHQUFHLElBQUksR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO2FBQ2xFO1NBQ0Y7S0FDRjtJQUVELE9BQU8sSUFBSSxDQUFDO0FBQ2QsQ0FBQyxDQUFDOzs7Ozs7O0FBUUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLEdBQUcsVUFBVSxJQUFJLEVBQUUsS0FBSztJQUNuRCxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7UUFDaEQsT0FBTyxJQUFJLENBQUM7S0FDYjs7UUFFRyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO0lBQzVDLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFO1FBQ25DLE9BQU8sSUFBSSxDQUFDO0tBQ2I7SUFFRCxPQUFPLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDbkMsQ0FBQyxDQUFDO0FBRUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsR0FBRyxVQUFVLGdCQUFnQixFQUFFLFNBQVMsRUFBRSxTQUFTO0lBQ3JGLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxDQUFDLFNBQVMsRUFBRTtRQUNuQyxPQUFPLElBQUksQ0FBQztLQUNiO0lBRUQsSUFBSSxDQUFDLFNBQVMsRUFBRTtRQUNkLFNBQVMsR0FBRyxFQUFFLENBQUM7S0FDaEI7SUFFRCxJQUFJLFNBQVMsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEVBQUU7UUFDNUMsMkNBQTJDO1FBQzNDLE9BQU8sSUFBSSxDQUFDO0tBQ2I7U0FBTTtRQUNMLFNBQVMsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO0tBQ2xEOztRQUVHLEtBQUssR0FBRyxJQUFJOztRQUNkLENBQUMsR0FBRyxDQUFDOztRQUNMLEtBQUs7O1FBQ0wsR0FBRztJQUVMLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFOztZQUNwRixLQUFLLEdBQUcsZ0JBQWdCLENBQUMsWUFBWTtRQUV6QyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O2dCQUM3QixPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUV0QixJQUFJLE9BQU8sQ0FBQyxLQUFLLEtBQUssU0FBUyxFQUFFO2dCQUMvQixLQUFLLEdBQUcsT0FBTyxDQUFDO2dCQUNoQixNQUFNO2FBQ1A7U0FDRjtLQUNGOztRQUVHLE1BQU0sR0FBRyxnQkFBZ0I7SUFDN0IsSUFBSSxNQUFNLENBQUMsS0FBSyxLQUFLLFNBQVMsSUFBSSxNQUFNLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRTtRQUMzRCxPQUFPLE1BQU0sQ0FBQztLQUNmO0lBQ0QsSUFBSSxNQUFNLENBQUMsSUFBSSxFQUFFO1FBQ2YsR0FBRyxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUIsSUFBSSxHQUFHLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRTtZQUMxQixPQUFPLE1BQU0sQ0FBQztTQUNmO0tBQ0Y7O1FBRUcsVUFBVTtJQUVkLDREQUE0RDtJQUM1RCxJQUFJLE1BQU0sQ0FBQyxLQUFLLElBQUksU0FBUyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7O1lBQ3RDLFFBQVEsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUN2QyxJQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO1lBQ2xDLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQztTQUNoRDthQUFNO1lBQ0wsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN0RDs7WUFDRyxPQUFPLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQztRQUM1RCxJQUFJLE9BQU8sRUFBRTtZQUNYLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7U0FDbEU7S0FDRjtJQUVELElBQUksTUFBTSxDQUFDLFFBQVEsRUFBRTtRQUNuQixLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2xELEtBQUssR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQztZQUNoRSxJQUFJLEtBQUssRUFBRTtnQkFDVCxNQUFNO2FBQ1A7WUFFRCxJQUFJLEtBQUssQ0FBQyxLQUFLLEVBQUU7O29CQUNYLFNBQVMsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQzs7b0JBQ25DLGNBQWMsR0FBRyxTQUFTLENBQUMsTUFBTSxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTTtnQkFDNUUsVUFBVSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQzs7b0JBRW5GLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDO2dCQUUvRCxJQUFJLFNBQVMsRUFBRTtvQkFDYixLQUFLLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7b0JBRXBFLElBQUksS0FBSyxFQUFFO3dCQUNULEtBQUssQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDO3dCQUN0QyxLQUFLLENBQUMsS0FBSyxHQUFHLGNBQWMsR0FBRyxHQUFHLEdBQUcsU0FBUyxDQUFDO3dCQUMvQyxNQUFNO3FCQUNQO2lCQUNGO2FBQ0Y7U0FDRjtLQUVGO0lBRUQsSUFBSSxDQUFDLEtBQUssSUFBSSxNQUFNLENBQUMsS0FBSyxLQUFLLFNBQVMsRUFBRTtRQUN4QyxPQUFPLE1BQU0sQ0FBQztLQUNmO0lBRUQsT0FBTyxLQUFLLENBQUM7QUFDZixDQUFDLENBQUM7QUFFRixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxVQUFVLEdBQUc7O1FBQy9CLElBQUksR0FBRyxJQUFJOztRQUNiLENBQUMsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQzs7UUFDcEIsS0FBSyxHQUFHLEVBQUU7O1FBQ1YsSUFBSSxHQUFHLElBQUk7O1FBQ1gsS0FBSyxHQUFHLElBQUk7O1FBQ1osTUFBTSxHQUFHLElBQUk7O1FBQ2IsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPO0lBRXhCLENBQUMsQ0FBQyxTQUFTLEdBQUcsVUFBVSxJQUFJOztZQUN0QixNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUk7O1lBQ2xCLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVTs7WUFFdkIsR0FBRyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzs7WUFDN0IsSUFBSTtRQUNSLElBQUksR0FBRyxFQUFFO1lBQ1AsSUFBSTtnQkFDRixHQUFHLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2FBQ2pEO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1YsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtvQkFDdkIsTUFBTSxDQUFDLENBQUM7aUJBQ1Q7cUJBQU07b0JBQ0wsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLE9BQU8sQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7aUJBQ2pEO2FBQ0Y7U0FDRjthQUFNO1lBQ0wsSUFBSSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDL0IsSUFBSSxJQUFJLEtBQUssYUFBYSxFQUFFO2dCQUMxQixJQUFJLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUN0RCxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2xCO2lCQUFNLElBQUksSUFBSSxLQUFLLFFBQVEsRUFBRTtnQkFDNUIsd0ZBQXdGO2dCQUN4RixJQUFJLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxhQUFhLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUNyRCxLQUFLLEdBQUcsSUFBSSxZQUFZLENBQUMsT0FBTyxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDMUMsTUFBTSxHQUFHLElBQUksYUFBYSxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ25ELEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDcEI7aUJBQU07Z0JBQ0wsTUFBTSxJQUFJLEtBQUssQ0FBQyw0Q0FBNEMsQ0FBQyxDQUFDO2FBQy9EO1NBQ0Y7SUFDSCxDQUFDLENBQUM7SUFFRixDQUFDLENBQUMsVUFBVSxHQUFHLFVBQVUsSUFBSTs7WUFDdkIsR0FBRyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUNqQyxNQUFNLENBQUMsR0FBRyxFQUFFLHVCQUF1QixHQUFHLElBQUksQ0FBQyxDQUFDO1FBRTVDLEdBQUcsQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzlCLENBQUMsQ0FBQztJQUVGLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7SUFFckIsT0FBTyxJQUFJLENBQUM7QUFDZCxDQUFDLENBQUM7QUFFRixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxVQUFVLEdBQUc7SUFDckMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxHQUFHO1FBQzlCLEtBQUssRUFBRSxFQUFFO0tBQ1YsQ0FBQztJQUNGLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO0FBQ2pCLENBQUMsQ0FBQztBQUVGLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxHQUFHLFVBQVUsUUFBUTtBQUVqRCxDQUFDLENBQUM7QUFJRixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRzs7UUFDckIsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSzs7UUFDOUIsR0FBRyxHQUFHLEVBQUU7SUFDWixLQUFLLElBQUksS0FBSyxJQUFJLEtBQUssRUFBRTtRQUN2QixJQUFJLEtBQUssS0FBSyxFQUFFLElBQUksS0FBSyxLQUFLLFVBQVUsRUFBRTtZQUN4QyxTQUFTO1NBQ1Y7O1lBQ0csRUFBRSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDckIsUUFBUSxFQUFFLEVBQUU7WUFDVixLQUFLLGdDQUFnQyxDQUFDLENBQUMsYUFBYTtZQUNwRCxLQUFLLGtDQUFrQyxDQUFDLENBQUMsT0FBTztZQUNoRCxLQUFLLHVDQUF1QyxDQUFDLENBQUMsV0FBVztZQUN6RCxLQUFLLHlDQUF5QyxDQUFDLENBQUMsYUFBYTtZQUM3RCxLQUFLLDJDQUEyQyxDQUFDLENBQUMsVUFBVTtZQUM1RCxLQUFLLGtDQUFrQyxFQUFFLE1BQU07Z0JBQzdDLFNBQVM7U0FDWjtRQUNELElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLDZCQUE2QixDQUFDLEVBQUU7WUFDOUMsU0FBUztTQUNWO1FBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsRUFBRTtZQUNyQyxTQUFTO1NBQ1Y7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQyxFQUFFO1lBQ3pDLFNBQVM7U0FDVjtRQUNELEdBQUcsSUFBSSxTQUFTLEdBQUcsS0FBSyxHQUFHLElBQUksR0FBRyxFQUFFLEdBQUcsR0FBRyxDQUFDO0tBQzVDO0lBQ0QsT0FBTyxHQUFHLENBQUM7QUFDYixDQUFDLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW9CRixTQUFTLG1CQUFtQixDQUFDLEdBQUcsRUFBRSxPQUFPOztRQUNuQyxTQUFTOztRQUNYLFVBQVU7SUFFWix1Q0FBdUM7SUFDdkMsd0JBQXdCO0lBQ3hCLGtCQUFrQjtJQUNsQixJQUFJO0lBRUosVUFBVSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUM7SUFFaEMsSUFBSSxTQUFTLEdBQUcsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1FBQy9CLG9EQUFvRDtRQUNwRCxPQUFPLFNBQVMsQ0FBQztLQUNsQjtJQUVELE9BQU8sU0FBUyxDQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsQ0FBQztBQUNqQyxDQUFDOzs7Ozs7QUFFRCxNQUFNLFVBQWdCLFNBQVMsQ0FBQyxHQUFHLEVBQUUsT0FBTzs7UUFDMUMsdUNBQXVDO1FBQ3ZDLHdCQUF3QjtRQUN4QixrQkFBa0I7UUFDbEIsSUFBSTs7Ozs7OztZQUdBLFVBQVUsR0FBRyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7O1lBQ3JDLGVBQWUsR0FBRyxPQUFPLENBQUMsWUFBWTs7WUFDdEMsZUFBZSxHQUFHLE9BQU8sQ0FBQyxZQUFZO1FBRTFDLFlBQVk7UUFDWiwrQkFBK0I7UUFDL0IsdUNBQXVDO1FBQ3ZDLDREQUE0RDtRQUM1RCxvQkFBb0I7UUFDcEIsMEJBQTBCO1FBQzFCLFdBQVc7UUFDWCxnQkFBZ0I7UUFDaEIsc0RBQXNEO1FBQ3RELHFDQUFxQztRQUNyQyx5Q0FBeUM7UUFDekMsbUNBQW1DO1FBQ25DLFdBQVc7UUFDWCxXQUFXO1FBQ1gsSUFBSTtRQUNKLFNBQVM7UUFDVCxtQ0FBbUM7UUFDbkMsb0VBQW9FO1FBQ3BFLHNGQUFzRjtRQUN0RixpQkFBaUI7UUFDakIsdUJBQXVCO1FBQ3ZCLDREQUE0RDtRQUM1RCxtREFBbUQ7UUFDbkQsa0NBQWtDO1FBQ2xDLHNDQUFzQztRQUN0QyxnQ0FBZ0M7UUFDaEMsZUFBZTtRQUNmLDRJQUE0STtRQUM1SSxRQUFRO1FBQ1IsMENBQTBDO1FBQzFDLElBQUk7UUFDSixlQUFlO1FBRWYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLENBQUMsQ0FBQzs7Y0FDOUIsVUFBVSxHQUFlLE9BQU8sQ0FBQyxVQUFVOztjQUMzQyxPQUFPLEdBQUcsTUFBTSxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLFNBQVMsRUFBRTs7Y0FDekUsT0FBTyxHQUFHLE1BQU0sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTs7a0JBQ3RDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUUsR0FBRyxFQUFFLE9BQU8sQ0FBQztZQUM1QyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1lBQzdCLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDOUIsQ0FBQyxDQUFDO1FBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUE7UUFDNUIsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQztDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIENvcHlyaWdodCAoYykgMjAxMSBWaW5heSBQdWxpbSA8dmluYXlAbWlsZXdpc2UuY29tPlxuICogTUlUIExpY2Vuc2VkXG4gKlxuICovXG4vKmpzaGludCBwcm90bzp0cnVlKi9cblxuXCJ1c2Ugc3RyaWN0XCI7XG5cbmltcG9ydCAqIGFzIHNheCBmcm9tICdzYXgnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE5hbWVzcGFjZUNvbnRleHQgfSDCoGZyb20gJy4vbnNjb250ZXh0JztcblxuaW1wb3J0ICogYXMgdXJsIGZyb20gJ3VybCc7XG5pbXBvcnQgeyBvayBhcyBhc3NlcnQgfSBmcm9tICdhc3NlcnQnO1xuLy8gaW1wb3J0IHN0cmlwQm9tIGZyb20gJ3N0cmlwLWJvbSc7XG5cbmNvbnN0IHN0cmlwQm9tID0gKHg6IHN0cmluZyk6IHN0cmluZyA9PiB7XG4gIC8vIENhdGNoZXMgRUZCQkJGIChVVEYtOCBCT00pIGJlY2F1c2UgdGhlIGJ1ZmZlci10by1zdHJpbmdcbiAgLy8gY29udmVyc2lvbiB0cmFuc2xhdGVzIGl0IHRvIEZFRkYgKFVURi0xNiBCT00pXG4gIGlmICh4LmNoYXJDb2RlQXQoMCkgPT09IDB4RkVGRikge1xuICAgIHJldHVybiB4LnNsaWNlKDEpO1xuICB9XG5cbiAgcmV0dXJuIHg7XG59XG5cbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCAqIGFzIHV0aWxzIGZyb20gJy4vdXRpbHMnO1xuXG5cbmxldCBUTlNfUFJFRklYID0gdXRpbHMuVE5TX1BSRUZJWDtcbmxldCBmaW5kUHJlZml4ID0gdXRpbHMuZmluZFByZWZpeDtcblxubGV0IFByaW1pdGl2ZXMgPSB7XG4gIHN0cmluZzogMSxcbiAgYm9vbGVhbjogMSxcbiAgZGVjaW1hbDogMSxcbiAgZmxvYXQ6IDEsXG4gIGRvdWJsZTogMSxcbiAgYW55VHlwZTogMSxcbiAgYnl0ZTogMSxcbiAgaW50OiAxLFxuICBsb25nOiAxLFxuICBzaG9ydDogMSxcbiAgbmVnYXRpdmVJbnRlZ2VyOiAxLFxuICBub25OZWdhdGl2ZUludGVnZXI6IDEsXG4gIHBvc2l0aXZlSW50ZWdlcjogMSxcbiAgbm9uUG9zaXRpdmVJbnRlZ2VyOiAxLFxuICB1bnNpZ25lZEJ5dGU6IDEsXG4gIHVuc2lnbmVkSW50OiAxLFxuICB1bnNpZ25lZExvbmc6IDEsXG4gIHVuc2lnbmVkU2hvcnQ6IDEsXG4gIGR1cmF0aW9uOiAwLFxuICBkYXRlVGltZTogMCxcbiAgdGltZTogMCxcbiAgZGF0ZTogMCxcbiAgZ1llYXJNb250aDogMCxcbiAgZ1llYXI6IDAsXG4gIGdNb250aERheTogMCxcbiAgZ0RheTogMCxcbiAgZ01vbnRoOiAwLFxuICBoZXhCaW5hcnk6IDAsXG4gIGJhc2U2NEJpbmFyeTogMCxcbiAgYW55VVJJOiAwLFxuICBRTmFtZTogMCxcbiAgTk9UQVRJT046IDBcbn07XG5cbmZ1bmN0aW9uIHNwbGl0UU5hbWUobnNOYW1lKSB7XG4gIGxldCBpID0gdHlwZW9mIG5zTmFtZSA9PT0gJ3N0cmluZycgPyBuc05hbWUuaW5kZXhPZignOicpIDogLTE7XG4gIHJldHVybiBpIDwgMCA/IHsgcHJlZml4OiBUTlNfUFJFRklYLCBuYW1lOiBuc05hbWUgfSA6XG4gICAgeyBwcmVmaXg6IG5zTmFtZS5zdWJzdHJpbmcoMCwgaSksIG5hbWU6IG5zTmFtZS5zdWJzdHJpbmcoaSArIDEpIH07XG59XG5cbmZ1bmN0aW9uIHhtbEVzY2FwZShvYmopIHtcbiAgaWYgKHR5cGVvZiAob2JqKSA9PT0gJ3N0cmluZycpIHtcbiAgICBpZiAob2JqLnN1YnN0cigwLCA5KSA9PT0gJzwhW0NEQVRBWycgJiYgb2JqLnN1YnN0cigtMykgPT09IFwiXV0+XCIpIHtcbiAgICAgIHJldHVybiBvYmo7XG4gICAgfVxuICAgIHJldHVybiBvYmpcbiAgICAgIC5yZXBsYWNlKC8mL2csICcmYW1wOycpXG4gICAgICAucmVwbGFjZSgvPC9nLCAnJmx0OycpXG4gICAgICAucmVwbGFjZSgvPi9nLCAnJmd0OycpXG4gICAgICAucmVwbGFjZSgvXCIvZywgJyZxdW90OycpXG4gICAgICAucmVwbGFjZSgvJy9nLCAnJmFwb3M7Jyk7XG4gIH1cblxuICByZXR1cm4gb2JqO1xufVxuXG5sZXQgdHJpbUxlZnQgPSAvXltcXHNcXHhBMF0rLztcbmxldCB0cmltUmlnaHQgPSAvW1xcc1xceEEwXSskLztcblxuZnVuY3Rpb24gdHJpbSh0ZXh0KSB7XG4gIHJldHVybiB0ZXh0LnJlcGxhY2UodHJpbUxlZnQsICcnKS5yZXBsYWNlKHRyaW1SaWdodCwgJycpO1xufVxuXG5mdW5jdGlvbiBkZWVwTWVyZ2UoZGVzdGluYXRpb24sIHNvdXJjZSkge1xuICByZXR1cm4gXy5tZXJnZVdpdGgoZGVzdGluYXRpb24gfHwge30sIHNvdXJjZSwgZnVuY3Rpb24gKGEsIGIpIHtcbiAgICByZXR1cm4gXy5pc0FycmF5KGEpID8gYS5jb25jYXQoYikgOiB1bmRlZmluZWQ7XG4gIH0pO1xufVxuXG5sZXQgRWxlbWVudDogYW55ID0gZnVuY3Rpb24gKG5zTmFtZSwgYXR0cnMsIG9wdGlvbnMpIHtcbiAgbGV0IHBhcnRzID0gc3BsaXRRTmFtZShuc05hbWUpO1xuXG4gIHRoaXMubnNOYW1lID0gbnNOYW1lO1xuICB0aGlzLnByZWZpeCA9IHBhcnRzLnByZWZpeDtcbiAgdGhpcy5uYW1lID0gcGFydHMubmFtZTtcbiAgdGhpcy5jaGlsZHJlbiA9IFtdO1xuICB0aGlzLnhtbG5zID0ge307XG5cbiAgdGhpcy5faW5pdGlhbGl6ZU9wdGlvbnMob3B0aW9ucyk7XG5cbiAgZm9yIChsZXQga2V5IGluIGF0dHJzKSB7XG4gICAgbGV0IG1hdGNoID0gL154bWxuczo/KC4qKSQvLmV4ZWMoa2V5KTtcbiAgICBpZiAobWF0Y2gpIHtcbiAgICAgIHRoaXMueG1sbnNbbWF0Y2hbMV0gPyBtYXRjaFsxXSA6IFROU19QUkVGSVhdID0gYXR0cnNba2V5XTtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICBpZiAoa2V5ID09PSAndmFsdWUnKSB7XG4gICAgICAgIHRoaXNbdGhpcy52YWx1ZUtleV0gPSBhdHRyc1trZXldO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpc1snJCcgKyBrZXldID0gYXR0cnNba2V5XTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgaWYgKHRoaXMuJHRhcmdldE5hbWVzcGFjZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgLy8gQWRkIHRhcmdldE5hbWVzcGFjZSB0byB0aGUgbWFwcGluZ1xuICAgIHRoaXMueG1sbnNbVE5TX1BSRUZJWF0gPSB0aGlzLiR0YXJnZXROYW1lc3BhY2U7XG4gIH1cbn07XG5cbkVsZW1lbnQucHJvdG90eXBlLl9pbml0aWFsaXplT3B0aW9ucyA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gIGlmIChvcHRpb25zKSB7XG4gICAgdGhpcy52YWx1ZUtleSA9IG9wdGlvbnMudmFsdWVLZXkgfHwgJyR2YWx1ZSc7XG4gICAgdGhpcy54bWxLZXkgPSBvcHRpb25zLnhtbEtleSB8fCAnJHhtbCc7XG4gICAgdGhpcy5pZ25vcmVkTmFtZXNwYWNlcyA9IG9wdGlvbnMuaWdub3JlZE5hbWVzcGFjZXMgfHwgW107XG4gIH0gZWxzZSB7XG4gICAgdGhpcy52YWx1ZUtleSA9ICckdmFsdWUnO1xuICAgIHRoaXMueG1sS2V5ID0gJyR4bWwnO1xuICAgIHRoaXMuaWdub3JlZE5hbWVzcGFjZXMgPSBbXTtcbiAgfVxufTtcblxuRWxlbWVudC5wcm90b3R5cGUuZGVsZXRlRml4ZWRBdHRycyA9IGZ1bmN0aW9uICgpIHtcbiAgdGhpcy5jaGlsZHJlbiAmJiB0aGlzLmNoaWxkcmVuLmxlbmd0aCA9PT0gMCAmJiBkZWxldGUgdGhpcy5jaGlsZHJlbjtcbiAgdGhpcy54bWxucyAmJiBPYmplY3Qua2V5cyh0aGlzLnhtbG5zKS5sZW5ndGggPT09IDAgJiYgZGVsZXRlIHRoaXMueG1sbnM7XG4gIGRlbGV0ZSB0aGlzLm5zTmFtZTtcbiAgZGVsZXRlIHRoaXMucHJlZml4O1xuICBkZWxldGUgdGhpcy5uYW1lO1xufTtcblxuRWxlbWVudC5wcm90b3R5cGUuYWxsb3dlZENoaWxkcmVuID0gW107XG5cbkVsZW1lbnQucHJvdG90eXBlLnN0YXJ0RWxlbWVudCA9IGZ1bmN0aW9uIChzdGFjaywgbnNOYW1lLCBhdHRycywgb3B0aW9ucykge1xuICBpZiAoIXRoaXMuYWxsb3dlZENoaWxkcmVuKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgbGV0IENoaWxkQ2xhc3MgPSB0aGlzLmFsbG93ZWRDaGlsZHJlbltzcGxpdFFOYW1lKG5zTmFtZSkubmFtZV0sXG4gICAgZWxlbWVudCA9IG51bGw7XG5cbiAgaWYgKENoaWxkQ2xhc3MpIHtcbiAgICBzdGFjay5wdXNoKG5ldyBDaGlsZENsYXNzKG5zTmFtZSwgYXR0cnMsIG9wdGlvbnMpKTtcbiAgfVxuICBlbHNlIHtcbiAgICB0aGlzLnVuZXhwZWN0ZWQobnNOYW1lKTtcbiAgfVxuXG59O1xuXG5FbGVtZW50LnByb3RvdHlwZS5lbmRFbGVtZW50ID0gZnVuY3Rpb24gKHN0YWNrLCBuc05hbWUpIHtcbiAgaWYgKHRoaXMubnNOYW1lID09PSBuc05hbWUpIHtcbiAgICBpZiAoc3RhY2subGVuZ3RoIDwgMilcbiAgICAgIHJldHVybjtcbiAgICBsZXQgcGFyZW50ID0gc3RhY2tbc3RhY2subGVuZ3RoIC0gMl07XG4gICAgaWYgKHRoaXMgIT09IHN0YWNrWzBdKSB7XG4gICAgICBfLmRlZmF1bHRzRGVlcChzdGFja1swXS54bWxucywgdGhpcy54bWxucyk7XG4gICAgICAvLyBkZWxldGUgdGhpcy54bWxucztcbiAgICAgIHBhcmVudC5jaGlsZHJlbi5wdXNoKHRoaXMpO1xuICAgICAgcGFyZW50LmFkZENoaWxkKHRoaXMpO1xuICAgIH1cbiAgICBzdGFjay5wb3AoKTtcbiAgfVxufTtcblxuRWxlbWVudC5wcm90b3R5cGUuYWRkQ2hpbGQgPSBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgcmV0dXJuO1xufTtcblxuRWxlbWVudC5wcm90b3R5cGUudW5leHBlY3RlZCA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gIHRocm93IG5ldyBFcnJvcignRm91bmQgdW5leHBlY3RlZCBlbGVtZW50ICgnICsgbmFtZSArICcpIGluc2lkZSAnICsgdGhpcy5uc05hbWUpO1xufTtcblxuRWxlbWVudC5wcm90b3R5cGUuZGVzY3JpcHRpb24gPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMpIHtcbiAgcmV0dXJuIHRoaXMuJG5hbWUgfHwgdGhpcy5uYW1lO1xufTtcblxuRWxlbWVudC5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcbn07XG5cbkVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MgPSBmdW5jdGlvbiAoKSB7XG4gIGxldCByb290ID0gdGhpcztcbiAgbGV0IHN1YkVsZW1lbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgcm9vdC5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgIHRoaXMuaW5pdCgpO1xuICB9O1xuICAvLyBpbmhlcml0cyhzdWJFbGVtZW50LCByb290KTtcbiAgc3ViRWxlbWVudC5wcm90b3R5cGUuX19wcm90b19fID0gcm9vdC5wcm90b3R5cGU7XG4gIHJldHVybiBzdWJFbGVtZW50O1xufTtcblxuXG5sZXQgRWxlbWVudEVsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgQW55RWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBJbnB1dEVsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgT3V0cHV0RWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBTaW1wbGVUeXBlRWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBSZXN0cmljdGlvbkVsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgRXh0ZW5zaW9uRWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBDaG9pY2VFbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xubGV0IEVudW1lcmF0aW9uRWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBDb21wbGV4VHlwZUVsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgQ29tcGxleENvbnRlbnRFbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xubGV0IFNpbXBsZUNvbnRlbnRFbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xubGV0IFNlcXVlbmNlRWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBBbGxFbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xubGV0IE1lc3NhZ2VFbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xubGV0IERvY3VtZW50YXRpb25FbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xuXG5sZXQgU2NoZW1hRWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBUeXBlc0VsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgT3BlcmF0aW9uRWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBQb3J0VHlwZUVsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgQmluZGluZ0VsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgUG9ydEVsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgU2VydmljZUVsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgRGVmaW5pdGlvbnNFbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xuXG5sZXQgRWxlbWVudFR5cGVNYXAgPSB7XG4gIHR5cGVzOiBbVHlwZXNFbGVtZW50LCAnc2NoZW1hIGRvY3VtZW50YXRpb24nXSxcbiAgc2NoZW1hOiBbU2NoZW1hRWxlbWVudCwgJ2VsZW1lbnQgY29tcGxleFR5cGUgc2ltcGxlVHlwZSBpbmNsdWRlIGltcG9ydCddLFxuICBlbGVtZW50OiBbRWxlbWVudEVsZW1lbnQsICdhbm5vdGF0aW9uIGNvbXBsZXhUeXBlJ10sXG4gIGFueTogW0FueUVsZW1lbnQsICcnXSxcbiAgc2ltcGxlVHlwZTogW1NpbXBsZVR5cGVFbGVtZW50LCAncmVzdHJpY3Rpb24nXSxcbiAgcmVzdHJpY3Rpb246IFtSZXN0cmljdGlvbkVsZW1lbnQsICdlbnVtZXJhdGlvbiBhbGwgY2hvaWNlIHNlcXVlbmNlJ10sXG4gIGV4dGVuc2lvbjogW0V4dGVuc2lvbkVsZW1lbnQsICdhbGwgc2VxdWVuY2UgY2hvaWNlJ10sXG4gIGNob2ljZTogW0Nob2ljZUVsZW1lbnQsICdlbGVtZW50IHNlcXVlbmNlIGNob2ljZSBhbnknXSxcbiAgLy8gZ3JvdXA6IFtHcm91cEVsZW1lbnQsICdlbGVtZW50IGdyb3VwJ10sXG4gIGVudW1lcmF0aW9uOiBbRW51bWVyYXRpb25FbGVtZW50LCAnJ10sXG4gIGNvbXBsZXhUeXBlOiBbQ29tcGxleFR5cGVFbGVtZW50LCAnYW5ub3RhdGlvbiBzZXF1ZW5jZSBhbGwgY29tcGxleENvbnRlbnQgc2ltcGxlQ29udGVudCBjaG9pY2UnXSxcbiAgY29tcGxleENvbnRlbnQ6IFtDb21wbGV4Q29udGVudEVsZW1lbnQsICdleHRlbnNpb24nXSxcbiAgc2ltcGxlQ29udGVudDogW1NpbXBsZUNvbnRlbnRFbGVtZW50LCAnZXh0ZW5zaW9uJ10sXG4gIHNlcXVlbmNlOiBbU2VxdWVuY2VFbGVtZW50LCAnZWxlbWVudCBzZXF1ZW5jZSBjaG9pY2UgYW55J10sXG4gIGFsbDogW0FsbEVsZW1lbnQsICdlbGVtZW50IGNob2ljZSddLFxuXG4gIHNlcnZpY2U6IFtTZXJ2aWNlRWxlbWVudCwgJ3BvcnQgZG9jdW1lbnRhdGlvbiddLFxuICBwb3J0OiBbUG9ydEVsZW1lbnQsICdhZGRyZXNzIGRvY3VtZW50YXRpb24nXSxcbiAgYmluZGluZzogW0JpbmRpbmdFbGVtZW50LCAnX2JpbmRpbmcgU2VjdXJpdHlTcGVjIG9wZXJhdGlvbiBkb2N1bWVudGF0aW9uJ10sXG4gIHBvcnRUeXBlOiBbUG9ydFR5cGVFbGVtZW50LCAnb3BlcmF0aW9uIGRvY3VtZW50YXRpb24nXSxcbiAgbWVzc2FnZTogW01lc3NhZ2VFbGVtZW50LCAncGFydCBkb2N1bWVudGF0aW9uJ10sXG4gIG9wZXJhdGlvbjogW09wZXJhdGlvbkVsZW1lbnQsICdkb2N1bWVudGF0aW9uIGlucHV0IG91dHB1dCBmYXVsdCBfb3BlcmF0aW9uJ10sXG4gIGlucHV0OiBbSW5wdXRFbGVtZW50LCAnYm9keSBTZWN1cml0eVNwZWNSZWYgZG9jdW1lbnRhdGlvbiBoZWFkZXInXSxcbiAgb3V0cHV0OiBbT3V0cHV0RWxlbWVudCwgJ2JvZHkgU2VjdXJpdHlTcGVjUmVmIGRvY3VtZW50YXRpb24gaGVhZGVyJ10sXG4gIGZhdWx0OiBbRWxlbWVudCwgJ19mYXVsdCBkb2N1bWVudGF0aW9uJ10sXG4gIGRlZmluaXRpb25zOiBbRGVmaW5pdGlvbnNFbGVtZW50LCAndHlwZXMgbWVzc2FnZSBwb3J0VHlwZSBiaW5kaW5nIHNlcnZpY2UgaW1wb3J0IGRvY3VtZW50YXRpb24nXSxcbiAgZG9jdW1lbnRhdGlvbjogW0RvY3VtZW50YXRpb25FbGVtZW50LCAnJ11cbn07XG5cbmZ1bmN0aW9uIG1hcEVsZW1lbnRUeXBlcyh0eXBlcykge1xuICBsZXQgcnRuID0ge307XG4gIHR5cGVzID0gdHlwZXMuc3BsaXQoJyAnKTtcbiAgdHlwZXMuZm9yRWFjaChmdW5jdGlvbiAodHlwZSkge1xuICAgIHJ0blt0eXBlLnJlcGxhY2UoL15fLywgJycpXSA9IChFbGVtZW50VHlwZU1hcFt0eXBlXSB8fCBbRWxlbWVudF0pWzBdO1xuICB9KTtcbiAgcmV0dXJuIHJ0bjtcbn1cblxuZm9yIChsZXQgbiBpbiBFbGVtZW50VHlwZU1hcCkge1xuICBsZXQgdiA9IEVsZW1lbnRUeXBlTWFwW25dO1xuICB2WzBdLnByb3RvdHlwZS5hbGxvd2VkQ2hpbGRyZW4gPSBtYXBFbGVtZW50VHlwZXModlsxXSk7XG59XG5cbk1lc3NhZ2VFbGVtZW50LnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24gKCkge1xuICB0aGlzLmVsZW1lbnQgPSBudWxsO1xuICB0aGlzLnBhcnRzID0gbnVsbDtcbn07XG5cblNjaGVtYUVsZW1lbnQucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiAoKSB7XG4gIHRoaXMuY29tcGxleFR5cGVzID0ge307XG4gIHRoaXMudHlwZXMgPSB7fTtcbiAgdGhpcy5lbGVtZW50cyA9IHt9O1xuICB0aGlzLmluY2x1ZGVzID0gW107XG59O1xuXG5UeXBlc0VsZW1lbnQucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiAoKSB7XG4gIHRoaXMuc2NoZW1hcyA9IHt9O1xufTtcblxuT3BlcmF0aW9uRWxlbWVudC5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcbiAgdGhpcy5pbnB1dCA9IG51bGw7XG4gIHRoaXMub3V0cHV0ID0gbnVsbDtcbiAgdGhpcy5pbnB1dFNvYXAgPSBudWxsO1xuICB0aGlzLm91dHB1dFNvYXAgPSBudWxsO1xuICB0aGlzLnN0eWxlID0gJyc7XG4gIHRoaXMuc29hcEFjdGlvbiA9ICcnO1xufTtcblxuUG9ydFR5cGVFbGVtZW50LnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24gKCkge1xuICB0aGlzLm1ldGhvZHMgPSB7fTtcbn07XG5cbkJpbmRpbmdFbGVtZW50LnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24gKCkge1xuICB0aGlzLnRyYW5zcG9ydCA9ICcnO1xuICB0aGlzLnN0eWxlID0gJyc7XG4gIHRoaXMubWV0aG9kcyA9IHt9O1xufTtcblxuUG9ydEVsZW1lbnQucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiAoKSB7XG4gIHRoaXMubG9jYXRpb24gPSBudWxsO1xufTtcblxuU2VydmljZUVsZW1lbnQucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiAoKSB7XG4gIHRoaXMucG9ydHMgPSB7fTtcbn07XG5cbkRlZmluaXRpb25zRWxlbWVudC5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcbiAgaWYgKHRoaXMubmFtZSAhPT0gJ2RlZmluaXRpb25zJykgdGhpcy51bmV4cGVjdGVkKHRoaXMubnNOYW1lKTtcbiAgdGhpcy5tZXNzYWdlcyA9IHt9O1xuICB0aGlzLnBvcnRUeXBlcyA9IHt9O1xuICB0aGlzLmJpbmRpbmdzID0ge307XG4gIHRoaXMuc2VydmljZXMgPSB7fTtcbiAgdGhpcy5zY2hlbWFzID0ge307XG59O1xuXG5Eb2N1bWVudGF0aW9uRWxlbWVudC5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcbn07XG5cblNjaGVtYUVsZW1lbnQucHJvdG90eXBlLm1lcmdlID0gZnVuY3Rpb24gKHNvdXJjZSkge1xuICBhc3NlcnQoc291cmNlIGluc3RhbmNlb2YgU2NoZW1hRWxlbWVudCk7XG4gIGlmICh0aGlzLiR0YXJnZXROYW1lc3BhY2UgPT09IHNvdXJjZS4kdGFyZ2V0TmFtZXNwYWNlKSB7XG4gICAgXy5tZXJnZSh0aGlzLmNvbXBsZXhUeXBlcywgc291cmNlLmNvbXBsZXhUeXBlcyk7XG4gICAgXy5tZXJnZSh0aGlzLnR5cGVzLCBzb3VyY2UudHlwZXMpO1xuICAgIF8ubWVyZ2UodGhpcy5lbGVtZW50cywgc291cmNlLmVsZW1lbnRzKTtcbiAgICBfLm1lcmdlKHRoaXMueG1sbnMsIHNvdXJjZS54bWxucyk7XG4gIH1cbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5cblNjaGVtYUVsZW1lbnQucHJvdG90eXBlLmFkZENoaWxkID0gZnVuY3Rpb24gKGNoaWxkKSB7XG4gIGlmIChjaGlsZC4kbmFtZSBpbiBQcmltaXRpdmVzKVxuICAgIHJldHVybjtcbiAgaWYgKGNoaWxkLm5hbWUgPT09ICdpbmNsdWRlJyB8fCBjaGlsZC5uYW1lID09PSAnaW1wb3J0Jykge1xuICAgIGxldCBsb2NhdGlvbiA9IGNoaWxkLiRzY2hlbWFMb2NhdGlvbiB8fCBjaGlsZC4kbG9jYXRpb247XG4gICAgaWYgKGxvY2F0aW9uKSB7XG4gICAgICB0aGlzLmluY2x1ZGVzLnB1c2goe1xuICAgICAgICBuYW1lc3BhY2U6IGNoaWxkLiRuYW1lc3BhY2UgfHwgY2hpbGQuJHRhcmdldE5hbWVzcGFjZSB8fCB0aGlzLiR0YXJnZXROYW1lc3BhY2UsXG4gICAgICAgIGxvY2F0aW9uOiBsb2NhdGlvblxuICAgICAgfSk7XG4gICAgfVxuICB9XG4gIGVsc2UgaWYgKGNoaWxkLm5hbWUgPT09ICdjb21wbGV4VHlwZScpIHtcbiAgICB0aGlzLmNvbXBsZXhUeXBlc1tjaGlsZC4kbmFtZV0gPSBjaGlsZDtcbiAgfVxuICBlbHNlIGlmIChjaGlsZC5uYW1lID09PSAnZWxlbWVudCcpIHtcbiAgICB0aGlzLmVsZW1lbnRzW2NoaWxkLiRuYW1lXSA9IGNoaWxkO1xuICB9XG4gIGVsc2UgaWYgKGNoaWxkLiRuYW1lKSB7XG4gICAgdGhpcy50eXBlc1tjaGlsZC4kbmFtZV0gPSBjaGlsZDtcbiAgfVxuICB0aGlzLmNoaWxkcmVuLnBvcCgpO1xuICAvLyBjaGlsZC5kZWxldGVGaXhlZEF0dHJzKCk7XG59O1xuLy9maXgjMzI1XG5UeXBlc0VsZW1lbnQucHJvdG90eXBlLmFkZENoaWxkID0gZnVuY3Rpb24gKGNoaWxkKSB7XG4gIGFzc2VydChjaGlsZCBpbnN0YW5jZW9mIFNjaGVtYUVsZW1lbnQpO1xuXG4gIGxldCB0YXJnZXROYW1lc3BhY2UgPSBjaGlsZC4kdGFyZ2V0TmFtZXNwYWNlO1xuXG4gIGlmICghdGhpcy5zY2hlbWFzLmhhc093blByb3BlcnR5KHRhcmdldE5hbWVzcGFjZSkpIHtcbiAgICB0aGlzLnNjaGVtYXNbdGFyZ2V0TmFtZXNwYWNlXSA9IGNoaWxkO1xuICB9IGVsc2Uge1xuICAgIGNvbnNvbGUuZXJyb3IoJ1RhcmdldC1OYW1lc3BhY2UgXCInICsgdGFyZ2V0TmFtZXNwYWNlICsgJ1wiIGFscmVhZHkgaW4gdXNlIGJ5IGFub3RoZXIgU2NoZW1hIScpO1xuICB9XG59O1xuXG5JbnB1dEVsZW1lbnQucHJvdG90eXBlLmFkZENoaWxkID0gZnVuY3Rpb24gKGNoaWxkKSB7XG4gIGlmIChjaGlsZC5uYW1lID09PSAnYm9keScpIHtcbiAgICB0aGlzLnVzZSA9IGNoaWxkLiR1c2U7XG4gICAgaWYgKHRoaXMudXNlID09PSAnZW5jb2RlZCcpIHtcbiAgICAgIHRoaXMuZW5jb2RpbmdTdHlsZSA9IGNoaWxkLiRlbmNvZGluZ1N0eWxlO1xuICAgIH1cbiAgICB0aGlzLmNoaWxkcmVuLnBvcCgpO1xuICB9XG59O1xuXG5PdXRwdXRFbGVtZW50LnByb3RvdHlwZS5hZGRDaGlsZCA9IGZ1bmN0aW9uIChjaGlsZCkge1xuICBpZiAoY2hpbGQubmFtZSA9PT0gJ2JvZHknKSB7XG4gICAgdGhpcy51c2UgPSBjaGlsZC4kdXNlO1xuICAgIGlmICh0aGlzLnVzZSA9PT0gJ2VuY29kZWQnKSB7XG4gICAgICB0aGlzLmVuY29kaW5nU3R5bGUgPSBjaGlsZC4kZW5jb2RpbmdTdHlsZTtcbiAgICB9XG4gICAgdGhpcy5jaGlsZHJlbi5wb3AoKTtcbiAgfVxufTtcblxuT3BlcmF0aW9uRWxlbWVudC5wcm90b3R5cGUuYWRkQ2hpbGQgPSBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgaWYgKGNoaWxkLm5hbWUgPT09ICdvcGVyYXRpb24nKSB7XG4gICAgdGhpcy5zb2FwQWN0aW9uID0gY2hpbGQuJHNvYXBBY3Rpb24gfHwgJyc7XG4gICAgdGhpcy5zdHlsZSA9IGNoaWxkLiRzdHlsZSB8fCAnJztcbiAgICB0aGlzLmNoaWxkcmVuLnBvcCgpO1xuICB9XG59O1xuXG5CaW5kaW5nRWxlbWVudC5wcm90b3R5cGUuYWRkQ2hpbGQgPSBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgaWYgKGNoaWxkLm5hbWUgPT09ICdiaW5kaW5nJykge1xuICAgIHRoaXMudHJhbnNwb3J0ID0gY2hpbGQuJHRyYW5zcG9ydDtcbiAgICB0aGlzLnN0eWxlID0gY2hpbGQuJHN0eWxlO1xuICAgIHRoaXMuY2hpbGRyZW4ucG9wKCk7XG4gIH1cbn07XG5cblBvcnRFbGVtZW50LnByb3RvdHlwZS5hZGRDaGlsZCA9IGZ1bmN0aW9uIChjaGlsZCkge1xuICBpZiAoY2hpbGQubmFtZSA9PT0gJ2FkZHJlc3MnICYmIHR5cGVvZiAoY2hpbGQuJGxvY2F0aW9uKSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICB0aGlzLmxvY2F0aW9uID0gY2hpbGQuJGxvY2F0aW9uO1xuICB9XG59O1xuXG5EZWZpbml0aW9uc0VsZW1lbnQucHJvdG90eXBlLmFkZENoaWxkID0gZnVuY3Rpb24gKGNoaWxkKSB7XG4gIGxldCBzZWxmID0gdGhpcztcbiAgaWYgKGNoaWxkIGluc3RhbmNlb2YgVHlwZXNFbGVtZW50KSB7XG4gICAgLy8gTWVyZ2UgdHlwZXMuc2NoZW1hcyBpbnRvIGRlZmluaXRpb25zLnNjaGVtYXNcbiAgICBfLm1lcmdlKHNlbGYuc2NoZW1hcywgY2hpbGQuc2NoZW1hcyk7XG4gIH1cbiAgZWxzZSBpZiAoY2hpbGQgaW5zdGFuY2VvZiBNZXNzYWdlRWxlbWVudCkge1xuICAgIHNlbGYubWVzc2FnZXNbY2hpbGQuJG5hbWVdID0gY2hpbGQ7XG4gIH1cbiAgZWxzZSBpZiAoY2hpbGQubmFtZSA9PT0gJ2ltcG9ydCcpIHtcbiAgICBzZWxmLnNjaGVtYXNbY2hpbGQuJG5hbWVzcGFjZV0gPSBuZXcgU2NoZW1hRWxlbWVudChjaGlsZC4kbmFtZXNwYWNlLCB7fSk7XG4gICAgc2VsZi5zY2hlbWFzW2NoaWxkLiRuYW1lc3BhY2VdLmFkZENoaWxkKGNoaWxkKTtcbiAgfVxuICBlbHNlIGlmIChjaGlsZCBpbnN0YW5jZW9mIFBvcnRUeXBlRWxlbWVudCkge1xuICAgIHNlbGYucG9ydFR5cGVzW2NoaWxkLiRuYW1lXSA9IGNoaWxkO1xuICB9XG4gIGVsc2UgaWYgKGNoaWxkIGluc3RhbmNlb2YgQmluZGluZ0VsZW1lbnQpIHtcbiAgICBpZiAoY2hpbGQudHJhbnNwb3J0ID09PSAnaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvc29hcC9odHRwJyB8fFxuICAgICAgY2hpbGQudHJhbnNwb3J0ID09PSAnaHR0cDovL3d3dy53My5vcmcvMjAwMy8wNS9zb2FwL2JpbmRpbmdzL0hUVFAvJylcbiAgICAgIHNlbGYuYmluZGluZ3NbY2hpbGQuJG5hbWVdID0gY2hpbGQ7XG4gIH1cbiAgZWxzZSBpZiAoY2hpbGQgaW5zdGFuY2VvZiBTZXJ2aWNlRWxlbWVudCkge1xuICAgIHNlbGYuc2VydmljZXNbY2hpbGQuJG5hbWVdID0gY2hpbGQ7XG4gIH1cbiAgZWxzZSBpZiAoY2hpbGQgaW5zdGFuY2VvZiBEb2N1bWVudGF0aW9uRWxlbWVudCkge1xuICB9XG4gIHRoaXMuY2hpbGRyZW4ucG9wKCk7XG59O1xuXG5NZXNzYWdlRWxlbWVudC5wcm90b3R5cGUucG9zdFByb2Nlc3MgPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMpIHtcbiAgbGV0IHBhcnQgPSBudWxsO1xuICBsZXQgY2hpbGQgPSB1bmRlZmluZWQ7XG4gIGxldCBjaGlsZHJlbiA9IHRoaXMuY2hpbGRyZW4gfHwgW107XG4gIGxldCBucyA9IHVuZGVmaW5lZDtcbiAgbGV0IG5zTmFtZSA9IHVuZGVmaW5lZDtcbiAgbGV0IGkgPSB1bmRlZmluZWQ7XG4gIGxldCB0eXBlID0gdW5kZWZpbmVkO1xuXG4gIGZvciAoaSBpbiBjaGlsZHJlbikge1xuICAgIGlmICgoY2hpbGQgPSBjaGlsZHJlbltpXSkubmFtZSA9PT0gJ3BhcnQnKSB7XG4gICAgICBwYXJ0ID0gY2hpbGQ7XG4gICAgICBicmVhaztcbiAgICB9XG4gIH1cblxuICBpZiAoIXBhcnQpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICBpZiAocGFydC4kZWxlbWVudCkge1xuICAgIGxldCBsb29rdXBUeXBlcyA9IFtdLFxuICAgICAgZWxlbWVudENoaWxkcmVuO1xuXG4gICAgZGVsZXRlIHRoaXMucGFydHM7XG5cbiAgICBuc05hbWUgPSBzcGxpdFFOYW1lKHBhcnQuJGVsZW1lbnQpO1xuICAgIG5zID0gbnNOYW1lLnByZWZpeDtcbiAgICBsZXQgc2NoZW1hID0gZGVmaW5pdGlvbnMuc2NoZW1hc1tkZWZpbml0aW9ucy54bWxuc1tuc11dO1xuICAgIHRoaXMuZWxlbWVudCA9IHNjaGVtYS5lbGVtZW50c1tuc05hbWUubmFtZV07XG4gICAgaWYgKCF0aGlzLmVsZW1lbnQpIHtcbiAgICAgIC8vIGRlYnVnKG5zTmFtZS5uYW1lICsgXCIgaXMgbm90IHByZXNlbnQgaW4gd3NkbCBhbmQgY2Fubm90IGJlIHByb2Nlc3NlZCBjb3JyZWN0bHkuXCIpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLmVsZW1lbnQudGFyZ2V0TlNBbGlhcyA9IG5zO1xuICAgIHRoaXMuZWxlbWVudC50YXJnZXROYW1lc3BhY2UgPSBkZWZpbml0aW9ucy54bWxuc1tuc107XG5cbiAgICAvLyBzZXQgdGhlIG9wdGlvbmFsICRsb29rdXBUeXBlIHRvIGJlIHVzZWQgd2l0aGluIGBjbGllbnQjX2ludm9rZSgpYCB3aGVuXG4gICAgLy8gY2FsbGluZyBgd3NkbCNvYmplY3RUb0RvY3VtZW50WE1MKClcbiAgICB0aGlzLmVsZW1lbnQuJGxvb2t1cFR5cGUgPSBwYXJ0LiRlbGVtZW50O1xuXG4gICAgZWxlbWVudENoaWxkcmVuID0gdGhpcy5lbGVtZW50LmNoaWxkcmVuO1xuXG4gICAgLy8gZ2V0IGFsbCBuZXN0ZWQgbG9va3VwIHR5cGVzIChvbmx5IGNvbXBsZXggdHlwZXMgYXJlIGZvbGxvd2VkKVxuICAgIGlmIChlbGVtZW50Q2hpbGRyZW4ubGVuZ3RoID4gMCkge1xuICAgICAgZm9yIChpID0gMDsgaSA8IGVsZW1lbnRDaGlsZHJlbi5sZW5ndGg7IGkrKykge1xuICAgICAgICBsb29rdXBUeXBlcy5wdXNoKHRoaXMuX2dldE5lc3RlZExvb2t1cFR5cGVTdHJpbmcoZWxlbWVudENoaWxkcmVuW2ldKSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gaWYgbmVzdGVkIGxvb2t1cCB0eXBlcyB3aGVyZSBmb3VuZCwgcHJlcGFyZSB0aGVtIGZvciBmdXJ0ZXIgdXNhZ2VcbiAgICBpZiAobG9va3VwVHlwZXMubGVuZ3RoID4gMCkge1xuICAgICAgbG9va3VwVHlwZXMgPSBsb29rdXBUeXBlcy5cbiAgICAgICAgam9pbignXycpLlxuICAgICAgICBzcGxpdCgnXycpLlxuICAgICAgICBmaWx0ZXIoZnVuY3Rpb24gcmVtb3ZlRW1wdHlMb29rdXBUeXBlcyh0eXBlKSB7XG4gICAgICAgICAgcmV0dXJuIHR5cGUgIT09ICdeJztcbiAgICAgICAgfSk7XG5cbiAgICAgIGxldCBzY2hlbWFYbWxucyA9IGRlZmluaXRpb25zLnNjaGVtYXNbdGhpcy5lbGVtZW50LnRhcmdldE5hbWVzcGFjZV0ueG1sbnM7XG5cbiAgICAgIGZvciAoaSA9IDA7IGkgPCBsb29rdXBUeXBlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICBsb29rdXBUeXBlc1tpXSA9IHRoaXMuX2NyZWF0ZUxvb2t1cFR5cGVPYmplY3QobG9va3VwVHlwZXNbaV0sIHNjaGVtYVhtbG5zKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLmVsZW1lbnQuJGxvb2t1cFR5cGVzID0gbG9va3VwVHlwZXM7XG5cbiAgICBpZiAodGhpcy5lbGVtZW50LiR0eXBlKSB7XG4gICAgICB0eXBlID0gc3BsaXRRTmFtZSh0aGlzLmVsZW1lbnQuJHR5cGUpO1xuICAgICAgbGV0IHR5cGVOcyA9IHNjaGVtYS54bWxucyAmJiBzY2hlbWEueG1sbnNbdHlwZS5wcmVmaXhdIHx8IGRlZmluaXRpb25zLnhtbG5zW3R5cGUucHJlZml4XTtcblxuICAgICAgaWYgKHR5cGVOcykge1xuICAgICAgICBpZiAodHlwZS5uYW1lIGluIFByaW1pdGl2ZXMpIHtcbiAgICAgICAgICAvLyB0aGlzLmVsZW1lbnQgPSB0aGlzLmVsZW1lbnQuJHR5cGU7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgLy8gZmlyc3QgY2hlY2sgbG9jYWwgbWFwcGluZyBvZiBucyBhbGlhcyB0byBuYW1lc3BhY2VcbiAgICAgICAgICBzY2hlbWEgPSBkZWZpbml0aW9ucy5zY2hlbWFzW3R5cGVOc107XG4gICAgICAgICAgbGV0IGN0eXBlID0gc2NoZW1hLmNvbXBsZXhUeXBlc1t0eXBlLm5hbWVdIHx8IHNjaGVtYS50eXBlc1t0eXBlLm5hbWVdIHx8IHNjaGVtYS5lbGVtZW50c1t0eXBlLm5hbWVdO1xuXG5cbiAgICAgICAgICBpZiAoY3R5cGUpIHtcbiAgICAgICAgICAgIHRoaXMucGFydHMgPSBjdHlwZS5kZXNjcmlwdGlvbihkZWZpbml0aW9ucywgc2NoZW1hLnhtbG5zKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICBsZXQgbWV0aG9kID0gdGhpcy5lbGVtZW50LmRlc2NyaXB0aW9uKGRlZmluaXRpb25zLCBzY2hlbWEueG1sbnMpO1xuICAgICAgdGhpcy5wYXJ0cyA9IG1ldGhvZFtuc05hbWUubmFtZV07XG4gICAgfVxuXG5cbiAgICB0aGlzLmNoaWxkcmVuLnNwbGljZSgwLCAxKTtcbiAgfSBlbHNlIHtcbiAgICAvLyBycGMgZW5jb2RpbmdcbiAgICB0aGlzLnBhcnRzID0ge307XG4gICAgZGVsZXRlIHRoaXMuZWxlbWVudDtcbiAgICBmb3IgKGkgPSAwOyBwYXJ0ID0gdGhpcy5jaGlsZHJlbltpXTsgaSsrKSB7XG4gICAgICBpZiAocGFydC5uYW1lID09PSAnZG9jdW1lbnRhdGlvbicpIHtcbiAgICAgICAgLy8gPHdzZGw6ZG9jdW1lbnRhdGlvbiBjYW4gYmUgcHJlc2VudCB1bmRlciA8d3NkbDptZXNzYWdlPlxuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cbiAgICAgIGFzc2VydChwYXJ0Lm5hbWUgPT09ICdwYXJ0JywgJ0V4cGVjdGVkIHBhcnQgZWxlbWVudCcpO1xuICAgICAgbnNOYW1lID0gc3BsaXRRTmFtZShwYXJ0LiR0eXBlKTtcbiAgICAgIG5zID0gZGVmaW5pdGlvbnMueG1sbnNbbnNOYW1lLnByZWZpeF07XG4gICAgICB0eXBlID0gbnNOYW1lLm5hbWU7XG4gICAgICBsZXQgc2NoZW1hRGVmaW5pdGlvbiA9IGRlZmluaXRpb25zLnNjaGVtYXNbbnNdO1xuICAgICAgaWYgKHR5cGVvZiBzY2hlbWFEZWZpbml0aW9uICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICB0aGlzLnBhcnRzW3BhcnQuJG5hbWVdID0gZGVmaW5pdGlvbnMuc2NoZW1hc1tuc10udHlwZXNbdHlwZV0gfHwgZGVmaW5pdGlvbnMuc2NoZW1hc1tuc10uY29tcGxleFR5cGVzW3R5cGVdO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5wYXJ0c1twYXJ0LiRuYW1lXSA9IHBhcnQuJHR5cGU7XG4gICAgICB9XG5cbiAgICAgIGlmICh0eXBlb2YgdGhpcy5wYXJ0c1twYXJ0LiRuYW1lXSA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgdGhpcy5wYXJ0c1twYXJ0LiRuYW1lXS5wcmVmaXggPSBuc05hbWUucHJlZml4O1xuICAgICAgICB0aGlzLnBhcnRzW3BhcnQuJG5hbWVdLnhtbG5zID0gbnM7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuY2hpbGRyZW4uc3BsaWNlKGktLSwgMSk7XG4gICAgfVxuICB9XG4gIHRoaXMuZGVsZXRlRml4ZWRBdHRycygpO1xufTtcblxuLyoqXG4gKiBUYWtlcyBhIGdpdmVuIG5hbWVzcGFjZWQgU3RyaW5nKGZvciBleGFtcGxlOiAnYWxpYXM6cHJvcGVydHknKSBhbmQgY3JlYXRlcyBhIGxvb2t1cFR5cGVcbiAqIG9iamVjdCBmb3IgZnVydGhlciB1c2UgaW4gYXMgZmlyc3QgKGxvb2t1cCkgYHBhcmFtZXRlclR5cGVPYmpgIHdpdGhpbiB0aGUgYG9iamVjdFRvWE1MYFxuICogbWV0aG9kIGFuZCBwcm92aWRlcyBhbiBlbnRyeSBwb2ludCBmb3IgdGhlIGFscmVhZHkgZXhpc3RpbmcgY29kZSBpbiBgZmluZENoaWxkU2NoZW1hT2JqZWN0YC5cbiAqXG4gKiBAbWV0aG9kIF9jcmVhdGVMb29rdXBUeXBlT2JqZWN0XG4gKiBAcGFyYW0ge1N0cmluZ30gICAgICAgICAgICBuc1N0cmluZyAgICAgICAgICBUaGUgTlMgU3RyaW5nIChmb3IgZXhhbXBsZSBcImFsaWFzOnR5cGVcIikuXG4gKiBAcGFyYW0ge09iamVjdH0gICAgICAgICAgICB4bWxucyAgICAgICBUaGUgZnVsbHkgcGFyc2VkIGB3c2RsYCBkZWZpbml0aW9ucyBvYmplY3QgKGluY2x1ZGluZyBhbGwgc2NoZW1hcykuXG4gKiBAcmV0dXJucyB7T2JqZWN0fVxuICogQHByaXZhdGVcbiAqL1xuTWVzc2FnZUVsZW1lbnQucHJvdG90eXBlLl9jcmVhdGVMb29rdXBUeXBlT2JqZWN0ID0gZnVuY3Rpb24gKG5zU3RyaW5nLCB4bWxucykge1xuICBsZXQgc3BsaXR0ZWROU1N0cmluZyA9IHNwbGl0UU5hbWUobnNTdHJpbmcpLFxuICAgIG5zQWxpYXMgPSBzcGxpdHRlZE5TU3RyaW5nLnByZWZpeCxcbiAgICBzcGxpdHRlZE5hbWUgPSBzcGxpdHRlZE5TU3RyaW5nLm5hbWUuc3BsaXQoJyMnKSxcbiAgICB0eXBlID0gc3BsaXR0ZWROYW1lWzBdLFxuICAgIG5hbWUgPSBzcGxpdHRlZE5hbWVbMV0sXG4gICAgbG9va3VwVHlwZU9iajogYW55ID0ge307XG5cbiAgbG9va3VwVHlwZU9iai4kbmFtZXNwYWNlID0geG1sbnNbbnNBbGlhc107XG4gIGxvb2t1cFR5cGVPYmouJHR5cGUgPSBuc0FsaWFzICsgJzonICsgdHlwZTtcbiAgbG9va3VwVHlwZU9iai4kbmFtZSA9IG5hbWU7XG5cbiAgcmV0dXJuIGxvb2t1cFR5cGVPYmo7XG59O1xuXG4vKipcbiAqIEl0ZXJhdGVzIHRocm91Z2ggdGhlIGVsZW1lbnQgYW5kIGV2ZXJ5IG5lc3RlZCBjaGlsZCB0byBmaW5kIGFueSBkZWZpbmVkIGAkdHlwZWBcbiAqIHByb3BlcnR5IGFuZCByZXR1cm5zIGl0IGluIGEgdW5kZXJzY29yZSAoJ18nKSBzZXBhcmF0ZWQgU3RyaW5nICh1c2luZyAnXicgYXMgZGVmYXVsdFxuICogdmFsdWUgaWYgbm8gYCR0eXBlYCBwcm9wZXJ0eSB3YXMgZm91bmQpLlxuICpcbiAqIEBtZXRob2QgX2dldE5lc3RlZExvb2t1cFR5cGVTdHJpbmdcbiAqIEBwYXJhbSB7T2JqZWN0fSAgICAgICAgICAgIGVsZW1lbnQgICAgICAgICBUaGUgZWxlbWVudCB3aGljaCAocHJvYmFibHkpIGNvbnRhaW5zIG5lc3RlZCBgJHR5cGVgIHZhbHVlcy5cbiAqIEByZXR1cm5zIHtTdHJpbmd9XG4gKiBAcHJpdmF0ZVxuICovXG5NZXNzYWdlRWxlbWVudC5wcm90b3R5cGUuX2dldE5lc3RlZExvb2t1cFR5cGVTdHJpbmcgPSBmdW5jdGlvbiAoZWxlbWVudCkge1xuICBsZXQgcmVzb2x2ZWRUeXBlID0gJ14nLFxuICAgIGV4Y2x1ZGVkID0gdGhpcy5pZ25vcmVkTmFtZXNwYWNlcy5jb25jYXQoJ3hzJyk7IC8vIGRvIG5vdCBwcm9jZXNzICR0eXBlIHZhbHVlcyB3aWNoIHN0YXJ0IHdpdGhcblxuICBpZiAoZWxlbWVudC5oYXNPd25Qcm9wZXJ0eSgnJHR5cGUnKSAmJiB0eXBlb2YgZWxlbWVudC4kdHlwZSA9PT0gJ3N0cmluZycpIHtcbiAgICBpZiAoZXhjbHVkZWQuaW5kZXhPZihlbGVtZW50LiR0eXBlLnNwbGl0KCc6JylbMF0pID09PSAtMSkge1xuICAgICAgcmVzb2x2ZWRUeXBlICs9ICgnXycgKyBlbGVtZW50LiR0eXBlICsgJyMnICsgZWxlbWVudC4kbmFtZSk7XG4gICAgfVxuICB9XG5cbiAgaWYgKGVsZW1lbnQuY2hpbGRyZW4ubGVuZ3RoID4gMCkge1xuICAgIGxldCBzZWxmID0gdGhpcztcblxuICAgIGVsZW1lbnQuY2hpbGRyZW4uZm9yRWFjaChmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgIGxldCByZXNvbHZlZENoaWxkVHlwZSA9IHNlbGYuX2dldE5lc3RlZExvb2t1cFR5cGVTdHJpbmcoY2hpbGQpLnJlcGxhY2UoL1xcXl8vLCAnJyk7XG5cbiAgICAgIGlmIChyZXNvbHZlZENoaWxkVHlwZSAmJiB0eXBlb2YgcmVzb2x2ZWRDaGlsZFR5cGUgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHJlc29sdmVkVHlwZSArPSAoJ18nICsgcmVzb2x2ZWRDaGlsZFR5cGUpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcmV0dXJuIHJlc29sdmVkVHlwZTtcbn07XG5cbk9wZXJhdGlvbkVsZW1lbnQucHJvdG90eXBlLnBvc3RQcm9jZXNzID0gZnVuY3Rpb24gKGRlZmluaXRpb25zLCB0YWcpIHtcbiAgbGV0IGNoaWxkcmVuID0gdGhpcy5jaGlsZHJlbjtcbiAgZm9yIChsZXQgaSA9IDAsIGNoaWxkOyBjaGlsZCA9IGNoaWxkcmVuW2ldOyBpKyspIHtcbiAgICBpZiAoY2hpbGQubmFtZSAhPT0gJ2lucHV0JyAmJiBjaGlsZC5uYW1lICE9PSAnb3V0cHV0JylcbiAgICAgIGNvbnRpbnVlO1xuICAgIGlmICh0YWcgPT09ICdiaW5kaW5nJykge1xuICAgICAgdGhpc1tjaGlsZC5uYW1lXSA9IGNoaWxkO1xuICAgICAgY2hpbGRyZW4uc3BsaWNlKGktLSwgMSk7XG4gICAgICBjb250aW51ZTtcbiAgICB9XG4gICAgbGV0IG1lc3NhZ2VOYW1lID0gc3BsaXRRTmFtZShjaGlsZC4kbWVzc2FnZSkubmFtZTtcbiAgICBsZXQgbWVzc2FnZSA9IGRlZmluaXRpb25zLm1lc3NhZ2VzW21lc3NhZ2VOYW1lXTtcbiAgICBtZXNzYWdlLnBvc3RQcm9jZXNzKGRlZmluaXRpb25zKTtcbiAgICBpZiAobWVzc2FnZS5lbGVtZW50KSB7XG4gICAgICBkZWZpbml0aW9ucy5tZXNzYWdlc1ttZXNzYWdlLmVsZW1lbnQuJG5hbWVdID0gbWVzc2FnZTtcbiAgICAgIHRoaXNbY2hpbGQubmFtZV0gPSBtZXNzYWdlLmVsZW1lbnQ7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgdGhpc1tjaGlsZC5uYW1lXSA9IG1lc3NhZ2U7XG4gICAgfVxuICAgIGNoaWxkcmVuLnNwbGljZShpLS0sIDEpO1xuICB9XG4gIHRoaXMuZGVsZXRlRml4ZWRBdHRycygpO1xufTtcblxuUG9ydFR5cGVFbGVtZW50LnByb3RvdHlwZS5wb3N0UHJvY2VzcyA9IGZ1bmN0aW9uIChkZWZpbml0aW9ucykge1xuICBsZXQgY2hpbGRyZW4gPSB0aGlzLmNoaWxkcmVuO1xuICBpZiAodHlwZW9mIGNoaWxkcmVuID09PSAndW5kZWZpbmVkJylcbiAgICByZXR1cm47XG4gIGZvciAobGV0IGkgPSAwLCBjaGlsZDsgY2hpbGQgPSBjaGlsZHJlbltpXTsgaSsrKSB7XG4gICAgaWYgKGNoaWxkLm5hbWUgIT09ICdvcGVyYXRpb24nKVxuICAgICAgY29udGludWU7XG4gICAgY2hpbGQucG9zdFByb2Nlc3MoZGVmaW5pdGlvbnMsICdwb3J0VHlwZScpO1xuICAgIHRoaXMubWV0aG9kc1tjaGlsZC4kbmFtZV0gPSBjaGlsZDtcbiAgICBjaGlsZHJlbi5zcGxpY2UoaS0tLCAxKTtcbiAgfVxuICBkZWxldGUgdGhpcy4kbmFtZTtcbiAgdGhpcy5kZWxldGVGaXhlZEF0dHJzKCk7XG59O1xuXG5CaW5kaW5nRWxlbWVudC5wcm90b3R5cGUucG9zdFByb2Nlc3MgPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMpIHtcbiAgbGV0IHR5cGUgPSBzcGxpdFFOYW1lKHRoaXMuJHR5cGUpLm5hbWUsXG4gICAgcG9ydFR5cGUgPSBkZWZpbml0aW9ucy5wb3J0VHlwZXNbdHlwZV0sXG4gICAgc3R5bGUgPSB0aGlzLnN0eWxlLFxuICAgIGNoaWxkcmVuID0gdGhpcy5jaGlsZHJlbjtcbiAgaWYgKHBvcnRUeXBlKSB7XG4gICAgcG9ydFR5cGUucG9zdFByb2Nlc3MoZGVmaW5pdGlvbnMpO1xuICAgIHRoaXMubWV0aG9kcyA9IHBvcnRUeXBlLm1ldGhvZHM7XG5cbiAgICBmb3IgKGxldCBpID0gMCwgY2hpbGQ7IGNoaWxkID0gY2hpbGRyZW5baV07IGkrKykge1xuICAgICAgaWYgKGNoaWxkLm5hbWUgIT09ICdvcGVyYXRpb24nKVxuICAgICAgICBjb250aW51ZTtcbiAgICAgIGNoaWxkLnBvc3RQcm9jZXNzKGRlZmluaXRpb25zLCAnYmluZGluZycpO1xuICAgICAgY2hpbGRyZW4uc3BsaWNlKGktLSwgMSk7XG4gICAgICBjaGlsZC5zdHlsZSB8fCAoY2hpbGQuc3R5bGUgPSBzdHlsZSk7XG4gICAgICBsZXQgbWV0aG9kID0gdGhpcy5tZXRob2RzW2NoaWxkLiRuYW1lXTtcblxuICAgICAgaWYgKG1ldGhvZCkge1xuICAgICAgICBtZXRob2Quc3R5bGUgPSBjaGlsZC5zdHlsZTtcbiAgICAgICAgbWV0aG9kLnNvYXBBY3Rpb24gPSBjaGlsZC5zb2FwQWN0aW9uO1xuICAgICAgICBtZXRob2QuaW5wdXRTb2FwID0gY2hpbGQuaW5wdXQgfHwgbnVsbDtcbiAgICAgICAgbWV0aG9kLm91dHB1dFNvYXAgPSBjaGlsZC5vdXRwdXQgfHwgbnVsbDtcbiAgICAgICAgbWV0aG9kLmlucHV0U29hcCAmJiBtZXRob2QuaW5wdXRTb2FwLmRlbGV0ZUZpeGVkQXR0cnMoKTtcbiAgICAgICAgbWV0aG9kLm91dHB1dFNvYXAgJiYgbWV0aG9kLm91dHB1dFNvYXAuZGVsZXRlRml4ZWRBdHRycygpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuICBkZWxldGUgdGhpcy4kbmFtZTtcbiAgZGVsZXRlIHRoaXMuJHR5cGU7XG4gIHRoaXMuZGVsZXRlRml4ZWRBdHRycygpO1xufTtcblxuU2VydmljZUVsZW1lbnQucHJvdG90eXBlLnBvc3RQcm9jZXNzID0gZnVuY3Rpb24gKGRlZmluaXRpb25zKSB7XG4gIGxldCBjaGlsZHJlbiA9IHRoaXMuY2hpbGRyZW4sXG4gICAgYmluZGluZ3MgPSBkZWZpbml0aW9ucy5iaW5kaW5ncztcbiAgaWYgKGNoaWxkcmVuICYmIGNoaWxkcmVuLmxlbmd0aCA+IDApIHtcbiAgICBmb3IgKGxldCBpID0gMCwgY2hpbGQ7IGNoaWxkID0gY2hpbGRyZW5baV07IGkrKykge1xuICAgICAgaWYgKGNoaWxkLm5hbWUgIT09ICdwb3J0JylcbiAgICAgICAgY29udGludWU7XG4gICAgICBsZXQgYmluZGluZ05hbWUgPSBzcGxpdFFOYW1lKGNoaWxkLiRiaW5kaW5nKS5uYW1lO1xuICAgICAgbGV0IGJpbmRpbmcgPSBiaW5kaW5nc1tiaW5kaW5nTmFtZV07XG4gICAgICBpZiAoYmluZGluZykge1xuICAgICAgICBiaW5kaW5nLnBvc3RQcm9jZXNzKGRlZmluaXRpb25zKTtcbiAgICAgICAgdGhpcy5wb3J0c1tjaGlsZC4kbmFtZV0gPSB7XG4gICAgICAgICAgbG9jYXRpb246IGNoaWxkLmxvY2F0aW9uLFxuICAgICAgICAgIGJpbmRpbmc6IGJpbmRpbmdcbiAgICAgICAgfTtcbiAgICAgICAgY2hpbGRyZW4uc3BsaWNlKGktLSwgMSk7XG4gICAgICB9XG4gICAgfVxuICB9XG4gIGRlbGV0ZSB0aGlzLiRuYW1lO1xuICB0aGlzLmRlbGV0ZUZpeGVkQXR0cnMoKTtcbn07XG5cblxuU2ltcGxlVHlwZUVsZW1lbnQucHJvdG90eXBlLmRlc2NyaXB0aW9uID0gZnVuY3Rpb24gKGRlZmluaXRpb25zKSB7XG4gIGxldCBjaGlsZHJlbiA9IHRoaXMuY2hpbGRyZW47XG4gIGZvciAobGV0IGkgPSAwLCBjaGlsZDsgY2hpbGQgPSBjaGlsZHJlbltpXTsgaSsrKSB7XG4gICAgaWYgKGNoaWxkIGluc3RhbmNlb2YgUmVzdHJpY3Rpb25FbGVtZW50KVxuICAgICAgcmV0dXJuIHRoaXMuJG5hbWUgKyBcInxcIiArIGNoaWxkLmRlc2NyaXB0aW9uKCk7XG4gIH1cbiAgcmV0dXJuIHt9O1xufTtcblxuUmVzdHJpY3Rpb25FbGVtZW50LnByb3RvdHlwZS5kZXNjcmlwdGlvbiA9IGZ1bmN0aW9uIChkZWZpbml0aW9ucywgeG1sbnMpIHtcbiAgbGV0IGNoaWxkcmVuID0gdGhpcy5jaGlsZHJlbjtcbiAgbGV0IGRlc2M7XG4gIGZvciAobGV0IGkgPSAwLCBjaGlsZDsgY2hpbGQgPSBjaGlsZHJlbltpXTsgaSsrKSB7XG4gICAgaWYgKGNoaWxkIGluc3RhbmNlb2YgU2VxdWVuY2VFbGVtZW50IHx8XG4gICAgICBjaGlsZCBpbnN0YW5jZW9mIENob2ljZUVsZW1lbnQpIHtcbiAgICAgIGRlc2MgPSBjaGlsZC5kZXNjcmlwdGlvbihkZWZpbml0aW9ucywgeG1sbnMpO1xuICAgICAgYnJlYWs7XG4gICAgfVxuICB9XG4gIGlmIChkZXNjICYmIHRoaXMuJGJhc2UpIHtcbiAgICBsZXQgdHlwZSA9IHNwbGl0UU5hbWUodGhpcy4kYmFzZSksXG4gICAgICB0eXBlTmFtZSA9IHR5cGUubmFtZSxcbiAgICAgIG5zID0geG1sbnMgJiYgeG1sbnNbdHlwZS5wcmVmaXhdIHx8IGRlZmluaXRpb25zLnhtbG5zW3R5cGUucHJlZml4XSxcbiAgICAgIHNjaGVtYSA9IGRlZmluaXRpb25zLnNjaGVtYXNbbnNdLFxuICAgICAgdHlwZUVsZW1lbnQgPSBzY2hlbWEgJiYgKHNjaGVtYS5jb21wbGV4VHlwZXNbdHlwZU5hbWVdIHx8IHNjaGVtYS50eXBlc1t0eXBlTmFtZV0gfHwgc2NoZW1hLmVsZW1lbnRzW3R5cGVOYW1lXSk7XG5cbiAgICBkZXNjLmdldEJhc2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gdHlwZUVsZW1lbnQuZGVzY3JpcHRpb24oZGVmaW5pdGlvbnMsIHNjaGVtYS54bWxucyk7XG4gICAgfTtcbiAgICByZXR1cm4gZGVzYztcbiAgfVxuXG4gIC8vIHRoZW4gc2ltcGxlIGVsZW1lbnRcbiAgbGV0IGJhc2UgPSB0aGlzLiRiYXNlID8gdGhpcy4kYmFzZSArIFwifFwiIDogXCJcIjtcbiAgcmV0dXJuIGJhc2UgKyB0aGlzLmNoaWxkcmVuLm1hcChmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICByZXR1cm4gY2hpbGQuZGVzY3JpcHRpb24oKTtcbiAgfSkuam9pbihcIixcIik7XG59O1xuXG5FeHRlbnNpb25FbGVtZW50LnByb3RvdHlwZS5kZXNjcmlwdGlvbiA9IGZ1bmN0aW9uIChkZWZpbml0aW9ucywgeG1sbnMpIHtcbiAgbGV0IGNoaWxkcmVuID0gdGhpcy5jaGlsZHJlbjtcbiAgbGV0IGRlc2MgPSB7fTtcbiAgZm9yIChsZXQgaSA9IDAsIGNoaWxkOyBjaGlsZCA9IGNoaWxkcmVuW2ldOyBpKyspIHtcbiAgICBpZiAoY2hpbGQgaW5zdGFuY2VvZiBTZXF1ZW5jZUVsZW1lbnQgfHxcbiAgICAgIGNoaWxkIGluc3RhbmNlb2YgQ2hvaWNlRWxlbWVudCkge1xuICAgICAgZGVzYyA9IGNoaWxkLmRlc2NyaXB0aW9uKGRlZmluaXRpb25zLCB4bWxucyk7XG4gICAgfVxuICB9XG4gIGlmICh0aGlzLiRiYXNlKSB7XG4gICAgbGV0IHR5cGUgPSBzcGxpdFFOYW1lKHRoaXMuJGJhc2UpLFxuICAgICAgdHlwZU5hbWUgPSB0eXBlLm5hbWUsXG4gICAgICBucyA9IHhtbG5zICYmIHhtbG5zW3R5cGUucHJlZml4XSB8fCBkZWZpbml0aW9ucy54bWxuc1t0eXBlLnByZWZpeF0sXG4gICAgICBzY2hlbWEgPSBkZWZpbml0aW9ucy5zY2hlbWFzW25zXTtcblxuICAgIGlmICh0eXBlTmFtZSBpbiBQcmltaXRpdmVzKSB7XG4gICAgICByZXR1cm4gdGhpcy4kYmFzZTtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICBsZXQgdHlwZUVsZW1lbnQgPSBzY2hlbWEgJiYgKHNjaGVtYS5jb21wbGV4VHlwZXNbdHlwZU5hbWVdIHx8XG4gICAgICAgIHNjaGVtYS50eXBlc1t0eXBlTmFtZV0gfHwgc2NoZW1hLmVsZW1lbnRzW3R5cGVOYW1lXSk7XG5cbiAgICAgIGlmICh0eXBlRWxlbWVudCkge1xuICAgICAgICBsZXQgYmFzZSA9IHR5cGVFbGVtZW50LmRlc2NyaXB0aW9uKGRlZmluaXRpb25zLCBzY2hlbWEueG1sbnMpO1xuICAgICAgICBkZXNjID0gXy5kZWZhdWx0c0RlZXAoYmFzZSwgZGVzYyk7XG4gICAgICB9XG4gICAgfVxuICB9XG4gIHJldHVybiBkZXNjO1xufTtcblxuRW51bWVyYXRpb25FbGVtZW50LnByb3RvdHlwZS5kZXNjcmlwdGlvbiA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXNbdGhpcy52YWx1ZUtleV07XG59O1xuXG5Db21wbGV4VHlwZUVsZW1lbnQucHJvdG90eXBlLmRlc2NyaXB0aW9uID0gZnVuY3Rpb24gKGRlZmluaXRpb25zLCB4bWxucykge1xuICBsZXQgY2hpbGRyZW4gPSB0aGlzLmNoaWxkcmVuIHx8IFtdO1xuICBmb3IgKGxldCBpID0gMCwgY2hpbGQ7IGNoaWxkID0gY2hpbGRyZW5baV07IGkrKykge1xuICAgIGlmIChjaGlsZCBpbnN0YW5jZW9mIENob2ljZUVsZW1lbnQgfHxcbiAgICAgIGNoaWxkIGluc3RhbmNlb2YgU2VxdWVuY2VFbGVtZW50IHx8XG4gICAgICBjaGlsZCBpbnN0YW5jZW9mIEFsbEVsZW1lbnQgfHxcbiAgICAgIGNoaWxkIGluc3RhbmNlb2YgU2ltcGxlQ29udGVudEVsZW1lbnQgfHxcbiAgICAgIGNoaWxkIGluc3RhbmNlb2YgQ29tcGxleENvbnRlbnRFbGVtZW50KSB7XG5cbiAgICAgIHJldHVybiBjaGlsZC5kZXNjcmlwdGlvbihkZWZpbml0aW9ucywgeG1sbnMpO1xuICAgIH1cbiAgfVxuICByZXR1cm4ge307XG59O1xuXG5Db21wbGV4Q29udGVudEVsZW1lbnQucHJvdG90eXBlLmRlc2NyaXB0aW9uID0gZnVuY3Rpb24gKGRlZmluaXRpb25zLCB4bWxucykge1xuICBsZXQgY2hpbGRyZW4gPSB0aGlzLmNoaWxkcmVuO1xuICBmb3IgKGxldCBpID0gMCwgY2hpbGQ7IGNoaWxkID0gY2hpbGRyZW5baV07IGkrKykge1xuICAgIGlmIChjaGlsZCBpbnN0YW5jZW9mIEV4dGVuc2lvbkVsZW1lbnQpIHtcbiAgICAgIHJldHVybiBjaGlsZC5kZXNjcmlwdGlvbihkZWZpbml0aW9ucywgeG1sbnMpO1xuICAgIH1cbiAgfVxuICByZXR1cm4ge307XG59O1xuXG5TaW1wbGVDb250ZW50RWxlbWVudC5wcm90b3R5cGUuZGVzY3JpcHRpb24gPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMsIHhtbG5zKSB7XG4gIGxldCBjaGlsZHJlbiA9IHRoaXMuY2hpbGRyZW47XG4gIGZvciAobGV0IGkgPSAwLCBjaGlsZDsgY2hpbGQgPSBjaGlsZHJlbltpXTsgaSsrKSB7XG4gICAgaWYgKGNoaWxkIGluc3RhbmNlb2YgRXh0ZW5zaW9uRWxlbWVudCkge1xuICAgICAgcmV0dXJuIGNoaWxkLmRlc2NyaXB0aW9uKGRlZmluaXRpb25zLCB4bWxucyk7XG4gICAgfVxuICB9XG4gIHJldHVybiB7fTtcbn07XG5cbkVsZW1lbnRFbGVtZW50LnByb3RvdHlwZS5kZXNjcmlwdGlvbiA9IGZ1bmN0aW9uIChkZWZpbml0aW9ucywgeG1sbnMpIHtcbiAgbGV0IGVsZW1lbnQgPSB7fSxcbiAgICBuYW1lID0gdGhpcy4kbmFtZTtcbiAgbGV0IGlzTWFueSA9ICF0aGlzLiRtYXhPY2N1cnMgPyBmYWxzZSA6IChpc05hTih0aGlzLiRtYXhPY2N1cnMpID8gKHRoaXMuJG1heE9jY3VycyA9PT0gJ3VuYm91bmRlZCcpIDogKHRoaXMuJG1heE9jY3VycyA+IDEpKTtcbiAgaWYgKHRoaXMuJG1pbk9jY3VycyAhPT0gdGhpcy4kbWF4T2NjdXJzICYmIGlzTWFueSkge1xuICAgIG5hbWUgKz0gJ1tdJztcbiAgfVxuXG4gIGlmICh4bWxucyAmJiB4bWxuc1tUTlNfUFJFRklYXSkge1xuICAgIHRoaXMuJHRhcmdldE5hbWVzcGFjZSA9IHhtbG5zW1ROU19QUkVGSVhdO1xuICB9XG4gIGxldCB0eXBlID0gdGhpcy4kdHlwZSB8fCB0aGlzLiRyZWY7XG4gIGlmICh0eXBlKSB7XG4gICAgdHlwZSA9IHNwbGl0UU5hbWUodHlwZSk7XG4gICAgbGV0IHR5cGVOYW1lID0gdHlwZS5uYW1lLFxuICAgICAgbnMgPSB4bWxucyAmJiB4bWxuc1t0eXBlLnByZWZpeF0gfHwgZGVmaW5pdGlvbnMueG1sbnNbdHlwZS5wcmVmaXhdLFxuICAgICAgc2NoZW1hID0gZGVmaW5pdGlvbnMuc2NoZW1hc1tuc10sXG4gICAgICB0eXBlRWxlbWVudCA9IHNjaGVtYSAmJiAodGhpcy4kdHlwZSA/IHNjaGVtYS5jb21wbGV4VHlwZXNbdHlwZU5hbWVdIHx8IHNjaGVtYS50eXBlc1t0eXBlTmFtZV0gOiBzY2hlbWEuZWxlbWVudHNbdHlwZU5hbWVdKTtcblxuICAgIGlmIChucyAmJiBkZWZpbml0aW9ucy5zY2hlbWFzW25zXSkge1xuICAgICAgeG1sbnMgPSBkZWZpbml0aW9ucy5zY2hlbWFzW25zXS54bWxucztcbiAgICB9XG5cbiAgICBpZiAodHlwZUVsZW1lbnQgJiYgISh0eXBlTmFtZSBpbiBQcmltaXRpdmVzKSkge1xuXG4gICAgICBpZiAoISh0eXBlTmFtZSBpbiBkZWZpbml0aW9ucy5kZXNjcmlwdGlvbnMudHlwZXMpKSB7XG5cbiAgICAgICAgbGV0IGVsZW06IGFueSA9IHt9O1xuICAgICAgICBkZWZpbml0aW9ucy5kZXNjcmlwdGlvbnMudHlwZXNbdHlwZU5hbWVdID0gZWxlbTtcbiAgICAgICAgbGV0IGRlc2NyaXB0aW9uID0gdHlwZUVsZW1lbnQuZGVzY3JpcHRpb24oZGVmaW5pdGlvbnMsIHhtbG5zKTtcbiAgICAgICAgaWYgKHR5cGVvZiBkZXNjcmlwdGlvbiA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBlbGVtID0gZGVzY3JpcHRpb247XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgT2JqZWN0LmtleXMoZGVzY3JpcHRpb24pLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgICAgZWxlbVtrZXldID0gZGVzY3JpcHRpb25ba2V5XTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0aGlzLiRyZWYpIHtcbiAgICAgICAgICBlbGVtZW50ID0gZWxlbTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICBlbGVtZW50W25hbWVdID0gZWxlbTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0eXBlb2YgZWxlbSA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgICBlbGVtLnRhcmdldE5TQWxpYXMgPSB0eXBlLnByZWZpeDtcbiAgICAgICAgICBlbGVtLnRhcmdldE5hbWVzcGFjZSA9IG5zO1xuICAgICAgICB9XG5cbiAgICAgICAgZGVmaW5pdGlvbnMuZGVzY3JpcHRpb25zLnR5cGVzW3R5cGVOYW1lXSA9IGVsZW07XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgaWYgKHRoaXMuJHJlZikge1xuICAgICAgICAgIGVsZW1lbnQgPSBkZWZpbml0aW9ucy5kZXNjcmlwdGlvbnMudHlwZXNbdHlwZU5hbWVdO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGVsZW1lbnRbbmFtZV0gPSBkZWZpbml0aW9ucy5kZXNjcmlwdGlvbnMudHlwZXNbdHlwZU5hbWVdO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICBlbGVtZW50W25hbWVdID0gdGhpcy4kdHlwZTtcbiAgICB9XG4gIH1cbiAgZWxzZSB7XG4gICAgbGV0IGNoaWxkcmVuID0gdGhpcy5jaGlsZHJlbjtcbiAgICBlbGVtZW50W25hbWVdID0ge307XG4gICAgZm9yIChsZXQgaSA9IDAsIGNoaWxkOyBjaGlsZCA9IGNoaWxkcmVuW2ldOyBpKyspIHtcbiAgICAgIGlmIChjaGlsZCBpbnN0YW5jZW9mIENvbXBsZXhUeXBlRWxlbWVudCkge1xuICAgICAgICBlbGVtZW50W25hbWVdID0gY2hpbGQuZGVzY3JpcHRpb24oZGVmaW5pdGlvbnMsIHhtbG5zKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgcmV0dXJuIGVsZW1lbnQ7XG59O1xuXG5BbGxFbGVtZW50LnByb3RvdHlwZS5kZXNjcmlwdGlvbiA9XG4gIFNlcXVlbmNlRWxlbWVudC5wcm90b3R5cGUuZGVzY3JpcHRpb24gPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMsIHhtbG5zKSB7XG4gICAgbGV0IGNoaWxkcmVuID0gdGhpcy5jaGlsZHJlbjtcbiAgICBsZXQgc2VxdWVuY2UgPSB7fTtcbiAgICBmb3IgKGxldCBpID0gMCwgY2hpbGQ7IGNoaWxkID0gY2hpbGRyZW5baV07IGkrKykge1xuICAgICAgaWYgKGNoaWxkIGluc3RhbmNlb2YgQW55RWxlbWVudCkge1xuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cbiAgICAgIGxldCBkZXNjcmlwdGlvbiA9IGNoaWxkLmRlc2NyaXB0aW9uKGRlZmluaXRpb25zLCB4bWxucyk7XG4gICAgICBmb3IgKGxldCBrZXkgaW4gZGVzY3JpcHRpb24pIHtcbiAgICAgICAgc2VxdWVuY2Vba2V5XSA9IGRlc2NyaXB0aW9uW2tleV07XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBzZXF1ZW5jZTtcbiAgfTtcblxuQ2hvaWNlRWxlbWVudC5wcm90b3R5cGUuZGVzY3JpcHRpb24gPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMsIHhtbG5zKSB7XG4gIGxldCBjaGlsZHJlbiA9IHRoaXMuY2hpbGRyZW47XG4gIGxldCBjaG9pY2UgPSB7fTtcbiAgZm9yIChsZXQgaSA9IDAsIGNoaWxkOyBjaGlsZCA9IGNoaWxkcmVuW2ldOyBpKyspIHtcbiAgICBsZXQgZGVzY3JpcHRpb24gPSBjaGlsZC5kZXNjcmlwdGlvbihkZWZpbml0aW9ucywgeG1sbnMpO1xuICAgIGZvciAobGV0IGtleSBpbiBkZXNjcmlwdGlvbikge1xuICAgICAgY2hvaWNlW2tleV0gPSBkZXNjcmlwdGlvbltrZXldO1xuICAgIH1cbiAgfVxuICByZXR1cm4gY2hvaWNlO1xufTtcblxuTWVzc2FnZUVsZW1lbnQucHJvdG90eXBlLmRlc2NyaXB0aW9uID0gZnVuY3Rpb24gKGRlZmluaXRpb25zKSB7XG4gIGlmICh0aGlzLmVsZW1lbnQpIHtcbiAgICByZXR1cm4gdGhpcy5lbGVtZW50ICYmIHRoaXMuZWxlbWVudC5kZXNjcmlwdGlvbihkZWZpbml0aW9ucyk7XG4gIH1cbiAgbGV0IGRlc2MgPSB7fTtcbiAgZGVzY1t0aGlzLiRuYW1lXSA9IHRoaXMucGFydHM7XG4gIHJldHVybiBkZXNjO1xufTtcblxuUG9ydFR5cGVFbGVtZW50LnByb3RvdHlwZS5kZXNjcmlwdGlvbiA9IGZ1bmN0aW9uIChkZWZpbml0aW9ucykge1xuICBsZXQgbWV0aG9kcyA9IHt9O1xuICBmb3IgKGxldCBuYW1lIGluIHRoaXMubWV0aG9kcykge1xuICAgIGxldCBtZXRob2QgPSB0aGlzLm1ldGhvZHNbbmFtZV07XG4gICAgbWV0aG9kc1tuYW1lXSA9IG1ldGhvZC5kZXNjcmlwdGlvbihkZWZpbml0aW9ucyk7XG4gIH1cbiAgcmV0dXJuIG1ldGhvZHM7XG59O1xuXG5PcGVyYXRpb25FbGVtZW50LnByb3RvdHlwZS5kZXNjcmlwdGlvbiA9IGZ1bmN0aW9uIChkZWZpbml0aW9ucykge1xuICBsZXQgaW5wdXREZXNjID0gdGhpcy5pbnB1dCA/IHRoaXMuaW5wdXQuZGVzY3JpcHRpb24oZGVmaW5pdGlvbnMpIDogbnVsbDtcbiAgbGV0IG91dHB1dERlc2MgPSB0aGlzLm91dHB1dCA/IHRoaXMub3V0cHV0LmRlc2NyaXB0aW9uKGRlZmluaXRpb25zKSA6IG51bGw7XG4gIHJldHVybiB7XG4gICAgaW5wdXQ6IGlucHV0RGVzYyAmJiBpbnB1dERlc2NbT2JqZWN0LmtleXMoaW5wdXREZXNjKVswXV0sXG4gICAgb3V0cHV0OiBvdXRwdXREZXNjICYmIG91dHB1dERlc2NbT2JqZWN0LmtleXMob3V0cHV0RGVzYylbMF1dXG4gIH07XG59O1xuXG5CaW5kaW5nRWxlbWVudC5wcm90b3R5cGUuZGVzY3JpcHRpb24gPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMpIHtcbiAgbGV0IG1ldGhvZHMgPSB7fTtcbiAgZm9yIChsZXQgbmFtZSBpbiB0aGlzLm1ldGhvZHMpIHtcbiAgICBsZXQgbWV0aG9kID0gdGhpcy5tZXRob2RzW25hbWVdO1xuICAgIG1ldGhvZHNbbmFtZV0gPSBtZXRob2QuZGVzY3JpcHRpb24oZGVmaW5pdGlvbnMpO1xuICB9XG4gIHJldHVybiBtZXRob2RzO1xufTtcblxuU2VydmljZUVsZW1lbnQucHJvdG90eXBlLmRlc2NyaXB0aW9uID0gZnVuY3Rpb24gKGRlZmluaXRpb25zKSB7XG4gIGxldCBwb3J0cyA9IHt9O1xuICBmb3IgKGxldCBuYW1lIGluIHRoaXMucG9ydHMpIHtcbiAgICBsZXQgcG9ydCA9IHRoaXMucG9ydHNbbmFtZV07XG4gICAgcG9ydHNbbmFtZV0gPSBwb3J0LmJpbmRpbmcuZGVzY3JpcHRpb24oZGVmaW5pdGlvbnMpO1xuICB9XG4gIHJldHVybiBwb3J0cztcbn07XG5cbmV4cG9ydCBsZXQgV1NETCA9IGZ1bmN0aW9uIChkZWZpbml0aW9uLCB1cmksIG9wdGlvbnMpIHtcbiAgbGV0IHNlbGYgPSB0aGlzLFxuICAgIGZyb21GdW5jO1xuXG4gIHRoaXMudXJpID0gdXJpO1xuICB0aGlzLmNhbGxiYWNrID0gZnVuY3Rpb24gKCkge1xuICB9O1xuICB0aGlzLl9pbmNsdWRlc1dzZGwgPSBbXTtcblxuICAvLyBpbml0aWFsaXplIFdTREwgY2FjaGVcbiAgdGhpcy5XU0RMX0NBQ0hFID0gKG9wdGlvbnMgfHwge30pLldTRExfQ0FDSEUgfHwge307XG5cbiAgdGhpcy5faW5pdGlhbGl6ZU9wdGlvbnMob3B0aW9ucyk7XG5cbiAgaWYgKHR5cGVvZiBkZWZpbml0aW9uID09PSAnc3RyaW5nJykge1xuICAgIGRlZmluaXRpb24gPSBzdHJpcEJvbShkZWZpbml0aW9uKTtcbiAgICBmcm9tRnVuYyA9IHRoaXMuX2Zyb21YTUw7XG4gIH1cbiAgZWxzZSBpZiAodHlwZW9mIGRlZmluaXRpb24gPT09ICdvYmplY3QnKSB7XG4gICAgZnJvbUZ1bmMgPSB0aGlzLl9mcm9tU2VydmljZXM7XG4gIH1cbiAgZWxzZSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdXU0RMIGxldHJ1Y3RvciB0YWtlcyBlaXRoZXIgYW4gWE1MIHN0cmluZyBvciBzZXJ2aWNlIGRlZmluaXRpb24nKTtcbiAgfVxuXG4gIFByb21pc2UucmVzb2x2ZSh0cnVlKS50aGVuKCgpID0+IHtcbiAgICB0cnkge1xuICAgICAgZnJvbUZ1bmMuY2FsbChzZWxmLCBkZWZpbml0aW9uKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICByZXR1cm4gc2VsZi5jYWxsYmFjayhlLm1lc3NhZ2UpO1xuICAgIH1cblxuICAgIHNlbGYucHJvY2Vzc0luY2x1ZGVzKCkudGhlbigoKSA9PiB7XG4gICAgICBzZWxmLmRlZmluaXRpb25zLmRlbGV0ZUZpeGVkQXR0cnMoKTtcbiAgICAgIGxldCBzZXJ2aWNlcyA9IHNlbGYuc2VydmljZXMgPSBzZWxmLmRlZmluaXRpb25zLnNlcnZpY2VzO1xuICAgICAgaWYgKHNlcnZpY2VzKSB7XG4gICAgICAgIGZvciAoY29uc3QgbmFtZSBpbiBzZXJ2aWNlcykge1xuICAgICAgICAgIHNlcnZpY2VzW25hbWVdLnBvc3RQcm9jZXNzKHNlbGYuZGVmaW5pdGlvbnMpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBsZXQgY29tcGxleFR5cGVzID0gc2VsZi5kZWZpbml0aW9ucy5jb21wbGV4VHlwZXM7XG4gICAgICBpZiAoY29tcGxleFR5cGVzKSB7XG4gICAgICAgIGZvciAoY29uc3QgbmFtZSBpbiBjb21wbGV4VHlwZXMpIHtcbiAgICAgICAgICBjb21wbGV4VHlwZXNbbmFtZV0uZGVsZXRlRml4ZWRBdHRycygpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIGZvciBkb2N1bWVudCBzdHlsZSwgZm9yIGV2ZXJ5IGJpbmRpbmcsIHByZXBhcmUgaW5wdXQgbWVzc2FnZSBlbGVtZW50IG5hbWUgdG8gKG1ldGhvZE5hbWUsIG91dHB1dCBtZXNzYWdlIGVsZW1lbnQgbmFtZSkgbWFwcGluZ1xuICAgICAgbGV0IGJpbmRpbmdzID0gc2VsZi5kZWZpbml0aW9ucy5iaW5kaW5ncztcbiAgICAgIGZvciAobGV0IGJpbmRpbmdOYW1lIGluIGJpbmRpbmdzKSB7XG4gICAgICAgIGxldCBiaW5kaW5nID0gYmluZGluZ3NbYmluZGluZ05hbWVdO1xuICAgICAgICBpZiAodHlwZW9mIGJpbmRpbmcuc3R5bGUgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgYmluZGluZy5zdHlsZSA9ICdkb2N1bWVudCc7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGJpbmRpbmcuc3R5bGUgIT09ICdkb2N1bWVudCcpXG4gICAgICAgICAgY29udGludWU7XG4gICAgICAgIGxldCBtZXRob2RzID0gYmluZGluZy5tZXRob2RzO1xuICAgICAgICBsZXQgdG9wRWxzID0gYmluZGluZy50b3BFbGVtZW50cyA9IHt9O1xuICAgICAgICBmb3IgKGxldCBtZXRob2ROYW1lIGluIG1ldGhvZHMpIHtcbiAgICAgICAgICBpZiAobWV0aG9kc1ttZXRob2ROYW1lXS5pbnB1dCkge1xuICAgICAgICAgICAgbGV0IGlucHV0TmFtZSA9IG1ldGhvZHNbbWV0aG9kTmFtZV0uaW5wdXQuJG5hbWU7XG4gICAgICAgICAgICBsZXQgb3V0cHV0TmFtZSA9IFwiXCI7XG4gICAgICAgICAgICBpZiAobWV0aG9kc1ttZXRob2ROYW1lXS5vdXRwdXQpXG4gICAgICAgICAgICAgIG91dHB1dE5hbWUgPSBtZXRob2RzW21ldGhvZE5hbWVdLm91dHB1dC4kbmFtZTtcbiAgICAgICAgICAgIHRvcEVsc1tpbnB1dE5hbWVdID0geyBcIm1ldGhvZE5hbWVcIjogbWV0aG9kTmFtZSwgXCJvdXRwdXROYW1lXCI6IG91dHB1dE5hbWUgfTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLy8gcHJlcGFyZSBzb2FwIGVudmVsb3BlIHhtbG5zIGRlZmluaXRpb24gc3RyaW5nXG4gICAgICBzZWxmLnhtbG5zSW5FbnZlbG9wZSA9IHNlbGYuX3htbG5zTWFwKCk7XG4gICAgICBzZWxmLmNhbGxiYWNrKG51bGwsIHNlbGYpO1xuICAgIH0pLmNhdGNoKGVyciA9PiBzZWxmLmNhbGxiYWNrKGVycikpO1xuXG4gIH0pO1xuXG4gIC8vIHByb2Nlc3MubmV4dFRpY2soZnVuY3Rpb24oKSB7XG4gIC8vICAgdHJ5IHtcbiAgLy8gICAgIGZyb21GdW5jLmNhbGwoc2VsZiwgZGVmaW5pdGlvbik7XG4gIC8vICAgfSBjYXRjaCAoZSkge1xuICAvLyAgICAgcmV0dXJuIHNlbGYuY2FsbGJhY2soZS5tZXNzYWdlKTtcbiAgLy8gICB9XG5cbiAgLy8gICBzZWxmLnByb2Nlc3NJbmNsdWRlcyhmdW5jdGlvbihlcnIpIHtcbiAgLy8gICAgIGxldCBuYW1lO1xuICAvLyAgICAgaWYgKGVycikge1xuICAvLyAgICAgICByZXR1cm4gc2VsZi5jYWxsYmFjayhlcnIpO1xuICAvLyAgICAgfVxuXG4gIC8vICAgICBzZWxmLmRlZmluaXRpb25zLmRlbGV0ZUZpeGVkQXR0cnMoKTtcbiAgLy8gICAgIGxldCBzZXJ2aWNlcyA9IHNlbGYuc2VydmljZXMgPSBzZWxmLmRlZmluaXRpb25zLnNlcnZpY2VzO1xuICAvLyAgICAgaWYgKHNlcnZpY2VzKSB7XG4gIC8vICAgICAgIGZvciAobmFtZSBpbiBzZXJ2aWNlcykge1xuICAvLyAgICAgICAgIHNlcnZpY2VzW25hbWVdLnBvc3RQcm9jZXNzKHNlbGYuZGVmaW5pdGlvbnMpO1xuICAvLyAgICAgICB9XG4gIC8vICAgICB9XG4gIC8vICAgICBsZXQgY29tcGxleFR5cGVzID0gc2VsZi5kZWZpbml0aW9ucy5jb21wbGV4VHlwZXM7XG4gIC8vICAgICBpZiAoY29tcGxleFR5cGVzKSB7XG4gIC8vICAgICAgIGZvciAobmFtZSBpbiBjb21wbGV4VHlwZXMpIHtcbiAgLy8gICAgICAgICBjb21wbGV4VHlwZXNbbmFtZV0uZGVsZXRlRml4ZWRBdHRycygpO1xuICAvLyAgICAgICB9XG4gIC8vICAgICB9XG5cbiAgLy8gICAgIC8vIGZvciBkb2N1bWVudCBzdHlsZSwgZm9yIGV2ZXJ5IGJpbmRpbmcsIHByZXBhcmUgaW5wdXQgbWVzc2FnZSBlbGVtZW50IG5hbWUgdG8gKG1ldGhvZE5hbWUsIG91dHB1dCBtZXNzYWdlIGVsZW1lbnQgbmFtZSkgbWFwcGluZ1xuICAvLyAgICAgbGV0IGJpbmRpbmdzID0gc2VsZi5kZWZpbml0aW9ucy5iaW5kaW5ncztcbiAgLy8gICAgIGZvciAobGV0IGJpbmRpbmdOYW1lIGluIGJpbmRpbmdzKSB7XG4gIC8vICAgICAgIGxldCBiaW5kaW5nID0gYmluZGluZ3NbYmluZGluZ05hbWVdO1xuICAvLyAgICAgICBpZiAodHlwZW9mIGJpbmRpbmcuc3R5bGUgPT09ICd1bmRlZmluZWQnKSB7XG4gIC8vICAgICAgICAgYmluZGluZy5zdHlsZSA9ICdkb2N1bWVudCc7XG4gIC8vICAgICAgIH1cbiAgLy8gICAgICAgaWYgKGJpbmRpbmcuc3R5bGUgIT09ICdkb2N1bWVudCcpXG4gIC8vICAgICAgICAgY29udGludWU7XG4gIC8vICAgICAgIGxldCBtZXRob2RzID0gYmluZGluZy5tZXRob2RzO1xuICAvLyAgICAgICBsZXQgdG9wRWxzID0gYmluZGluZy50b3BFbGVtZW50cyA9IHt9O1xuICAvLyAgICAgICBmb3IgKGxldCBtZXRob2ROYW1lIGluIG1ldGhvZHMpIHtcbiAgLy8gICAgICAgICBpZiAobWV0aG9kc1ttZXRob2ROYW1lXS5pbnB1dCkge1xuICAvLyAgICAgICAgICAgbGV0IGlucHV0TmFtZSA9IG1ldGhvZHNbbWV0aG9kTmFtZV0uaW5wdXQuJG5hbWU7XG4gIC8vICAgICAgICAgICBsZXQgb3V0cHV0TmFtZT1cIlwiO1xuICAvLyAgICAgICAgICAgaWYobWV0aG9kc1ttZXRob2ROYW1lXS5vdXRwdXQgKVxuICAvLyAgICAgICAgICAgICBvdXRwdXROYW1lID0gbWV0aG9kc1ttZXRob2ROYW1lXS5vdXRwdXQuJG5hbWU7XG4gIC8vICAgICAgICAgICB0b3BFbHNbaW5wdXROYW1lXSA9IHtcIm1ldGhvZE5hbWVcIjogbWV0aG9kTmFtZSwgXCJvdXRwdXROYW1lXCI6IG91dHB1dE5hbWV9O1xuICAvLyAgICAgICAgIH1cbiAgLy8gICAgICAgfVxuICAvLyAgICAgfVxuXG4gIC8vICAgICAvLyBwcmVwYXJlIHNvYXAgZW52ZWxvcGUgeG1sbnMgZGVmaW5pdGlvbiBzdHJpbmdcbiAgLy8gICAgIHNlbGYueG1sbnNJbkVudmVsb3BlID0gc2VsZi5feG1sbnNNYXAoKTtcblxuICAvLyAgICAgc2VsZi5jYWxsYmFjayhlcnIsIHNlbGYpO1xuICAvLyAgIH0pO1xuXG4gIC8vIH0pO1xufTtcblxuV1NETC5wcm90b3R5cGUuaWdub3JlZE5hbWVzcGFjZXMgPSBbJ3RucycsICd0YXJnZXROYW1lc3BhY2UnLCAndHlwZWROYW1lc3BhY2UnXTtcblxuV1NETC5wcm90b3R5cGUuaWdub3JlQmFzZU5hbWVTcGFjZXMgPSBmYWxzZTtcblxuV1NETC5wcm90b3R5cGUudmFsdWVLZXkgPSAnJHZhbHVlJztcbldTREwucHJvdG90eXBlLnhtbEtleSA9ICckeG1sJztcblxuV1NETC5wcm90b3R5cGUuX2luaXRpYWxpemVPcHRpb25zID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgdGhpcy5fb3JpZ2luYWxJZ25vcmVkTmFtZXNwYWNlcyA9IChvcHRpb25zIHx8IHt9KS5pZ25vcmVkTmFtZXNwYWNlcztcbiAgdGhpcy5vcHRpb25zID0ge307XG5cbiAgbGV0IGlnbm9yZWROYW1lc3BhY2VzID0gb3B0aW9ucyA/IG9wdGlvbnMuaWdub3JlZE5hbWVzcGFjZXMgOiBudWxsO1xuXG4gIGlmIChpZ25vcmVkTmFtZXNwYWNlcyAmJlxuICAgIChBcnJheS5pc0FycmF5KGlnbm9yZWROYW1lc3BhY2VzLm5hbWVzcGFjZXMpIHx8IHR5cGVvZiBpZ25vcmVkTmFtZXNwYWNlcy5uYW1lc3BhY2VzID09PSAnc3RyaW5nJykpIHtcbiAgICBpZiAoaWdub3JlZE5hbWVzcGFjZXMub3ZlcnJpZGUpIHtcbiAgICAgIHRoaXMub3B0aW9ucy5pZ25vcmVkTmFtZXNwYWNlcyA9IGlnbm9yZWROYW1lc3BhY2VzLm5hbWVzcGFjZXM7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMub3B0aW9ucy5pZ25vcmVkTmFtZXNwYWNlcyA9IHRoaXMuaWdub3JlZE5hbWVzcGFjZXMuY29uY2F0KGlnbm9yZWROYW1lc3BhY2VzLm5hbWVzcGFjZXMpO1xuICAgIH1cbiAgfSBlbHNlIHtcbiAgICB0aGlzLm9wdGlvbnMuaWdub3JlZE5hbWVzcGFjZXMgPSB0aGlzLmlnbm9yZWROYW1lc3BhY2VzO1xuICB9XG5cbiAgdGhpcy5vcHRpb25zLnZhbHVlS2V5ID0gb3B0aW9ucy52YWx1ZUtleSB8fCB0aGlzLnZhbHVlS2V5O1xuICB0aGlzLm9wdGlvbnMueG1sS2V5ID0gb3B0aW9ucy54bWxLZXkgfHwgdGhpcy54bWxLZXk7XG4gIGlmIChvcHRpb25zLmVzY2FwZVhNTCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgdGhpcy5vcHRpb25zLmVzY2FwZVhNTCA9IG9wdGlvbnMuZXNjYXBlWE1MO1xuICB9IGVsc2Uge1xuICAgIHRoaXMub3B0aW9ucy5lc2NhcGVYTUwgPSB0cnVlO1xuICB9XG4gIGlmIChvcHRpb25zLnJldHVybkZhdWx0ICE9PSB1bmRlZmluZWQpIHtcbiAgICB0aGlzLm9wdGlvbnMucmV0dXJuRmF1bHQgPSBvcHRpb25zLnJldHVybkZhdWx0O1xuICB9IGVsc2Uge1xuICAgIHRoaXMub3B0aW9ucy5yZXR1cm5GYXVsdCA9IGZhbHNlO1xuICB9XG4gIHRoaXMub3B0aW9ucy5oYW5kbGVOaWxBc051bGwgPSAhIW9wdGlvbnMuaGFuZGxlTmlsQXNOdWxsO1xuXG4gIGlmIChvcHRpb25zLm5hbWVzcGFjZUFycmF5RWxlbWVudHMgIT09IHVuZGVmaW5lZCkge1xuICAgIHRoaXMub3B0aW9ucy5uYW1lc3BhY2VBcnJheUVsZW1lbnRzID0gb3B0aW9ucy5uYW1lc3BhY2VBcnJheUVsZW1lbnRzO1xuICB9IGVsc2Uge1xuICAgIHRoaXMub3B0aW9ucy5uYW1lc3BhY2VBcnJheUVsZW1lbnRzID0gdHJ1ZTtcbiAgfVxuXG4gIC8vIEFsbG93IGFueSByZXF1ZXN0IGhlYWRlcnMgdG8ga2VlcCBwYXNzaW5nIHRocm91Z2hcbiAgdGhpcy5vcHRpb25zLndzZGxfaGVhZGVycyA9IG9wdGlvbnMud3NkbF9oZWFkZXJzO1xuICB0aGlzLm9wdGlvbnMud3NkbF9vcHRpb25zID0gb3B0aW9ucy53c2RsX29wdGlvbnM7XG4gIGlmIChvcHRpb25zLmh0dHBDbGllbnQpIHtcbiAgICB0aGlzLm9wdGlvbnMuaHR0cENsaWVudCA9IG9wdGlvbnMuaHR0cENsaWVudDtcbiAgfVxuXG4gIC8vIFRoZSBzdXBwbGllZCByZXF1ZXN0LW9iamVjdCBzaG91bGQgYmUgcGFzc2VkIHRocm91Z2hcbiAgaWYgKG9wdGlvbnMucmVxdWVzdCkge1xuICAgIHRoaXMub3B0aW9ucy5yZXF1ZXN0ID0gb3B0aW9ucy5yZXF1ZXN0O1xuICB9XG5cbiAgbGV0IGlnbm9yZUJhc2VOYW1lU3BhY2VzID0gb3B0aW9ucyA/IG9wdGlvbnMuaWdub3JlQmFzZU5hbWVTcGFjZXMgOiBudWxsO1xuICBpZiAoaWdub3JlQmFzZU5hbWVTcGFjZXMgIT09IG51bGwgJiYgdHlwZW9mIGlnbm9yZUJhc2VOYW1lU3BhY2VzICE9PSAndW5kZWZpbmVkJykge1xuICAgIHRoaXMub3B0aW9ucy5pZ25vcmVCYXNlTmFtZVNwYWNlcyA9IGlnbm9yZUJhc2VOYW1lU3BhY2VzO1xuICB9IGVsc2Uge1xuICAgIHRoaXMub3B0aW9ucy5pZ25vcmVCYXNlTmFtZVNwYWNlcyA9IHRoaXMuaWdub3JlQmFzZU5hbWVTcGFjZXM7XG4gIH1cblxuICAvLyBXb3JrcyBvbmx5IGluIGNsaWVudFxuICB0aGlzLm9wdGlvbnMuZm9yY2VTb2FwMTJIZWFkZXJzID0gb3B0aW9ucy5mb3JjZVNvYXAxMkhlYWRlcnM7XG4gIHRoaXMub3B0aW9ucy5jdXN0b21EZXNlcmlhbGl6ZXIgPSBvcHRpb25zLmN1c3RvbURlc2VyaWFsaXplcjtcblxuICBpZiAob3B0aW9ucy5vdmVycmlkZVJvb3RFbGVtZW50ICE9PSB1bmRlZmluZWQpIHtcbiAgICB0aGlzLm9wdGlvbnMub3ZlcnJpZGVSb290RWxlbWVudCA9IG9wdGlvbnMub3ZlcnJpZGVSb290RWxlbWVudDtcbiAgfVxuXG4gIHRoaXMub3B0aW9ucy51c2VFbXB0eVRhZyA9ICEhb3B0aW9ucy51c2VFbXB0eVRhZztcbn07XG5cbldTREwucHJvdG90eXBlLm9uUmVhZHkgPSBmdW5jdGlvbiAoY2FsbGJhY2spIHtcbiAgaWYgKGNhbGxiYWNrKVxuICAgIHRoaXMuY2FsbGJhY2sgPSBjYWxsYmFjaztcbn07XG5cbldTREwucHJvdG90eXBlLl9wcm9jZXNzTmV4dEluY2x1ZGUgPSBhc3luYyBmdW5jdGlvbiAoaW5jbHVkZXMpIHtcbiAgbGV0IHNlbGYgPSB0aGlzLFxuICAgIGluY2x1ZGUgPSBpbmNsdWRlcy5zaGlmdCgpLFxuICAgIG9wdGlvbnM7XG5cbiAgaWYgKCFpbmNsdWRlKVxuICAgIHJldHVybjsgLy8gY2FsbGJhY2soKTtcblxuICBsZXQgaW5jbHVkZVBhdGg7XG4gIGlmICghL15odHRwcz86Ly50ZXN0KHNlbGYudXJpKSAmJiAhL15odHRwcz86Ly50ZXN0KGluY2x1ZGUubG9jYXRpb24pKSB7XG4gICAgLy8gaW5jbHVkZVBhdGggPSBwYXRoLnJlc29sdmUocGF0aC5kaXJuYW1lKHNlbGYudXJpKSwgaW5jbHVkZS5sb2NhdGlvbik7XG4gIH0gZWxzZSB7XG4gICAgaW5jbHVkZVBhdGggPSB1cmwucmVzb2x2ZShzZWxmLnVyaSB8fCAnJywgaW5jbHVkZS5sb2NhdGlvbik7XG4gIH1cblxuICBvcHRpb25zID0gXy5hc3NpZ24oe30sIHRoaXMub3B0aW9ucyk7XG4gIC8vIGZvbGxvdyBzdXBwbGllZCBpZ25vcmVkTmFtZXNwYWNlcyBvcHRpb25cbiAgb3B0aW9ucy5pZ25vcmVkTmFtZXNwYWNlcyA9IHRoaXMuX29yaWdpbmFsSWdub3JlZE5hbWVzcGFjZXMgfHwgdGhpcy5vcHRpb25zLmlnbm9yZWROYW1lc3BhY2VzO1xuICBvcHRpb25zLldTRExfQ0FDSEUgPSB0aGlzLldTRExfQ0FDSEU7XG5cbiAgY29uc3Qgd3NkbCA9IGF3YWl0IG9wZW5fd3NkbF9yZWN1cnNpdmUoaW5jbHVkZVBhdGgsIG9wdGlvbnMpXG4gIHNlbGYuX2luY2x1ZGVzV3NkbC5wdXNoKHdzZGwpO1xuXG4gIGlmICh3c2RsLmRlZmluaXRpb25zIGluc3RhbmNlb2YgRGVmaW5pdGlvbnNFbGVtZW50KSB7XG4gICAgXy5tZXJnZVdpdGgoc2VsZi5kZWZpbml0aW9ucywgd3NkbC5kZWZpbml0aW9ucywgZnVuY3Rpb24gKGEsIGIpIHtcbiAgICAgIHJldHVybiAoYSBpbnN0YW5jZW9mIFNjaGVtYUVsZW1lbnQpID8gYS5tZXJnZShiKSA6IHVuZGVmaW5lZDtcbiAgICB9KTtcbiAgfSBlbHNlIHtcbiAgICBzZWxmLmRlZmluaXRpb25zLnNjaGVtYXNbaW5jbHVkZS5uYW1lc3BhY2UgfHwgd3NkbC5kZWZpbml0aW9ucy4kdGFyZ2V0TmFtZXNwYWNlXSA9IGRlZXBNZXJnZShzZWxmLmRlZmluaXRpb25zLnNjaGVtYXNbaW5jbHVkZS5uYW1lc3BhY2UgfHwgd3NkbC5kZWZpbml0aW9ucy4kdGFyZ2V0TmFtZXNwYWNlXSwgd3NkbC5kZWZpbml0aW9ucyk7XG4gIH1cblxuICByZXR1cm4gc2VsZi5fcHJvY2Vzc05leHRJbmNsdWRlKGluY2x1ZGVzKTtcblxuICAvLyBvcGVuX3dzZGxfcmVjdXJzaXZlKGluY2x1ZGVQYXRoLCBvcHRpb25zLCBmdW5jdGlvbihlcnIsIHdzZGwpIHtcbiAgLy8gICBpZiAoZXJyKSB7XG4gIC8vICAgICByZXR1cm4gY2FsbGJhY2soZXJyKTtcbiAgLy8gICB9XG5cbiAgLy8gICBzZWxmLl9pbmNsdWRlc1dzZGwucHVzaCh3c2RsKTtcblxuICAvLyAgIGlmICh3c2RsLmRlZmluaXRpb25zIGluc3RhbmNlb2YgRGVmaW5pdGlvbnNFbGVtZW50KSB7XG4gIC8vICAgICBfLm1lcmdlV2l0aChzZWxmLmRlZmluaXRpb25zLCB3c2RsLmRlZmluaXRpb25zLCBmdW5jdGlvbihhLGIpIHtcbiAgLy8gICAgICAgcmV0dXJuIChhIGluc3RhbmNlb2YgU2NoZW1hRWxlbWVudCkgPyBhLm1lcmdlKGIpIDogdW5kZWZpbmVkO1xuICAvLyAgICAgfSk7XG4gIC8vICAgfSBlbHNlIHtcbiAgLy8gICAgIHNlbGYuZGVmaW5pdGlvbnMuc2NoZW1hc1tpbmNsdWRlLm5hbWVzcGFjZSB8fCB3c2RsLmRlZmluaXRpb25zLiR0YXJnZXROYW1lc3BhY2VdID0gZGVlcE1lcmdlKHNlbGYuZGVmaW5pdGlvbnMuc2NoZW1hc1tpbmNsdWRlLm5hbWVzcGFjZSB8fCB3c2RsLmRlZmluaXRpb25zLiR0YXJnZXROYW1lc3BhY2VdLCB3c2RsLmRlZmluaXRpb25zKTtcbiAgLy8gICB9XG4gIC8vICAgc2VsZi5fcHJvY2Vzc05leHRJbmNsdWRlKGluY2x1ZGVzLCBmdW5jdGlvbihlcnIpIHtcbiAgLy8gICAgIGNhbGxiYWNrKGVycik7XG4gIC8vICAgfSk7XG4gIC8vIH0pO1xufTtcblxuV1NETC5wcm90b3R5cGUucHJvY2Vzc0luY2x1ZGVzID0gYXN5bmMgZnVuY3Rpb24gKCkge1xuICBsZXQgc2NoZW1hcyA9IHRoaXMuZGVmaW5pdGlvbnMuc2NoZW1hcyxcbiAgICBpbmNsdWRlcyA9IFtdO1xuXG4gIGZvciAobGV0IG5zIGluIHNjaGVtYXMpIHtcbiAgICBsZXQgc2NoZW1hID0gc2NoZW1hc1tuc107XG4gICAgaW5jbHVkZXMgPSBpbmNsdWRlcy5jb25jYXQoc2NoZW1hLmluY2x1ZGVzIHx8IFtdKTtcbiAgfVxuXG4gIHJldHVybiB0aGlzLl9wcm9jZXNzTmV4dEluY2x1ZGUoaW5jbHVkZXMpO1xufTtcblxuV1NETC5wcm90b3R5cGUuZGVzY3JpYmVTZXJ2aWNlcyA9IGZ1bmN0aW9uICgpIHtcbiAgbGV0IHNlcnZpY2VzID0ge307XG4gIGZvciAobGV0IG5hbWUgaW4gdGhpcy5zZXJ2aWNlcykge1xuICAgIGxldCBzZXJ2aWNlID0gdGhpcy5zZXJ2aWNlc1tuYW1lXTtcbiAgICBzZXJ2aWNlc1tuYW1lXSA9IHNlcnZpY2UuZGVzY3JpcHRpb24odGhpcy5kZWZpbml0aW9ucyk7XG4gIH1cbiAgcmV0dXJuIHNlcnZpY2VzO1xufTtcblxuV1NETC5wcm90b3R5cGUudG9YTUwgPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiB0aGlzLnhtbCB8fCAnJztcbn07XG5cbldTREwucHJvdG90eXBlLnhtbFRvT2JqZWN0ID0gZnVuY3Rpb24gKHhtbCwgY2FsbGJhY2spIHtcbiAgbGV0IHNlbGYgPSB0aGlzO1xuICBsZXQgcCA9IHR5cGVvZiBjYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IHt9IDogc2F4LnBhcnNlcih0cnVlKTtcbiAgbGV0IG9iamVjdE5hbWUgPSBudWxsO1xuICBsZXQgcm9vdDogYW55ID0ge307XG4gIGxldCBzY2hlbWEgPSB7XG4gICAgRW52ZWxvcGU6IHtcbiAgICAgIEhlYWRlcjoge1xuICAgICAgICBTZWN1cml0eToge1xuICAgICAgICAgIFVzZXJuYW1lVG9rZW46IHtcbiAgICAgICAgICAgIFVzZXJuYW1lOiAnc3RyaW5nJyxcbiAgICAgICAgICAgIFBhc3N3b3JkOiAnc3RyaW5nJ1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIEJvZHk6IHtcbiAgICAgICAgRmF1bHQ6IHtcbiAgICAgICAgICBmYXVsdGNvZGU6ICdzdHJpbmcnLFxuICAgICAgICAgIGZhdWx0c3RyaW5nOiAnc3RyaW5nJyxcbiAgICAgICAgICBkZXRhaWw6ICdzdHJpbmcnXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH07XG4gIGxldCBzdGFjazogYW55W10gPSBbeyBuYW1lOiBudWxsLCBvYmplY3Q6IHJvb3QsIHNjaGVtYTogc2NoZW1hIH1dO1xuICBsZXQgeG1sbnM6IGFueSA9IHt9O1xuXG4gIGxldCByZWZzID0ge30sIGlkOyAvLyB7aWQ6e2hyZWZzOltdLG9iajp9LCAuLi59XG5cbiAgcC5vbm9wZW50YWcgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgIGxldCBuc05hbWUgPSBub2RlLm5hbWU7XG4gICAgbGV0IGF0dHJzOiBhbnkgPSBub2RlLmF0dHJpYnV0ZXM7XG4gICAgbGV0IG5hbWUgPSBzcGxpdFFOYW1lKG5zTmFtZSkubmFtZSxcbiAgICAgIGF0dHJpYnV0ZU5hbWUsXG4gICAgICB0b3AgPSBzdGFja1tzdGFjay5sZW5ndGggLSAxXSxcbiAgICAgIHRvcFNjaGVtYSA9IHRvcC5zY2hlbWEsXG4gICAgICBlbGVtZW50QXR0cmlidXRlcyA9IHt9LFxuICAgICAgaGFzTm9uWG1sbnNBdHRyaWJ1dGUgPSBmYWxzZSxcbiAgICAgIGhhc05pbEF0dHJpYnV0ZSA9IGZhbHNlLFxuICAgICAgb2JqID0ge307XG4gICAgbGV0IG9yaWdpbmFsTmFtZSA9IG5hbWU7XG5cbiAgICBpZiAoIW9iamVjdE5hbWUgJiYgdG9wLm5hbWUgPT09ICdCb2R5JyAmJiBuYW1lICE9PSAnRmF1bHQnKSB7XG4gICAgICBsZXQgbWVzc2FnZSA9IHNlbGYuZGVmaW5pdGlvbnMubWVzc2FnZXNbbmFtZV07XG4gICAgICAvLyBTdXBwb3J0IFJQQy9saXRlcmFsIG1lc3NhZ2VzIHdoZXJlIHJlc3BvbnNlIGJvZHkgY29udGFpbnMgb25lIGVsZW1lbnQgbmFtZWRcbiAgICAgIC8vIGFmdGVyIHRoZSBvcGVyYXRpb24gKyAnUmVzcG9uc2UnLiBTZWUgaHR0cDovL3d3dy53My5vcmcvVFIvd3NkbCNfbmFtZXNcbiAgICAgIGlmICghbWVzc2FnZSkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgIC8vIERldGVybWluZSBpZiB0aGlzIGlzIHJlcXVlc3Qgb3IgcmVzcG9uc2VcbiAgICAgICAgICBsZXQgaXNJbnB1dCA9IGZhbHNlO1xuICAgICAgICAgIGxldCBpc091dHB1dCA9IGZhbHNlO1xuICAgICAgICAgIGlmICgoL1Jlc3BvbnNlJC8pLnRlc3QobmFtZSkpIHtcbiAgICAgICAgICAgIGlzT3V0cHV0ID0gdHJ1ZTtcbiAgICAgICAgICAgIG5hbWUgPSBuYW1lLnJlcGxhY2UoL1Jlc3BvbnNlJC8sICcnKTtcbiAgICAgICAgICB9IGVsc2UgaWYgKCgvUmVxdWVzdCQvKS50ZXN0KG5hbWUpKSB7XG4gICAgICAgICAgICBpc0lucHV0ID0gdHJ1ZTtcbiAgICAgICAgICAgIG5hbWUgPSBuYW1lLnJlcGxhY2UoL1JlcXVlc3QkLywgJycpO1xuICAgICAgICAgIH0gZWxzZSBpZiAoKC9Tb2xpY2l0JC8pLnRlc3QobmFtZSkpIHtcbiAgICAgICAgICAgIGlzSW5wdXQgPSB0cnVlO1xuICAgICAgICAgICAgbmFtZSA9IG5hbWUucmVwbGFjZSgvU29saWNpdCQvLCAnJyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIC8vIExvb2sgdXAgdGhlIGFwcHJvcHJpYXRlIG1lc3NhZ2UgYXMgZ2l2ZW4gaW4gdGhlIHBvcnRUeXBlJ3Mgb3BlcmF0aW9uc1xuICAgICAgICAgIGxldCBwb3J0VHlwZXMgPSBzZWxmLmRlZmluaXRpb25zLnBvcnRUeXBlcztcbiAgICAgICAgICBsZXQgcG9ydFR5cGVOYW1lcyA9IE9iamVjdC5rZXlzKHBvcnRUeXBlcyk7XG4gICAgICAgICAgLy8gQ3VycmVudGx5IHRoaXMgc3VwcG9ydHMgb25seSBvbmUgcG9ydFR5cGUgZGVmaW5pdGlvbi5cbiAgICAgICAgICBsZXQgcG9ydFR5cGUgPSBwb3J0VHlwZXNbcG9ydFR5cGVOYW1lc1swXV07XG4gICAgICAgICAgaWYgKGlzSW5wdXQpIHtcbiAgICAgICAgICAgIG5hbWUgPSBwb3J0VHlwZS5tZXRob2RzW25hbWVdLmlucHV0LiRuYW1lO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBuYW1lID0gcG9ydFR5cGUubWV0aG9kc1tuYW1lXS5vdXRwdXQuJG5hbWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIG1lc3NhZ2UgPSBzZWxmLmRlZmluaXRpb25zLm1lc3NhZ2VzW25hbWVdO1xuICAgICAgICAgIC8vICdjYWNoZScgdGhpcyBhbGlhcyB0byBzcGVlZCBmdXR1cmUgbG9va3Vwc1xuICAgICAgICAgIHNlbGYuZGVmaW5pdGlvbnMubWVzc2FnZXNbb3JpZ2luYWxOYW1lXSA9IHNlbGYuZGVmaW5pdGlvbnMubWVzc2FnZXNbbmFtZV07XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICBpZiAoc2VsZi5vcHRpb25zLnJldHVybkZhdWx0KSB7XG4gICAgICAgICAgICBwLm9uZXJyb3IoZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHRvcFNjaGVtYSA9IG1lc3NhZ2UuZGVzY3JpcHRpb24oc2VsZi5kZWZpbml0aW9ucyk7XG4gICAgICBvYmplY3ROYW1lID0gb3JpZ2luYWxOYW1lO1xuICAgIH1cblxuICAgIGlmIChhdHRycy5ocmVmKSB7XG4gICAgICBpZCA9IGF0dHJzLmhyZWYuc3Vic3RyKDEpO1xuICAgICAgaWYgKCFyZWZzW2lkXSkge1xuICAgICAgICByZWZzW2lkXSA9IHsgaHJlZnM6IFtdLCBvYmo6IG51bGwgfTtcbiAgICAgIH1cbiAgICAgIHJlZnNbaWRdLmhyZWZzLnB1c2goeyBwYXI6IHRvcC5vYmplY3QsIGtleTogbmFtZSwgb2JqOiBvYmogfSk7XG4gICAgfVxuICAgIGlmIChpZCA9IGF0dHJzLmlkKSB7XG4gICAgICBpZiAoIXJlZnNbaWRdKSB7XG4gICAgICAgIHJlZnNbaWRdID0geyBocmVmczogW10sIG9iajogbnVsbCB9O1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vSGFuZGxlIGVsZW1lbnQgYXR0cmlidXRlc1xuICAgIGZvciAoYXR0cmlidXRlTmFtZSBpbiBhdHRycykge1xuICAgICAgaWYgKC9eeG1sbnM6fF54bWxucyQvLnRlc3QoYXR0cmlidXRlTmFtZSkpIHtcbiAgICAgICAgeG1sbnNbc3BsaXRRTmFtZShhdHRyaWJ1dGVOYW1lKS5uYW1lXSA9IGF0dHJzW2F0dHJpYnV0ZU5hbWVdO1xuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cbiAgICAgIGhhc05vblhtbG5zQXR0cmlidXRlID0gdHJ1ZTtcbiAgICAgIGVsZW1lbnRBdHRyaWJ1dGVzW2F0dHJpYnV0ZU5hbWVdID0gYXR0cnNbYXR0cmlidXRlTmFtZV07XG4gICAgfVxuXG4gICAgZm9yIChhdHRyaWJ1dGVOYW1lIGluIGVsZW1lbnRBdHRyaWJ1dGVzKSB7XG4gICAgICBsZXQgcmVzID0gc3BsaXRRTmFtZShhdHRyaWJ1dGVOYW1lKTtcbiAgICAgIGlmIChyZXMubmFtZSA9PT0gJ25pbCcgJiYgeG1sbnNbcmVzLnByZWZpeF0gPT09ICdodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZScgJiYgZWxlbWVudEF0dHJpYnV0ZXNbYXR0cmlidXRlTmFtZV0gJiZcbiAgICAgICAgKGVsZW1lbnRBdHRyaWJ1dGVzW2F0dHJpYnV0ZU5hbWVdLnRvTG93ZXJDYXNlKCkgPT09ICd0cnVlJyB8fCBlbGVtZW50QXR0cmlidXRlc1thdHRyaWJ1dGVOYW1lXSA9PT0gJzEnKVxuICAgICAgKSB7XG4gICAgICAgIGhhc05pbEF0dHJpYnV0ZSA9IHRydWU7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChoYXNOb25YbWxuc0F0dHJpYnV0ZSkge1xuICAgICAgb2JqW3NlbGYub3B0aW9ucy5hdHRyaWJ1dGVzS2V5XSA9IGVsZW1lbnRBdHRyaWJ1dGVzO1xuICAgIH1cblxuICAgIC8vIFBpY2sgdXAgdGhlIHNjaGVtYSBmb3IgdGhlIHR5cGUgc3BlY2lmaWVkIGluIGVsZW1lbnQncyB4c2k6dHlwZSBhdHRyaWJ1dGUuXG4gICAgbGV0IHhzaVR5cGVTY2hlbWE7XG4gICAgbGV0IHhzaVR5cGUgPSBlbGVtZW50QXR0cmlidXRlc1sneHNpOnR5cGUnXTtcbiAgICBpZiAoeHNpVHlwZSkge1xuICAgICAgbGV0IHR5cGUgPSBzcGxpdFFOYW1lKHhzaVR5cGUpO1xuICAgICAgbGV0IHR5cGVVUkk7XG4gICAgICBpZiAodHlwZS5wcmVmaXggPT09IFROU19QUkVGSVgpIHtcbiAgICAgICAgLy8gSW4gY2FzZSBvZiB4c2k6dHlwZSA9IFwiTXlUeXBlXCJcbiAgICAgICAgdHlwZVVSSSA9IHhtbG5zW3R5cGUucHJlZml4XSB8fCB4bWxucy54bWxucztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHR5cGVVUkkgPSB4bWxuc1t0eXBlLnByZWZpeF07XG4gICAgICB9XG4gICAgICBsZXQgdHlwZURlZiA9IHNlbGYuZmluZFNjaGVtYU9iamVjdCh0eXBlVVJJLCB0eXBlLm5hbWUpO1xuICAgICAgaWYgKHR5cGVEZWYpIHtcbiAgICAgICAgeHNpVHlwZVNjaGVtYSA9IHR5cGVEZWYuZGVzY3JpcHRpb24oc2VsZi5kZWZpbml0aW9ucyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHRvcFNjaGVtYSAmJiB0b3BTY2hlbWFbbmFtZSArICdbXSddKSB7XG4gICAgICBuYW1lID0gbmFtZSArICdbXSc7XG4gICAgfVxuICAgIHN0YWNrLnB1c2goe1xuICAgICAgbmFtZTogb3JpZ2luYWxOYW1lLFxuICAgICAgb2JqZWN0OiBvYmosXG4gICAgICBzY2hlbWE6ICh4c2lUeXBlU2NoZW1hIHx8ICh0b3BTY2hlbWEgJiYgdG9wU2NoZW1hW25hbWVdKSksXG4gICAgICBpZDogYXR0cnMuaWQsXG4gICAgICBuaWw6IGhhc05pbEF0dHJpYnV0ZVxuICAgIH0pO1xuICB9O1xuXG4gIHAub25jbG9zZXRhZyA9IGZ1bmN0aW9uIChuc05hbWUpIHtcbiAgICBsZXQgY3VyOiBhbnkgPSBzdGFjay5wb3AoKSxcbiAgICAgIG9iaiA9IGN1ci5vYmplY3QsXG4gICAgICB0b3AgPSBzdGFja1tzdGFjay5sZW5ndGggLSAxXSxcbiAgICAgIHRvcE9iamVjdCA9IHRvcC5vYmplY3QsXG4gICAgICB0b3BTY2hlbWEgPSB0b3Auc2NoZW1hLFxuICAgICAgbmFtZSA9IHNwbGl0UU5hbWUobnNOYW1lKS5uYW1lO1xuXG4gICAgaWYgKHR5cGVvZiBjdXIuc2NoZW1hID09PSAnc3RyaW5nJyAmJiAoY3VyLnNjaGVtYSA9PT0gJ3N0cmluZycgfHwgKDxzdHJpbmc+Y3VyLnNjaGVtYSkuc3BsaXQoJzonKVsxXSA9PT0gJ3N0cmluZycpKSB7XG4gICAgICBpZiAodHlwZW9mIG9iaiA9PT0gJ29iamVjdCcgJiYgT2JqZWN0LmtleXMob2JqKS5sZW5ndGggPT09IDApIG9iaiA9IGN1ci5vYmplY3QgPSAnJztcbiAgICB9XG5cbiAgICBpZiAoY3VyLm5pbCA9PT0gdHJ1ZSkge1xuICAgICAgaWYgKHNlbGYub3B0aW9ucy5oYW5kbGVOaWxBc051bGwpIHtcbiAgICAgICAgb2JqID0gbnVsbDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoXy5pc1BsYWluT2JqZWN0KG9iaikgJiYgIU9iamVjdC5rZXlzKG9iaikubGVuZ3RoKSB7XG4gICAgICBvYmogPSBudWxsO1xuICAgIH1cblxuICAgIGlmICh0b3BTY2hlbWEgJiYgdG9wU2NoZW1hW25hbWUgKyAnW10nXSkge1xuICAgICAgaWYgKCF0b3BPYmplY3RbbmFtZV0pIHtcbiAgICAgICAgdG9wT2JqZWN0W25hbWVdID0gW107XG4gICAgICB9XG4gICAgICB0b3BPYmplY3RbbmFtZV0ucHVzaChvYmopO1xuICAgIH0gZWxzZSBpZiAobmFtZSBpbiB0b3BPYmplY3QpIHtcbiAgICAgIGlmICghQXJyYXkuaXNBcnJheSh0b3BPYmplY3RbbmFtZV0pKSB7XG4gICAgICAgIHRvcE9iamVjdFtuYW1lXSA9IFt0b3BPYmplY3RbbmFtZV1dO1xuICAgICAgfVxuICAgICAgdG9wT2JqZWN0W25hbWVdLnB1c2gob2JqKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdG9wT2JqZWN0W25hbWVdID0gb2JqO1xuICAgIH1cblxuICAgIGlmIChjdXIuaWQpIHtcbiAgICAgIHJlZnNbY3VyLmlkXS5vYmogPSBvYmo7XG4gICAgfVxuICB9O1xuXG4gIHAub25jZGF0YSA9IGZ1bmN0aW9uICh0ZXh0KSB7XG4gICAgbGV0IG9yaWdpbmFsVGV4dCA9IHRleHQ7XG4gICAgdGV4dCA9IHRyaW0odGV4dCk7XG4gICAgaWYgKCF0ZXh0Lmxlbmd0aCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmICgvPFxcP3htbFtcXHNcXFNdK1xcPz4vLnRlc3QodGV4dCkpIHtcbiAgICAgIGxldCB0b3AgPSBzdGFja1tzdGFjay5sZW5ndGggLSAxXTtcbiAgICAgIGxldCB2YWx1ZSA9IHNlbGYueG1sVG9PYmplY3QodGV4dCk7XG4gICAgICBpZiAodG9wLm9iamVjdFtzZWxmLm9wdGlvbnMuYXR0cmlidXRlc0tleV0pIHtcbiAgICAgICAgdG9wLm9iamVjdFtzZWxmLm9wdGlvbnMudmFsdWVLZXldID0gdmFsdWU7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0b3Aub2JqZWN0ID0gdmFsdWU7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHAub250ZXh0KG9yaWdpbmFsVGV4dCk7XG4gICAgfVxuICB9O1xuXG4gIHAub25lcnJvciA9IGZ1bmN0aW9uIChlKSB7XG4gICAgcC5yZXN1bWUoKTtcbiAgICB0aHJvdyB7XG4gICAgICBGYXVsdDoge1xuICAgICAgICBmYXVsdGNvZGU6IDUwMCxcbiAgICAgICAgZmF1bHRzdHJpbmc6ICdJbnZhbGlkIFhNTCcsXG4gICAgICAgIGRldGFpbDogbmV3IEVycm9yKGUpLm1lc3NhZ2UsXG4gICAgICAgIHN0YXR1c0NvZGU6IDUwMFxuICAgICAgfVxuICAgIH07XG4gIH07XG5cbiAgcC5vbnRleHQgPSBmdW5jdGlvbiAodGV4dCkge1xuICAgIGxldCBvcmlnaW5hbFRleHQgPSB0ZXh0O1xuICAgIHRleHQgPSB0cmltKHRleHQpO1xuICAgIGlmICghdGV4dC5sZW5ndGgpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBsZXQgdG9wID0gc3RhY2tbc3RhY2subGVuZ3RoIC0gMV07XG4gICAgbGV0IG5hbWUgPSBzcGxpdFFOYW1lKHRvcC5zY2hlbWEpLm5hbWUsXG4gICAgICB2YWx1ZTtcbiAgICBpZiAoc2VsZi5vcHRpb25zICYmIHNlbGYub3B0aW9ucy5jdXN0b21EZXNlcmlhbGl6ZXIgJiYgc2VsZi5vcHRpb25zLmN1c3RvbURlc2VyaWFsaXplcltuYW1lXSkge1xuICAgICAgdmFsdWUgPSBzZWxmLm9wdGlvbnMuY3VzdG9tRGVzZXJpYWxpemVyW25hbWVdKHRleHQsIHRvcCk7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgaWYgKG5hbWUgPT09ICdpbnQnIHx8IG5hbWUgPT09ICdpbnRlZ2VyJykge1xuICAgICAgICB2YWx1ZSA9IHBhcnNlSW50KHRleHQsIDEwKTtcbiAgICAgIH0gZWxzZSBpZiAobmFtZSA9PT0gJ2Jvb2wnIHx8IG5hbWUgPT09ICdib29sZWFuJykge1xuICAgICAgICB2YWx1ZSA9IHRleHQudG9Mb3dlckNhc2UoKSA9PT0gJ3RydWUnIHx8IHRleHQgPT09ICcxJztcbiAgICAgIH0gZWxzZSBpZiAobmFtZSA9PT0gJ2RhdGVUaW1lJyB8fCBuYW1lID09PSAnZGF0ZScpIHtcbiAgICAgICAgdmFsdWUgPSBuZXcgRGF0ZSh0ZXh0KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmIChzZWxmLm9wdGlvbnMucHJlc2VydmVXaGl0ZXNwYWNlKSB7XG4gICAgICAgICAgdGV4dCA9IG9yaWdpbmFsVGV4dDtcbiAgICAgICAgfVxuICAgICAgICAvLyBoYW5kbGUgc3RyaW5nIG9yIG90aGVyIHR5cGVzXG4gICAgICAgIGlmICh0eXBlb2YgdG9wLm9iamVjdCAhPT0gJ3N0cmluZycpIHtcbiAgICAgICAgICB2YWx1ZSA9IHRleHQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdmFsdWUgPSB0b3Aub2JqZWN0ICsgdGV4dDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmICh0b3Aub2JqZWN0W3NlbGYub3B0aW9ucy5hdHRyaWJ1dGVzS2V5XSkge1xuICAgICAgdG9wLm9iamVjdFtzZWxmLm9wdGlvbnMudmFsdWVLZXldID0gdmFsdWU7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRvcC5vYmplY3QgPSB2YWx1ZTtcbiAgICB9XG4gIH07XG5cbiAgaWYgKHR5cGVvZiBjYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIC8vIHdlIGJlIHN0cmVhbWluZ1xuICAgIGxldCBzYXhTdHJlYW0gPSBzYXguY3JlYXRlU3RyZWFtKHRydWUpO1xuICAgIHNheFN0cmVhbS5vbignb3BlbnRhZycsIHAub25vcGVudGFnKTtcbiAgICBzYXhTdHJlYW0ub24oJ2Nsb3NldGFnJywgcC5vbmNsb3NldGFnKTtcbiAgICBzYXhTdHJlYW0ub24oJ2NkYXRhJywgcC5vbmNkYXRhKTtcbiAgICBzYXhTdHJlYW0ub24oJ3RleHQnLCBwLm9udGV4dCk7XG4gICAgeG1sLnBpcGUoc2F4U3RyZWFtKVxuICAgICAgLm9uKCdlcnJvcicsIGZ1bmN0aW9uIChlcnIpIHtcbiAgICAgICAgY2FsbGJhY2soZXJyKTtcbiAgICAgIH0pXG4gICAgICAub24oJ2VuZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgbGV0IHI7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgciA9IGZpbmlzaCgpO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgcmV0dXJuIGNhbGxiYWNrKGUpO1xuICAgICAgICB9XG4gICAgICAgIGNhbGxiYWNrKG51bGwsIHIpO1xuICAgICAgfSk7XG4gICAgcmV0dXJuO1xuICB9XG4gIHAud3JpdGUoeG1sKS5jbG9zZSgpO1xuXG4gIHJldHVybiBmaW5pc2goKTtcblxuICBmdW5jdGlvbiBmaW5pc2goKSB7XG4gICAgLy8gTXVsdGlSZWYgc3VwcG9ydDogbWVyZ2Ugb2JqZWN0cyBpbnN0ZWFkIG9mIHJlcGxhY2luZ1xuICAgIGZvciAobGV0IG4gaW4gcmVmcykge1xuICAgICAgbGV0IHJlZiA9IHJlZnNbbl07XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJlZi5ocmVmcy5sZW5ndGg7IGkrKykge1xuICAgICAgICBfLmFzc2lnbihyZWYuaHJlZnNbaV0ub2JqLCByZWYub2JqKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAocm9vdC5FbnZlbG9wZSkge1xuICAgICAgbGV0IGJvZHkgPSByb290LkVudmVsb3BlLkJvZHk7XG4gICAgICBpZiAoYm9keSAmJiBib2R5LkZhdWx0KSB7XG4gICAgICAgIGxldCBjb2RlID0gYm9keS5GYXVsdC5mYXVsdGNvZGUgJiYgYm9keS5GYXVsdC5mYXVsdGNvZGUuJHZhbHVlO1xuICAgICAgICBsZXQgc3RyaW5nID0gYm9keS5GYXVsdC5mYXVsdHN0cmluZyAmJiBib2R5LkZhdWx0LmZhdWx0c3RyaW5nLiR2YWx1ZTtcbiAgICAgICAgbGV0IGRldGFpbCA9IGJvZHkuRmF1bHQuZGV0YWlsICYmIGJvZHkuRmF1bHQuZGV0YWlsLiR2YWx1ZTtcblxuICAgICAgICBjb2RlID0gY29kZSB8fCBib2R5LkZhdWx0LmZhdWx0Y29kZTtcbiAgICAgICAgc3RyaW5nID0gc3RyaW5nIHx8IGJvZHkuRmF1bHQuZmF1bHRzdHJpbmc7XG4gICAgICAgIGRldGFpbCA9IGRldGFpbCB8fCBib2R5LkZhdWx0LmRldGFpbDtcblxuICAgICAgICBsZXQgZXJyb3I6IGFueSA9IG5ldyBFcnJvcihjb2RlICsgJzogJyArIHN0cmluZyArIChkZXRhaWwgPyAnOiAnICsgZGV0YWlsIDogJycpKTtcblxuICAgICAgICBlcnJvci5yb290ID0gcm9vdDtcbiAgICAgICAgdGhyb3cgZXJyb3I7XG4gICAgICB9XG4gICAgICByZXR1cm4gcm9vdC5FbnZlbG9wZTtcbiAgICB9XG4gICAgcmV0dXJuIHJvb3Q7XG4gIH1cbn07XG5cbi8qKlxuICogTG9vayB1cCBhIFhTRCB0eXBlIG9yIGVsZW1lbnQgYnkgbmFtZXNwYWNlIFVSSSBhbmQgbmFtZVxuICogQHBhcmFtIHtTdHJpbmd9IG5zVVJJIE5hbWVzcGFjZSBVUklcbiAqIEBwYXJhbSB7U3RyaW5nfSBxbmFtZSBMb2NhbCBvciBxdWFsaWZpZWQgbmFtZVxuICogQHJldHVybnMgeyp9IFRoZSBYU0QgdHlwZS9lbGVtZW50IGRlZmluaXRpb25cbiAqL1xuV1NETC5wcm90b3R5cGUuZmluZFNjaGVtYU9iamVjdCA9IGZ1bmN0aW9uIChuc1VSSSwgcW5hbWUpIHtcbiAgaWYgKCFuc1VSSSB8fCAhcW5hbWUpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIGxldCBkZWYgPSBudWxsO1xuXG4gIGlmICh0aGlzLmRlZmluaXRpb25zLnNjaGVtYXMpIHtcbiAgICBsZXQgc2NoZW1hID0gdGhpcy5kZWZpbml0aW9ucy5zY2hlbWFzW25zVVJJXTtcbiAgICBpZiAoc2NoZW1hKSB7XG4gICAgICBpZiAocW5hbWUuaW5kZXhPZignOicpICE9PSAtMSkge1xuICAgICAgICBxbmFtZSA9IHFuYW1lLnN1YnN0cmluZyhxbmFtZS5pbmRleE9mKCc6JykgKyAxLCBxbmFtZS5sZW5ndGgpO1xuICAgICAgfVxuXG4gICAgICAvLyBpZiB0aGUgY2xpZW50IHBhc3NlZCBhbiBpbnB1dCBlbGVtZW50IHdoaWNoIGhhcyBhIGAkbG9va3VwVHlwZWAgcHJvcGVydHkgaW5zdGVhZCBvZiBgJHR5cGVgXG4gICAgICAvLyB0aGUgYGRlZmAgaXMgZm91bmQgaW4gYHNjaGVtYS5lbGVtZW50c2AuXG4gICAgICBkZWYgPSBzY2hlbWEuY29tcGxleFR5cGVzW3FuYW1lXSB8fCBzY2hlbWEudHlwZXNbcW5hbWVdIHx8IHNjaGVtYS5lbGVtZW50c1txbmFtZV07XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGRlZjtcbn07XG5cbi8qKlxuICogQ3JlYXRlIGRvY3VtZW50IHN0eWxlIHhtbCBzdHJpbmcgZnJvbSB0aGUgcGFyYW1ldGVyc1xuICogQHBhcmFtIHtTdHJpbmd9IG5hbWVcbiAqIEBwYXJhbSB7Kn0gcGFyYW1zXG4gKiBAcGFyYW0ge1N0cmluZ30gbnNQcmVmaXhcbiAqIEBwYXJhbSB7U3RyaW5nfSBuc1VSSVxuICogQHBhcmFtIHtTdHJpbmd9IHR5cGVcbiAqL1xuV1NETC5wcm90b3R5cGUub2JqZWN0VG9Eb2N1bWVudFhNTCA9IGZ1bmN0aW9uIChuYW1lLCBwYXJhbXMsIG5zUHJlZml4LCBuc1VSSSwgdHlwZSkge1xuICAvL0lmIHVzZXIgc3VwcGxpZXMgWE1MIGFscmVhZHksIGp1c3QgdXNlIHRoYXQuICBYTUwgRGVjbGFyYXRpb24gc2hvdWxkIG5vdCBiZSBwcmVzZW50LlxuICBpZiAocGFyYW1zICYmIHBhcmFtcy5feG1sKSB7XG4gICAgcmV0dXJuIHBhcmFtcy5feG1sO1xuICB9XG4gIGxldCBhcmdzID0ge307XG4gIGFyZ3NbbmFtZV0gPSBwYXJhbXM7XG4gIGxldCBwYXJhbWV0ZXJUeXBlT2JqID0gdHlwZSA/IHRoaXMuZmluZFNjaGVtYU9iamVjdChuc1VSSSwgdHlwZSkgOiBudWxsO1xuICByZXR1cm4gdGhpcy5vYmplY3RUb1hNTChhcmdzLCBudWxsLCBuc1ByZWZpeCwgbnNVUkksIHRydWUsIG51bGwsIHBhcmFtZXRlclR5cGVPYmopO1xufTtcblxuLyoqXG4gKiBDcmVhdGUgUlBDIHN0eWxlIHhtbCBzdHJpbmcgZnJvbSB0aGUgcGFyYW1ldGVyc1xuICogQHBhcmFtIHtTdHJpbmd9IG5hbWVcbiAqIEBwYXJhbSB7Kn0gcGFyYW1zXG4gKiBAcGFyYW0ge1N0cmluZ30gbnNQcmVmaXhcbiAqIEBwYXJhbSB7U3RyaW5nfSBuc1VSSVxuICogQHJldHVybnMge3N0cmluZ31cbiAqL1xuV1NETC5wcm90b3R5cGUub2JqZWN0VG9ScGNYTUwgPSBmdW5jdGlvbiAobmFtZSwgcGFyYW1zLCBuc1ByZWZpeCwgbnNVUkksIGlzUGFydHMpIHtcbiAgbGV0IHBhcnRzID0gW107XG4gIGxldCBkZWZzID0gdGhpcy5kZWZpbml0aW9ucztcbiAgbGV0IG5zQXR0ck5hbWUgPSAnX3htbG5zJztcblxuICBuc1ByZWZpeCA9IG5zUHJlZml4IHx8IGZpbmRQcmVmaXgoZGVmcy54bWxucywgbnNVUkkpO1xuXG4gIG5zVVJJID0gbnNVUkkgfHwgZGVmcy54bWxuc1tuc1ByZWZpeF07XG4gIG5zUHJlZml4ID0gbnNQcmVmaXggPT09IFROU19QUkVGSVggPyAnJyA6IChuc1ByZWZpeCArICc6Jyk7XG5cbiAgcGFydHMucHVzaChbJzwnLCBuc1ByZWZpeCwgbmFtZSwgJz4nXS5qb2luKCcnKSk7XG5cbiAgZm9yIChsZXQga2V5IGluIHBhcmFtcykge1xuICAgIGlmICghcGFyYW1zLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgIGNvbnRpbnVlO1xuICAgIH1cbiAgICBpZiAoa2V5ICE9PSBuc0F0dHJOYW1lKSB7XG4gICAgICBsZXQgdmFsdWUgPSBwYXJhbXNba2V5XTtcbiAgICAgIGxldCBwcmVmaXhlZEtleSA9IChpc1BhcnRzID8gJycgOiBuc1ByZWZpeCkgKyBrZXk7XG4gICAgICBsZXQgYXR0cmlidXRlcyA9IFtdO1xuICAgICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUuaGFzT3duUHJvcGVydHkodGhpcy5vcHRpb25zLmF0dHJpYnV0ZXNLZXkpKSB7XG4gICAgICAgIGxldCBhdHRycyA9IHZhbHVlW3RoaXMub3B0aW9ucy5hdHRyaWJ1dGVzS2V5XTtcbiAgICAgICAgZm9yIChsZXQgbiBpbiBhdHRycykge1xuICAgICAgICAgIGF0dHJpYnV0ZXMucHVzaCgnICcgKyBuICsgJz0nICsgJ1wiJyArIGF0dHJzW25dICsgJ1wiJyk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHBhcnRzLnB1c2goWyc8JywgcHJlZml4ZWRLZXldLmNvbmNhdChhdHRyaWJ1dGVzKS5jb25jYXQoJz4nKS5qb2luKCcnKSk7XG4gICAgICBwYXJ0cy5wdXNoKCh0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnKSA/IHRoaXMub2JqZWN0VG9YTUwodmFsdWUsIGtleSwgbnNQcmVmaXgsIG5zVVJJKSA6IHhtbEVzY2FwZSh2YWx1ZSkpO1xuICAgICAgcGFydHMucHVzaChbJzwvJywgcHJlZml4ZWRLZXksICc+J10uam9pbignJykpO1xuICAgIH1cbiAgfVxuICBwYXJ0cy5wdXNoKFsnPC8nLCBuc1ByZWZpeCwgbmFtZSwgJz4nXS5qb2luKCcnKSk7XG4gIHJldHVybiBwYXJ0cy5qb2luKCcnKTtcbn07XG5cblxuZnVuY3Rpb24gYXBwZW5kQ29sb24obnMpIHtcbiAgcmV0dXJuIChucyAmJiBucy5jaGFyQXQobnMubGVuZ3RoIC0gMSkgIT09ICc6JykgPyBucyArICc6JyA6IG5zO1xufVxuXG5mdW5jdGlvbiBub0NvbG9uTmFtZVNwYWNlKG5zKSB7XG4gIHJldHVybiAobnMgJiYgbnMuY2hhckF0KG5zLmxlbmd0aCAtIDEpID09PSAnOicpID8gbnMuc3Vic3RyaW5nKDAsIG5zLmxlbmd0aCAtIDEpIDogbnM7XG59XG5cbldTREwucHJvdG90eXBlLmlzSWdub3JlZE5hbWVTcGFjZSA9IGZ1bmN0aW9uIChucykge1xuICByZXR1cm4gdGhpcy5vcHRpb25zLmlnbm9yZWROYW1lc3BhY2VzLmluZGV4T2YobnMpID4gLTE7XG59O1xuXG5XU0RMLnByb3RvdHlwZS5maWx0ZXJPdXRJZ25vcmVkTmFtZVNwYWNlID0gZnVuY3Rpb24gKG5zKSB7XG4gIGxldCBuYW1lc3BhY2UgPSBub0NvbG9uTmFtZVNwYWNlKG5zKTtcbiAgcmV0dXJuIHRoaXMuaXNJZ25vcmVkTmFtZVNwYWNlKG5hbWVzcGFjZSkgPyAnJyA6IG5hbWVzcGFjZTtcbn07XG5cblxuXG4vKipcbiAqIENvbnZlcnQgYW4gb2JqZWN0IHRvIFhNTC4gIFRoaXMgaXMgYSByZWN1cnNpdmUgbWV0aG9kIGFzIGl0IGNhbGxzIGl0c2VsZi5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqIHRoZSBvYmplY3QgdG8gY29udmVydC5cbiAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lIHRoZSBuYW1lIG9mIHRoZSBlbGVtZW50IChpZiB0aGUgb2JqZWN0IGJlaW5nIHRyYXZlcnNlZCBpc1xuICogYW4gZWxlbWVudCkuXG4gKiBAcGFyYW0ge1N0cmluZ30gbnNQcmVmaXggdGhlIG5hbWVzcGFjZSBwcmVmaXggb2YgdGhlIG9iamVjdCBJLkUuIHhzZC5cbiAqIEBwYXJhbSB7U3RyaW5nfSBuc1VSSSB0aGUgZnVsbCBuYW1lc3BhY2Ugb2YgdGhlIG9iamVjdCBJLkUuIGh0dHA6Ly93My5vcmcvc2NoZW1hLlxuICogQHBhcmFtIHtCb29sZWFufSBpc0ZpcnN0IHdoZXRoZXIgb3Igbm90IHRoaXMgaXMgdGhlIGZpcnN0IGl0ZW0gYmVpbmcgdHJhdmVyc2VkLlxuICogQHBhcmFtIHs/fSB4bWxuc0F0dHJcbiAqIEBwYXJhbSB7P30gcGFyYW1ldGVyVHlwZU9iamVjdFxuICogQHBhcmFtIHtOYW1lc3BhY2VDb250ZXh0fSBuc0NvbnRleHQgTmFtZXNwYWNlIGNvbnRleHRcbiAqL1xuV1NETC5wcm90b3R5cGUub2JqZWN0VG9YTUwgPSBmdW5jdGlvbiAob2JqLCBuYW1lLCBuc1ByZWZpeCwgbnNVUkksIGlzRmlyc3QsIHhtbG5zQXR0ciwgc2NoZW1hT2JqZWN0LCBuc0NvbnRleHQpIHtcbiAgbGV0IHNlbGYgPSB0aGlzO1xuICBsZXQgc2NoZW1hID0gdGhpcy5kZWZpbml0aW9ucy5zY2hlbWFzW25zVVJJXTtcblxuICBsZXQgcGFyZW50TnNQcmVmaXggPSBuc1ByZWZpeCA/IG5zUHJlZml4LnBhcmVudCA6IHVuZGVmaW5lZDtcbiAgaWYgKHR5cGVvZiBwYXJlbnROc1ByZWZpeCAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAvL3dlIGdvdCB0aGUgcGFyZW50TnNQcmVmaXggZm9yIG91ciBhcnJheS4gc2V0dGluZyB0aGUgbmFtZXNwYWNlLWxldGlhYmxlIGJhY2sgdG8gdGhlIGN1cnJlbnQgbmFtZXNwYWNlIHN0cmluZ1xuICAgIG5zUHJlZml4ID0gbnNQcmVmaXguY3VycmVudDtcbiAgfVxuXG4gIHBhcmVudE5zUHJlZml4ID0gbm9Db2xvbk5hbWVTcGFjZShwYXJlbnROc1ByZWZpeCk7XG4gIGlmICh0aGlzLmlzSWdub3JlZE5hbWVTcGFjZShwYXJlbnROc1ByZWZpeCkpIHtcbiAgICBwYXJlbnROc1ByZWZpeCA9ICcnO1xuICB9XG5cbiAgbGV0IHNvYXBIZWFkZXIgPSAhc2NoZW1hO1xuICBsZXQgcXVhbGlmaWVkID0gc2NoZW1hICYmIHNjaGVtYS4kZWxlbWVudEZvcm1EZWZhdWx0ID09PSAncXVhbGlmaWVkJztcbiAgbGV0IHBhcnRzID0gW107XG4gIGxldCBwcmVmaXhOYW1lc3BhY2UgPSAobnNQcmVmaXggfHwgcXVhbGlmaWVkKSAmJiBuc1ByZWZpeCAhPT0gVE5TX1BSRUZJWDtcblxuICBsZXQgeG1sbnNBdHRyaWIgPSAnJztcbiAgaWYgKG5zVVJJICYmIGlzRmlyc3QpIHtcbiAgICBpZiAoc2VsZi5vcHRpb25zLm92ZXJyaWRlUm9vdEVsZW1lbnQgJiYgc2VsZi5vcHRpb25zLm92ZXJyaWRlUm9vdEVsZW1lbnQueG1sbnNBdHRyaWJ1dGVzKSB7XG4gICAgICBzZWxmLm9wdGlvbnMub3ZlcnJpZGVSb290RWxlbWVudC54bWxuc0F0dHJpYnV0ZXMuZm9yRWFjaChmdW5jdGlvbiAoYXR0cmlidXRlKSB7XG4gICAgICAgIHhtbG5zQXR0cmliICs9ICcgJyArIGF0dHJpYnV0ZS5uYW1lICsgJz1cIicgKyBhdHRyaWJ1dGUudmFsdWUgKyAnXCInO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChwcmVmaXhOYW1lc3BhY2UgJiYgIXRoaXMuaXNJZ25vcmVkTmFtZVNwYWNlKG5zUHJlZml4KSkge1xuICAgICAgICAvLyByZXNvbHZlIHRoZSBwcmVmaXggbmFtZXNwYWNlXG4gICAgICAgIHhtbG5zQXR0cmliICs9ICcgeG1sbnM6JyArIG5zUHJlZml4ICsgJz1cIicgKyBuc1VSSSArICdcIic7XG4gICAgICB9XG4gICAgICAvLyBvbmx5IGFkZCBkZWZhdWx0IG5hbWVzcGFjZSBpZiB0aGUgc2NoZW1hIGVsZW1lbnRGb3JtRGVmYXVsdCBpcyBxdWFsaWZpZWRcbiAgICAgIGlmIChxdWFsaWZpZWQgfHwgc29hcEhlYWRlcikgeG1sbnNBdHRyaWIgKz0gJyB4bWxucz1cIicgKyBuc1VSSSArICdcIic7XG4gICAgfVxuICB9XG5cbiAgaWYgKCFuc0NvbnRleHQpIHtcbiAgICBuc0NvbnRleHQgPSBuZXcgTmFtZXNwYWNlQ29udGV4dCgpO1xuICAgIG5zQ29udGV4dC5kZWNsYXJlTmFtZXNwYWNlKG5zUHJlZml4LCBuc1VSSSk7XG4gIH0gZWxzZSB7XG4gICAgbnNDb250ZXh0LnB1c2hDb250ZXh0KCk7XG4gIH1cblxuICAvLyBleHBsaWNpdGx5IHVzZSB4bWxucyBhdHRyaWJ1dGUgaWYgYXZhaWxhYmxlXG4gIGlmICh4bWxuc0F0dHIgJiYgIShzZWxmLm9wdGlvbnMub3ZlcnJpZGVSb290RWxlbWVudCAmJiBzZWxmLm9wdGlvbnMub3ZlcnJpZGVSb290RWxlbWVudC54bWxuc0F0dHJpYnV0ZXMpKSB7XG4gICAgeG1sbnNBdHRyaWIgPSB4bWxuc0F0dHI7XG4gIH1cblxuICBsZXQgbnMgPSAnJztcblxuICBpZiAoc2VsZi5vcHRpb25zLm92ZXJyaWRlUm9vdEVsZW1lbnQgJiYgaXNGaXJzdCkge1xuICAgIG5zID0gc2VsZi5vcHRpb25zLm92ZXJyaWRlUm9vdEVsZW1lbnQubmFtZXNwYWNlO1xuICB9IGVsc2UgaWYgKHByZWZpeE5hbWVzcGFjZSAmJiAocXVhbGlmaWVkIHx8IGlzRmlyc3QgfHwgc29hcEhlYWRlcikgJiYgIXRoaXMuaXNJZ25vcmVkTmFtZVNwYWNlKG5zUHJlZml4KSkge1xuICAgIG5zID0gbnNQcmVmaXg7XG4gIH1cblxuICBsZXQgaSwgbjtcbiAgLy8gc3RhcnQgYnVpbGRpbmcgb3V0IFhNTCBzdHJpbmcuXG4gIGlmIChBcnJheS5pc0FycmF5KG9iaikpIHtcbiAgICBmb3IgKGkgPSAwLCBuID0gb2JqLmxlbmd0aDsgaSA8IG47IGkrKykge1xuICAgICAgbGV0IGl0ZW0gPSBvYmpbaV07XG4gICAgICBsZXQgYXJyYXlBdHRyID0gc2VsZi5wcm9jZXNzQXR0cmlidXRlcyhpdGVtLCBuc0NvbnRleHQpLFxuICAgICAgICBjb3JyZWN0T3V0ZXJOc1ByZWZpeCA9IHBhcmVudE5zUHJlZml4IHx8IG5zOyAvL3VzaW5nIHRoZSBwYXJlbnQgbmFtZXNwYWNlIHByZWZpeCBpZiBnaXZlblxuXG4gICAgICBsZXQgYm9keSA9IHNlbGYub2JqZWN0VG9YTUwoaXRlbSwgbmFtZSwgbnNQcmVmaXgsIG5zVVJJLCBmYWxzZSwgbnVsbCwgc2NoZW1hT2JqZWN0LCBuc0NvbnRleHQpO1xuXG4gICAgICBsZXQgb3BlbmluZ1RhZ1BhcnRzID0gWyc8JywgYXBwZW5kQ29sb24oY29ycmVjdE91dGVyTnNQcmVmaXgpLCBuYW1lLCBhcnJheUF0dHIsIHhtbG5zQXR0cmliXTtcblxuICAgICAgaWYgKGJvZHkgPT09ICcnICYmIHNlbGYub3B0aW9ucy51c2VFbXB0eVRhZykge1xuICAgICAgICAvLyBVc2UgZW1wdHkgKHNlbGYtY2xvc2luZykgdGFncyBpZiBubyBjb250ZW50c1xuICAgICAgICBvcGVuaW5nVGFnUGFydHMucHVzaCgnIC8+Jyk7XG4gICAgICAgIHBhcnRzLnB1c2gob3BlbmluZ1RhZ1BhcnRzLmpvaW4oJycpKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIG9wZW5pbmdUYWdQYXJ0cy5wdXNoKCc+Jyk7XG4gICAgICAgIGlmIChzZWxmLm9wdGlvbnMubmFtZXNwYWNlQXJyYXlFbGVtZW50cyB8fCBpID09PSAwKSB7XG4gICAgICAgICAgcGFydHMucHVzaChvcGVuaW5nVGFnUGFydHMuam9pbignJykpO1xuICAgICAgICB9XG4gICAgICAgIHBhcnRzLnB1c2goYm9keSk7XG4gICAgICAgIGlmIChzZWxmLm9wdGlvbnMubmFtZXNwYWNlQXJyYXlFbGVtZW50cyB8fCBpID09PSBuIC0gMSkge1xuICAgICAgICAgIHBhcnRzLnB1c2goWyc8LycsIGFwcGVuZENvbG9uKGNvcnJlY3RPdXRlck5zUHJlZml4KSwgbmFtZSwgJz4nXS5qb2luKCcnKSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH0gZWxzZSBpZiAodHlwZW9mIG9iaiA9PT0gJ29iamVjdCcpIHtcbiAgICBmb3IgKG5hbWUgaW4gb2JqKSB7XG4gICAgICBpZiAoIW9iai5oYXNPd25Qcm9wZXJ0eShuYW1lKSkgY29udGludWU7XG4gICAgICAvL2Rvbid0IHByb2Nlc3MgYXR0cmlidXRlcyBhcyBlbGVtZW50XG4gICAgICBpZiAobmFtZSA9PT0gc2VsZi5vcHRpb25zLmF0dHJpYnV0ZXNLZXkpIHtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG4gICAgICAvL0l0cyB0aGUgdmFsdWUgb2YgYSB4bWwgb2JqZWN0LiBSZXR1cm4gaXQgZGlyZWN0bHkuXG4gICAgICBpZiAobmFtZSA9PT0gc2VsZi5vcHRpb25zLnhtbEtleSkge1xuICAgICAgICBuc0NvbnRleHQucG9wQ29udGV4dCgpO1xuICAgICAgICByZXR1cm4gb2JqW25hbWVdO1xuICAgICAgfVxuICAgICAgLy9JdHMgdGhlIHZhbHVlIG9mIGFuIGl0ZW0uIFJldHVybiBpdCBkaXJlY3RseS5cbiAgICAgIGlmIChuYW1lID09PSBzZWxmLm9wdGlvbnMudmFsdWVLZXkpIHtcbiAgICAgICAgbnNDb250ZXh0LnBvcENvbnRleHQoKTtcbiAgICAgICAgcmV0dXJuIHhtbEVzY2FwZShvYmpbbmFtZV0pO1xuICAgICAgfVxuXG4gICAgICBsZXQgY2hpbGQgPSBvYmpbbmFtZV07XG4gICAgICBpZiAodHlwZW9mIGNoaWxkID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cblxuICAgICAgbGV0IGF0dHIgPSBzZWxmLnByb2Nlc3NBdHRyaWJ1dGVzKGNoaWxkLCBuc0NvbnRleHQpO1xuXG4gICAgICBsZXQgdmFsdWUgPSAnJztcbiAgICAgIGxldCBub25TdWJOYW1lU3BhY2UgPSAnJztcbiAgICAgIGxldCBlbXB0eU5vblN1Yk5hbWVTcGFjZSA9IGZhbHNlO1xuXG4gICAgICBsZXQgbmFtZVdpdGhOc1JlZ2V4ID0gL14oW146XSspOihbXjpdKykkLy5leGVjKG5hbWUpO1xuICAgICAgaWYgKG5hbWVXaXRoTnNSZWdleCkge1xuICAgICAgICBub25TdWJOYW1lU3BhY2UgPSBuYW1lV2l0aE5zUmVnZXhbMV0gKyAnOic7XG4gICAgICAgIG5hbWUgPSBuYW1lV2l0aE5zUmVnZXhbMl07XG4gICAgICB9IGVsc2UgaWYgKG5hbWVbMF0gPT09ICc6Jykge1xuICAgICAgICBlbXB0eU5vblN1Yk5hbWVTcGFjZSA9IHRydWU7XG4gICAgICAgIG5hbWUgPSBuYW1lLnN1YnN0cigxKTtcbiAgICAgIH1cblxuICAgICAgaWYgKGlzRmlyc3QpIHtcbiAgICAgICAgdmFsdWUgPSBzZWxmLm9iamVjdFRvWE1MKGNoaWxkLCBuYW1lLCBuc1ByZWZpeCwgbnNVUkksIGZhbHNlLCBudWxsLCBzY2hlbWFPYmplY3QsIG5zQ29udGV4dCk7XG4gICAgICB9IGVsc2Uge1xuXG4gICAgICAgIGlmIChzZWxmLmRlZmluaXRpb25zLnNjaGVtYXMpIHtcbiAgICAgICAgICBpZiAoc2NoZW1hKSB7XG4gICAgICAgICAgICBsZXQgY2hpbGRTY2hlbWFPYmplY3QgPSBzZWxmLmZpbmRDaGlsZFNjaGVtYU9iamVjdChzY2hlbWFPYmplY3QsIG5hbWUpO1xuICAgICAgICAgICAgLy9maW5kIHN1YiBuYW1lc3BhY2UgaWYgbm90IGEgcHJpbWl0aXZlXG4gICAgICAgICAgICBpZiAoY2hpbGRTY2hlbWFPYmplY3QgJiZcbiAgICAgICAgICAgICAgKChjaGlsZFNjaGVtYU9iamVjdC4kdHlwZSAmJiAoY2hpbGRTY2hlbWFPYmplY3QuJHR5cGUuaW5kZXhPZigneHNkOicpID09PSAtMSkpIHx8XG4gICAgICAgICAgICAgICAgY2hpbGRTY2hlbWFPYmplY3QuJHJlZiB8fCBjaGlsZFNjaGVtYU9iamVjdC4kbmFtZSkpIHtcbiAgICAgICAgICAgICAgLyppZiB0aGUgYmFzZSBuYW1lIHNwYWNlIG9mIHRoZSBjaGlsZHJlbiBpcyBub3QgaW4gdGhlIGluZ29yZWRTY2hlbWFOYW1zcGFjZXMgd2UgdXNlIGl0LlxuICAgICAgICAgICAgICAgVGhpcyBpcyBiZWNhdXNlIGluIHNvbWUgc2VydmljZXMgdGhlIGNoaWxkIG5vZGVzIGRvIG5vdCBuZWVkIHRoZSBiYXNlTmFtZVNwYWNlLlxuICAgICAgICAgICAgICAgKi9cblxuICAgICAgICAgICAgICBsZXQgY2hpbGROc1ByZWZpeDogYW55ID0gJyc7XG4gICAgICAgICAgICAgIGxldCBjaGlsZE5hbWUgPSAnJztcbiAgICAgICAgICAgICAgbGV0IGNoaWxkTnNVUkk7XG4gICAgICAgICAgICAgIGxldCBjaGlsZFhtbG5zQXR0cmliID0gJyc7XG5cbiAgICAgICAgICAgICAgbGV0IGVsZW1lbnRRTmFtZSA9IGNoaWxkU2NoZW1hT2JqZWN0LiRyZWYgfHwgY2hpbGRTY2hlbWFPYmplY3QuJG5hbWU7XG4gICAgICAgICAgICAgIGlmIChlbGVtZW50UU5hbWUpIHtcbiAgICAgICAgICAgICAgICBlbGVtZW50UU5hbWUgPSBzcGxpdFFOYW1lKGVsZW1lbnRRTmFtZSk7XG4gICAgICAgICAgICAgICAgY2hpbGROYW1lID0gZWxlbWVudFFOYW1lLm5hbWU7XG4gICAgICAgICAgICAgICAgaWYgKGVsZW1lbnRRTmFtZS5wcmVmaXggPT09IFROU19QUkVGSVgpIHtcbiAgICAgICAgICAgICAgICAgIC8vIExvY2FsIGVsZW1lbnRcbiAgICAgICAgICAgICAgICAgIGNoaWxkTnNVUkkgPSBjaGlsZFNjaGVtYU9iamVjdC4kdGFyZ2V0TmFtZXNwYWNlO1xuICAgICAgICAgICAgICAgICAgY2hpbGROc1ByZWZpeCA9IG5zQ29udGV4dC5yZWdpc3Rlck5hbWVzcGFjZShjaGlsZE5zVVJJKTtcbiAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmlzSWdub3JlZE5hbWVTcGFjZShjaGlsZE5zUHJlZml4KSkge1xuICAgICAgICAgICAgICAgICAgICBjaGlsZE5zUHJlZml4ID0gbnNQcmVmaXg7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIGNoaWxkTnNQcmVmaXggPSBlbGVtZW50UU5hbWUucHJlZml4O1xuICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuaXNJZ25vcmVkTmFtZVNwYWNlKGNoaWxkTnNQcmVmaXgpKSB7XG4gICAgICAgICAgICAgICAgICAgIGNoaWxkTnNQcmVmaXggPSBuc1ByZWZpeDtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGNoaWxkTnNVUkkgPSBzY2hlbWEueG1sbnNbY2hpbGROc1ByZWZpeF0gfHwgc2VsZi5kZWZpbml0aW9ucy54bWxuc1tjaGlsZE5zUHJlZml4XTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBsZXQgdW5xdWFsaWZpZWQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAvLyBDaGVjayBxdWFsaWZpY2F0aW9uIGZvcm0gZm9yIGxvY2FsIGVsZW1lbnRzXG4gICAgICAgICAgICAgICAgaWYgKGNoaWxkU2NoZW1hT2JqZWN0LiRuYW1lICYmIGNoaWxkU2NoZW1hT2JqZWN0LnRhcmdldE5hbWVzcGFjZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICBpZiAoY2hpbGRTY2hlbWFPYmplY3QuJGZvcm0gPT09ICd1bnF1YWxpZmllZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgdW5xdWFsaWZpZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChjaGlsZFNjaGVtYU9iamVjdC4kZm9ybSA9PT0gJ3F1YWxpZmllZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgdW5xdWFsaWZpZWQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHVucXVhbGlmaWVkID0gc2NoZW1hLiRlbGVtZW50Rm9ybURlZmF1bHQgIT09ICdxdWFsaWZpZWQnO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAodW5xdWFsaWZpZWQpIHtcbiAgICAgICAgICAgICAgICAgIGNoaWxkTnNQcmVmaXggPSAnJztcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoY2hpbGROc1VSSSAmJiBjaGlsZE5zUHJlZml4KSB7XG4gICAgICAgICAgICAgICAgICBpZiAobnNDb250ZXh0LmRlY2xhcmVOYW1lc3BhY2UoY2hpbGROc1ByZWZpeCwgY2hpbGROc1VSSSkpIHtcbiAgICAgICAgICAgICAgICAgICAgY2hpbGRYbWxuc0F0dHJpYiA9ICcgeG1sbnM6JyArIGNoaWxkTnNQcmVmaXggKyAnPVwiJyArIGNoaWxkTnNVUkkgKyAnXCInO1xuICAgICAgICAgICAgICAgICAgICB4bWxuc0F0dHJpYiArPSBjaGlsZFhtbG5zQXR0cmliO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgIGxldCByZXNvbHZlZENoaWxkU2NoZW1hT2JqZWN0O1xuICAgICAgICAgICAgICBpZiAoY2hpbGRTY2hlbWFPYmplY3QuJHR5cGUpIHtcbiAgICAgICAgICAgICAgICBsZXQgdHlwZVFOYW1lID0gc3BsaXRRTmFtZShjaGlsZFNjaGVtYU9iamVjdC4kdHlwZSk7XG4gICAgICAgICAgICAgICAgbGV0IHR5cGVQcmVmaXggPSB0eXBlUU5hbWUucHJlZml4O1xuICAgICAgICAgICAgICAgIGxldCB0eXBlVVJJID0gc2NoZW1hLnhtbG5zW3R5cGVQcmVmaXhdIHx8IHNlbGYuZGVmaW5pdGlvbnMueG1sbnNbdHlwZVByZWZpeF07XG4gICAgICAgICAgICAgICAgY2hpbGROc1VSSSA9IHR5cGVVUkk7XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVVUkkgIT09ICdodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYScgJiYgdHlwZVByZWZpeCAhPT0gVE5TX1BSRUZJWCkge1xuICAgICAgICAgICAgICAgICAgLy8gQWRkIHRoZSBwcmVmaXgvbmFtZXNwYWNlIG1hcHBpbmcsIGJ1dCBub3QgZGVjbGFyZSBpdFxuICAgICAgICAgICAgICAgICAgbnNDb250ZXh0LmFkZE5hbWVzcGFjZSh0eXBlUHJlZml4LCB0eXBlVVJJKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmVzb2x2ZWRDaGlsZFNjaGVtYU9iamVjdCA9XG4gICAgICAgICAgICAgICAgICBzZWxmLmZpbmRTY2hlbWFUeXBlKHR5cGVRTmFtZS5uYW1lLCB0eXBlVVJJKSB8fCBjaGlsZFNjaGVtYU9iamVjdDtcbiAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXNvbHZlZENoaWxkU2NoZW1hT2JqZWN0ID1cbiAgICAgICAgICAgICAgICAgIHNlbGYuZmluZFNjaGVtYU9iamVjdChjaGlsZE5zVVJJLCBjaGlsZE5hbWUpIHx8IGNoaWxkU2NoZW1hT2JqZWN0O1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgaWYgKGNoaWxkU2NoZW1hT2JqZWN0LiRiYXNlTmFtZVNwYWNlICYmIHRoaXMub3B0aW9ucy5pZ25vcmVCYXNlTmFtZVNwYWNlcykge1xuICAgICAgICAgICAgICAgIGNoaWxkTnNQcmVmaXggPSBuc1ByZWZpeDtcbiAgICAgICAgICAgICAgICBjaGlsZE5zVVJJID0gbnNVUkk7XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmlnbm9yZUJhc2VOYW1lU3BhY2VzKSB7XG4gICAgICAgICAgICAgICAgY2hpbGROc1ByZWZpeCA9ICcnO1xuICAgICAgICAgICAgICAgIGNoaWxkTnNVUkkgPSAnJztcbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgIG5zID0gY2hpbGROc1ByZWZpeDtcblxuICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShjaGlsZCkpIHtcbiAgICAgICAgICAgICAgICAvL2ZvciBhcnJheXMsIHdlIG5lZWQgdG8gcmVtZW1iZXIgdGhlIGN1cnJlbnQgbmFtZXNwYWNlXG4gICAgICAgICAgICAgICAgY2hpbGROc1ByZWZpeCA9IHtcbiAgICAgICAgICAgICAgICAgIGN1cnJlbnQ6IGNoaWxkTnNQcmVmaXgsXG4gICAgICAgICAgICAgICAgICBwYXJlbnQ6IG5zXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAvL3BhcmVudCAoYXJyYXkpIGFscmVhZHkgZ290IHRoZSBuYW1lc3BhY2VcbiAgICAgICAgICAgICAgICBjaGlsZFhtbG5zQXR0cmliID0gbnVsbDtcbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgIHZhbHVlID0gc2VsZi5vYmplY3RUb1hNTChjaGlsZCwgbmFtZSwgY2hpbGROc1ByZWZpeCwgY2hpbGROc1VSSSxcbiAgICAgICAgICAgICAgICBmYWxzZSwgY2hpbGRYbWxuc0F0dHJpYiwgcmVzb2x2ZWRDaGlsZFNjaGVtYU9iamVjdCwgbnNDb250ZXh0KTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAob2JqW3NlbGYub3B0aW9ucy5hdHRyaWJ1dGVzS2V5XSAmJiBvYmpbc2VsZi5vcHRpb25zLmF0dHJpYnV0ZXNLZXldLnhzaV90eXBlKSB7XG4gICAgICAgICAgICAgIC8vaWYgcGFyZW50IG9iamVjdCBoYXMgY29tcGxleCB0eXBlIGRlZmluZWQgYW5kIGNoaWxkIG5vdCBmb3VuZCBpbiBwYXJlbnRcbiAgICAgICAgICAgICAgbGV0IGNvbXBsZXRlQ2hpbGRQYXJhbVR5cGVPYmplY3QgPSBzZWxmLmZpbmRDaGlsZFNjaGVtYU9iamVjdChcbiAgICAgICAgICAgICAgICBvYmpbc2VsZi5vcHRpb25zLmF0dHJpYnV0ZXNLZXldLnhzaV90eXBlLnR5cGUsXG4gICAgICAgICAgICAgICAgb2JqW3NlbGYub3B0aW9ucy5hdHRyaWJ1dGVzS2V5XS54c2lfdHlwZS54bWxucyk7XG5cbiAgICAgICAgICAgICAgbm9uU3ViTmFtZVNwYWNlID0gb2JqW3NlbGYub3B0aW9ucy5hdHRyaWJ1dGVzS2V5XS54c2lfdHlwZS5wcmVmaXg7XG4gICAgICAgICAgICAgIG5zQ29udGV4dC5hZGROYW1lc3BhY2Uob2JqW3NlbGYub3B0aW9ucy5hdHRyaWJ1dGVzS2V5XS54c2lfdHlwZS5wcmVmaXgsXG4gICAgICAgICAgICAgICAgb2JqW3NlbGYub3B0aW9ucy5hdHRyaWJ1dGVzS2V5XS54c2lfdHlwZS54bWxucyk7XG4gICAgICAgICAgICAgIHZhbHVlID0gc2VsZi5vYmplY3RUb1hNTChjaGlsZCwgbmFtZSwgb2JqW3NlbGYub3B0aW9ucy5hdHRyaWJ1dGVzS2V5XS54c2lfdHlwZS5wcmVmaXgsXG4gICAgICAgICAgICAgICAgb2JqW3NlbGYub3B0aW9ucy5hdHRyaWJ1dGVzS2V5XS54c2lfdHlwZS54bWxucywgZmFsc2UsIG51bGwsIG51bGwsIG5zQ29udGV4dCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShjaGlsZCkpIHtcbiAgICAgICAgICAgICAgICBuYW1lID0gbm9uU3ViTmFtZVNwYWNlICsgbmFtZTtcbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgIHZhbHVlID0gc2VsZi5vYmplY3RUb1hNTChjaGlsZCwgbmFtZSwgbnNQcmVmaXgsIG5zVVJJLCBmYWxzZSwgbnVsbCwgbnVsbCwgbnNDb250ZXh0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdmFsdWUgPSBzZWxmLm9iamVjdFRvWE1MKGNoaWxkLCBuYW1lLCBuc1ByZWZpeCwgbnNVUkksIGZhbHNlLCBudWxsLCBudWxsLCBuc0NvbnRleHQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBucyA9IG5vQ29sb25OYW1lU3BhY2UobnMpO1xuICAgICAgaWYgKHByZWZpeE5hbWVzcGFjZSAmJiAhcXVhbGlmaWVkICYmIGlzRmlyc3QgJiYgIXNlbGYub3B0aW9ucy5vdmVycmlkZVJvb3RFbGVtZW50KSB7XG4gICAgICAgIG5zID0gbnNQcmVmaXg7XG4gICAgICB9IGVsc2UgaWYgKHRoaXMuaXNJZ25vcmVkTmFtZVNwYWNlKG5zKSkge1xuICAgICAgICBucyA9ICcnO1xuICAgICAgfVxuXG4gICAgICBsZXQgdXNlRW1wdHlUYWcgPSAhdmFsdWUgJiYgc2VsZi5vcHRpb25zLnVzZUVtcHR5VGFnO1xuICAgICAgaWYgKCFBcnJheS5pc0FycmF5KGNoaWxkKSkge1xuICAgICAgICAvLyBzdGFydCB0YWdcbiAgICAgICAgcGFydHMucHVzaChbJzwnLCBlbXB0eU5vblN1Yk5hbWVTcGFjZSA/ICcnIDogYXBwZW5kQ29sb24obm9uU3ViTmFtZVNwYWNlIHx8IG5zKSwgbmFtZSwgYXR0ciwgeG1sbnNBdHRyaWIsXG4gICAgICAgICAgKGNoaWxkID09PSBudWxsID8gJyB4c2k6bmlsPVwidHJ1ZVwiJyA6ICcnKSxcbiAgICAgICAgICB1c2VFbXB0eVRhZyA/ICcgLz4nIDogJz4nXG4gICAgICAgIF0uam9pbignJykpO1xuICAgICAgfVxuXG4gICAgICBpZiAoIXVzZUVtcHR5VGFnKSB7XG4gICAgICAgIHBhcnRzLnB1c2godmFsdWUpO1xuICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkoY2hpbGQpKSB7XG4gICAgICAgICAgLy8gZW5kIHRhZ1xuICAgICAgICAgIHBhcnRzLnB1c2goWyc8LycsIGVtcHR5Tm9uU3ViTmFtZVNwYWNlID8gJycgOiBhcHBlbmRDb2xvbihub25TdWJOYW1lU3BhY2UgfHwgbnMpLCBuYW1lLCAnPiddLmpvaW4oJycpKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSBlbHNlIGlmIChvYmogIT09IHVuZGVmaW5lZCkge1xuICAgIHBhcnRzLnB1c2goKHNlbGYub3B0aW9ucy5lc2NhcGVYTUwpID8geG1sRXNjYXBlKG9iaikgOiBvYmopO1xuICB9XG4gIG5zQ29udGV4dC5wb3BDb250ZXh0KCk7XG4gIHJldHVybiBwYXJ0cy5qb2luKCcnKTtcbn07XG5cbldTREwucHJvdG90eXBlLnByb2Nlc3NBdHRyaWJ1dGVzID0gZnVuY3Rpb24gKGNoaWxkLCBuc0NvbnRleHQpIHtcbiAgbGV0IGF0dHIgPSAnJztcblxuICBpZiAoY2hpbGQgPT09IG51bGwpIHtcbiAgICBjaGlsZCA9IFtdO1xuICB9XG5cbiAgbGV0IGF0dHJPYmogPSBjaGlsZFt0aGlzLm9wdGlvbnMuYXR0cmlidXRlc0tleV07XG4gIGlmIChhdHRyT2JqICYmIGF0dHJPYmoueHNpX3R5cGUpIHtcbiAgICBsZXQgeHNpVHlwZSA9IGF0dHJPYmoueHNpX3R5cGU7XG5cbiAgICBsZXQgcHJlZml4ID0geHNpVHlwZS5wcmVmaXggfHwgeHNpVHlwZS5uYW1lc3BhY2U7XG4gICAgLy8gR2VuZXJhdGUgYSBuZXcgbmFtZXNwYWNlIGZvciBjb21wbGV4IGV4dGVuc2lvbiBpZiBvbmUgbm90IHByb3ZpZGVkXG4gICAgaWYgKCFwcmVmaXgpIHtcbiAgICAgIHByZWZpeCA9IG5zQ29udGV4dC5yZWdpc3Rlck5hbWVzcGFjZSh4c2lUeXBlLnhtbG5zKTtcbiAgICB9IGVsc2Uge1xuICAgICAgbnNDb250ZXh0LmRlY2xhcmVOYW1lc3BhY2UocHJlZml4LCB4c2lUeXBlLnhtbG5zKTtcbiAgICB9XG4gICAgeHNpVHlwZS5wcmVmaXggPSBwcmVmaXg7XG4gIH1cblxuXG4gIGlmIChhdHRyT2JqKSB7XG4gICAgZm9yIChsZXQgYXR0cktleSBpbiBhdHRyT2JqKSB7XG4gICAgICAvL2hhbmRsZSBjb21wbGV4IGV4dGVuc2lvbiBzZXBhcmF0ZWx5XG4gICAgICBpZiAoYXR0cktleSA9PT0gJ3hzaV90eXBlJykge1xuICAgICAgICBsZXQgYXR0clZhbHVlID0gYXR0ck9ialthdHRyS2V5XTtcbiAgICAgICAgYXR0ciArPSAnIHhzaTp0eXBlPVwiJyArIGF0dHJWYWx1ZS5wcmVmaXggKyAnOicgKyBhdHRyVmFsdWUudHlwZSArICdcIic7XG4gICAgICAgIGF0dHIgKz0gJyB4bWxuczonICsgYXR0clZhbHVlLnByZWZpeCArICc9XCInICsgYXR0clZhbHVlLnhtbG5zICsgJ1wiJztcblxuICAgICAgICBjb250aW51ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGF0dHIgKz0gJyAnICsgYXR0cktleSArICc9XCInICsgeG1sRXNjYXBlKGF0dHJPYmpbYXR0cktleV0pICsgJ1wiJztcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gYXR0cjtcbn07XG5cbi8qKlxuICogTG9vayB1cCBhIHNjaGVtYSB0eXBlIGRlZmluaXRpb25cbiAqIEBwYXJhbSBuYW1lXG4gKiBAcGFyYW0gbnNVUklcbiAqIEByZXR1cm5zIHsqfVxuICovXG5XU0RMLnByb3RvdHlwZS5maW5kU2NoZW1hVHlwZSA9IGZ1bmN0aW9uIChuYW1lLCBuc1VSSSkge1xuICBpZiAoIXRoaXMuZGVmaW5pdGlvbnMuc2NoZW1hcyB8fCAhbmFtZSB8fCAhbnNVUkkpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIGxldCBzY2hlbWEgPSB0aGlzLmRlZmluaXRpb25zLnNjaGVtYXNbbnNVUkldO1xuICBpZiAoIXNjaGVtYSB8fCAhc2NoZW1hLmNvbXBsZXhUeXBlcykge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgcmV0dXJuIHNjaGVtYS5jb21wbGV4VHlwZXNbbmFtZV07XG59O1xuXG5XU0RMLnByb3RvdHlwZS5maW5kQ2hpbGRTY2hlbWFPYmplY3QgPSBmdW5jdGlvbiAocGFyYW1ldGVyVHlwZU9iaiwgY2hpbGROYW1lLCBiYWNrdHJhY2UpIHtcbiAgaWYgKCFwYXJhbWV0ZXJUeXBlT2JqIHx8ICFjaGlsZE5hbWUpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIGlmICghYmFja3RyYWNlKSB7XG4gICAgYmFja3RyYWNlID0gW107XG4gIH1cblxuICBpZiAoYmFja3RyYWNlLmluZGV4T2YocGFyYW1ldGVyVHlwZU9iaikgPj0gMCkge1xuICAgIC8vIFdlJ3ZlIHJlY3Vyc2VkIGJhY2sgdG8gb3Vyc2VsdmVzOyBicmVhay5cbiAgICByZXR1cm4gbnVsbDtcbiAgfSBlbHNlIHtcbiAgICBiYWNrdHJhY2UgPSBiYWNrdHJhY2UuY29uY2F0KFtwYXJhbWV0ZXJUeXBlT2JqXSk7XG4gIH1cblxuICBsZXQgZm91bmQgPSBudWxsLFxuICAgIGkgPSAwLFxuICAgIGNoaWxkLFxuICAgIHJlZjtcblxuICBpZiAoQXJyYXkuaXNBcnJheShwYXJhbWV0ZXJUeXBlT2JqLiRsb29rdXBUeXBlcykgJiYgcGFyYW1ldGVyVHlwZU9iai4kbG9va3VwVHlwZXMubGVuZ3RoKSB7XG4gICAgbGV0IHR5cGVzID0gcGFyYW1ldGVyVHlwZU9iai4kbG9va3VwVHlwZXM7XG5cbiAgICBmb3IgKGkgPSAwOyBpIDwgdHlwZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGxldCB0eXBlT2JqID0gdHlwZXNbaV07XG5cbiAgICAgIGlmICh0eXBlT2JqLiRuYW1lID09PSBjaGlsZE5hbWUpIHtcbiAgICAgICAgZm91bmQgPSB0eXBlT2JqO1xuICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBsZXQgb2JqZWN0ID0gcGFyYW1ldGVyVHlwZU9iajtcbiAgaWYgKG9iamVjdC4kbmFtZSA9PT0gY2hpbGROYW1lICYmIG9iamVjdC5uYW1lID09PSAnZWxlbWVudCcpIHtcbiAgICByZXR1cm4gb2JqZWN0O1xuICB9XG4gIGlmIChvYmplY3QuJHJlZikge1xuICAgIHJlZiA9IHNwbGl0UU5hbWUob2JqZWN0LiRyZWYpO1xuICAgIGlmIChyZWYubmFtZSA9PT0gY2hpbGROYW1lKSB7XG4gICAgICByZXR1cm4gb2JqZWN0O1xuICAgIH1cbiAgfVxuXG4gIGxldCBjaGlsZE5zVVJJO1xuXG4gIC8vIHdhbnQgdG8gYXZvaWQgdW5lY2Vzc2FyeSByZWN1cnNpb24gdG8gaW1wcm92ZSBwZXJmb3JtYW5jZVxuICBpZiAob2JqZWN0LiR0eXBlICYmIGJhY2t0cmFjZS5sZW5ndGggPT09IDEpIHtcbiAgICBsZXQgdHlwZUluZm8gPSBzcGxpdFFOYW1lKG9iamVjdC4kdHlwZSk7XG4gICAgaWYgKHR5cGVJbmZvLnByZWZpeCA9PT0gVE5TX1BSRUZJWCkge1xuICAgICAgY2hpbGROc1VSSSA9IHBhcmFtZXRlclR5cGVPYmouJHRhcmdldE5hbWVzcGFjZTtcbiAgICB9IGVsc2Uge1xuICAgICAgY2hpbGROc1VSSSA9IHRoaXMuZGVmaW5pdGlvbnMueG1sbnNbdHlwZUluZm8ucHJlZml4XTtcbiAgICB9XG4gICAgbGV0IHR5cGVEZWYgPSB0aGlzLmZpbmRTY2hlbWFUeXBlKHR5cGVJbmZvLm5hbWUsIGNoaWxkTnNVUkkpO1xuICAgIGlmICh0eXBlRGVmKSB7XG4gICAgICByZXR1cm4gdGhpcy5maW5kQ2hpbGRTY2hlbWFPYmplY3QodHlwZURlZiwgY2hpbGROYW1lLCBiYWNrdHJhY2UpO1xuICAgIH1cbiAgfVxuXG4gIGlmIChvYmplY3QuY2hpbGRyZW4pIHtcbiAgICBmb3IgKGkgPSAwLCBjaGlsZDsgY2hpbGQgPSBvYmplY3QuY2hpbGRyZW5baV07IGkrKykge1xuICAgICAgZm91bmQgPSB0aGlzLmZpbmRDaGlsZFNjaGVtYU9iamVjdChjaGlsZCwgY2hpbGROYW1lLCBiYWNrdHJhY2UpO1xuICAgICAgaWYgKGZvdW5kKSB7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuXG4gICAgICBpZiAoY2hpbGQuJGJhc2UpIHtcbiAgICAgICAgbGV0IGJhc2VRTmFtZSA9IHNwbGl0UU5hbWUoY2hpbGQuJGJhc2UpO1xuICAgICAgICBsZXQgY2hpbGROYW1lU3BhY2UgPSBiYXNlUU5hbWUucHJlZml4ID09PSBUTlNfUFJFRklYID8gJycgOiBiYXNlUU5hbWUucHJlZml4O1xuICAgICAgICBjaGlsZE5zVVJJID0gY2hpbGQueG1sbnNbYmFzZVFOYW1lLnByZWZpeF0gfHwgdGhpcy5kZWZpbml0aW9ucy54bWxuc1tiYXNlUU5hbWUucHJlZml4XTtcblxuICAgICAgICBsZXQgZm91bmRCYXNlID0gdGhpcy5maW5kU2NoZW1hVHlwZShiYXNlUU5hbWUubmFtZSwgY2hpbGROc1VSSSk7XG5cbiAgICAgICAgaWYgKGZvdW5kQmFzZSkge1xuICAgICAgICAgIGZvdW5kID0gdGhpcy5maW5kQ2hpbGRTY2hlbWFPYmplY3QoZm91bmRCYXNlLCBjaGlsZE5hbWUsIGJhY2t0cmFjZSk7XG5cbiAgICAgICAgICBpZiAoZm91bmQpIHtcbiAgICAgICAgICAgIGZvdW5kLiRiYXNlTmFtZVNwYWNlID0gY2hpbGROYW1lU3BhY2U7XG4gICAgICAgICAgICBmb3VuZC4kdHlwZSA9IGNoaWxkTmFtZVNwYWNlICsgJzonICsgY2hpbGROYW1lO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gIH1cblxuICBpZiAoIWZvdW5kICYmIG9iamVjdC4kbmFtZSA9PT0gY2hpbGROYW1lKSB7XG4gICAgcmV0dXJuIG9iamVjdDtcbiAgfVxuXG4gIHJldHVybiBmb3VuZDtcbn07XG5cbldTREwucHJvdG90eXBlLl9wYXJzZSA9IGZ1bmN0aW9uICh4bWwpIHtcbiAgbGV0IHNlbGYgPSB0aGlzLFxuICAgIHAgPSBzYXgucGFyc2VyKHRydWUpLFxuICAgIHN0YWNrID0gW10sXG4gICAgcm9vdCA9IG51bGwsXG4gICAgdHlwZXMgPSBudWxsLFxuICAgIHNjaGVtYSA9IG51bGwsXG4gICAgb3B0aW9ucyA9IHNlbGYub3B0aW9ucztcblxuICBwLm9ub3BlbnRhZyA9IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgbGV0IG5zTmFtZSA9IG5vZGUubmFtZTtcbiAgICBsZXQgYXR0cnMgPSBub2RlLmF0dHJpYnV0ZXM7XG5cbiAgICBsZXQgdG9wID0gc3RhY2tbc3RhY2subGVuZ3RoIC0gMV07XG4gICAgbGV0IG5hbWU7XG4gICAgaWYgKHRvcCkge1xuICAgICAgdHJ5IHtcbiAgICAgICAgdG9wLnN0YXJ0RWxlbWVudChzdGFjaywgbnNOYW1lLCBhdHRycywgb3B0aW9ucyk7XG4gICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIGlmIChzZWxmLm9wdGlvbnMuc3RyaWN0KSB7XG4gICAgICAgICAgdGhyb3cgZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBzdGFjay5wdXNoKG5ldyBFbGVtZW50KG5zTmFtZSwgYXR0cnMsIG9wdGlvbnMpKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBuYW1lID0gc3BsaXRRTmFtZShuc05hbWUpLm5hbWU7XG4gICAgICBpZiAobmFtZSA9PT0gJ2RlZmluaXRpb25zJykge1xuICAgICAgICByb290ID0gbmV3IERlZmluaXRpb25zRWxlbWVudChuc05hbWUsIGF0dHJzLCBvcHRpb25zKTtcbiAgICAgICAgc3RhY2sucHVzaChyb290KTtcbiAgICAgIH0gZWxzZSBpZiAobmFtZSA9PT0gJ3NjaGVtYScpIHtcbiAgICAgICAgLy8gU2hpbSBhIHN0cnVjdHVyZSBpbiBoZXJlIHRvIGFsbG93IHRoZSBwcm9wZXIgb2JqZWN0cyB0byBiZSBjcmVhdGVkIHdoZW4gbWVyZ2luZyBiYWNrLlxuICAgICAgICByb290ID0gbmV3IERlZmluaXRpb25zRWxlbWVudCgnZGVmaW5pdGlvbnMnLCB7fSwge30pO1xuICAgICAgICB0eXBlcyA9IG5ldyBUeXBlc0VsZW1lbnQoJ3R5cGVzJywge30sIHt9KTtcbiAgICAgICAgc2NoZW1hID0gbmV3IFNjaGVtYUVsZW1lbnQobnNOYW1lLCBhdHRycywgb3B0aW9ucyk7XG4gICAgICAgIHR5cGVzLmFkZENoaWxkKHNjaGVtYSk7XG4gICAgICAgIHJvb3QuYWRkQ2hpbGQodHlwZXMpO1xuICAgICAgICBzdGFjay5wdXNoKHNjaGVtYSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1VuZXhwZWN0ZWQgcm9vdCBlbGVtZW50IG9mIFdTREwgb3IgaW5jbHVkZScpO1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICBwLm9uY2xvc2V0YWcgPSBmdW5jdGlvbiAobmFtZSkge1xuICAgIGxldCB0b3AgPSBzdGFja1tzdGFjay5sZW5ndGggLSAxXTtcbiAgICBhc3NlcnQodG9wLCAnVW5tYXRjaGVkIGNsb3NlIHRhZzogJyArIG5hbWUpO1xuXG4gICAgdG9wLmVuZEVsZW1lbnQoc3RhY2ssIG5hbWUpO1xuICB9O1xuXG4gIHAud3JpdGUoeG1sKS5jbG9zZSgpO1xuXG4gIHJldHVybiByb290O1xufTtcblxuV1NETC5wcm90b3R5cGUuX2Zyb21YTUwgPSBmdW5jdGlvbiAoeG1sKSB7XG4gIHRoaXMuZGVmaW5pdGlvbnMgPSB0aGlzLl9wYXJzZSh4bWwpO1xuICB0aGlzLmRlZmluaXRpb25zLmRlc2NyaXB0aW9ucyA9IHtcbiAgICB0eXBlczoge31cbiAgfTtcbiAgdGhpcy54bWwgPSB4bWw7XG59O1xuXG5XU0RMLnByb3RvdHlwZS5fZnJvbVNlcnZpY2VzID0gZnVuY3Rpb24gKHNlcnZpY2VzKSB7XG5cbn07XG5cblxuXG5XU0RMLnByb3RvdHlwZS5feG1sbnNNYXAgPSBmdW5jdGlvbiAoKSB7XG4gIGxldCB4bWxucyA9IHRoaXMuZGVmaW5pdGlvbnMueG1sbnM7XG4gIGxldCBzdHIgPSAnJztcbiAgZm9yIChsZXQgYWxpYXMgaW4geG1sbnMpIHtcbiAgICBpZiAoYWxpYXMgPT09ICcnIHx8IGFsaWFzID09PSBUTlNfUFJFRklYKSB7XG4gICAgICBjb250aW51ZTtcbiAgICB9XG4gICAgbGV0IG5zID0geG1sbnNbYWxpYXNdO1xuICAgIHN3aXRjaCAobnMpIHtcbiAgICAgIGNhc2UgXCJodHRwOi8veG1sLmFwYWNoZS5vcmcveG1sLXNvYXBcIjogLy8gYXBhY2hlc29hcFxuICAgICAgY2FzZSBcImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzZGwvXCI6IC8vIHdzZGxcbiAgICAgIGNhc2UgXCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93c2RsL3NvYXAvXCI6IC8vIHdzZGxzb2FwXG4gICAgICBjYXNlIFwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3NkbC9zb2FwMTIvXCI6IC8vIHdzZGxzb2FwMTJcbiAgICAgIGNhc2UgXCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy9zb2FwL2VuY29kaW5nL1wiOiAvLyBzb2FwZW5jXG4gICAgICBjYXNlIFwiaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWFcIjogLy8geHNkXG4gICAgICAgIGNvbnRpbnVlO1xuICAgIH1cbiAgICBpZiAofm5zLmluZGV4T2YoJ2h0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnLycpKSB7XG4gICAgICBjb250aW51ZTtcbiAgICB9XG4gICAgaWYgKH5ucy5pbmRleE9mKCdodHRwOi8vd3d3LnczLm9yZy8nKSkge1xuICAgICAgY29udGludWU7XG4gICAgfVxuICAgIGlmICh+bnMuaW5kZXhPZignaHR0cDovL3htbC5hcGFjaGUub3JnLycpKSB7XG4gICAgICBjb250aW51ZTtcbiAgICB9XG4gICAgc3RyICs9ICcgeG1sbnM6JyArIGFsaWFzICsgJz1cIicgKyBucyArICdcIic7XG4gIH1cbiAgcmV0dXJuIHN0cjtcbn07XG5cbi8qXG4gKiBIYXZlIGFub3RoZXIgZnVuY3Rpb24gdG8gbG9hZCBwcmV2aW91cyBXU0RMcyBhcyB3ZVxuICogZG9uJ3Qgd2FudCB0aGlzIHRvIGJlIGludm9rZWQgZXh0ZXJuYWxseSAoZXhwZWN0IGZvciB0ZXN0cylcbiAqIFRoaXMgd2lsbCBhdHRlbXB0IHRvIGZpeCBjaXJjdWxhciBkZXBlbmRlbmNpZXMgd2l0aCBYU0QgZmlsZXMsXG4gKiBHaXZlblxuICogLSBmaWxlLndzZGxcbiAqICAgLSB4czppbXBvcnQgbmFtZXNwYWNlPVwiQVwiIHNjaGVtYUxvY2F0aW9uOiBBLnhzZFxuICogLSBBLnhzZFxuICogICAtIHhzOmltcG9ydCBuYW1lc3BhY2U9XCJCXCIgc2NoZW1hTG9jYXRpb246IEIueHNkXG4gKiAtIEIueHNkXG4gKiAgIC0geHM6aW1wb3J0IG5hbWVzcGFjZT1cIkFcIiBzY2hlbWFMb2NhdGlvbjogQS54c2RcbiAqIGZpbGUud3NkbCB3aWxsIHN0YXJ0IGxvYWRpbmcsIGltcG9ydCBBLCB0aGVuIEEgd2lsbCBpbXBvcnQgQiwgd2hpY2ggd2lsbCB0aGVuIGltcG9ydCBBXG4gKiBCZWNhdXNlIEEgaGFzIGFscmVhZHkgc3RhcnRlZCB0byBsb2FkIHByZXZpb3VzbHkgaXQgd2lsbCBiZSByZXR1cm5lZCByaWdodCBhd2F5IGFuZFxuICogaGF2ZSBhbiBpbnRlcm5hbCBjaXJjdWxhciByZWZlcmVuY2VcbiAqIEIgd291bGQgdGhlbiBjb21wbGV0ZSBsb2FkaW5nLCB0aGVuIEEsIHRoZW4gZmlsZS53c2RsXG4gKiBCeSB0aGUgdGltZSBmaWxlIEEgc3RhcnRzIHByb2Nlc3NpbmcgaXRzIGluY2x1ZGVzIGl0cyBkZWZpbml0aW9ucyB3aWxsIGJlIGFscmVhZHkgbG9hZGVkLFxuICogdGhpcyBpcyB0aGUgb25seSB0aGluZyB0aGF0IEIgd2lsbCBkZXBlbmQgb24gd2hlbiBcIm9wZW5pbmdcIiBBXG4gKi9cbmZ1bmN0aW9uIG9wZW5fd3NkbF9yZWN1cnNpdmUodXJpLCBvcHRpb25zKTogUHJvbWlzZTxhbnk+IHtcbiAgbGV0IGZyb21DYWNoZSxcbiAgICBXU0RMX0NBQ0hFO1xuXG4gIC8vIGlmICh0eXBlb2Ygb3B0aW9ucyA9PT0gJ2Z1bmN0aW9uJykge1xuICAvLyAgIGNhbGxiYWNrID0gb3B0aW9ucztcbiAgLy8gICBvcHRpb25zID0ge307XG4gIC8vIH1cblxuICBXU0RMX0NBQ0hFID0gb3B0aW9ucy5XU0RMX0NBQ0hFO1xuXG4gIGlmIChmcm9tQ2FjaGUgPSBXU0RMX0NBQ0hFW3VyaV0pIHtcbiAgICAvLyByZXR1cm4gY2FsbGJhY2suY2FsbChmcm9tQ2FjaGUsIG51bGwsIGZyb21DYWNoZSk7XG4gICAgcmV0dXJuIGZyb21DYWNoZTtcbiAgfVxuXG4gIHJldHVybiBvcGVuX3dzZGwodXJpLCBvcHRpb25zKTtcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIG9wZW5fd3NkbCh1cmksIG9wdGlvbnMpOiBQcm9taXNlPGFueT4ge1xuICAvLyBpZiAodHlwZW9mIG9wdGlvbnMgPT09ICdmdW5jdGlvbicpIHtcbiAgLy8gICBjYWxsYmFjayA9IG9wdGlvbnM7XG4gIC8vICAgb3B0aW9ucyA9IHt9O1xuICAvLyB9XG5cbiAgLy8gaW5pdGlhbGl6ZSBjYWNoZSB3aGVuIGNhbGxpbmcgb3Blbl93c2RsIGRpcmVjdGx5XG4gIGxldCBXU0RMX0NBQ0hFID0gb3B0aW9ucy5XU0RMX0NBQ0hFIHx8IHt9O1xuICBsZXQgcmVxdWVzdF9oZWFkZXJzID0gb3B0aW9ucy53c2RsX2hlYWRlcnM7XG4gIGxldCByZXF1ZXN0X29wdGlvbnMgPSBvcHRpb25zLndzZGxfb3B0aW9ucztcblxuICAvLyBsZXQgd3NkbDtcbiAgLy8gaWYgKCEvXmh0dHBzPzovLnRlc3QodXJpKSkge1xuICAvLyAgIC8vIGRlYnVnKCdSZWFkaW5nIGZpbGU6ICVzJywgdXJpKTtcbiAgLy8gICAvLyBmcy5yZWFkRmlsZSh1cmksICd1dGY4JywgZnVuY3Rpb24oZXJyLCBkZWZpbml0aW9uKSB7XG4gIC8vICAgLy8gICBpZiAoZXJyKSB7XG4gIC8vICAgLy8gICAgIGNhbGxiYWNrKGVycik7XG4gIC8vICAgLy8gICB9XG4gIC8vICAgLy8gICBlbHNlIHtcbiAgLy8gICAvLyAgICAgd3NkbCA9IG5ldyBXU0RMKGRlZmluaXRpb24sIHVyaSwgb3B0aW9ucyk7XG4gIC8vICAgLy8gICAgIFdTRExfQ0FDSEVbIHVyaSBdID0gd3NkbDtcbiAgLy8gICAvLyAgICAgd3NkbC5XU0RMX0NBQ0hFID0gV1NETF9DQUNIRTtcbiAgLy8gICAvLyAgICAgd3NkbC5vblJlYWR5KGNhbGxiYWNrKTtcbiAgLy8gICAvLyAgIH1cbiAgLy8gICAvLyB9KTtcbiAgLy8gfVxuICAvLyBlbHNlIHtcbiAgLy8gICBkZWJ1ZygnUmVhZGluZyB1cmw6ICVzJywgdXJpKTtcbiAgLy8gICBsZXQgaHR0cENsaWVudCA9IG9wdGlvbnMuaHR0cENsaWVudCB8fCBuZXcgSHR0cENsaWVudChvcHRpb25zKTtcbiAgLy8gICBodHRwQ2xpZW50LnJlcXVlc3QodXJpLCBudWxsIC8qIG9wdGlvbnMgKi8sIGZ1bmN0aW9uKGVyciwgcmVzcG9uc2UsIGRlZmluaXRpb24pIHtcbiAgLy8gICAgIGlmIChlcnIpIHtcbiAgLy8gICAgICAgY2FsbGJhY2soZXJyKTtcbiAgLy8gICAgIH0gZWxzZSBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gMjAwKSB7XG4gIC8vICAgICAgIHdzZGwgPSBuZXcgV1NETChkZWZpbml0aW9uLCB1cmksIG9wdGlvbnMpO1xuICAvLyAgICAgICBXU0RMX0NBQ0hFWyB1cmkgXSA9IHdzZGw7XG4gIC8vICAgICAgIHdzZGwuV1NETF9DQUNIRSA9IFdTRExfQ0FDSEU7XG4gIC8vICAgICAgIHdzZGwub25SZWFkeShjYWxsYmFjayk7XG4gIC8vICAgICB9IGVsc2Uge1xuICAvLyAgICAgICBjYWxsYmFjayhuZXcgRXJyb3IoJ0ludmFsaWQgV1NETCBVUkw6ICcgKyB1cmkgKyBcIlxcblxcblxcciBDb2RlOiBcIiArIHJlc3BvbnNlLnN0YXR1c0NvZGUgKyBcIlxcblxcblxcciBSZXNwb25zZSBCb2R5OiBcIiArIHJlc3BvbnNlLmJvZHkpKTtcbiAgLy8gICAgIH1cbiAgLy8gICB9LCByZXF1ZXN0X2hlYWRlcnMsIHJlcXVlc3Rfb3B0aW9ucyk7XG4gIC8vIH1cbiAgLy8gcmV0dXJuIHdzZGw7XG5cbiAgY29uc29sZS5sb2coJ1JlYWRpbmcgdXJsOiAlcycsIHVyaSk7XG4gIGNvbnN0IGh0dHBDbGllbnQ6IEh0dHBDbGllbnQgPSBvcHRpb25zLmh0dHBDbGllbnQ7XG4gIGNvbnN0IHdzZGxEZWYgPSBhd2FpdCBodHRwQ2xpZW50LmdldCh1cmksIHsgcmVzcG9uc2VUeXBlOiAndGV4dCcgfSkudG9Qcm9taXNlKCk7XG4gIGNvbnN0IHdzZGxPYmogPSBhd2FpdCBuZXcgUHJvbWlzZSgocmVzb2x2ZSkgPT4ge1xuICAgIGNvbnN0IHdzZGwgPSBuZXcgV1NETCh3c2RsRGVmLCB1cmksIG9wdGlvbnMpO1xuICAgIFdTRExfQ0FDSEVbdXJpXSA9IHdzZGw7XG4gICAgd3NkbC5XU0RMX0NBQ0hFID0gV1NETF9DQUNIRTtcbiAgICB3c2RsLm9uUmVhZHkocmVzb2x2ZSh3c2RsKSk7XG4gIH0pO1xuICBjb25zb2xlLmxvZyhcIndzZGxcIiwgd3NkbE9iailcbiAgcmV0dXJuIHdzZGxPYmo7XG59XG4iXX0=
