/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*
 * Copyright (c) 2011 Vinay Pulim <vinay@milewise.com>
 * MIT Licensed
 *
 */
/*jshint proto:true*/
"use strict";
import * as sax from 'sax';
import { NamespaceContext } from './nscontext';
import * as url from 'url';
import { ok as assert } from 'assert';
// import stripBom from 'strip-bom';
/** @type {?} */
var stripBom = function (x) {
    // Catches EFBBBF (UTF-8 BOM) because the buffer-to-string
    // conversion translates it to FEFF (UTF-16 BOM)
    if (x.charCodeAt(0) === 0xFEFF) {
        return x.slice(1);
    }
    return x;
};
var ɵ0 = stripBom;
import * as _ from 'lodash';
import * as utils from './utils';
/** @type {?} */
var TNS_PREFIX = utils.TNS_PREFIX;
/** @type {?} */
var findPrefix = utils.findPrefix;
/** @type {?} */
var Primitives = {
    string: 1,
    boolean: 1,
    decimal: 1,
    float: 1,
    double: 1,
    anyType: 1,
    byte: 1,
    int: 1,
    long: 1,
    short: 1,
    negativeInteger: 1,
    nonNegativeInteger: 1,
    positiveInteger: 1,
    nonPositiveInteger: 1,
    unsignedByte: 1,
    unsignedInt: 1,
    unsignedLong: 1,
    unsignedShort: 1,
    duration: 0,
    dateTime: 0,
    time: 0,
    date: 0,
    gYearMonth: 0,
    gYear: 0,
    gMonthDay: 0,
    gDay: 0,
    gMonth: 0,
    hexBinary: 0,
    base64Binary: 0,
    anyURI: 0,
    QName: 0,
    NOTATION: 0
};
/**
 * @param {?} nsName
 * @return {?}
 */
function splitQName(nsName) {
    /** @type {?} */
    var i = typeof nsName === 'string' ? nsName.indexOf(':') : -1;
    return i < 0 ? { prefix: TNS_PREFIX, name: nsName } :
        { prefix: nsName.substring(0, i), name: nsName.substring(i + 1) };
}
/**
 * @param {?} obj
 * @return {?}
 */
function xmlEscape(obj) {
    if (typeof (obj) === 'string') {
        if (obj.substr(0, 9) === '<![CDATA[' && obj.substr(-3) === "]]>") {
            return obj;
        }
        return obj
            .replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&apos;');
    }
    return obj;
}
/** @type {?} */
var trimLeft = /^[\s\xA0]+/;
/** @type {?} */
var trimRight = /[\s\xA0]+$/;
/**
 * @param {?} text
 * @return {?}
 */
function trim(text) {
    return text.replace(trimLeft, '').replace(trimRight, '');
}
/**
 * @param {?} destination
 * @param {?} source
 * @return {?}
 */
function deepMerge(destination, source) {
    return _.mergeWith(destination || {}, source, function (a, b) {
        return _.isArray(a) ? a.concat(b) : undefined;
    });
}
/** @type {?} */
var Element = function (nsName, attrs, options) {
    /** @type {?} */
    var parts = splitQName(nsName);
    this.nsName = nsName;
    this.prefix = parts.prefix;
    this.name = parts.name;
    this.children = [];
    this.xmlns = {};
    this._initializeOptions(options);
    for (var key in attrs) {
        /** @type {?} */
        var match = /^xmlns:?(.*)$/.exec(key);
        if (match) {
            this.xmlns[match[1] ? match[1] : TNS_PREFIX] = attrs[key];
        }
        else {
            if (key === 'value') {
                this[this.valueKey] = attrs[key];
            }
            else {
                this['$' + key] = attrs[key];
            }
        }
    }
    if (this.$targetNamespace !== undefined) {
        // Add targetNamespace to the mapping
        this.xmlns[TNS_PREFIX] = this.$targetNamespace;
    }
};
var ɵ1 = Element;
Element.prototype._initializeOptions = function (options) {
    if (options) {
        this.valueKey = options.valueKey || '$value';
        this.xmlKey = options.xmlKey || '$xml';
        this.ignoredNamespaces = options.ignoredNamespaces || [];
    }
    else {
        this.valueKey = '$value';
        this.xmlKey = '$xml';
        this.ignoredNamespaces = [];
    }
};
Element.prototype.deleteFixedAttrs = function () {
    this.children && this.children.length === 0 && delete this.children;
    this.xmlns && Object.keys(this.xmlns).length === 0 && delete this.xmlns;
    delete this.nsName;
    delete this.prefix;
    delete this.name;
};
Element.prototype.allowedChildren = [];
Element.prototype.startElement = function (stack, nsName, attrs, options) {
    if (!this.allowedChildren) {
        return;
    }
    /** @type {?} */
    var ChildClass = this.allowedChildren[splitQName(nsName).name];
    /** @type {?} */
    var element = null;
    if (ChildClass) {
        stack.push(new ChildClass(nsName, attrs, options));
    }
    else {
        this.unexpected(nsName);
    }
};
Element.prototype.endElement = function (stack, nsName) {
    if (this.nsName === nsName) {
        if (stack.length < 2)
            return;
        /** @type {?} */
        var parent_1 = stack[stack.length - 2];
        if (this !== stack[0]) {
            _.defaultsDeep(stack[0].xmlns, this.xmlns);
            // delete this.xmlns;
            parent_1.children.push(this);
            parent_1.addChild(this);
        }
        stack.pop();
    }
};
Element.prototype.addChild = function (child) {
    return;
};
Element.prototype.unexpected = function (name) {
    throw new Error('Found unexpected element (' + name + ') inside ' + this.nsName);
};
Element.prototype.description = function (definitions) {
    return this.$name || this.name;
};
Element.prototype.init = function () {
};
Element.createSubClass = function () {
    /** @type {?} */
    var root = this;
    /** @type {?} */
    var subElement = function () {
        root.apply(this, arguments);
        this.init();
    };
    // inherits(subElement, root);
    subElement.prototype.__proto__ = root.prototype;
    return subElement;
};
/** @type {?} */
var ElementElement = Element.createSubClass();
/** @type {?} */
var AnyElement = Element.createSubClass();
/** @type {?} */
var InputElement = Element.createSubClass();
/** @type {?} */
var OutputElement = Element.createSubClass();
/** @type {?} */
var SimpleTypeElement = Element.createSubClass();
/** @type {?} */
var RestrictionElement = Element.createSubClass();
/** @type {?} */
var ExtensionElement = Element.createSubClass();
/** @type {?} */
var ChoiceElement = Element.createSubClass();
/** @type {?} */
var EnumerationElement = Element.createSubClass();
/** @type {?} */
var ComplexTypeElement = Element.createSubClass();
/** @type {?} */
var ComplexContentElement = Element.createSubClass();
/** @type {?} */
var SimpleContentElement = Element.createSubClass();
/** @type {?} */
var SequenceElement = Element.createSubClass();
/** @type {?} */
var AllElement = Element.createSubClass();
/** @type {?} */
var MessageElement = Element.createSubClass();
/** @type {?} */
var DocumentationElement = Element.createSubClass();
/** @type {?} */
var SchemaElement = Element.createSubClass();
/** @type {?} */
var TypesElement = Element.createSubClass();
/** @type {?} */
var OperationElement = Element.createSubClass();
/** @type {?} */
var PortTypeElement = Element.createSubClass();
/** @type {?} */
var BindingElement = Element.createSubClass();
/** @type {?} */
var PortElement = Element.createSubClass();
/** @type {?} */
var ServiceElement = Element.createSubClass();
/** @type {?} */
var DefinitionsElement = Element.createSubClass();
/** @type {?} */
var ElementTypeMap = {
    types: [TypesElement, 'schema documentation'],
    schema: [SchemaElement, 'element complexType simpleType include import'],
    element: [ElementElement, 'annotation complexType'],
    any: [AnyElement, ''],
    simpleType: [SimpleTypeElement, 'restriction'],
    restriction: [RestrictionElement, 'enumeration all choice sequence'],
    extension: [ExtensionElement, 'all sequence choice'],
    choice: [ChoiceElement, 'element sequence choice any'],
    // group: [GroupElement, 'element group'],
    enumeration: [EnumerationElement, ''],
    complexType: [ComplexTypeElement, 'annotation sequence all complexContent simpleContent choice'],
    complexContent: [ComplexContentElement, 'extension'],
    simpleContent: [SimpleContentElement, 'extension'],
    sequence: [SequenceElement, 'element sequence choice any'],
    all: [AllElement, 'element choice'],
    service: [ServiceElement, 'port documentation'],
    port: [PortElement, 'address documentation'],
    binding: [BindingElement, '_binding SecuritySpec operation documentation'],
    portType: [PortTypeElement, 'operation documentation'],
    message: [MessageElement, 'part documentation'],
    operation: [OperationElement, 'documentation input output fault _operation'],
    input: [InputElement, 'body SecuritySpecRef documentation header'],
    output: [OutputElement, 'body SecuritySpecRef documentation header'],
    fault: [Element, '_fault documentation'],
    definitions: [DefinitionsElement, 'types message portType binding service import documentation'],
    documentation: [DocumentationElement, '']
};
/**
 * @param {?} types
 * @return {?}
 */
function mapElementTypes(types) {
    /** @type {?} */
    var rtn = {};
    types = types.split(' ');
    types.forEach(function (type) {
        rtn[type.replace(/^_/, '')] = (ElementTypeMap[type] || [Element])[0];
    });
    return rtn;
}
for (var n in ElementTypeMap) {
    /** @type {?} */
    var v = ElementTypeMap[n];
    v[0].prototype.allowedChildren = mapElementTypes(v[1]);
}
MessageElement.prototype.init = function () {
    this.element = null;
    this.parts = null;
};
SchemaElement.prototype.init = function () {
    this.complexTypes = {};
    this.types = {};
    this.elements = {};
    this.includes = [];
};
TypesElement.prototype.init = function () {
    this.schemas = {};
};
OperationElement.prototype.init = function () {
    this.input = null;
    this.output = null;
    this.inputSoap = null;
    this.outputSoap = null;
    this.style = '';
    this.soapAction = '';
};
PortTypeElement.prototype.init = function () {
    this.methods = {};
};
BindingElement.prototype.init = function () {
    this.transport = '';
    this.style = '';
    this.methods = {};
};
PortElement.prototype.init = function () {
    this.location = null;
};
ServiceElement.prototype.init = function () {
    this.ports = {};
};
DefinitionsElement.prototype.init = function () {
    if (this.name !== 'definitions')
        this.unexpected(this.nsName);
    this.messages = {};
    this.portTypes = {};
    this.bindings = {};
    this.services = {};
    this.schemas = {};
};
DocumentationElement.prototype.init = function () {
};
SchemaElement.prototype.merge = function (source) {
    assert(source instanceof SchemaElement);
    if (this.$targetNamespace === source.$targetNamespace) {
        _.merge(this.complexTypes, source.complexTypes);
        _.merge(this.types, source.types);
        _.merge(this.elements, source.elements);
        _.merge(this.xmlns, source.xmlns);
    }
    return this;
};
SchemaElement.prototype.addChild = function (child) {
    if (child.$name in Primitives)
        return;
    if (child.name === 'include' || child.name === 'import') {
        /** @type {?} */
        var location_1 = child.$schemaLocation || child.$location;
        if (location_1) {
            this.includes.push({
                namespace: child.$namespace || child.$targetNamespace || this.$targetNamespace,
                location: location_1
            });
        }
    }
    else if (child.name === 'complexType') {
        this.complexTypes[child.$name] = child;
    }
    else if (child.name === 'element') {
        this.elements[child.$name] = child;
    }
    else if (child.$name) {
        this.types[child.$name] = child;
    }
    this.children.pop();
    // child.deleteFixedAttrs();
};
//fix#325
TypesElement.prototype.addChild = function (child) {
    assert(child instanceof SchemaElement);
    /** @type {?} */
    var targetNamespace = child.$targetNamespace;
    if (!this.schemas.hasOwnProperty(targetNamespace)) {
        this.schemas[targetNamespace] = child;
    }
    else {
        console.error('Target-Namespace "' + targetNamespace + '" already in use by another Schema!');
    }
};
InputElement.prototype.addChild = function (child) {
    if (child.name === 'body') {
        this.use = child.$use;
        if (this.use === 'encoded') {
            this.encodingStyle = child.$encodingStyle;
        }
        this.children.pop();
    }
};
OutputElement.prototype.addChild = function (child) {
    if (child.name === 'body') {
        this.use = child.$use;
        if (this.use === 'encoded') {
            this.encodingStyle = child.$encodingStyle;
        }
        this.children.pop();
    }
};
OperationElement.prototype.addChild = function (child) {
    if (child.name === 'operation') {
        this.soapAction = child.$soapAction || '';
        this.style = child.$style || '';
        this.children.pop();
    }
};
BindingElement.prototype.addChild = function (child) {
    if (child.name === 'binding') {
        this.transport = child.$transport;
        this.style = child.$style;
        this.children.pop();
    }
};
PortElement.prototype.addChild = function (child) {
    if (child.name === 'address' && typeof (child.$location) !== 'undefined') {
        this.location = child.$location;
    }
};
DefinitionsElement.prototype.addChild = function (child) {
    /** @type {?} */
    var self = this;
    if (child instanceof TypesElement) {
        // Merge types.schemas into definitions.schemas
        _.merge(self.schemas, child.schemas);
    }
    else if (child instanceof MessageElement) {
        self.messages[child.$name] = child;
    }
    else if (child.name === 'import') {
        self.schemas[child.$namespace] = new SchemaElement(child.$namespace, {});
        self.schemas[child.$namespace].addChild(child);
    }
    else if (child instanceof PortTypeElement) {
        self.portTypes[child.$name] = child;
    }
    else if (child instanceof BindingElement) {
        if (child.transport === 'http://schemas.xmlsoap.org/soap/http' ||
            child.transport === 'http://www.w3.org/2003/05/soap/bindings/HTTP/')
            self.bindings[child.$name] = child;
    }
    else if (child instanceof ServiceElement) {
        self.services[child.$name] = child;
    }
    else if (child instanceof DocumentationElement) {
    }
    this.children.pop();
};
MessageElement.prototype.postProcess = function (definitions) {
    /** @type {?} */
    var part = null;
    /** @type {?} */
    var child = undefined;
    /** @type {?} */
    var children = this.children || [];
    /** @type {?} */
    var ns = undefined;
    /** @type {?} */
    var nsName = undefined;
    /** @type {?} */
    var i = undefined;
    /** @type {?} */
    var type = undefined;
    for (i in children) {
        if ((child = children[i]).name === 'part') {
            part = child;
            break;
        }
    }
    if (!part) {
        return;
    }
    if (part.$element) {
        /** @type {?} */
        var lookupTypes = [];
        /** @type {?} */
        var elementChildren = void 0;
        delete this.parts;
        nsName = splitQName(part.$element);
        ns = nsName.prefix;
        /** @type {?} */
        var schema = definitions.schemas[definitions.xmlns[ns]];
        this.element = schema.elements[nsName.name];
        if (!this.element) {
            // debug(nsName.name + " is not present in wsdl and cannot be processed correctly.");
            return;
        }
        this.element.targetNSAlias = ns;
        this.element.targetNamespace = definitions.xmlns[ns];
        // set the optional $lookupType to be used within `client#_invoke()` when
        // calling `wsdl#objectToDocumentXML()
        this.element.$lookupType = part.$element;
        elementChildren = this.element.children;
        // get all nested lookup types (only complex types are followed)
        if (elementChildren.length > 0) {
            for (i = 0; i < elementChildren.length; i++) {
                lookupTypes.push(this._getNestedLookupTypeString(elementChildren[i]));
            }
        }
        // if nested lookup types where found, prepare them for furter usage
        if (lookupTypes.length > 0) {
            lookupTypes = lookupTypes.
                join('_').
                split('_').
                filter(function removeEmptyLookupTypes(type) {
                return type !== '^';
            });
            /** @type {?} */
            var schemaXmlns = definitions.schemas[this.element.targetNamespace].xmlns;
            for (i = 0; i < lookupTypes.length; i++) {
                lookupTypes[i] = this._createLookupTypeObject(lookupTypes[i], schemaXmlns);
            }
        }
        this.element.$lookupTypes = lookupTypes;
        if (this.element.$type) {
            type = splitQName(this.element.$type);
            /** @type {?} */
            var typeNs = schema.xmlns && schema.xmlns[type.prefix] || definitions.xmlns[type.prefix];
            if (typeNs) {
                if (type.name in Primitives) {
                    // this.element = this.element.$type;
                }
                else {
                    // first check local mapping of ns alias to namespace
                    schema = definitions.schemas[typeNs];
                    /** @type {?} */
                    var ctype = schema.complexTypes[type.name] || schema.types[type.name] || schema.elements[type.name];
                    if (ctype) {
                        this.parts = ctype.description(definitions, schema.xmlns);
                    }
                }
            }
        }
        else {
            /** @type {?} */
            var method = this.element.description(definitions, schema.xmlns);
            this.parts = method[nsName.name];
        }
        this.children.splice(0, 1);
    }
    else {
        // rpc encoding
        this.parts = {};
        delete this.element;
        for (i = 0; part = this.children[i]; i++) {
            if (part.name === 'documentation') {
                // <wsdl:documentation can be present under <wsdl:message>
                continue;
            }
            assert(part.name === 'part', 'Expected part element');
            nsName = splitQName(part.$type);
            ns = definitions.xmlns[nsName.prefix];
            type = nsName.name;
            /** @type {?} */
            var schemaDefinition = definitions.schemas[ns];
            if (typeof schemaDefinition !== 'undefined') {
                this.parts[part.$name] = definitions.schemas[ns].types[type] || definitions.schemas[ns].complexTypes[type];
            }
            else {
                this.parts[part.$name] = part.$type;
            }
            if (typeof this.parts[part.$name] === 'object') {
                this.parts[part.$name].prefix = nsName.prefix;
                this.parts[part.$name].xmlns = ns;
            }
            this.children.splice(i--, 1);
        }
    }
    this.deleteFixedAttrs();
};
/**
 * Takes a given namespaced String(for example: 'alias:property') and creates a lookupType
 * object for further use in as first (lookup) `parameterTypeObj` within the `objectToXML`
 * method and provides an entry point for the already existing code in `findChildSchemaObject`.
 *
 * @method _createLookupTypeObject
 * @param {String}            nsString          The NS String (for example "alias:type").
 * @param {Object}            xmlns       The fully parsed `wsdl` definitions object (including all schemas).
 * @returns {Object}
 * @private
 */
MessageElement.prototype._createLookupTypeObject = function (nsString, xmlns) {
    /** @type {?} */
    var splittedNSString = splitQName(nsString);
    /** @type {?} */
    var nsAlias = splittedNSString.prefix;
    /** @type {?} */
    var splittedName = splittedNSString.name.split('#');
    /** @type {?} */
    var type = splittedName[0];
    /** @type {?} */
    var name = splittedName[1];
    /** @type {?} */
    var lookupTypeObj = {};
    lookupTypeObj.$namespace = xmlns[nsAlias];
    lookupTypeObj.$type = nsAlias + ':' + type;
    lookupTypeObj.$name = name;
    return lookupTypeObj;
};
/**
 * Iterates through the element and every nested child to find any defined `$type`
 * property and returns it in a underscore ('_') separated String (using '^' as default
 * value if no `$type` property was found).
 *
 * @method _getNestedLookupTypeString
 * @param {Object}            element         The element which (probably) contains nested `$type` values.
 * @returns {String}
 * @private
 */
MessageElement.prototype._getNestedLookupTypeString = function (element) {
    /** @type {?} */
    var resolvedType = '^';
    /** @type {?} */
    var excluded = this.ignoredNamespaces.concat('xs');
    if (element.hasOwnProperty('$type') && typeof element.$type === 'string') {
        if (excluded.indexOf(element.$type.split(':')[0]) === -1) {
            resolvedType += ('_' + element.$type + '#' + element.$name);
        }
    }
    if (element.children.length > 0) {
        /** @type {?} */
        var self_1 = this;
        element.children.forEach(function (child) {
            /** @type {?} */
            var resolvedChildType = self_1._getNestedLookupTypeString(child).replace(/\^_/, '');
            if (resolvedChildType && typeof resolvedChildType === 'string') {
                resolvedType += ('_' + resolvedChildType);
            }
        });
    }
    return resolvedType;
};
OperationElement.prototype.postProcess = function (definitions, tag) {
    /** @type {?} */
    var children = this.children;
    for (var i = 0, child = void 0; child = children[i]; i++) {
        if (child.name !== 'input' && child.name !== 'output')
            continue;
        if (tag === 'binding') {
            this[child.name] = child;
            children.splice(i--, 1);
            continue;
        }
        /** @type {?} */
        var messageName = splitQName(child.$message).name;
        /** @type {?} */
        var message = definitions.messages[messageName];
        message.postProcess(definitions);
        if (message.element) {
            definitions.messages[message.element.$name] = message;
            this[child.name] = message.element;
        }
        else {
            this[child.name] = message;
        }
        children.splice(i--, 1);
    }
    this.deleteFixedAttrs();
};
PortTypeElement.prototype.postProcess = function (definitions) {
    /** @type {?} */
    var children = this.children;
    if (typeof children === 'undefined')
        return;
    for (var i = 0, child = void 0; child = children[i]; i++) {
        if (child.name !== 'operation')
            continue;
        child.postProcess(definitions, 'portType');
        this.methods[child.$name] = child;
        children.splice(i--, 1);
    }
    delete this.$name;
    this.deleteFixedAttrs();
};
BindingElement.prototype.postProcess = function (definitions) {
    /** @type {?} */
    var type = splitQName(this.$type).name;
    /** @type {?} */
    var portType = definitions.portTypes[type];
    /** @type {?} */
    var style = this.style;
    /** @type {?} */
    var children = this.children;
    if (portType) {
        portType.postProcess(definitions);
        this.methods = portType.methods;
        for (var i = 0, child = void 0; child = children[i]; i++) {
            if (child.name !== 'operation')
                continue;
            child.postProcess(definitions, 'binding');
            children.splice(i--, 1);
            child.style || (child.style = style);
            /** @type {?} */
            var method = this.methods[child.$name];
            if (method) {
                method.style = child.style;
                method.soapAction = child.soapAction;
                method.inputSoap = child.input || null;
                method.outputSoap = child.output || null;
                method.inputSoap && method.inputSoap.deleteFixedAttrs();
                method.outputSoap && method.outputSoap.deleteFixedAttrs();
            }
        }
    }
    delete this.$name;
    delete this.$type;
    this.deleteFixedAttrs();
};
ServiceElement.prototype.postProcess = function (definitions) {
    /** @type {?} */
    var children = this.children;
    /** @type {?} */
    var bindings = definitions.bindings;
    if (children && children.length > 0) {
        for (var i = 0, child = void 0; child = children[i]; i++) {
            if (child.name !== 'port')
                continue;
            /** @type {?} */
            var bindingName = splitQName(child.$binding).name;
            /** @type {?} */
            var binding = bindings[bindingName];
            if (binding) {
                binding.postProcess(definitions);
                this.ports[child.$name] = {
                    location: child.location,
                    binding: binding
                };
                children.splice(i--, 1);
            }
        }
    }
    delete this.$name;
    this.deleteFixedAttrs();
};
SimpleTypeElement.prototype.description = function (definitions) {
    /** @type {?} */
    var children = this.children;
    for (var i = 0, child = void 0; child = children[i]; i++) {
        if (child instanceof RestrictionElement)
            return this.$name + "|" + child.description();
    }
    return {};
};
RestrictionElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    var children = this.children;
    /** @type {?} */
    var desc;
    for (var i = 0, child = void 0; child = children[i]; i++) {
        if (child instanceof SequenceElement ||
            child instanceof ChoiceElement) {
            desc = child.description(definitions, xmlns);
            break;
        }
    }
    if (desc && this.$base) {
        /** @type {?} */
        var type = splitQName(this.$base);
        /** @type {?} */
        var typeName = type.name;
        /** @type {?} */
        var ns = xmlns && xmlns[type.prefix] || definitions.xmlns[type.prefix];
        /** @type {?} */
        var schema_1 = definitions.schemas[ns];
        /** @type {?} */
        var typeElement_1 = schema_1 && (schema_1.complexTypes[typeName] || schema_1.types[typeName] || schema_1.elements[typeName]);
        desc.getBase = function () {
            return typeElement_1.description(definitions, schema_1.xmlns);
        };
        return desc;
    }
    // then simple element
    /** @type {?} */
    var base = this.$base ? this.$base + "|" : "";
    return base + this.children.map(function (child) {
        return child.description();
    }).join(",");
};
ExtensionElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    var children = this.children;
    /** @type {?} */
    var desc = {};
    for (var i = 0, child = void 0; child = children[i]; i++) {
        if (child instanceof SequenceElement ||
            child instanceof ChoiceElement) {
            desc = child.description(definitions, xmlns);
        }
    }
    if (this.$base) {
        /** @type {?} */
        var type = splitQName(this.$base);
        /** @type {?} */
        var typeName = type.name;
        /** @type {?} */
        var ns = xmlns && xmlns[type.prefix] || definitions.xmlns[type.prefix];
        /** @type {?} */
        var schema = definitions.schemas[ns];
        if (typeName in Primitives) {
            return this.$base;
        }
        else {
            /** @type {?} */
            var typeElement = schema && (schema.complexTypes[typeName] ||
                schema.types[typeName] || schema.elements[typeName]);
            if (typeElement) {
                /** @type {?} */
                var base = typeElement.description(definitions, schema.xmlns);
                desc = _.defaultsDeep(base, desc);
            }
        }
    }
    return desc;
};
EnumerationElement.prototype.description = function () {
    return this[this.valueKey];
};
ComplexTypeElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    var children = this.children || [];
    for (var i = 0, child = void 0; child = children[i]; i++) {
        if (child instanceof ChoiceElement ||
            child instanceof SequenceElement ||
            child instanceof AllElement ||
            child instanceof SimpleContentElement ||
            child instanceof ComplexContentElement) {
            return child.description(definitions, xmlns);
        }
    }
    return {};
};
ComplexContentElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    var children = this.children;
    for (var i = 0, child = void 0; child = children[i]; i++) {
        if (child instanceof ExtensionElement) {
            return child.description(definitions, xmlns);
        }
    }
    return {};
};
SimpleContentElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    var children = this.children;
    for (var i = 0, child = void 0; child = children[i]; i++) {
        if (child instanceof ExtensionElement) {
            return child.description(definitions, xmlns);
        }
    }
    return {};
};
ElementElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    var element = {};
    /** @type {?} */
    var name = this.$name;
    /** @type {?} */
    var isMany = !this.$maxOccurs ? false : (isNaN(this.$maxOccurs) ? (this.$maxOccurs === 'unbounded') : (this.$maxOccurs > 1));
    if (this.$minOccurs !== this.$maxOccurs && isMany) {
        name += '[]';
    }
    if (xmlns && xmlns[TNS_PREFIX]) {
        this.$targetNamespace = xmlns[TNS_PREFIX];
    }
    /** @type {?} */
    var type = this.$type || this.$ref;
    if (type) {
        type = splitQName(type);
        /** @type {?} */
        var typeName = type.name;
        /** @type {?} */
        var ns = xmlns && xmlns[type.prefix] || definitions.xmlns[type.prefix];
        /** @type {?} */
        var schema = definitions.schemas[ns];
        /** @type {?} */
        var typeElement = schema && (this.$type ? schema.complexTypes[typeName] || schema.types[typeName] : schema.elements[typeName]);
        if (ns && definitions.schemas[ns]) {
            xmlns = definitions.schemas[ns].xmlns;
        }
        if (typeElement && !(typeName in Primitives)) {
            if (!(typeName in definitions.descriptions.types)) {
                /** @type {?} */
                var elem_1 = {};
                definitions.descriptions.types[typeName] = elem_1;
                /** @type {?} */
                var description_1 = typeElement.description(definitions, xmlns);
                if (typeof description_1 === 'string') {
                    elem_1 = description_1;
                }
                else {
                    Object.keys(description_1).forEach(function (key) {
                        elem_1[key] = description_1[key];
                    });
                }
                if (this.$ref) {
                    element = elem_1;
                }
                else {
                    element[name] = elem_1;
                }
                if (typeof elem_1 === 'object') {
                    elem_1.targetNSAlias = type.prefix;
                    elem_1.targetNamespace = ns;
                }
                definitions.descriptions.types[typeName] = elem_1;
            }
            else {
                if (this.$ref) {
                    element = definitions.descriptions.types[typeName];
                }
                else {
                    element[name] = definitions.descriptions.types[typeName];
                }
            }
        }
        else {
            element[name] = this.$type;
        }
    }
    else {
        /** @type {?} */
        var children = this.children;
        element[name] = {};
        for (var i = 0, child = void 0; child = children[i]; i++) {
            if (child instanceof ComplexTypeElement) {
                element[name] = child.description(definitions, xmlns);
            }
        }
    }
    return element;
};
AllElement.prototype.description =
    SequenceElement.prototype.description = function (definitions, xmlns) {
        /** @type {?} */
        var children = this.children;
        /** @type {?} */
        var sequence = {};
        for (var i = 0, child = void 0; child = children[i]; i++) {
            if (child instanceof AnyElement) {
                continue;
            }
            /** @type {?} */
            var description = child.description(definitions, xmlns);
            for (var key in description) {
                sequence[key] = description[key];
            }
        }
        return sequence;
    };
ChoiceElement.prototype.description = function (definitions, xmlns) {
    /** @type {?} */
    var children = this.children;
    /** @type {?} */
    var choice = {};
    for (var i = 0, child = void 0; child = children[i]; i++) {
        /** @type {?} */
        var description = child.description(definitions, xmlns);
        for (var key in description) {
            choice[key] = description[key];
        }
    }
    return choice;
};
MessageElement.prototype.description = function (definitions) {
    if (this.element) {
        return this.element && this.element.description(definitions);
    }
    /** @type {?} */
    var desc = {};
    desc[this.$name] = this.parts;
    return desc;
};
PortTypeElement.prototype.description = function (definitions) {
    /** @type {?} */
    var methods = {};
    for (var name_1 in this.methods) {
        /** @type {?} */
        var method = this.methods[name_1];
        methods[name_1] = method.description(definitions);
    }
    return methods;
};
OperationElement.prototype.description = function (definitions) {
    /** @type {?} */
    var inputDesc = this.input ? this.input.description(definitions) : null;
    /** @type {?} */
    var outputDesc = this.output ? this.output.description(definitions) : null;
    return {
        input: inputDesc && inputDesc[Object.keys(inputDesc)[0]],
        output: outputDesc && outputDesc[Object.keys(outputDesc)[0]]
    };
};
BindingElement.prototype.description = function (definitions) {
    /** @type {?} */
    var methods = {};
    for (var name_2 in this.methods) {
        /** @type {?} */
        var method = this.methods[name_2];
        methods[name_2] = method.description(definitions);
    }
    return methods;
};
ServiceElement.prototype.description = function (definitions) {
    /** @type {?} */
    var ports = {};
    for (var name_3 in this.ports) {
        /** @type {?} */
        var port = this.ports[name_3];
        ports[name_3] = port.binding.description(definitions);
    }
    return ports;
};
/** @type {?} */
export var WSDL = function (definition, uri, options) {
    /** @type {?} */
    var self = this;
    /** @type {?} */
    var fromFunc;
    this.uri = uri;
    this.callback = function () {
    };
    this._includesWsdl = [];
    // initialize WSDL cache
    this.WSDL_CACHE = (options || {}).WSDL_CACHE || {};
    this._initializeOptions(options);
    if (typeof definition === 'string') {
        definition = stripBom(definition);
        fromFunc = this._fromXML;
    }
    else if (typeof definition === 'object') {
        fromFunc = this._fromServices;
    }
    else {
        throw new Error('WSDL letructor takes either an XML string or service definition');
    }
    Promise.resolve(true).then(function () {
        try {
            fromFunc.call(self, definition);
        }
        catch (e) {
            return self.callback(e.message);
        }
        self.processIncludes().then(function () {
            self.definitions.deleteFixedAttrs();
            /** @type {?} */
            var services = self.services = self.definitions.services;
            if (services) {
                for (var name_4 in services) {
                    services[name_4].postProcess(self.definitions);
                }
            }
            /** @type {?} */
            var complexTypes = self.definitions.complexTypes;
            if (complexTypes) {
                for (var name_5 in complexTypes) {
                    complexTypes[name_5].deleteFixedAttrs();
                }
            }
            // for document style, for every binding, prepare input message element name to (methodName, output message element name) mapping
            /** @type {?} */
            var bindings = self.definitions.bindings;
            for (var bindingName in bindings) {
                /** @type {?} */
                var binding = bindings[bindingName];
                if (typeof binding.style === 'undefined') {
                    binding.style = 'document';
                }
                if (binding.style !== 'document')
                    continue;
                /** @type {?} */
                var methods = binding.methods;
                /** @type {?} */
                var topEls = binding.topElements = {};
                for (var methodName in methods) {
                    if (methods[methodName].input) {
                        /** @type {?} */
                        var inputName = methods[methodName].input.$name;
                        /** @type {?} */
                        var outputName = "";
                        if (methods[methodName].output)
                            outputName = methods[methodName].output.$name;
                        topEls[inputName] = { "methodName": methodName, "outputName": outputName };
                    }
                }
            }
            // prepare soap envelope xmlns definition string
            self.xmlnsInEnvelope = self._xmlnsMap();
            self.callback(null, self);
        }).catch(function (err) { return self.callback(err); });
    });
    // process.nextTick(function() {
    //   try {
    //     fromFunc.call(self, definition);
    //   } catch (e) {
    //     return self.callback(e.message);
    //   }
    //   self.processIncludes(function(err) {
    //     let name;
    //     if (err) {
    //       return self.callback(err);
    //     }
    //     self.definitions.deleteFixedAttrs();
    //     let services = self.services = self.definitions.services;
    //     if (services) {
    //       for (name in services) {
    //         services[name].postProcess(self.definitions);
    //       }
    //     }
    //     let complexTypes = self.definitions.complexTypes;
    //     if (complexTypes) {
    //       for (name in complexTypes) {
    //         complexTypes[name].deleteFixedAttrs();
    //       }
    //     }
    //     // for document style, for every binding, prepare input message element name to (methodName, output message element name) mapping
    //     let bindings = self.definitions.bindings;
    //     for (let bindingName in bindings) {
    //       let binding = bindings[bindingName];
    //       if (typeof binding.style === 'undefined') {
    //         binding.style = 'document';
    //       }
    //       if (binding.style !== 'document')
    //         continue;
    //       let methods = binding.methods;
    //       let topEls = binding.topElements = {};
    //       for (let methodName in methods) {
    //         if (methods[methodName].input) {
    //           let inputName = methods[methodName].input.$name;
    //           let outputName="";
    //           if(methods[methodName].output )
    //             outputName = methods[methodName].output.$name;
    //           topEls[inputName] = {"methodName": methodName, "outputName": outputName};
    //         }
    //       }
    //     }
    //     // prepare soap envelope xmlns definition string
    //     self.xmlnsInEnvelope = self._xmlnsMap();
    //     self.callback(err, self);
    //   });
    // });
};
WSDL.prototype.ignoredNamespaces = ['tns', 'targetNamespace', 'typedNamespace'];
WSDL.prototype.ignoreBaseNameSpaces = false;
WSDL.prototype.valueKey = '$value';
WSDL.prototype.xmlKey = '$xml';
WSDL.prototype._initializeOptions = function (options) {
    this._originalIgnoredNamespaces = (options || {}).ignoredNamespaces;
    this.options = {};
    /** @type {?} */
    var ignoredNamespaces = options ? options.ignoredNamespaces : null;
    if (ignoredNamespaces &&
        (Array.isArray(ignoredNamespaces.namespaces) || typeof ignoredNamespaces.namespaces === 'string')) {
        if (ignoredNamespaces.override) {
            this.options.ignoredNamespaces = ignoredNamespaces.namespaces;
        }
        else {
            this.options.ignoredNamespaces = this.ignoredNamespaces.concat(ignoredNamespaces.namespaces);
        }
    }
    else {
        this.options.ignoredNamespaces = this.ignoredNamespaces;
    }
    this.options.valueKey = options.valueKey || this.valueKey;
    this.options.xmlKey = options.xmlKey || this.xmlKey;
    if (options.escapeXML !== undefined) {
        this.options.escapeXML = options.escapeXML;
    }
    else {
        this.options.escapeXML = true;
    }
    if (options.returnFault !== undefined) {
        this.options.returnFault = options.returnFault;
    }
    else {
        this.options.returnFault = false;
    }
    this.options.handleNilAsNull = !!options.handleNilAsNull;
    if (options.namespaceArrayElements !== undefined) {
        this.options.namespaceArrayElements = options.namespaceArrayElements;
    }
    else {
        this.options.namespaceArrayElements = true;
    }
    // Allow any request headers to keep passing through
    this.options.wsdl_headers = options.wsdl_headers;
    this.options.wsdl_options = options.wsdl_options;
    if (options.httpClient) {
        this.options.httpClient = options.httpClient;
    }
    // The supplied request-object should be passed through
    if (options.request) {
        this.options.request = options.request;
    }
    /** @type {?} */
    var ignoreBaseNameSpaces = options ? options.ignoreBaseNameSpaces : null;
    if (ignoreBaseNameSpaces !== null && typeof ignoreBaseNameSpaces !== 'undefined') {
        this.options.ignoreBaseNameSpaces = ignoreBaseNameSpaces;
    }
    else {
        this.options.ignoreBaseNameSpaces = this.ignoreBaseNameSpaces;
    }
    // Works only in client
    this.options.forceSoap12Headers = options.forceSoap12Headers;
    this.options.customDeserializer = options.customDeserializer;
    if (options.overrideRootElement !== undefined) {
        this.options.overrideRootElement = options.overrideRootElement;
    }
    this.options.useEmptyTag = !!options.useEmptyTag;
};
WSDL.prototype.onReady = function (callback) {
    if (callback)
        this.callback = callback;
};
WSDL.prototype._processNextInclude = function (includes) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var self, include, options, includePath, wsdl;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    self = this;
                    include = includes.shift();
                    if (!include)
                        return [2 /*return*/]; // callback();
                    // callback();
                    if (!/^https?:/.test(self.uri) && !/^https?:/.test(include.location)) {
                        // includePath = path.resolve(path.dirname(self.uri), include.location);
                    }
                    else {
                        includePath = url.resolve(self.uri || '', include.location);
                    }
                    options = _.assign({}, this.options);
                    // follow supplied ignoredNamespaces option
                    options.ignoredNamespaces = this._originalIgnoredNamespaces || this.options.ignoredNamespaces;
                    options.WSDL_CACHE = this.WSDL_CACHE;
                    return [4 /*yield*/, open_wsdl_recursive(includePath, options)];
                case 1:
                    wsdl = _a.sent();
                    self._includesWsdl.push(wsdl);
                    if (wsdl.definitions instanceof DefinitionsElement) {
                        _.mergeWith(self.definitions, wsdl.definitions, function (a, b) {
                            return (a instanceof SchemaElement) ? a.merge(b) : undefined;
                        });
                    }
                    else {
                        self.definitions.schemas[include.namespace || wsdl.definitions.$targetNamespace] = deepMerge(self.definitions.schemas[include.namespace || wsdl.definitions.$targetNamespace], wsdl.definitions);
                    }
                    return [2 /*return*/, self._processNextInclude(includes)];
            }
        });
    });
};
WSDL.prototype.processIncludes = function () {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var schemas, includes, ns, schema;
        return tslib_1.__generator(this, function (_a) {
            schemas = this.definitions.schemas;
            includes = [];
            for (ns in schemas) {
                schema = schemas[ns];
                includes = includes.concat(schema.includes || []);
            }
            return [2 /*return*/, this._processNextInclude(includes)];
        });
    });
};
WSDL.prototype.describeServices = function () {
    /** @type {?} */
    var services = {};
    for (var name_6 in this.services) {
        /** @type {?} */
        var service = this.services[name_6];
        services[name_6] = service.description(this.definitions);
    }
    return services;
};
WSDL.prototype.toXML = function () {
    return this.xml || '';
};
WSDL.prototype.xmlToObject = function (xml, callback) {
    /** @type {?} */
    var self = this;
    /** @type {?} */
    var p = typeof callback === 'function' ? {} : sax.parser(true);
    /** @type {?} */
    var objectName = null;
    /** @type {?} */
    var root = {};
    /** @type {?} */
    var schema = {
        Envelope: {
            Header: {
                Security: {
                    UsernameToken: {
                        Username: 'string',
                        Password: 'string'
                    }
                }
            },
            Body: {
                Fault: {
                    faultcode: 'string',
                    faultstring: 'string',
                    detail: 'string'
                }
            }
        }
    };
    /** @type {?} */
    var stack = [{ name: null, object: root, schema: schema }];
    /** @type {?} */
    var xmlns = {};
    /** @type {?} */
    var refs = {};
    /** @type {?} */
    var id;
    p.onopentag = function (node) {
        /** @type {?} */
        var nsName = node.name;
        /** @type {?} */
        var attrs = node.attributes;
        /** @type {?} */
        var name = splitQName(nsName).name;
        /** @type {?} */
        var attributeName;
        /** @type {?} */
        var top = stack[stack.length - 1];
        /** @type {?} */
        var topSchema = top.schema;
        /** @type {?} */
        var elementAttributes = {};
        /** @type {?} */
        var hasNonXmlnsAttribute = false;
        /** @type {?} */
        var hasNilAttribute = false;
        /** @type {?} */
        var obj = {};
        /** @type {?} */
        var originalName = name;
        if (!objectName && top.name === 'Body' && name !== 'Fault') {
            /** @type {?} */
            var message = self.definitions.messages[name];
            // Support RPC/literal messages where response body contains one element named
            // after the operation + 'Response'. See http://www.w3.org/TR/wsdl#_names
            if (!message) {
                try {
                    // Determine if this is request or response
                    /** @type {?} */
                    var isInput = false;
                    /** @type {?} */
                    var isOutput = false;
                    if ((/Response$/).test(name)) {
                        isOutput = true;
                        name = name.replace(/Response$/, '');
                    }
                    else if ((/Request$/).test(name)) {
                        isInput = true;
                        name = name.replace(/Request$/, '');
                    }
                    else if ((/Solicit$/).test(name)) {
                        isInput = true;
                        name = name.replace(/Solicit$/, '');
                    }
                    // Look up the appropriate message as given in the portType's operations
                    /** @type {?} */
                    var portTypes = self.definitions.portTypes;
                    /** @type {?} */
                    var portTypeNames = Object.keys(portTypes);
                    // Currently this supports only one portType definition.
                    /** @type {?} */
                    var portType = portTypes[portTypeNames[0]];
                    if (isInput) {
                        name = portType.methods[name].input.$name;
                    }
                    else {
                        name = portType.methods[name].output.$name;
                    }
                    message = self.definitions.messages[name];
                    // 'cache' this alias to speed future lookups
                    self.definitions.messages[originalName] = self.definitions.messages[name];
                }
                catch (e) {
                    if (self.options.returnFault) {
                        p.onerror(e);
                    }
                }
            }
            topSchema = message.description(self.definitions);
            objectName = originalName;
        }
        if (attrs.href) {
            id = attrs.href.substr(1);
            if (!refs[id]) {
                refs[id] = { hrefs: [], obj: null };
            }
            refs[id].hrefs.push({ par: top.object, key: name, obj: obj });
        }
        if (id = attrs.id) {
            if (!refs[id]) {
                refs[id] = { hrefs: [], obj: null };
            }
        }
        //Handle element attributes
        for (attributeName in attrs) {
            if (/^xmlns:|^xmlns$/.test(attributeName)) {
                xmlns[splitQName(attributeName).name] = attrs[attributeName];
                continue;
            }
            hasNonXmlnsAttribute = true;
            elementAttributes[attributeName] = attrs[attributeName];
        }
        for (attributeName in elementAttributes) {
            /** @type {?} */
            var res = splitQName(attributeName);
            if (res.name === 'nil' && xmlns[res.prefix] === 'http://www.w3.org/2001/XMLSchema-instance' && elementAttributes[attributeName] &&
                (elementAttributes[attributeName].toLowerCase() === 'true' || elementAttributes[attributeName] === '1')) {
                hasNilAttribute = true;
                break;
            }
        }
        if (hasNonXmlnsAttribute) {
            obj[self.options.attributesKey] = elementAttributes;
        }
        // Pick up the schema for the type specified in element's xsi:type attribute.
        /** @type {?} */
        var xsiTypeSchema;
        /** @type {?} */
        var xsiType = elementAttributes['xsi:type'];
        if (xsiType) {
            /** @type {?} */
            var type = splitQName(xsiType);
            /** @type {?} */
            var typeURI = void 0;
            if (type.prefix === TNS_PREFIX) {
                // In case of xsi:type = "MyType"
                typeURI = xmlns[type.prefix] || xmlns.xmlns;
            }
            else {
                typeURI = xmlns[type.prefix];
            }
            /** @type {?} */
            var typeDef = self.findSchemaObject(typeURI, type.name);
            if (typeDef) {
                xsiTypeSchema = typeDef.description(self.definitions);
            }
        }
        if (topSchema && topSchema[name + '[]']) {
            name = name + '[]';
        }
        stack.push({
            name: originalName,
            object: obj,
            schema: (xsiTypeSchema || (topSchema && topSchema[name])),
            id: attrs.id,
            nil: hasNilAttribute
        });
    };
    p.onclosetag = function (nsName) {
        /** @type {?} */
        var cur = stack.pop();
        /** @type {?} */
        var obj = cur.object;
        /** @type {?} */
        var top = stack[stack.length - 1];
        /** @type {?} */
        var topObject = top.object;
        /** @type {?} */
        var topSchema = top.schema;
        /** @type {?} */
        var name = splitQName(nsName).name;
        if (typeof cur.schema === 'string' && (cur.schema === 'string' || ((/** @type {?} */ (cur.schema))).split(':')[1] === 'string')) {
            if (typeof obj === 'object' && Object.keys(obj).length === 0)
                obj = cur.object = '';
        }
        if (cur.nil === true) {
            if (self.options.handleNilAsNull) {
                obj = null;
            }
            else {
                return;
            }
        }
        if (_.isPlainObject(obj) && !Object.keys(obj).length) {
            obj = null;
        }
        if (topSchema && topSchema[name + '[]']) {
            if (!topObject[name]) {
                topObject[name] = [];
            }
            topObject[name].push(obj);
        }
        else if (name in topObject) {
            if (!Array.isArray(topObject[name])) {
                topObject[name] = [topObject[name]];
            }
            topObject[name].push(obj);
        }
        else {
            topObject[name] = obj;
        }
        if (cur.id) {
            refs[cur.id].obj = obj;
        }
    };
    p.oncdata = function (text) {
        /** @type {?} */
        var originalText = text;
        text = trim(text);
        if (!text.length) {
            return;
        }
        if (/<\?xml[\s\S]+\?>/.test(text)) {
            /** @type {?} */
            var top_1 = stack[stack.length - 1];
            /** @type {?} */
            var value = self.xmlToObject(text);
            if (top_1.object[self.options.attributesKey]) {
                top_1.object[self.options.valueKey] = value;
            }
            else {
                top_1.object = value;
            }
        }
        else {
            p.ontext(originalText);
        }
    };
    p.onerror = function (e) {
        p.resume();
        throw {
            Fault: {
                faultcode: 500,
                faultstring: 'Invalid XML',
                detail: new Error(e).message,
                statusCode: 500
            }
        };
    };
    p.ontext = function (text) {
        /** @type {?} */
        var originalText = text;
        text = trim(text);
        if (!text.length) {
            return;
        }
        /** @type {?} */
        var top = stack[stack.length - 1];
        /** @type {?} */
        var name = splitQName(top.schema).name;
        /** @type {?} */
        var value;
        if (self.options && self.options.customDeserializer && self.options.customDeserializer[name]) {
            value = self.options.customDeserializer[name](text, top);
        }
        else {
            if (name === 'int' || name === 'integer') {
                value = parseInt(text, 10);
            }
            else if (name === 'bool' || name === 'boolean') {
                value = text.toLowerCase() === 'true' || text === '1';
            }
            else if (name === 'dateTime' || name === 'date') {
                value = new Date(text);
            }
            else {
                if (self.options.preserveWhitespace) {
                    text = originalText;
                }
                // handle string or other types
                if (typeof top.object !== 'string') {
                    value = text;
                }
                else {
                    value = top.object + text;
                }
            }
        }
        if (top.object[self.options.attributesKey]) {
            top.object[self.options.valueKey] = value;
        }
        else {
            top.object = value;
        }
    };
    if (typeof callback === 'function') {
        // we be streaming
        /** @type {?} */
        var saxStream = sax.createStream(true);
        saxStream.on('opentag', p.onopentag);
        saxStream.on('closetag', p.onclosetag);
        saxStream.on('cdata', p.oncdata);
        saxStream.on('text', p.ontext);
        xml.pipe(saxStream)
            .on('error', function (err) {
            callback(err);
        })
            .on('end', function () {
            /** @type {?} */
            var r;
            try {
                r = finish();
            }
            catch (e) {
                return callback(e);
            }
            callback(null, r);
        });
        return;
    }
    p.write(xml).close();
    return finish();
    /**
     * @return {?}
     */
    function finish() {
        // MultiRef support: merge objects instead of replacing
        for (var n in refs) {
            /** @type {?} */
            var ref = refs[n];
            for (var i = 0; i < ref.hrefs.length; i++) {
                _.assign(ref.hrefs[i].obj, ref.obj);
            }
        }
        if (root.Envelope) {
            /** @type {?} */
            var body = root.Envelope.Body;
            if (body && body.Fault) {
                /** @type {?} */
                var code = body.Fault.faultcode && body.Fault.faultcode.$value;
                /** @type {?} */
                var string = body.Fault.faultstring && body.Fault.faultstring.$value;
                /** @type {?} */
                var detail = body.Fault.detail && body.Fault.detail.$value;
                code = code || body.Fault.faultcode;
                string = string || body.Fault.faultstring;
                detail = detail || body.Fault.detail;
                /** @type {?} */
                var error = new Error(code + ': ' + string + (detail ? ': ' + detail : ''));
                error.root = root;
                throw error;
            }
            return root.Envelope;
        }
        return root;
    }
};
/**
 * Look up a XSD type or element by namespace URI and name
 * @param {String} nsURI Namespace URI
 * @param {String} qname Local or qualified name
 * @returns {*} The XSD type/element definition
 */
WSDL.prototype.findSchemaObject = function (nsURI, qname) {
    if (!nsURI || !qname) {
        return null;
    }
    /** @type {?} */
    var def = null;
    if (this.definitions.schemas) {
        /** @type {?} */
        var schema = this.definitions.schemas[nsURI];
        if (schema) {
            if (qname.indexOf(':') !== -1) {
                qname = qname.substring(qname.indexOf(':') + 1, qname.length);
            }
            // if the client passed an input element which has a `$lookupType` property instead of `$type`
            // the `def` is found in `schema.elements`.
            def = schema.complexTypes[qname] || schema.types[qname] || schema.elements[qname];
        }
    }
    return def;
};
/**
 * Create document style xml string from the parameters
 * @param {String} name
 * @param {*} params
 * @param {String} nsPrefix
 * @param {String} nsURI
 * @param {String} type
 */
WSDL.prototype.objectToDocumentXML = function (name, params, nsPrefix, nsURI, type) {
    //If user supplies XML already, just use that.  XML Declaration should not be present.
    if (params && params._xml) {
        return params._xml;
    }
    /** @type {?} */
    var args = {};
    args[name] = params;
    /** @type {?} */
    var parameterTypeObj = type ? this.findSchemaObject(nsURI, type) : null;
    return this.objectToXML(args, null, nsPrefix, nsURI, true, null, parameterTypeObj);
};
/**
 * Create RPC style xml string from the parameters
 * @param {String} name
 * @param {*} params
 * @param {String} nsPrefix
 * @param {String} nsURI
 * @returns {string}
 */
WSDL.prototype.objectToRpcXML = function (name, params, nsPrefix, nsURI, isParts) {
    /** @type {?} */
    var parts = [];
    /** @type {?} */
    var defs = this.definitions;
    /** @type {?} */
    var nsAttrName = '_xmlns';
    nsPrefix = nsPrefix || findPrefix(defs.xmlns, nsURI);
    nsURI = nsURI || defs.xmlns[nsPrefix];
    nsPrefix = nsPrefix === TNS_PREFIX ? '' : (nsPrefix + ':');
    parts.push(['<', nsPrefix, name, '>'].join(''));
    for (var key in params) {
        if (!params.hasOwnProperty(key)) {
            continue;
        }
        if (key !== nsAttrName) {
            /** @type {?} */
            var value = params[key];
            /** @type {?} */
            var prefixedKey = (isParts ? '' : nsPrefix) + key;
            /** @type {?} */
            var attributes = [];
            if (typeof value === 'object' && value.hasOwnProperty(this.options.attributesKey)) {
                /** @type {?} */
                var attrs = value[this.options.attributesKey];
                for (var n in attrs) {
                    attributes.push(' ' + n + '=' + '"' + attrs[n] + '"');
                }
            }
            parts.push(['<', prefixedKey].concat(attributes).concat('>').join(''));
            parts.push((typeof value === 'object') ? this.objectToXML(value, key, nsPrefix, nsURI) : xmlEscape(value));
            parts.push(['</', prefixedKey, '>'].join(''));
        }
    }
    parts.push(['</', nsPrefix, name, '>'].join(''));
    return parts.join('');
};
/**
 * @param {?} ns
 * @return {?}
 */
function appendColon(ns) {
    return (ns && ns.charAt(ns.length - 1) !== ':') ? ns + ':' : ns;
}
/**
 * @param {?} ns
 * @return {?}
 */
function noColonNameSpace(ns) {
    return (ns && ns.charAt(ns.length - 1) === ':') ? ns.substring(0, ns.length - 1) : ns;
}
WSDL.prototype.isIgnoredNameSpace = function (ns) {
    return this.options.ignoredNamespaces.indexOf(ns) > -1;
};
WSDL.prototype.filterOutIgnoredNameSpace = function (ns) {
    /** @type {?} */
    var namespace = noColonNameSpace(ns);
    return this.isIgnoredNameSpace(namespace) ? '' : namespace;
};
/**
 * Convert an object to XML.  This is a recursive method as it calls itself.
 *
 * @param {Object} obj the object to convert.
 * @param {String} name the name of the element (if the object being traversed is
 * an element).
 * @param {String} nsPrefix the namespace prefix of the object I.E. xsd.
 * @param {String} nsURI the full namespace of the object I.E. http://w3.org/schema.
 * @param {Boolean} isFirst whether or not this is the first item being traversed.
 * @param {?} xmlnsAttr
 * @param {?} parameterTypeObject
 * @param {NamespaceContext} nsContext Namespace context
 */
WSDL.prototype.objectToXML = function (obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext) {
    /** @type {?} */
    var self = this;
    /** @type {?} */
    var schema = this.definitions.schemas[nsURI];
    /** @type {?} */
    var parentNsPrefix = nsPrefix ? nsPrefix.parent : undefined;
    if (typeof parentNsPrefix !== 'undefined') {
        //we got the parentNsPrefix for our array. setting the namespace-letiable back to the current namespace string
        nsPrefix = nsPrefix.current;
    }
    parentNsPrefix = noColonNameSpace(parentNsPrefix);
    if (this.isIgnoredNameSpace(parentNsPrefix)) {
        parentNsPrefix = '';
    }
    /** @type {?} */
    var soapHeader = !schema;
    /** @type {?} */
    var qualified = schema && schema.$elementFormDefault === 'qualified';
    /** @type {?} */
    var parts = [];
    /** @type {?} */
    var prefixNamespace = (nsPrefix || qualified) && nsPrefix !== TNS_PREFIX;
    /** @type {?} */
    var xmlnsAttrib = '';
    if (nsURI && isFirst) {
        if (self.options.overrideRootElement && self.options.overrideRootElement.xmlnsAttributes) {
            self.options.overrideRootElement.xmlnsAttributes.forEach(function (attribute) {
                xmlnsAttrib += ' ' + attribute.name + '="' + attribute.value + '"';
            });
        }
        else {
            if (prefixNamespace && !this.isIgnoredNameSpace(nsPrefix)) {
                // resolve the prefix namespace
                xmlnsAttrib += ' xmlns:' + nsPrefix + '="' + nsURI + '"';
            }
            // only add default namespace if the schema elementFormDefault is qualified
            if (qualified || soapHeader)
                xmlnsAttrib += ' xmlns="' + nsURI + '"';
        }
    }
    if (!nsContext) {
        nsContext = new NamespaceContext();
        nsContext.declareNamespace(nsPrefix, nsURI);
    }
    else {
        nsContext.pushContext();
    }
    // explicitly use xmlns attribute if available
    if (xmlnsAttr && !(self.options.overrideRootElement && self.options.overrideRootElement.xmlnsAttributes)) {
        xmlnsAttrib = xmlnsAttr;
    }
    /** @type {?} */
    var ns = '';
    if (self.options.overrideRootElement && isFirst) {
        ns = self.options.overrideRootElement.namespace;
    }
    else if (prefixNamespace && (qualified || isFirst || soapHeader) && !this.isIgnoredNameSpace(nsPrefix)) {
        ns = nsPrefix;
    }
    /** @type {?} */
    var i;
    /** @type {?} */
    var n;
    // start building out XML string.
    if (Array.isArray(obj)) {
        for (i = 0, n = obj.length; i < n; i++) {
            /** @type {?} */
            var item = obj[i];
            /** @type {?} */
            var arrayAttr = self.processAttributes(item, nsContext);
            /** @type {?} */
            var correctOuterNsPrefix = parentNsPrefix || ns;
            //using the parent namespace prefix if given
            /** @type {?} */
            var body = self.objectToXML(item, name, nsPrefix, nsURI, false, null, schemaObject, nsContext);
            /** @type {?} */
            var openingTagParts = ['<', appendColon(correctOuterNsPrefix), name, arrayAttr, xmlnsAttrib];
            if (body === '' && self.options.useEmptyTag) {
                // Use empty (self-closing) tags if no contents
                openingTagParts.push(' />');
                parts.push(openingTagParts.join(''));
            }
            else {
                openingTagParts.push('>');
                if (self.options.namespaceArrayElements || i === 0) {
                    parts.push(openingTagParts.join(''));
                }
                parts.push(body);
                if (self.options.namespaceArrayElements || i === n - 1) {
                    parts.push(['</', appendColon(correctOuterNsPrefix), name, '>'].join(''));
                }
            }
        }
    }
    else if (typeof obj === 'object') {
        for (name in obj) {
            if (!obj.hasOwnProperty(name))
                continue;
            //don't process attributes as element
            if (name === self.options.attributesKey) {
                continue;
            }
            //Its the value of a xml object. Return it directly.
            if (name === self.options.xmlKey) {
                nsContext.popContext();
                return obj[name];
            }
            //Its the value of an item. Return it directly.
            if (name === self.options.valueKey) {
                nsContext.popContext();
                return xmlEscape(obj[name]);
            }
            /** @type {?} */
            var child = obj[name];
            if (typeof child === 'undefined') {
                continue;
            }
            /** @type {?} */
            var attr = self.processAttributes(child, nsContext);
            /** @type {?} */
            var value = '';
            /** @type {?} */
            var nonSubNameSpace = '';
            /** @type {?} */
            var emptyNonSubNameSpace = false;
            /** @type {?} */
            var nameWithNsRegex = /^([^:]+):([^:]+)$/.exec(name);
            if (nameWithNsRegex) {
                nonSubNameSpace = nameWithNsRegex[1] + ':';
                name = nameWithNsRegex[2];
            }
            else if (name[0] === ':') {
                emptyNonSubNameSpace = true;
                name = name.substr(1);
            }
            if (isFirst) {
                value = self.objectToXML(child, name, nsPrefix, nsURI, false, null, schemaObject, nsContext);
            }
            else {
                if (self.definitions.schemas) {
                    if (schema) {
                        /** @type {?} */
                        var childSchemaObject = self.findChildSchemaObject(schemaObject, name);
                        //find sub namespace if not a primitive
                        if (childSchemaObject &&
                            ((childSchemaObject.$type && (childSchemaObject.$type.indexOf('xsd:') === -1)) ||
                                childSchemaObject.$ref || childSchemaObject.$name)) {
                            /*if the base name space of the children is not in the ingoredSchemaNamspaces we use it.
                                           This is because in some services the child nodes do not need the baseNameSpace.
                                           */
                            /** @type {?} */
                            var childNsPrefix = '';
                            /** @type {?} */
                            var childName = '';
                            /** @type {?} */
                            var childNsURI = void 0;
                            /** @type {?} */
                            var childXmlnsAttrib = '';
                            /** @type {?} */
                            var elementQName = childSchemaObject.$ref || childSchemaObject.$name;
                            if (elementQName) {
                                elementQName = splitQName(elementQName);
                                childName = elementQName.name;
                                if (elementQName.prefix === TNS_PREFIX) {
                                    // Local element
                                    childNsURI = childSchemaObject.$targetNamespace;
                                    childNsPrefix = nsContext.registerNamespace(childNsURI);
                                    if (this.isIgnoredNameSpace(childNsPrefix)) {
                                        childNsPrefix = nsPrefix;
                                    }
                                }
                                else {
                                    childNsPrefix = elementQName.prefix;
                                    if (this.isIgnoredNameSpace(childNsPrefix)) {
                                        childNsPrefix = nsPrefix;
                                    }
                                    childNsURI = schema.xmlns[childNsPrefix] || self.definitions.xmlns[childNsPrefix];
                                }
                                /** @type {?} */
                                var unqualified = false;
                                // Check qualification form for local elements
                                if (childSchemaObject.$name && childSchemaObject.targetNamespace === undefined) {
                                    if (childSchemaObject.$form === 'unqualified') {
                                        unqualified = true;
                                    }
                                    else if (childSchemaObject.$form === 'qualified') {
                                        unqualified = false;
                                    }
                                    else {
                                        unqualified = schema.$elementFormDefault !== 'qualified';
                                    }
                                }
                                if (unqualified) {
                                    childNsPrefix = '';
                                }
                                if (childNsURI && childNsPrefix) {
                                    if (nsContext.declareNamespace(childNsPrefix, childNsURI)) {
                                        childXmlnsAttrib = ' xmlns:' + childNsPrefix + '="' + childNsURI + '"';
                                        xmlnsAttrib += childXmlnsAttrib;
                                    }
                                }
                            }
                            /** @type {?} */
                            var resolvedChildSchemaObject = void 0;
                            if (childSchemaObject.$type) {
                                /** @type {?} */
                                var typeQName = splitQName(childSchemaObject.$type);
                                /** @type {?} */
                                var typePrefix = typeQName.prefix;
                                /** @type {?} */
                                var typeURI = schema.xmlns[typePrefix] || self.definitions.xmlns[typePrefix];
                                childNsURI = typeURI;
                                if (typeURI !== 'http://www.w3.org/2001/XMLSchema' && typePrefix !== TNS_PREFIX) {
                                    // Add the prefix/namespace mapping, but not declare it
                                    nsContext.addNamespace(typePrefix, typeURI);
                                }
                                resolvedChildSchemaObject =
                                    self.findSchemaType(typeQName.name, typeURI) || childSchemaObject;
                            }
                            else {
                                resolvedChildSchemaObject =
                                    self.findSchemaObject(childNsURI, childName) || childSchemaObject;
                            }
                            if (childSchemaObject.$baseNameSpace && this.options.ignoreBaseNameSpaces) {
                                childNsPrefix = nsPrefix;
                                childNsURI = nsURI;
                            }
                            if (this.options.ignoreBaseNameSpaces) {
                                childNsPrefix = '';
                                childNsURI = '';
                            }
                            ns = childNsPrefix;
                            if (Array.isArray(child)) {
                                //for arrays, we need to remember the current namespace
                                childNsPrefix = {
                                    current: childNsPrefix,
                                    parent: ns
                                };
                            }
                            else {
                                //parent (array) already got the namespace
                                childXmlnsAttrib = null;
                            }
                            value = self.objectToXML(child, name, childNsPrefix, childNsURI, false, childXmlnsAttrib, resolvedChildSchemaObject, nsContext);
                        }
                        else if (obj[self.options.attributesKey] && obj[self.options.attributesKey].xsi_type) {
                            //if parent object has complex type defined and child not found in parent
                            /** @type {?} */
                            var completeChildParamTypeObject = self.findChildSchemaObject(obj[self.options.attributesKey].xsi_type.type, obj[self.options.attributesKey].xsi_type.xmlns);
                            nonSubNameSpace = obj[self.options.attributesKey].xsi_type.prefix;
                            nsContext.addNamespace(obj[self.options.attributesKey].xsi_type.prefix, obj[self.options.attributesKey].xsi_type.xmlns);
                            value = self.objectToXML(child, name, obj[self.options.attributesKey].xsi_type.prefix, obj[self.options.attributesKey].xsi_type.xmlns, false, null, null, nsContext);
                        }
                        else {
                            if (Array.isArray(child)) {
                                name = nonSubNameSpace + name;
                            }
                            value = self.objectToXML(child, name, nsPrefix, nsURI, false, null, null, nsContext);
                        }
                    }
                    else {
                        value = self.objectToXML(child, name, nsPrefix, nsURI, false, null, null, nsContext);
                    }
                }
            }
            ns = noColonNameSpace(ns);
            if (prefixNamespace && !qualified && isFirst && !self.options.overrideRootElement) {
                ns = nsPrefix;
            }
            else if (this.isIgnoredNameSpace(ns)) {
                ns = '';
            }
            /** @type {?} */
            var useEmptyTag = !value && self.options.useEmptyTag;
            if (!Array.isArray(child)) {
                // start tag
                parts.push(['<', emptyNonSubNameSpace ? '' : appendColon(nonSubNameSpace || ns), name, attr, xmlnsAttrib,
                    (child === null ? ' xsi:nil="true"' : ''),
                    useEmptyTag ? ' />' : '>'
                ].join(''));
            }
            if (!useEmptyTag) {
                parts.push(value);
                if (!Array.isArray(child)) {
                    // end tag
                    parts.push(['</', emptyNonSubNameSpace ? '' : appendColon(nonSubNameSpace || ns), name, '>'].join(''));
                }
            }
        }
    }
    else if (obj !== undefined) {
        parts.push((self.options.escapeXML) ? xmlEscape(obj) : obj);
    }
    nsContext.popContext();
    return parts.join('');
};
WSDL.prototype.processAttributes = function (child, nsContext) {
    /** @type {?} */
    var attr = '';
    if (child === null) {
        child = [];
    }
    /** @type {?} */
    var attrObj = child[this.options.attributesKey];
    if (attrObj && attrObj.xsi_type) {
        /** @type {?} */
        var xsiType = attrObj.xsi_type;
        /** @type {?} */
        var prefix = xsiType.prefix || xsiType.namespace;
        // Generate a new namespace for complex extension if one not provided
        if (!prefix) {
            prefix = nsContext.registerNamespace(xsiType.xmlns);
        }
        else {
            nsContext.declareNamespace(prefix, xsiType.xmlns);
        }
        xsiType.prefix = prefix;
    }
    if (attrObj) {
        for (var attrKey in attrObj) {
            //handle complex extension separately
            if (attrKey === 'xsi_type') {
                /** @type {?} */
                var attrValue = attrObj[attrKey];
                attr += ' xsi:type="' + attrValue.prefix + ':' + attrValue.type + '"';
                attr += ' xmlns:' + attrValue.prefix + '="' + attrValue.xmlns + '"';
                continue;
            }
            else {
                attr += ' ' + attrKey + '="' + xmlEscape(attrObj[attrKey]) + '"';
            }
        }
    }
    return attr;
};
/**
 * Look up a schema type definition
 * @param name
 * @param nsURI
 * @returns {*}
 */
WSDL.prototype.findSchemaType = function (name, nsURI) {
    if (!this.definitions.schemas || !name || !nsURI) {
        return null;
    }
    /** @type {?} */
    var schema = this.definitions.schemas[nsURI];
    if (!schema || !schema.complexTypes) {
        return null;
    }
    return schema.complexTypes[name];
};
WSDL.prototype.findChildSchemaObject = function (parameterTypeObj, childName, backtrace) {
    if (!parameterTypeObj || !childName) {
        return null;
    }
    if (!backtrace) {
        backtrace = [];
    }
    if (backtrace.indexOf(parameterTypeObj) >= 0) {
        // We've recursed back to ourselves; break.
        return null;
    }
    else {
        backtrace = backtrace.concat([parameterTypeObj]);
    }
    /** @type {?} */
    var found = null;
    /** @type {?} */
    var i = 0;
    /** @type {?} */
    var child;
    /** @type {?} */
    var ref;
    if (Array.isArray(parameterTypeObj.$lookupTypes) && parameterTypeObj.$lookupTypes.length) {
        /** @type {?} */
        var types = parameterTypeObj.$lookupTypes;
        for (i = 0; i < types.length; i++) {
            /** @type {?} */
            var typeObj = types[i];
            if (typeObj.$name === childName) {
                found = typeObj;
                break;
            }
        }
    }
    /** @type {?} */
    var object = parameterTypeObj;
    if (object.$name === childName && object.name === 'element') {
        return object;
    }
    if (object.$ref) {
        ref = splitQName(object.$ref);
        if (ref.name === childName) {
            return object;
        }
    }
    /** @type {?} */
    var childNsURI;
    // want to avoid unecessary recursion to improve performance
    if (object.$type && backtrace.length === 1) {
        /** @type {?} */
        var typeInfo = splitQName(object.$type);
        if (typeInfo.prefix === TNS_PREFIX) {
            childNsURI = parameterTypeObj.$targetNamespace;
        }
        else {
            childNsURI = this.definitions.xmlns[typeInfo.prefix];
        }
        /** @type {?} */
        var typeDef = this.findSchemaType(typeInfo.name, childNsURI);
        if (typeDef) {
            return this.findChildSchemaObject(typeDef, childName, backtrace);
        }
    }
    if (object.children) {
        for (i = 0, child; child = object.children[i]; i++) {
            found = this.findChildSchemaObject(child, childName, backtrace);
            if (found) {
                break;
            }
            if (child.$base) {
                /** @type {?} */
                var baseQName = splitQName(child.$base);
                /** @type {?} */
                var childNameSpace = baseQName.prefix === TNS_PREFIX ? '' : baseQName.prefix;
                childNsURI = child.xmlns[baseQName.prefix] || this.definitions.xmlns[baseQName.prefix];
                /** @type {?} */
                var foundBase = this.findSchemaType(baseQName.name, childNsURI);
                if (foundBase) {
                    found = this.findChildSchemaObject(foundBase, childName, backtrace);
                    if (found) {
                        found.$baseNameSpace = childNameSpace;
                        found.$type = childNameSpace + ':' + childName;
                        break;
                    }
                }
            }
        }
    }
    if (!found && object.$name === childName) {
        return object;
    }
    return found;
};
WSDL.prototype._parse = function (xml) {
    /** @type {?} */
    var self = this;
    /** @type {?} */
    var p = sax.parser(true);
    /** @type {?} */
    var stack = [];
    /** @type {?} */
    var root = null;
    /** @type {?} */
    var types = null;
    /** @type {?} */
    var schema = null;
    /** @type {?} */
    var options = self.options;
    p.onopentag = function (node) {
        /** @type {?} */
        var nsName = node.name;
        /** @type {?} */
        var attrs = node.attributes;
        /** @type {?} */
        var top = stack[stack.length - 1];
        /** @type {?} */
        var name;
        if (top) {
            try {
                top.startElement(stack, nsName, attrs, options);
            }
            catch (e) {
                if (self.options.strict) {
                    throw e;
                }
                else {
                    stack.push(new Element(nsName, attrs, options));
                }
            }
        }
        else {
            name = splitQName(nsName).name;
            if (name === 'definitions') {
                root = new DefinitionsElement(nsName, attrs, options);
                stack.push(root);
            }
            else if (name === 'schema') {
                // Shim a structure in here to allow the proper objects to be created when merging back.
                root = new DefinitionsElement('definitions', {}, {});
                types = new TypesElement('types', {}, {});
                schema = new SchemaElement(nsName, attrs, options);
                types.addChild(schema);
                root.addChild(types);
                stack.push(schema);
            }
            else {
                throw new Error('Unexpected root element of WSDL or include');
            }
        }
    };
    p.onclosetag = function (name) {
        /** @type {?} */
        var top = stack[stack.length - 1];
        assert(top, 'Unmatched close tag: ' + name);
        top.endElement(stack, name);
    };
    p.write(xml).close();
    return root;
};
WSDL.prototype._fromXML = function (xml) {
    this.definitions = this._parse(xml);
    this.definitions.descriptions = {
        types: {}
    };
    this.xml = xml;
};
WSDL.prototype._fromServices = function (services) {
};
WSDL.prototype._xmlnsMap = function () {
    /** @type {?} */
    var xmlns = this.definitions.xmlns;
    /** @type {?} */
    var str = '';
    for (var alias in xmlns) {
        if (alias === '' || alias === TNS_PREFIX) {
            continue;
        }
        /** @type {?} */
        var ns = xmlns[alias];
        switch (ns) {
            case "http://xml.apache.org/xml-soap": // apachesoap
            case "http://schemas.xmlsoap.org/wsdl/": // wsdl
            case "http://schemas.xmlsoap.org/wsdl/soap/": // wsdlsoap
            case "http://schemas.xmlsoap.org/wsdl/soap12/": // wsdlsoap12
            case "http://schemas.xmlsoap.org/soap/encoding/": // soapenc
            case "http://www.w3.org/2001/XMLSchema": // xsd
                continue;
        }
        if (~ns.indexOf('http://schemas.xmlsoap.org/')) {
            continue;
        }
        if (~ns.indexOf('http://www.w3.org/')) {
            continue;
        }
        if (~ns.indexOf('http://xml.apache.org/')) {
            continue;
        }
        str += ' xmlns:' + alias + '="' + ns + '"';
    }
    return str;
};
/*
 * Have another function to load previous WSDLs as we
 * don't want this to be invoked externally (expect for tests)
 * This will attempt to fix circular dependencies with XSD files,
 * Given
 * - file.wsdl
 *   - xs:import namespace="A" schemaLocation: A.xsd
 * - A.xsd
 *   - xs:import namespace="B" schemaLocation: B.xsd
 * - B.xsd
 *   - xs:import namespace="A" schemaLocation: A.xsd
 * file.wsdl will start loading, import A, then A will import B, which will then import A
 * Because A has already started to load previously it will be returned right away and
 * have an internal circular reference
 * B would then complete loading, then A, then file.wsdl
 * By the time file A starts processing its includes its definitions will be already loaded,
 * this is the only thing that B will depend on when "opening" A
 */
/**
 * @param {?} uri
 * @param {?} options
 * @return {?}
 */
function open_wsdl_recursive(uri, options) {
    /** @type {?} */
    var fromCache;
    /** @type {?} */
    var WSDL_CACHE;
    // if (typeof options === 'function') {
    //   callback = options;
    //   options = {};
    // }
    WSDL_CACHE = options.WSDL_CACHE;
    if (fromCache = WSDL_CACHE[uri]) {
        // return callback.call(fromCache, null, fromCache);
        return fromCache;
    }
    return open_wsdl(uri, options);
}
/**
 * @param {?} uri
 * @param {?} options
 * @return {?}
 */
export function open_wsdl(uri, options) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var WSDL_CACHE, request_headers, request_options, httpClient, wsdlDef, wsdlObj;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    // if (typeof options === 'function') {
                    //   callback = options;
                    //   options = {};
                    // }
                    // initialize cache when calling open_wsdl directly
                    WSDL_CACHE = options.WSDL_CACHE || {};
                    request_headers = options.wsdl_headers;
                    request_options = options.wsdl_options;
                    // let wsdl;
                    // if (!/^https?:/.test(uri)) {
                    //   // debug('Reading file: %s', uri);
                    //   // fs.readFile(uri, 'utf8', function(err, definition) {
                    //   //   if (err) {
                    //   //     callback(err);
                    //   //   }
                    //   //   else {
                    //   //     wsdl = new WSDL(definition, uri, options);
                    //   //     WSDL_CACHE[ uri ] = wsdl;
                    //   //     wsdl.WSDL_CACHE = WSDL_CACHE;
                    //   //     wsdl.onReady(callback);
                    //   //   }
                    //   // });
                    // }
                    // else {
                    //   debug('Reading url: %s', uri);
                    //   let httpClient = options.httpClient || new HttpClient(options);
                    //   httpClient.request(uri, null /* options */, function(err, response, definition) {
                    //     if (err) {
                    //       callback(err);
                    //     } else if (response && response.statusCode === 200) {
                    //       wsdl = new WSDL(definition, uri, options);
                    //       WSDL_CACHE[ uri ] = wsdl;
                    //       wsdl.WSDL_CACHE = WSDL_CACHE;
                    //       wsdl.onReady(callback);
                    //     } else {
                    //       callback(new Error('Invalid WSDL URL: ' + uri + "\n\n\r Code: " + response.statusCode + "\n\n\r Response Body: " + response.body));
                    //     }
                    //   }, request_headers, request_options);
                    // }
                    // return wsdl;
                    // console.log('Reading url: %s', uri);
                    httpClient = options.httpClient;
                    return [4 /*yield*/, httpClient.get(uri, { responseType: 'text' }).toPromise()];
                case 1:
                    wsdlDef = _a.sent();
                    return [4 /*yield*/, new Promise(function (resolve) {
                            /** @type {?} */
                            var wsdl = new WSDL(wsdlDef, uri, options);
                            WSDL_CACHE[uri] = wsdl;
                            wsdl.WSDL_CACHE = WSDL_CACHE;
                            wsdl.onReady(resolve(wsdl));
                        })];
                case 2:
                    wsdlObj = _a.sent();
                    // console.log("wsdl", wsdlObj);
                    return [2 /*return*/, wsdlObj];
            }
        });
    });
}
export { ɵ0, ɵ1 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid3NkbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1zb2FwLyIsInNvdXJjZXMiOlsibGliL3NvYXAvd3NkbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQU9BLFlBQVksQ0FBQztBQUViLE9BQU8sS0FBSyxHQUFHLE1BQU0sS0FBSyxDQUFDO0FBRTNCLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFPLGFBQWEsQ0FBQztBQUVoRCxPQUFPLEtBQUssR0FBRyxNQUFNLEtBQUssQ0FBQztBQUMzQixPQUFPLEVBQUUsRUFBRSxJQUFJLE1BQU0sRUFBRSxNQUFNLFFBQVEsQ0FBQzs7O0lBR2hDLFFBQVEsR0FBRyxVQUFDLENBQVM7SUFDekIsMERBQTBEO0lBQzFELGdEQUFnRDtJQUNoRCxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssTUFBTSxFQUFFO1FBQzlCLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUNuQjtJQUVELE9BQU8sQ0FBQyxDQUFDO0FBQ1gsQ0FBQzs7QUFFRCxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEtBQUssS0FBSyxNQUFNLFNBQVMsQ0FBQzs7SUFHN0IsVUFBVSxHQUFHLEtBQUssQ0FBQyxVQUFVOztJQUM3QixVQUFVLEdBQUcsS0FBSyxDQUFDLFVBQVU7O0lBRTdCLFVBQVUsR0FBRztJQUNmLE1BQU0sRUFBRSxDQUFDO0lBQ1QsT0FBTyxFQUFFLENBQUM7SUFDVixPQUFPLEVBQUUsQ0FBQztJQUNWLEtBQUssRUFBRSxDQUFDO0lBQ1IsTUFBTSxFQUFFLENBQUM7SUFDVCxPQUFPLEVBQUUsQ0FBQztJQUNWLElBQUksRUFBRSxDQUFDO0lBQ1AsR0FBRyxFQUFFLENBQUM7SUFDTixJQUFJLEVBQUUsQ0FBQztJQUNQLEtBQUssRUFBRSxDQUFDO0lBQ1IsZUFBZSxFQUFFLENBQUM7SUFDbEIsa0JBQWtCLEVBQUUsQ0FBQztJQUNyQixlQUFlLEVBQUUsQ0FBQztJQUNsQixrQkFBa0IsRUFBRSxDQUFDO0lBQ3JCLFlBQVksRUFBRSxDQUFDO0lBQ2YsV0FBVyxFQUFFLENBQUM7SUFDZCxZQUFZLEVBQUUsQ0FBQztJQUNmLGFBQWEsRUFBRSxDQUFDO0lBQ2hCLFFBQVEsRUFBRSxDQUFDO0lBQ1gsUUFBUSxFQUFFLENBQUM7SUFDWCxJQUFJLEVBQUUsQ0FBQztJQUNQLElBQUksRUFBRSxDQUFDO0lBQ1AsVUFBVSxFQUFFLENBQUM7SUFDYixLQUFLLEVBQUUsQ0FBQztJQUNSLFNBQVMsRUFBRSxDQUFDO0lBQ1osSUFBSSxFQUFFLENBQUM7SUFDUCxNQUFNLEVBQUUsQ0FBQztJQUNULFNBQVMsRUFBRSxDQUFDO0lBQ1osWUFBWSxFQUFFLENBQUM7SUFDZixNQUFNLEVBQUUsQ0FBQztJQUNULEtBQUssRUFBRSxDQUFDO0lBQ1IsUUFBUSxFQUFFLENBQUM7Q0FDWjs7Ozs7QUFFRCxTQUFTLFVBQVUsQ0FBQyxNQUFNOztRQUNwQixDQUFDLEdBQUcsT0FBTyxNQUFNLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDN0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDbkQsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUM7QUFDdEUsQ0FBQzs7Ozs7QUFFRCxTQUFTLFNBQVMsQ0FBQyxHQUFHO0lBQ3BCLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLFFBQVEsRUFBRTtRQUM3QixJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLFdBQVcsSUFBSSxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxFQUFFO1lBQ2hFLE9BQU8sR0FBRyxDQUFDO1NBQ1o7UUFDRCxPQUFPLEdBQUc7YUFDUCxPQUFPLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQzthQUN0QixPQUFPLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQzthQUNyQixPQUFPLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQzthQUNyQixPQUFPLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQzthQUN2QixPQUFPLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0tBQzVCO0lBRUQsT0FBTyxHQUFHLENBQUM7QUFDYixDQUFDOztJQUVHLFFBQVEsR0FBRyxZQUFZOztJQUN2QixTQUFTLEdBQUcsWUFBWTs7Ozs7QUFFNUIsU0FBUyxJQUFJLENBQUMsSUFBSTtJQUNoQixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLENBQUM7QUFDM0QsQ0FBQzs7Ozs7O0FBRUQsU0FBUyxTQUFTLENBQUMsV0FBVyxFQUFFLE1BQU07SUFDcEMsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUFDLFdBQVcsSUFBSSxFQUFFLEVBQUUsTUFBTSxFQUFFLFVBQVUsQ0FBQyxFQUFFLENBQUM7UUFDMUQsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7SUFDaEQsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDOztJQUVHLE9BQU8sR0FBUSxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsT0FBTzs7UUFDN0MsS0FBSyxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUM7SUFFOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDckIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQzNCLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztJQUN2QixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNuQixJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztJQUVoQixJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLENBQUM7SUFFakMsS0FBSyxJQUFJLEdBQUcsSUFBSSxLQUFLLEVBQUU7O1lBQ2pCLEtBQUssR0FBRyxlQUFlLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUNyQyxJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUMzRDthQUNJO1lBQ0gsSUFBSSxHQUFHLEtBQUssT0FBTyxFQUFFO2dCQUNuQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNsQztpQkFBTTtnQkFDTCxJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUM5QjtTQUNGO0tBQ0Y7SUFDRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxTQUFTLEVBQUU7UUFDdkMscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO0tBQ2hEO0FBQ0gsQ0FBQzs7QUFFRCxPQUFPLENBQUMsU0FBUyxDQUFDLGtCQUFrQixHQUFHLFVBQVUsT0FBTztJQUN0RCxJQUFJLE9BQU8sRUFBRTtRQUNYLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLFFBQVEsSUFBSSxRQUFRLENBQUM7UUFDN0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQztRQUN2QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixJQUFJLEVBQUUsQ0FBQztLQUMxRDtTQUFNO1FBQ0wsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDekIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztLQUM3QjtBQUNILENBQUMsQ0FBQztBQUVGLE9BQU8sQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLEdBQUc7SUFDbkMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3BFLElBQUksQ0FBQyxLQUFLLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDeEUsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ25CLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNuQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7QUFDbkIsQ0FBQyxDQUFDO0FBRUYsT0FBTyxDQUFDLFNBQVMsQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFDO0FBRXZDLE9BQU8sQ0FBQyxTQUFTLENBQUMsWUFBWSxHQUFHLFVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsT0FBTztJQUN0RSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRTtRQUN6QixPQUFPO0tBQ1I7O1FBRUcsVUFBVSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQzs7UUFDNUQsT0FBTyxHQUFHLElBQUk7SUFFaEIsSUFBSSxVQUFVLEVBQUU7UUFDZCxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksVUFBVSxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztLQUNwRDtTQUNJO1FBQ0gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztLQUN6QjtBQUVILENBQUMsQ0FBQztBQUVGLE9BQU8sQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFVBQVUsS0FBSyxFQUFFLE1BQU07SUFDcEQsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLE1BQU0sRUFBRTtRQUMxQixJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQztZQUNsQixPQUFPOztZQUNMLFFBQU0sR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDcEMsSUFBSSxJQUFJLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3JCLENBQUMsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDM0MscUJBQXFCO1lBQ3JCLFFBQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNCLFFBQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkI7UUFDRCxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7S0FDYjtBQUNILENBQUMsQ0FBQztBQUVGLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsS0FBSztJQUMxQyxPQUFPO0FBQ1QsQ0FBQyxDQUFDO0FBRUYsT0FBTyxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsVUFBVSxJQUFJO0lBQzNDLE1BQU0sSUFBSSxLQUFLLENBQUMsNEJBQTRCLEdBQUcsSUFBSSxHQUFHLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDbkYsQ0FBQyxDQUFDO0FBRUYsT0FBTyxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXO0lBQ25ELE9BQU8sSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDO0FBQ2pDLENBQUMsQ0FBQztBQUVGLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHO0FBQ3pCLENBQUMsQ0FBQztBQUVGLE9BQU8sQ0FBQyxjQUFjLEdBQUc7O1FBQ25CLElBQUksR0FBRyxJQUFJOztRQUNYLFVBQVUsR0FBRztRQUNmLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQzVCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFDRCw4QkFBOEI7SUFDOUIsVUFBVSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUNoRCxPQUFPLFVBQVUsQ0FBQztBQUNwQixDQUFDLENBQUM7O0lBR0UsY0FBYyxHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUU7O0lBQ3pDLFVBQVUsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUNyQyxZQUFZLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFDdkMsYUFBYSxHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUU7O0lBQ3hDLGlCQUFpQixHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUU7O0lBQzVDLGtCQUFrQixHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUU7O0lBQzdDLGdCQUFnQixHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUU7O0lBQzNDLGFBQWEsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUN4QyxrQkFBa0IsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUM3QyxrQkFBa0IsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUM3QyxxQkFBcUIsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUNoRCxvQkFBb0IsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUMvQyxlQUFlLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFDMUMsVUFBVSxHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUU7O0lBQ3JDLGNBQWMsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUN6QyxvQkFBb0IsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUUvQyxhQUFhLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFDeEMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUU7O0lBQ3ZDLGdCQUFnQixHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUU7O0lBQzNDLGVBQWUsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUMxQyxjQUFjLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRTs7SUFDekMsV0FBVyxHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUU7O0lBQ3RDLGNBQWMsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUN6QyxrQkFBa0IsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFOztJQUU3QyxjQUFjLEdBQUc7SUFDbkIsS0FBSyxFQUFFLENBQUMsWUFBWSxFQUFFLHNCQUFzQixDQUFDO0lBQzdDLE1BQU0sRUFBRSxDQUFDLGFBQWEsRUFBRSwrQ0FBK0MsQ0FBQztJQUN4RSxPQUFPLEVBQUUsQ0FBQyxjQUFjLEVBQUUsd0JBQXdCLENBQUM7SUFDbkQsR0FBRyxFQUFFLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQztJQUNyQixVQUFVLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxhQUFhLENBQUM7SUFDOUMsV0FBVyxFQUFFLENBQUMsa0JBQWtCLEVBQUUsaUNBQWlDLENBQUM7SUFDcEUsU0FBUyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUscUJBQXFCLENBQUM7SUFDcEQsTUFBTSxFQUFFLENBQUMsYUFBYSxFQUFFLDZCQUE2QixDQUFDOztJQUV0RCxXQUFXLEVBQUUsQ0FBQyxrQkFBa0IsRUFBRSxFQUFFLENBQUM7SUFDckMsV0FBVyxFQUFFLENBQUMsa0JBQWtCLEVBQUUsNkRBQTZELENBQUM7SUFDaEcsY0FBYyxFQUFFLENBQUMscUJBQXFCLEVBQUUsV0FBVyxDQUFDO0lBQ3BELGFBQWEsRUFBRSxDQUFDLG9CQUFvQixFQUFFLFdBQVcsQ0FBQztJQUNsRCxRQUFRLEVBQUUsQ0FBQyxlQUFlLEVBQUUsNkJBQTZCLENBQUM7SUFDMUQsR0FBRyxFQUFFLENBQUMsVUFBVSxFQUFFLGdCQUFnQixDQUFDO0lBRW5DLE9BQU8sRUFBRSxDQUFDLGNBQWMsRUFBRSxvQkFBb0IsQ0FBQztJQUMvQyxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsdUJBQXVCLENBQUM7SUFDNUMsT0FBTyxFQUFFLENBQUMsY0FBYyxFQUFFLCtDQUErQyxDQUFDO0lBQzFFLFFBQVEsRUFBRSxDQUFDLGVBQWUsRUFBRSx5QkFBeUIsQ0FBQztJQUN0RCxPQUFPLEVBQUUsQ0FBQyxjQUFjLEVBQUUsb0JBQW9CLENBQUM7SUFDL0MsU0FBUyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsNkNBQTZDLENBQUM7SUFDNUUsS0FBSyxFQUFFLENBQUMsWUFBWSxFQUFFLDJDQUEyQyxDQUFDO0lBQ2xFLE1BQU0sRUFBRSxDQUFDLGFBQWEsRUFBRSwyQ0FBMkMsQ0FBQztJQUNwRSxLQUFLLEVBQUUsQ0FBQyxPQUFPLEVBQUUsc0JBQXNCLENBQUM7SUFDeEMsV0FBVyxFQUFFLENBQUMsa0JBQWtCLEVBQUUsNkRBQTZELENBQUM7SUFDaEcsYUFBYSxFQUFFLENBQUMsb0JBQW9CLEVBQUUsRUFBRSxDQUFDO0NBQzFDOzs7OztBQUVELFNBQVMsZUFBZSxDQUFDLEtBQUs7O1FBQ3hCLEdBQUcsR0FBRyxFQUFFO0lBQ1osS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDekIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUk7UUFDMUIsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZFLENBQUMsQ0FBQyxDQUFDO0lBQ0gsT0FBTyxHQUFHLENBQUM7QUFDYixDQUFDO0FBRUQsS0FBSyxJQUFJLENBQUMsSUFBSSxjQUFjLEVBQUU7O1FBQ3hCLENBQUMsR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDO0lBQ3pCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztDQUN4RDtBQUVELGNBQWMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHO0lBQzlCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0lBQ3BCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0FBQ3BCLENBQUMsQ0FBQztBQUVGLGFBQWEsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHO0lBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO0lBQ3ZCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO0lBQ2hCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ25CLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0FBQ3JCLENBQUMsQ0FBQztBQUVGLFlBQVksQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHO0lBQzVCLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO0FBQ3BCLENBQUMsQ0FBQztBQUVGLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUc7SUFDaEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7SUFDbEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDbkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7SUFDdEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7SUFDdkIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7SUFDaEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7QUFDdkIsQ0FBQyxDQUFDO0FBRUYsZUFBZSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUc7SUFDL0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7QUFDcEIsQ0FBQyxDQUFDO0FBRUYsY0FBYyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUc7SUFDOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDcEIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7SUFDaEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7QUFDcEIsQ0FBQyxDQUFDO0FBRUYsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUc7SUFDM0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7QUFDdkIsQ0FBQyxDQUFDO0FBRUYsY0FBYyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUc7SUFDOUIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7QUFDbEIsQ0FBQyxDQUFDO0FBRUYsa0JBQWtCLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRztJQUNsQyxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssYUFBYTtRQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzlELElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ25CLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3BCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ25CLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ25CLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO0FBQ3BCLENBQUMsQ0FBQztBQUVGLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUc7QUFDdEMsQ0FBQyxDQUFDO0FBRUYsYUFBYSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsVUFBVSxNQUFNO0lBQzlDLE1BQU0sQ0FBQyxNQUFNLFlBQVksYUFBYSxDQUFDLENBQUM7SUFDeEMsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEtBQUssTUFBTSxDQUFDLGdCQUFnQixFQUFFO1FBQ3JELENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDaEQsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNsQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3hDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDbkM7SUFDRCxPQUFPLElBQUksQ0FBQztBQUNkLENBQUMsQ0FBQztBQUdGLGFBQWEsQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsS0FBSztJQUNoRCxJQUFJLEtBQUssQ0FBQyxLQUFLLElBQUksVUFBVTtRQUMzQixPQUFPO0lBQ1QsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLFNBQVMsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLFFBQVEsRUFBRTs7WUFDbkQsVUFBUSxHQUFHLEtBQUssQ0FBQyxlQUFlLElBQUksS0FBSyxDQUFDLFNBQVM7UUFDdkQsSUFBSSxVQUFRLEVBQUU7WUFDWixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFDakIsU0FBUyxFQUFFLEtBQUssQ0FBQyxVQUFVLElBQUksS0FBSyxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0I7Z0JBQzlFLFFBQVEsRUFBRSxVQUFRO2FBQ25CLENBQUMsQ0FBQztTQUNKO0tBQ0Y7U0FDSSxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssYUFBYSxFQUFFO1FBQ3JDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQztLQUN4QztTQUNJLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxTQUFTLEVBQUU7UUFDakMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsS0FBSyxDQUFDO0tBQ3BDO1NBQ0ksSUFBSSxLQUFLLENBQUMsS0FBSyxFQUFFO1FBQ3BCLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQztLQUNqQztJQUNELElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDcEIsNEJBQTRCO0FBQzlCLENBQUMsQ0FBQzs7QUFFRixZQUFZLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxVQUFVLEtBQUs7SUFDL0MsTUFBTSxDQUFDLEtBQUssWUFBWSxhQUFhLENBQUMsQ0FBQzs7UUFFbkMsZUFBZSxHQUFHLEtBQUssQ0FBQyxnQkFBZ0I7SUFFNUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxFQUFFO1FBQ2pELElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsS0FBSyxDQUFDO0tBQ3ZDO1NBQU07UUFDTCxPQUFPLENBQUMsS0FBSyxDQUFDLG9CQUFvQixHQUFHLGVBQWUsR0FBRyxxQ0FBcUMsQ0FBQyxDQUFDO0tBQy9GO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsWUFBWSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsVUFBVSxLQUFLO0lBQy9DLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxNQUFNLEVBQUU7UUFDekIsSUFBSSxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1FBQ3RCLElBQUksSUFBSSxDQUFDLEdBQUcsS0FBSyxTQUFTLEVBQUU7WUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUMsY0FBYyxDQUFDO1NBQzNDO1FBQ0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztLQUNyQjtBQUNILENBQUMsQ0FBQztBQUVGLGFBQWEsQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsS0FBSztJQUNoRCxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssTUFBTSxFQUFFO1FBQ3pCLElBQUksQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztRQUN0QixJQUFJLElBQUksQ0FBQyxHQUFHLEtBQUssU0FBUyxFQUFFO1lBQzFCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDLGNBQWMsQ0FBQztTQUMzQztRQUNELElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7S0FDckI7QUFDSCxDQUFDLENBQUM7QUFFRixnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsS0FBSztJQUNuRCxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssV0FBVyxFQUFFO1FBQzlCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQUM7UUFDMUMsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxJQUFJLEVBQUUsQ0FBQztRQUNoQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDO0tBQ3JCO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsY0FBYyxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsVUFBVSxLQUFLO0lBQ2pELElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxTQUFTLEVBQUU7UUFDNUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUMxQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDO0tBQ3JCO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsV0FBVyxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsVUFBVSxLQUFLO0lBQzlDLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxTQUFTLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxXQUFXLEVBQUU7UUFDeEUsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDO0tBQ2pDO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsa0JBQWtCLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxVQUFVLEtBQUs7O1FBQ2pELElBQUksR0FBRyxJQUFJO0lBQ2YsSUFBSSxLQUFLLFlBQVksWUFBWSxFQUFFO1FBQ2pDLCtDQUErQztRQUMvQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0tBQ3RDO1NBQ0ksSUFBSSxLQUFLLFlBQVksY0FBYyxFQUFFO1FBQ3hDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQztLQUNwQztTQUNJLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxRQUFRLEVBQUU7UUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxhQUFhLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDaEQ7U0FDSSxJQUFJLEtBQUssWUFBWSxlQUFlLEVBQUU7UUFDekMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsS0FBSyxDQUFDO0tBQ3JDO1NBQ0ksSUFBSSxLQUFLLFlBQVksY0FBYyxFQUFFO1FBQ3hDLElBQUksS0FBSyxDQUFDLFNBQVMsS0FBSyxzQ0FBc0M7WUFDNUQsS0FBSyxDQUFDLFNBQVMsS0FBSywrQ0FBK0M7WUFDbkUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsS0FBSyxDQUFDO0tBQ3RDO1NBQ0ksSUFBSSxLQUFLLFlBQVksY0FBYyxFQUFFO1FBQ3hDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQztLQUNwQztTQUNJLElBQUksS0FBSyxZQUFZLG9CQUFvQixFQUFFO0tBQy9DO0lBQ0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztBQUN0QixDQUFDLENBQUM7QUFFRixjQUFjLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxVQUFVLFdBQVc7O1FBQ3RELElBQUksR0FBRyxJQUFJOztRQUNYLEtBQUssR0FBRyxTQUFTOztRQUNqQixRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxFQUFFOztRQUM5QixFQUFFLEdBQUcsU0FBUzs7UUFDZCxNQUFNLEdBQUcsU0FBUzs7UUFDbEIsQ0FBQyxHQUFHLFNBQVM7O1FBQ2IsSUFBSSxHQUFHLFNBQVM7SUFFcEIsS0FBSyxDQUFDLElBQUksUUFBUSxFQUFFO1FBQ2xCLElBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLE1BQU0sRUFBRTtZQUN6QyxJQUFJLEdBQUcsS0FBSyxDQUFDO1lBQ2IsTUFBTTtTQUNQO0tBQ0Y7SUFFRCxJQUFJLENBQUMsSUFBSSxFQUFFO1FBQ1QsT0FBTztLQUNSO0lBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFOztZQUNiLFdBQVcsR0FBRyxFQUFFOztZQUNsQixlQUFlLFNBQUE7UUFFakIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBRWxCLE1BQU0sR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ25DLEVBQUUsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDOztZQUNmLE1BQU0sR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNqQixxRkFBcUY7WUFDckYsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7UUFFckQseUVBQXlFO1FBQ3pFLHNDQUFzQztRQUN0QyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBRXpDLGVBQWUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztRQUV4QyxnRUFBZ0U7UUFDaEUsSUFBSSxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUM5QixLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGVBQWUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzNDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDdkU7U0FDRjtRQUVELG9FQUFvRTtRQUNwRSxJQUFJLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzFCLFdBQVcsR0FBRyxXQUFXO2dCQUN2QixJQUFJLENBQUMsR0FBRyxDQUFDO2dCQUNULEtBQUssQ0FBQyxHQUFHLENBQUM7Z0JBQ1YsTUFBTSxDQUFDLFNBQVMsc0JBQXNCLENBQUMsSUFBSTtnQkFDekMsT0FBTyxJQUFJLEtBQUssR0FBRyxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDOztnQkFFRCxXQUFXLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEtBQUs7WUFFekUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUN2QyxXQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxXQUFXLENBQUMsQ0FBQzthQUM1RTtTQUNGO1FBRUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEdBQUcsV0FBVyxDQUFDO1FBRXhDLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUU7WUFDdEIsSUFBSSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDOztnQkFDbEMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxLQUFLLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBRXhGLElBQUksTUFBTSxFQUFFO2dCQUNWLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxVQUFVLEVBQUU7b0JBQzNCLHFDQUFxQztpQkFDdEM7cUJBQ0k7b0JBQ0gscURBQXFEO29CQUNyRCxNQUFNLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQzs7d0JBQ2pDLEtBQUssR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBR25HLElBQUksS0FBSyxFQUFFO3dCQUNULElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUMzRDtpQkFDRjthQUNGO1NBQ0Y7YUFDSTs7Z0JBQ0MsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDO1lBQ2hFLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNsQztRQUdELElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztLQUM1QjtTQUFNO1FBQ0wsZUFBZTtRQUNmLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUNwQixLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDeEMsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLGVBQWUsRUFBRTtnQkFDakMsMERBQTBEO2dCQUMxRCxTQUFTO2FBQ1Y7WUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxNQUFNLEVBQUUsdUJBQXVCLENBQUMsQ0FBQztZQUN0RCxNQUFNLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoQyxFQUFFLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDdEMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7O2dCQUNmLGdCQUFnQixHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDO1lBQzlDLElBQUksT0FBTyxnQkFBZ0IsS0FBSyxXQUFXLEVBQUU7Z0JBQzNDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzVHO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7YUFDckM7WUFFRCxJQUFJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssUUFBUSxFQUFFO2dCQUM5QyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztnQkFDOUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQzthQUNuQztZQUVELElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQzlCO0tBQ0Y7SUFDRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztBQUMxQixDQUFDLENBQUM7Ozs7Ozs7Ozs7OztBQWFGLGNBQWMsQ0FBQyxTQUFTLENBQUMsdUJBQXVCLEdBQUcsVUFBVSxRQUFRLEVBQUUsS0FBSzs7UUFDdEUsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQzs7UUFDekMsT0FBTyxHQUFHLGdCQUFnQixDQUFDLE1BQU07O1FBQ2pDLFlBQVksR0FBRyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzs7UUFDL0MsSUFBSSxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUM7O1FBQ3RCLElBQUksR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDOztRQUN0QixhQUFhLEdBQVEsRUFBRTtJQUV6QixhQUFhLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMxQyxhQUFhLENBQUMsS0FBSyxHQUFHLE9BQU8sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDO0lBQzNDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0lBRTNCLE9BQU8sYUFBYSxDQUFDO0FBQ3ZCLENBQUMsQ0FBQzs7Ozs7Ozs7Ozs7QUFZRixjQUFjLENBQUMsU0FBUyxDQUFDLDBCQUEwQixHQUFHLFVBQVUsT0FBTzs7UUFDakUsWUFBWSxHQUFHLEdBQUc7O1FBQ3BCLFFBQVEsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztJQUVoRCxJQUFJLE9BQU8sQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksT0FBTyxPQUFPLENBQUMsS0FBSyxLQUFLLFFBQVEsRUFBRTtRQUN4RSxJQUFJLFFBQVEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtZQUN4RCxZQUFZLElBQUksQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLEtBQUssR0FBRyxHQUFHLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzdEO0tBQ0Y7SUFFRCxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs7WUFDM0IsTUFBSSxHQUFHLElBQUk7UUFFZixPQUFPLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEtBQUs7O2dCQUNsQyxpQkFBaUIsR0FBRyxNQUFJLENBQUMsMEJBQTBCLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUM7WUFFakYsSUFBSSxpQkFBaUIsSUFBSSxPQUFPLGlCQUFpQixLQUFLLFFBQVEsRUFBRTtnQkFDOUQsWUFBWSxJQUFJLENBQUMsR0FBRyxHQUFHLGlCQUFpQixDQUFDLENBQUM7YUFDM0M7UUFDSCxDQUFDLENBQUMsQ0FBQztLQUNKO0lBRUQsT0FBTyxZQUFZLENBQUM7QUFDdEIsQ0FBQyxDQUFDO0FBRUYsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxVQUFVLFdBQVcsRUFBRSxHQUFHOztRQUM3RCxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVE7SUFDNUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxTQUFBLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUMvQyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssT0FBTyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssUUFBUTtZQUNuRCxTQUFTO1FBQ1gsSUFBSSxHQUFHLEtBQUssU0FBUyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQ3pCLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDeEIsU0FBUztTQUNWOztZQUNHLFdBQVcsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUk7O1lBQzdDLE9BQU8sR0FBRyxXQUFXLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztRQUMvQyxPQUFPLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2pDLElBQUksT0FBTyxDQUFDLE9BQU8sRUFBRTtZQUNuQixXQUFXLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsT0FBTyxDQUFDO1lBQ3RELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQztTQUNwQzthQUNJO1lBQ0gsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUM7U0FDNUI7UUFDRCxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO0tBQ3pCO0lBQ0QsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7QUFDMUIsQ0FBQyxDQUFDO0FBRUYsZUFBZSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXOztRQUN2RCxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVE7SUFDNUIsSUFBSSxPQUFPLFFBQVEsS0FBSyxXQUFXO1FBQ2pDLE9BQU87SUFDVCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLFNBQUEsRUFBRSxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1FBQy9DLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxXQUFXO1lBQzVCLFNBQVM7UUFDWCxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDbEMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztLQUN6QjtJQUNELE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNsQixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztBQUMxQixDQUFDLENBQUM7QUFFRixjQUFjLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxVQUFVLFdBQVc7O1FBQ3RELElBQUksR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUk7O1FBQ3BDLFFBQVEsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQzs7UUFDdEMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLOztRQUNsQixRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVE7SUFDMUIsSUFBSSxRQUFRLEVBQUU7UUFDWixRQUFRLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQztRQUVoQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLFNBQUEsRUFBRSxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQy9DLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxXQUFXO2dCQUM1QixTQUFTO1lBQ1gsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDMUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUN4QixLQUFLLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQzs7Z0JBQ2pDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFFdEMsSUFBSSxNQUFNLEVBQUU7Z0JBQ1YsTUFBTSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO2dCQUMzQixNQUFNLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUM7Z0JBQ3JDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUM7Z0JBQ3ZDLE1BQU0sQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUM7Z0JBQ3pDLE1BQU0sQ0FBQyxTQUFTLElBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2dCQUN4RCxNQUFNLENBQUMsVUFBVSxJQUFJLE1BQU0sQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzthQUMzRDtTQUNGO0tBQ0Y7SUFDRCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDbEIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ2xCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0FBQzFCLENBQUMsQ0FBQztBQUVGLGNBQWMsQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsV0FBVzs7UUFDdEQsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFROztRQUMxQixRQUFRLEdBQUcsV0FBVyxDQUFDLFFBQVE7SUFDakMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7UUFDbkMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxTQUFBLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMvQyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssTUFBTTtnQkFDdkIsU0FBUzs7Z0JBQ1AsV0FBVyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSTs7Z0JBQzdDLE9BQU8sR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDO1lBQ25DLElBQUksT0FBTyxFQUFFO2dCQUNYLE9BQU8sQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2pDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHO29CQUN4QixRQUFRLEVBQUUsS0FBSyxDQUFDLFFBQVE7b0JBQ3hCLE9BQU8sRUFBRSxPQUFPO2lCQUNqQixDQUFDO2dCQUNGLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDekI7U0FDRjtLQUNGO0lBQ0QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ2xCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0FBQzFCLENBQUMsQ0FBQztBQUdGLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXOztRQUN6RCxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVE7SUFDNUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxTQUFBLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUMvQyxJQUFJLEtBQUssWUFBWSxrQkFBa0I7WUFDckMsT0FBTyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7S0FDakQ7SUFDRCxPQUFPLEVBQUUsQ0FBQztBQUNaLENBQUMsQ0FBQztBQUVGLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXLEVBQUUsS0FBSzs7UUFDakUsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFROztRQUN4QixJQUFJO0lBQ1IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxTQUFBLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUMvQyxJQUFJLEtBQUssWUFBWSxlQUFlO1lBQ2xDLEtBQUssWUFBWSxhQUFhLEVBQUU7WUFDaEMsSUFBSSxHQUFHLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzdDLE1BQU07U0FDUDtLQUNGO0lBQ0QsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTs7WUFDbEIsSUFBSSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDOztZQUMvQixRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUk7O1lBQ3BCLEVBQUUsR0FBRyxLQUFLLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7O1lBQ2xFLFFBQU0sR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQzs7WUFDaEMsYUFBVyxHQUFHLFFBQU0sSUFBSSxDQUFDLFFBQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLElBQUksUUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxRQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRWhILElBQUksQ0FBQyxPQUFPLEdBQUc7WUFDYixPQUFPLGFBQVcsQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLFFBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM1RCxDQUFDLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQztLQUNiOzs7UUFHRyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUU7SUFDN0MsT0FBTyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBVSxLQUFLO1FBQzdDLE9BQU8sS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQzdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNmLENBQUMsQ0FBQztBQUVGLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXLEVBQUUsS0FBSzs7UUFDL0QsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFROztRQUN4QixJQUFJLEdBQUcsRUFBRTtJQUNiLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssU0FBQSxFQUFFLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7UUFDL0MsSUFBSSxLQUFLLFlBQVksZUFBZTtZQUNsQyxLQUFLLFlBQVksYUFBYSxFQUFFO1lBQ2hDLElBQUksR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUM5QztLQUNGO0lBQ0QsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFOztZQUNWLElBQUksR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQzs7WUFDL0IsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJOztZQUNwQixFQUFFLEdBQUcsS0FBSyxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDOztZQUNsRSxNQUFNLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7UUFFbEMsSUFBSSxRQUFRLElBQUksVUFBVSxFQUFFO1lBQzFCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztTQUNuQjthQUNJOztnQkFDQyxXQUFXLEdBQUcsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUM7Z0JBQ3hELE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUV0RCxJQUFJLFdBQVcsRUFBRTs7b0JBQ1gsSUFBSSxHQUFHLFdBQVcsQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUM7Z0JBQzdELElBQUksR0FBRyxDQUFDLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQzthQUNuQztTQUNGO0tBQ0Y7SUFDRCxPQUFPLElBQUksQ0FBQztBQUNkLENBQUMsQ0FBQztBQUVGLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUc7SUFDekMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQzdCLENBQUMsQ0FBQztBQUVGLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXLEVBQUUsS0FBSzs7UUFDakUsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLElBQUksRUFBRTtJQUNsQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLFNBQUEsRUFBRSxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1FBQy9DLElBQUksS0FBSyxZQUFZLGFBQWE7WUFDaEMsS0FBSyxZQUFZLGVBQWU7WUFDaEMsS0FBSyxZQUFZLFVBQVU7WUFDM0IsS0FBSyxZQUFZLG9CQUFvQjtZQUNyQyxLQUFLLFlBQVkscUJBQXFCLEVBQUU7WUFFeEMsT0FBTyxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUM5QztLQUNGO0lBQ0QsT0FBTyxFQUFFLENBQUM7QUFDWixDQUFDLENBQUM7QUFFRixxQkFBcUIsQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsV0FBVyxFQUFFLEtBQUs7O1FBQ3BFLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUTtJQUM1QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLFNBQUEsRUFBRSxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1FBQy9DLElBQUksS0FBSyxZQUFZLGdCQUFnQixFQUFFO1lBQ3JDLE9BQU8sS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDOUM7S0FDRjtJQUNELE9BQU8sRUFBRSxDQUFDO0FBQ1osQ0FBQyxDQUFDO0FBRUYsb0JBQW9CLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxVQUFVLFdBQVcsRUFBRSxLQUFLOztRQUNuRSxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVE7SUFDNUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxTQUFBLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUMvQyxJQUFJLEtBQUssWUFBWSxnQkFBZ0IsRUFBRTtZQUNyQyxPQUFPLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQzlDO0tBQ0Y7SUFDRCxPQUFPLEVBQUUsQ0FBQztBQUNaLENBQUMsQ0FBQztBQUVGLGNBQWMsQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsV0FBVyxFQUFFLEtBQUs7O1FBQzdELE9BQU8sR0FBRyxFQUFFOztRQUNkLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSzs7UUFDZixNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDNUgsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLElBQUksQ0FBQyxVQUFVLElBQUksTUFBTSxFQUFFO1FBQ2pELElBQUksSUFBSSxJQUFJLENBQUM7S0FDZDtJQUVELElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxVQUFVLENBQUMsRUFBRTtRQUM5QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0tBQzNDOztRQUNHLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxJQUFJO0lBQ2xDLElBQUksSUFBSSxFQUFFO1FBQ1IsSUFBSSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQzs7WUFDcEIsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJOztZQUN0QixFQUFFLEdBQUcsS0FBSyxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDOztZQUNsRSxNQUFNLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7O1lBQ2hDLFdBQVcsR0FBRyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFNUgsSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUNqQyxLQUFLLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUM7U0FDdkM7UUFFRCxJQUFJLFdBQVcsSUFBSSxDQUFDLENBQUMsUUFBUSxJQUFJLFVBQVUsQ0FBQyxFQUFFO1lBRTVDLElBQUksQ0FBQyxDQUFDLFFBQVEsSUFBSSxXQUFXLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFFOztvQkFFN0MsTUFBSSxHQUFRLEVBQUU7Z0JBQ2xCLFdBQVcsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxHQUFHLE1BQUksQ0FBQzs7b0JBQzVDLGFBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUM7Z0JBQzdELElBQUksT0FBTyxhQUFXLEtBQUssUUFBUSxFQUFFO29CQUNuQyxNQUFJLEdBQUcsYUFBVyxDQUFDO2lCQUNwQjtxQkFDSTtvQkFDSCxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUc7d0JBQzVDLE1BQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxhQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQy9CLENBQUMsQ0FBQyxDQUFDO2lCQUNKO2dCQUVELElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDYixPQUFPLEdBQUcsTUFBSSxDQUFDO2lCQUNoQjtxQkFDSTtvQkFDSCxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsTUFBSSxDQUFDO2lCQUN0QjtnQkFFRCxJQUFJLE9BQU8sTUFBSSxLQUFLLFFBQVEsRUFBRTtvQkFDNUIsTUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO29CQUNqQyxNQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQztpQkFDM0I7Z0JBRUQsV0FBVyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBSSxDQUFDO2FBQ2pEO2lCQUNJO2dCQUNILElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDYixPQUFPLEdBQUcsV0FBVyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ3BEO3FCQUNJO29CQUNILE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDMUQ7YUFDRjtTQUVGO2FBQ0k7WUFDSCxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztTQUM1QjtLQUNGO1NBQ0k7O1lBQ0MsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRO1FBQzVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDbkIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxTQUFBLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMvQyxJQUFJLEtBQUssWUFBWSxrQkFBa0IsRUFBRTtnQkFDdkMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO2FBQ3ZEO1NBQ0Y7S0FDRjtJQUNELE9BQU8sT0FBTyxDQUFDO0FBQ2pCLENBQUMsQ0FBQztBQUVGLFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVztJQUM5QixlQUFlLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxVQUFVLFdBQVcsRUFBRSxLQUFLOztZQUM5RCxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVE7O1lBQ3hCLFFBQVEsR0FBRyxFQUFFO1FBQ2pCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssU0FBQSxFQUFFLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDL0MsSUFBSSxLQUFLLFlBQVksVUFBVSxFQUFFO2dCQUMvQixTQUFTO2FBQ1Y7O2dCQUNHLFdBQVcsR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUM7WUFDdkQsS0FBSyxJQUFJLEdBQUcsSUFBSSxXQUFXLEVBQUU7Z0JBQzNCLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDbEM7U0FDRjtRQUNELE9BQU8sUUFBUSxDQUFDO0lBQ2xCLENBQUMsQ0FBQztBQUVKLGFBQWEsQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsV0FBVyxFQUFFLEtBQUs7O1FBQzVELFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUTs7UUFDeEIsTUFBTSxHQUFHLEVBQUU7SUFDZixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLFNBQUEsRUFBRSxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFOztZQUMzQyxXQUFXLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDO1FBQ3ZELEtBQUssSUFBSSxHQUFHLElBQUksV0FBVyxFQUFFO1lBQzNCLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDaEM7S0FDRjtJQUNELE9BQU8sTUFBTSxDQUFDO0FBQ2hCLENBQUMsQ0FBQztBQUVGLGNBQWMsQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsV0FBVztJQUMxRCxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7UUFDaEIsT0FBTyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0tBQzlEOztRQUNHLElBQUksR0FBRyxFQUFFO0lBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQzlCLE9BQU8sSUFBSSxDQUFDO0FBQ2QsQ0FBQyxDQUFDO0FBRUYsZUFBZSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXOztRQUN2RCxPQUFPLEdBQUcsRUFBRTtJQUNoQixLQUFLLElBQUksTUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7O1lBQ3pCLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQUksQ0FBQztRQUMvQixPQUFPLENBQUMsTUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztLQUNqRDtJQUNELE9BQU8sT0FBTyxDQUFDO0FBQ2pCLENBQUMsQ0FBQztBQUVGLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXOztRQUN4RCxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7O1FBQ25FLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtJQUMxRSxPQUFPO1FBQ0wsS0FBSyxFQUFFLFNBQVMsSUFBSSxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4RCxNQUFNLEVBQUUsVUFBVSxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQzdELENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixjQUFjLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxVQUFVLFdBQVc7O1FBQ3RELE9BQU8sR0FBRyxFQUFFO0lBQ2hCLEtBQUssSUFBSSxNQUFJLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTs7WUFDekIsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBSSxDQUFDO1FBQy9CLE9BQU8sQ0FBQyxNQUFJLENBQUMsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0tBQ2pEO0lBQ0QsT0FBTyxPQUFPLENBQUM7QUFDakIsQ0FBQyxDQUFDO0FBRUYsY0FBYyxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsVUFBVSxXQUFXOztRQUN0RCxLQUFLLEdBQUcsRUFBRTtJQUNkLEtBQUssSUFBSSxNQUFJLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTs7WUFDdkIsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBSSxDQUFDO1FBQzNCLEtBQUssQ0FBQyxNQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztLQUNyRDtJQUNELE9BQU8sS0FBSyxDQUFDO0FBQ2YsQ0FBQyxDQUFDOztBQUVGLE1BQU0sS0FBSyxJQUFJLEdBQUcsVUFBVSxVQUFVLEVBQUUsR0FBRyxFQUFFLE9BQU87O1FBQzlDLElBQUksR0FBRyxJQUFJOztRQUNiLFFBQVE7SUFFVixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztJQUNmLElBQUksQ0FBQyxRQUFRLEdBQUc7SUFDaEIsQ0FBQyxDQUFDO0lBQ0YsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7SUFFeEIsd0JBQXdCO0lBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDLENBQUMsVUFBVSxJQUFJLEVBQUUsQ0FBQztJQUVuRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLENBQUM7SUFFakMsSUFBSSxPQUFPLFVBQVUsS0FBSyxRQUFRLEVBQUU7UUFDbEMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNsQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztLQUMxQjtTQUNJLElBQUksT0FBTyxVQUFVLEtBQUssUUFBUSxFQUFFO1FBQ3ZDLFFBQVEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO0tBQy9CO1NBQ0k7UUFDSCxNQUFNLElBQUksS0FBSyxDQUFDLGlFQUFpRSxDQUFDLENBQUM7S0FDcEY7SUFFRCxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQztRQUN6QixJQUFJO1lBQ0YsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7U0FDakM7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNWLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDakM7UUFFRCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsSUFBSSxDQUFDO1lBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzs7Z0JBQ2hDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUTtZQUN4RCxJQUFJLFFBQVEsRUFBRTtnQkFDWixLQUFLLElBQU0sTUFBSSxJQUFJLFFBQVEsRUFBRTtvQkFDM0IsUUFBUSxDQUFDLE1BQUksQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7aUJBQzlDO2FBQ0Y7O2dCQUNHLFlBQVksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVk7WUFDaEQsSUFBSSxZQUFZLEVBQUU7Z0JBQ2hCLEtBQUssSUFBTSxNQUFJLElBQUksWUFBWSxFQUFFO29CQUMvQixZQUFZLENBQUMsTUFBSSxDQUFDLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztpQkFDdkM7YUFDRjs7O2dCQUdHLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVE7WUFDeEMsS0FBSyxJQUFJLFdBQVcsSUFBSSxRQUFRLEVBQUU7O29CQUM1QixPQUFPLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQztnQkFDbkMsSUFBSSxPQUFPLE9BQU8sQ0FBQyxLQUFLLEtBQUssV0FBVyxFQUFFO29CQUN4QyxPQUFPLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQztpQkFDNUI7Z0JBQ0QsSUFBSSxPQUFPLENBQUMsS0FBSyxLQUFLLFVBQVU7b0JBQzlCLFNBQVM7O29CQUNQLE9BQU8sR0FBRyxPQUFPLENBQUMsT0FBTzs7b0JBQ3pCLE1BQU0sR0FBRyxPQUFPLENBQUMsV0FBVyxHQUFHLEVBQUU7Z0JBQ3JDLEtBQUssSUFBSSxVQUFVLElBQUksT0FBTyxFQUFFO29CQUM5QixJQUFJLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLEVBQUU7OzRCQUN6QixTQUFTLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLOzs0QkFDM0MsVUFBVSxHQUFHLEVBQUU7d0JBQ25CLElBQUksT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU07NEJBQzVCLFVBQVUsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQzt3QkFDaEQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLENBQUM7cUJBQzVFO2lCQUNGO2FBQ0Y7WUFFRCxnREFBZ0Q7WUFDaEQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDeEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBbEIsQ0FBa0IsQ0FBQyxDQUFDO0lBRXRDLENBQUMsQ0FBQyxDQUFDO0lBRUgsZ0NBQWdDO0lBQ2hDLFVBQVU7SUFDVix1Q0FBdUM7SUFDdkMsa0JBQWtCO0lBQ2xCLHVDQUF1QztJQUN2QyxNQUFNO0lBRU4seUNBQXlDO0lBQ3pDLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsbUNBQW1DO0lBQ25DLFFBQVE7SUFFUiwyQ0FBMkM7SUFDM0MsZ0VBQWdFO0lBQ2hFLHNCQUFzQjtJQUN0QixpQ0FBaUM7SUFDakMsd0RBQXdEO0lBQ3hELFVBQVU7SUFDVixRQUFRO0lBQ1Isd0RBQXdEO0lBQ3hELDBCQUEwQjtJQUMxQixxQ0FBcUM7SUFDckMsaURBQWlEO0lBQ2pELFVBQVU7SUFDVixRQUFRO0lBRVIsd0lBQXdJO0lBQ3hJLGdEQUFnRDtJQUNoRCwwQ0FBMEM7SUFDMUMsNkNBQTZDO0lBQzdDLG9EQUFvRDtJQUNwRCxzQ0FBc0M7SUFDdEMsVUFBVTtJQUNWLDBDQUEwQztJQUMxQyxvQkFBb0I7SUFDcEIsdUNBQXVDO0lBQ3ZDLCtDQUErQztJQUMvQywwQ0FBMEM7SUFDMUMsMkNBQTJDO0lBQzNDLDZEQUE2RDtJQUM3RCwrQkFBK0I7SUFDL0IsNENBQTRDO0lBQzVDLDZEQUE2RDtJQUM3RCxzRkFBc0Y7SUFDdEYsWUFBWTtJQUNaLFVBQVU7SUFDVixRQUFRO0lBRVIsdURBQXVEO0lBQ3ZELCtDQUErQztJQUUvQyxnQ0FBZ0M7SUFDaEMsUUFBUTtJQUVSLE1BQU07QUFDUixDQUFDO0FBRUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLEtBQUssRUFBRSxpQkFBaUIsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO0FBRWhGLElBQUksQ0FBQyxTQUFTLENBQUMsb0JBQW9CLEdBQUcsS0FBSyxDQUFDO0FBRTVDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztBQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7QUFFL0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsR0FBRyxVQUFVLE9BQU87SUFDbkQsSUFBSSxDQUFDLDBCQUEwQixHQUFHLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQyxDQUFDLGlCQUFpQixDQUFDO0lBQ3BFLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDOztRQUVkLGlCQUFpQixHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJO0lBRWxFLElBQUksaUJBQWlCO1FBQ25CLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsSUFBSSxPQUFPLGlCQUFpQixDQUFDLFVBQVUsS0FBSyxRQUFRLENBQUMsRUFBRTtRQUNuRyxJQUFJLGlCQUFpQixDQUFDLFFBQVEsRUFBRTtZQUM5QixJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixHQUFHLGlCQUFpQixDQUFDLFVBQVUsQ0FBQztTQUMvRDthQUFNO1lBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQzlGO0tBQ0Y7U0FBTTtRQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO0tBQ3pEO0lBRUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQzFELElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNwRCxJQUFJLE9BQU8sQ0FBQyxTQUFTLEtBQUssU0FBUyxFQUFFO1FBQ25DLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUM7S0FDNUM7U0FBTTtRQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztLQUMvQjtJQUNELElBQUksT0FBTyxDQUFDLFdBQVcsS0FBSyxTQUFTLEVBQUU7UUFDckMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQztLQUNoRDtTQUFNO1FBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO0tBQ2xDO0lBQ0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUM7SUFFekQsSUFBSSxPQUFPLENBQUMsc0JBQXNCLEtBQUssU0FBUyxFQUFFO1FBQ2hELElBQUksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLEdBQUcsT0FBTyxDQUFDLHNCQUFzQixDQUFDO0tBQ3RFO1NBQU07UUFDTCxJQUFJLENBQUMsT0FBTyxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQztLQUM1QztJQUVELG9EQUFvRDtJQUNwRCxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDO0lBQ2pELElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUM7SUFDakQsSUFBSSxPQUFPLENBQUMsVUFBVSxFQUFFO1FBQ3RCLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUM7S0FDOUM7SUFFRCx1REFBdUQ7SUFDdkQsSUFBSSxPQUFPLENBQUMsT0FBTyxFQUFFO1FBQ25CLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUM7S0FDeEM7O1FBRUcsb0JBQW9CLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLElBQUk7SUFDeEUsSUFBSSxvQkFBb0IsS0FBSyxJQUFJLElBQUksT0FBTyxvQkFBb0IsS0FBSyxXQUFXLEVBQUU7UUFDaEYsSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsR0FBRyxvQkFBb0IsQ0FBQztLQUMxRDtTQUFNO1FBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7S0FDL0Q7SUFFRCx1QkFBdUI7SUFDdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsR0FBRyxPQUFPLENBQUMsa0JBQWtCLENBQUM7SUFDN0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsR0FBRyxPQUFPLENBQUMsa0JBQWtCLENBQUM7SUFFN0QsSUFBSSxPQUFPLENBQUMsbUJBQW1CLEtBQUssU0FBUyxFQUFFO1FBQzdDLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLEdBQUcsT0FBTyxDQUFDLG1CQUFtQixDQUFDO0tBQ2hFO0lBRUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUM7QUFDbkQsQ0FBQyxDQUFDO0FBRUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEdBQUcsVUFBVSxRQUFRO0lBQ3pDLElBQUksUUFBUTtRQUNWLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO0FBQzdCLENBQUMsQ0FBQztBQUVGLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLEdBQUcsVUFBZ0IsUUFBUTs7Ozs7O29CQUN2RCxJQUFJLEdBQUcsSUFBSTtvQkFDYixPQUFPLEdBQUcsUUFBUSxDQUFDLEtBQUssRUFBRTtvQkFHNUIsSUFBSSxDQUFDLE9BQU87d0JBQ1Ysc0JBQU8sQ0FBQyxjQUFjOztvQkFHeEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7d0JBQ3BFLHdFQUF3RTtxQkFDekU7eUJBQU07d0JBQ0wsV0FBVyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxFQUFFLEVBQUUsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO3FCQUM3RDtvQkFFRCxPQUFPLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNyQywyQ0FBMkM7b0JBQzNDLE9BQU8sQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsMEJBQTBCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQztvQkFDOUYsT0FBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO29CQUV4QixxQkFBTSxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLEVBQUE7O29CQUF0RCxJQUFJLEdBQUcsU0FBK0M7b0JBQzVELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUU5QixJQUFJLElBQUksQ0FBQyxXQUFXLFlBQVksa0JBQWtCLEVBQUU7d0JBQ2xELENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxFQUFFLENBQUM7NEJBQzVELE9BQU8sQ0FBQyxDQUFDLFlBQVksYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQzt3QkFDL0QsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7eUJBQU07d0JBQ0wsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztxQkFDbE07b0JBRUQsc0JBQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxFQUFDOzs7O0NBb0IzQyxDQUFDO0FBRUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLEdBQUc7Ozs7WUFDM0IsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTztZQUNwQyxRQUFRLEdBQUcsRUFBRTtZQUVmLEtBQVMsRUFBRSxJQUFJLE9BQU8sRUFBRTtnQkFDbEIsTUFBTSxHQUFHLE9BQU8sQ0FBQyxFQUFFLENBQUM7Z0JBQ3hCLFFBQVEsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLElBQUksRUFBRSxDQUFDLENBQUM7YUFDbkQ7WUFFRCxzQkFBTyxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLEVBQUM7OztDQUMzQyxDQUFDO0FBRUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsR0FBRzs7UUFDNUIsUUFBUSxHQUFHLEVBQUU7SUFDakIsS0FBSyxJQUFJLE1BQUksSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFOztZQUMxQixPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFJLENBQUM7UUFDakMsUUFBUSxDQUFDLE1BQUksQ0FBQyxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0tBQ3hEO0lBQ0QsT0FBTyxRQUFRLENBQUM7QUFDbEIsQ0FBQyxDQUFDO0FBRUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUc7SUFDckIsT0FBTyxJQUFJLENBQUMsR0FBRyxJQUFJLEVBQUUsQ0FBQztBQUN4QixDQUFDLENBQUM7QUFFRixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxVQUFVLEdBQUcsRUFBRSxRQUFROztRQUM5QyxJQUFJLEdBQUcsSUFBSTs7UUFDWCxDQUFDLEdBQUcsT0FBTyxRQUFRLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDOztRQUMxRCxVQUFVLEdBQUcsSUFBSTs7UUFDakIsSUFBSSxHQUFRLEVBQUU7O1FBQ2QsTUFBTSxHQUFHO1FBQ1gsUUFBUSxFQUFFO1lBQ1IsTUFBTSxFQUFFO2dCQUNOLFFBQVEsRUFBRTtvQkFDUixhQUFhLEVBQUU7d0JBQ2IsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLFFBQVEsRUFBRSxRQUFRO3FCQUNuQjtpQkFDRjthQUNGO1lBQ0QsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRTtvQkFDTCxTQUFTLEVBQUUsUUFBUTtvQkFDbkIsV0FBVyxFQUFFLFFBQVE7b0JBQ3JCLE1BQU0sRUFBRSxRQUFRO2lCQUNqQjthQUNGO1NBQ0Y7S0FDRjs7UUFDRyxLQUFLLEdBQVUsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLENBQUM7O1FBQzdELEtBQUssR0FBUSxFQUFFOztRQUVmLElBQUksR0FBRyxFQUFFOztRQUFFLEVBQUU7SUFFakIsQ0FBQyxDQUFDLFNBQVMsR0FBRyxVQUFVLElBQUk7O1lBQ3RCLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSTs7WUFDbEIsS0FBSyxHQUFRLElBQUksQ0FBQyxVQUFVOztZQUM1QixJQUFJLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUk7O1lBQ2hDLGFBQWE7O1lBQ2IsR0FBRyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzs7WUFDN0IsU0FBUyxHQUFHLEdBQUcsQ0FBQyxNQUFNOztZQUN0QixpQkFBaUIsR0FBRyxFQUFFOztZQUN0QixvQkFBb0IsR0FBRyxLQUFLOztZQUM1QixlQUFlLEdBQUcsS0FBSzs7WUFDdkIsR0FBRyxHQUFHLEVBQUU7O1lBQ04sWUFBWSxHQUFHLElBQUk7UUFFdkIsSUFBSSxDQUFDLFVBQVUsSUFBSSxHQUFHLENBQUMsSUFBSSxLQUFLLE1BQU0sSUFBSSxJQUFJLEtBQUssT0FBTyxFQUFFOztnQkFDdEQsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztZQUM3Qyw4RUFBOEU7WUFDOUUseUVBQXlFO1lBQ3pFLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQ1osSUFBSTs7O3dCQUVFLE9BQU8sR0FBRyxLQUFLOzt3QkFDZixRQUFRLEdBQUcsS0FBSztvQkFDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDNUIsUUFBUSxHQUFHLElBQUksQ0FBQzt3QkFDaEIsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxDQUFDO3FCQUN0Qzt5QkFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUNsQyxPQUFPLEdBQUcsSUFBSSxDQUFDO3dCQUNmLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQztxQkFDckM7eUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDbEMsT0FBTyxHQUFHLElBQUksQ0FBQzt3QkFDZixJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7cUJBQ3JDOzs7d0JBRUcsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUzs7d0JBQ3RDLGFBQWEsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQzs7O3dCQUV0QyxRQUFRLEdBQUcsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDMUMsSUFBSSxPQUFPLEVBQUU7d0JBQ1gsSUFBSSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztxQkFDM0M7eUJBQU07d0JBQ0wsSUFBSSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztxQkFDNUM7b0JBQ0QsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMxQyw2Q0FBNkM7b0JBQzdDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUMzRTtnQkFBQyxPQUFPLENBQUMsRUFBRTtvQkFDVixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFO3dCQUM1QixDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUNkO2lCQUNGO2FBQ0Y7WUFFRCxTQUFTLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDbEQsVUFBVSxHQUFHLFlBQVksQ0FBQztTQUMzQjtRQUVELElBQUksS0FBSyxDQUFDLElBQUksRUFBRTtZQUNkLEVBQUUsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFO2dCQUNiLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDO2FBQ3JDO1lBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1NBQy9EO1FBQ0QsSUFBSSxFQUFFLEdBQUcsS0FBSyxDQUFDLEVBQUUsRUFBRTtZQUNqQixJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFO2dCQUNiLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDO2FBQ3JDO1NBQ0Y7UUFFRCwyQkFBMkI7UUFDM0IsS0FBSyxhQUFhLElBQUksS0FBSyxFQUFFO1lBQzNCLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFO2dCQUN6QyxLQUFLLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDN0QsU0FBUzthQUNWO1lBQ0Qsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO1lBQzVCLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUN6RDtRQUVELEtBQUssYUFBYSxJQUFJLGlCQUFpQixFQUFFOztnQkFDbkMsR0FBRyxHQUFHLFVBQVUsQ0FBQyxhQUFhLENBQUM7WUFDbkMsSUFBSSxHQUFHLENBQUMsSUFBSSxLQUFLLEtBQUssSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLDJDQUEyQyxJQUFJLGlCQUFpQixDQUFDLGFBQWEsQ0FBQztnQkFDN0gsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxXQUFXLEVBQUUsS0FBSyxNQUFNLElBQUksaUJBQWlCLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQ3ZHO2dCQUNBLGVBQWUsR0FBRyxJQUFJLENBQUM7Z0JBQ3ZCLE1BQU07YUFDUDtTQUNGO1FBRUQsSUFBSSxvQkFBb0IsRUFBRTtZQUN4QixHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsR0FBRyxpQkFBaUIsQ0FBQztTQUNyRDs7O1lBR0csYUFBYTs7WUFDYixPQUFPLEdBQUcsaUJBQWlCLENBQUMsVUFBVSxDQUFDO1FBQzNDLElBQUksT0FBTyxFQUFFOztnQkFDUCxJQUFJLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQzs7Z0JBQzFCLE9BQU8sU0FBQTtZQUNYLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxVQUFVLEVBQUU7Z0JBQzlCLGlDQUFpQztnQkFDakMsT0FBTyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQzthQUM3QztpQkFBTTtnQkFDTCxPQUFPLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUM5Qjs7Z0JBQ0csT0FBTyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQztZQUN2RCxJQUFJLE9BQU8sRUFBRTtnQkFDWCxhQUFhLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDdkQ7U0FDRjtRQUVELElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUU7WUFDdkMsSUFBSSxHQUFHLElBQUksR0FBRyxJQUFJLENBQUM7U0FDcEI7UUFDRCxLQUFLLENBQUMsSUFBSSxDQUFDO1lBQ1QsSUFBSSxFQUFFLFlBQVk7WUFDbEIsTUFBTSxFQUFFLEdBQUc7WUFDWCxNQUFNLEVBQUUsQ0FBQyxhQUFhLElBQUksQ0FBQyxTQUFTLElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDekQsRUFBRSxFQUFFLEtBQUssQ0FBQyxFQUFFO1lBQ1osR0FBRyxFQUFFLGVBQWU7U0FDckIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQyxDQUFDO0lBRUYsQ0FBQyxDQUFDLFVBQVUsR0FBRyxVQUFVLE1BQU07O1lBQ3pCLEdBQUcsR0FBUSxLQUFLLENBQUMsR0FBRyxFQUFFOztZQUN4QixHQUFHLEdBQUcsR0FBRyxDQUFDLE1BQU07O1lBQ2hCLEdBQUcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7O1lBQzdCLFNBQVMsR0FBRyxHQUFHLENBQUMsTUFBTTs7WUFDdEIsU0FBUyxHQUFHLEdBQUcsQ0FBQyxNQUFNOztZQUN0QixJQUFJLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUk7UUFFaEMsSUFBSSxPQUFPLEdBQUcsQ0FBQyxNQUFNLEtBQUssUUFBUSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sS0FBSyxRQUFRLElBQUksQ0FBQyxtQkFBUSxHQUFHLENBQUMsTUFBTSxFQUFBLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssUUFBUSxDQUFDLEVBQUU7WUFDbEgsSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQztnQkFBRSxHQUFHLEdBQUcsR0FBRyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7U0FDckY7UUFFRCxJQUFJLEdBQUcsQ0FBQyxHQUFHLEtBQUssSUFBSSxFQUFFO1lBQ3BCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUU7Z0JBQ2hDLEdBQUcsR0FBRyxJQUFJLENBQUM7YUFDWjtpQkFBTTtnQkFDTCxPQUFPO2FBQ1I7U0FDRjtRQUVELElBQUksQ0FBQyxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxFQUFFO1lBQ3BELEdBQUcsR0FBRyxJQUFJLENBQUM7U0FDWjtRQUVELElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUU7WUFDdkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDcEIsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUN0QjtZQUNELFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDM0I7YUFBTSxJQUFJLElBQUksSUFBSSxTQUFTLEVBQUU7WUFDNUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUU7Z0JBQ25DLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQ3JDO1lBQ0QsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUMzQjthQUFNO1lBQ0wsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQztTQUN2QjtRQUVELElBQUksR0FBRyxDQUFDLEVBQUUsRUFBRTtZQUNWLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztTQUN4QjtJQUNILENBQUMsQ0FBQztJQUVGLENBQUMsQ0FBQyxPQUFPLEdBQUcsVUFBVSxJQUFJOztZQUNwQixZQUFZLEdBQUcsSUFBSTtRQUN2QixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2hCLE9BQU87U0FDUjtRQUVELElBQUksa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFOztnQkFDN0IsS0FBRyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzs7Z0JBQzdCLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztZQUNsQyxJQUFJLEtBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRTtnQkFDMUMsS0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxHQUFHLEtBQUssQ0FBQzthQUMzQztpQkFBTTtnQkFDTCxLQUFHLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQzthQUNwQjtTQUNGO2FBQU07WUFDTCxDQUFDLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ3hCO0lBQ0gsQ0FBQyxDQUFDO0lBRUYsQ0FBQyxDQUFDLE9BQU8sR0FBRyxVQUFVLENBQUM7UUFDckIsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsTUFBTTtZQUNKLEtBQUssRUFBRTtnQkFDTCxTQUFTLEVBQUUsR0FBRztnQkFDZCxXQUFXLEVBQUUsYUFBYTtnQkFDMUIsTUFBTSxFQUFFLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU87Z0JBQzVCLFVBQVUsRUFBRSxHQUFHO2FBQ2hCO1NBQ0YsQ0FBQztJQUNKLENBQUMsQ0FBQztJQUVGLENBQUMsQ0FBQyxNQUFNLEdBQUcsVUFBVSxJQUFJOztZQUNuQixZQUFZLEdBQUcsSUFBSTtRQUN2QixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2hCLE9BQU87U0FDUjs7WUFFRyxHQUFHLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDOztZQUM3QixJQUFJLEdBQUcsVUFBVSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJOztZQUNwQyxLQUFLO1FBQ1AsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUM1RixLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDMUQ7YUFDSTtZQUNILElBQUksSUFBSSxLQUFLLEtBQUssSUFBSSxJQUFJLEtBQUssU0FBUyxFQUFFO2dCQUN4QyxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQzthQUM1QjtpQkFBTSxJQUFJLElBQUksS0FBSyxNQUFNLElBQUksSUFBSSxLQUFLLFNBQVMsRUFBRTtnQkFDaEQsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxNQUFNLElBQUksSUFBSSxLQUFLLEdBQUcsQ0FBQzthQUN2RDtpQkFBTSxJQUFJLElBQUksS0FBSyxVQUFVLElBQUksSUFBSSxLQUFLLE1BQU0sRUFBRTtnQkFDakQsS0FBSyxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3hCO2lCQUFNO2dCQUNMLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRTtvQkFDbkMsSUFBSSxHQUFHLFlBQVksQ0FBQztpQkFDckI7Z0JBQ0QsK0JBQStCO2dCQUMvQixJQUFJLE9BQU8sR0FBRyxDQUFDLE1BQU0sS0FBSyxRQUFRLEVBQUU7b0JBQ2xDLEtBQUssR0FBRyxJQUFJLENBQUM7aUJBQ2Q7cUJBQU07b0JBQ0wsS0FBSyxHQUFHLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2lCQUMzQjthQUNGO1NBQ0Y7UUFFRCxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUMxQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxDQUFDO1NBQzNDO2FBQU07WUFDTCxHQUFHLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUNwQjtJQUNILENBQUMsQ0FBQztJQUVGLElBQUksT0FBTyxRQUFRLEtBQUssVUFBVSxFQUFFOzs7WUFFOUIsU0FBUyxHQUFHLEdBQUcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO1FBQ3RDLFNBQVMsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNyQyxTQUFTLENBQUMsRUFBRSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdkMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2pDLFNBQVMsQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvQixHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQzthQUNoQixFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsR0FBRztZQUN4QixRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEIsQ0FBQyxDQUFDO2FBQ0QsRUFBRSxDQUFDLEtBQUssRUFBRTs7Z0JBQ0wsQ0FBQztZQUNMLElBQUk7Z0JBQ0YsQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDO2FBQ2Q7WUFBQyxPQUFPLENBQUMsRUFBRTtnQkFDVixPQUFPLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNwQjtZQUNELFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDcEIsQ0FBQyxDQUFDLENBQUM7UUFDTCxPQUFPO0tBQ1I7SUFDRCxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBRXJCLE9BQU8sTUFBTSxFQUFFLENBQUM7Ozs7SUFFaEIsU0FBUyxNQUFNO1FBQ2IsdURBQXVEO1FBQ3ZELEtBQUssSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFOztnQkFDZCxHQUFHLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNqQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3pDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ3JDO1NBQ0Y7UUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7O2dCQUNiLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUk7WUFDN0IsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTs7b0JBQ2xCLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNOztvQkFDMUQsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE1BQU07O29CQUNoRSxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTTtnQkFFMUQsSUFBSSxHQUFHLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDcEMsTUFBTSxHQUFHLE1BQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQztnQkFDMUMsTUFBTSxHQUFHLE1BQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQzs7b0JBRWpDLEtBQUssR0FBUSxJQUFJLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLE1BQU0sR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBRWhGLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUNsQixNQUFNLEtBQUssQ0FBQzthQUNiO1lBQ0QsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO1NBQ3RCO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0FBQ0gsQ0FBQyxDQUFDOzs7Ozs7O0FBUUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsR0FBRyxVQUFVLEtBQUssRUFBRSxLQUFLO0lBQ3RELElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxLQUFLLEVBQUU7UUFDcEIsT0FBTyxJQUFJLENBQUM7S0FDYjs7UUFFRyxHQUFHLEdBQUcsSUFBSTtJQUVkLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUU7O1lBQ3hCLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFDNUMsSUFBSSxNQUFNLEVBQUU7WUFDVixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQzdCLEtBQUssR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUMvRDtZQUVELDhGQUE4RjtZQUM5RiwyQ0FBMkM7WUFDM0MsR0FBRyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ25GO0tBQ0Y7SUFFRCxPQUFPLEdBQUcsQ0FBQztBQUNiLENBQUMsQ0FBQzs7Ozs7Ozs7O0FBVUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsR0FBRyxVQUFVLElBQUksRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxJQUFJO0lBQ2hGLHNGQUFzRjtJQUN0RixJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsSUFBSSxFQUFFO1FBQ3pCLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQztLQUNwQjs7UUFDRyxJQUFJLEdBQUcsRUFBRTtJQUNiLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUM7O1FBQ2hCLGdCQUFnQixHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtJQUN2RSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztBQUNyRixDQUFDLENBQUM7Ozs7Ozs7OztBQVVGLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxHQUFHLFVBQVUsSUFBSSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLE9BQU87O1FBQzFFLEtBQUssR0FBRyxFQUFFOztRQUNWLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVzs7UUFDdkIsVUFBVSxHQUFHLFFBQVE7SUFFekIsUUFBUSxHQUFHLFFBQVEsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztJQUVyRCxLQUFLLEdBQUcsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDdEMsUUFBUSxHQUFHLFFBQVEsS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLENBQUM7SUFFM0QsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBRWhELEtBQUssSUFBSSxHQUFHLElBQUksTUFBTSxFQUFFO1FBQ3RCLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQy9CLFNBQVM7U0FDVjtRQUNELElBQUksR0FBRyxLQUFLLFVBQVUsRUFBRTs7Z0JBQ2xCLEtBQUssR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDOztnQkFDbkIsV0FBVyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxHQUFHLEdBQUc7O2dCQUM3QyxVQUFVLEdBQUcsRUFBRTtZQUNuQixJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsSUFBSSxLQUFLLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUU7O29CQUM3RSxLQUFLLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDO2dCQUM3QyxLQUFLLElBQUksQ0FBQyxJQUFJLEtBQUssRUFBRTtvQkFDbkIsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO2lCQUN2RDthQUNGO1lBQ0QsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3ZFLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLEtBQUssS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDM0csS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxXQUFXLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDL0M7S0FDRjtJQUNELEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNqRCxPQUFPLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7QUFDeEIsQ0FBQyxDQUFDOzs7OztBQUdGLFNBQVMsV0FBVyxDQUFDLEVBQUU7SUFDckIsT0FBTyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztBQUNsRSxDQUFDOzs7OztBQUVELFNBQVMsZ0JBQWdCLENBQUMsRUFBRTtJQUMxQixPQUFPLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0FBQ3hGLENBQUM7QUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLGtCQUFrQixHQUFHLFVBQVUsRUFBRTtJQUM5QyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0FBQ3pELENBQUMsQ0FBQztBQUVGLElBQUksQ0FBQyxTQUFTLENBQUMseUJBQXlCLEdBQUcsVUFBVSxFQUFFOztRQUNqRCxTQUFTLEdBQUcsZ0JBQWdCLENBQUMsRUFBRSxDQUFDO0lBQ3BDLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztBQUM3RCxDQUFDLENBQUM7Ozs7Ozs7Ozs7Ozs7O0FBaUJGLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLFVBQVUsR0FBRyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLFNBQVM7O1FBQ3hHLElBQUksR0FBRyxJQUFJOztRQUNYLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7O1FBRXhDLGNBQWMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFNBQVM7SUFDM0QsSUFBSSxPQUFPLGNBQWMsS0FBSyxXQUFXLEVBQUU7UUFDekMsOEdBQThHO1FBQzlHLFFBQVEsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDO0tBQzdCO0lBRUQsY0FBYyxHQUFHLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2xELElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxFQUFFO1FBQzNDLGNBQWMsR0FBRyxFQUFFLENBQUM7S0FDckI7O1FBRUcsVUFBVSxHQUFHLENBQUMsTUFBTTs7UUFDcEIsU0FBUyxHQUFHLE1BQU0sSUFBSSxNQUFNLENBQUMsbUJBQW1CLEtBQUssV0FBVzs7UUFDaEUsS0FBSyxHQUFHLEVBQUU7O1FBQ1YsZUFBZSxHQUFHLENBQUMsUUFBUSxJQUFJLFNBQVMsQ0FBQyxJQUFJLFFBQVEsS0FBSyxVQUFVOztRQUVwRSxXQUFXLEdBQUcsRUFBRTtJQUNwQixJQUFJLEtBQUssSUFBSSxPQUFPLEVBQUU7UUFDcEIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsZUFBZSxFQUFFO1lBQ3hGLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFVLFNBQVM7Z0JBQzFFLFdBQVcsSUFBSSxHQUFHLEdBQUcsU0FBUyxDQUFDLElBQUksR0FBRyxJQUFJLEdBQUcsU0FBUyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7WUFDckUsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3pELCtCQUErQjtnQkFDL0IsV0FBVyxJQUFJLFNBQVMsR0FBRyxRQUFRLEdBQUcsSUFBSSxHQUFHLEtBQUssR0FBRyxHQUFHLENBQUM7YUFDMUQ7WUFDRCwyRUFBMkU7WUFDM0UsSUFBSSxTQUFTLElBQUksVUFBVTtnQkFBRSxXQUFXLElBQUksVUFBVSxHQUFHLEtBQUssR0FBRyxHQUFHLENBQUM7U0FDdEU7S0FDRjtJQUVELElBQUksQ0FBQyxTQUFTLEVBQUU7UUFDZCxTQUFTLEdBQUcsSUFBSSxnQkFBZ0IsRUFBRSxDQUFDO1FBQ25DLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7S0FDN0M7U0FBTTtRQUNMLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztLQUN6QjtJQUVELDhDQUE4QztJQUM5QyxJQUFJLFNBQVMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLGVBQWUsQ0FBQyxFQUFFO1FBQ3hHLFdBQVcsR0FBRyxTQUFTLENBQUM7S0FDekI7O1FBRUcsRUFBRSxHQUFHLEVBQUU7SUFFWCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLElBQUksT0FBTyxFQUFFO1FBQy9DLEVBQUUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQztLQUNqRDtTQUFNLElBQUksZUFBZSxJQUFJLENBQUMsU0FBUyxJQUFJLE9BQU8sSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsRUFBRTtRQUN4RyxFQUFFLEdBQUcsUUFBUSxDQUFDO0tBQ2Y7O1FBRUcsQ0FBQzs7UUFBRSxDQUFDO0lBQ1IsaUNBQWlDO0lBQ2pDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtRQUN0QixLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTs7Z0JBQ2xDLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDOztnQkFDYixTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxTQUFTLENBQUM7O2dCQUNyRCxvQkFBb0IsR0FBRyxjQUFjLElBQUksRUFBRTs7O2dCQUV6QyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsU0FBUyxDQUFDOztnQkFFMUYsZUFBZSxHQUFHLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsV0FBVyxDQUFDO1lBRTVGLElBQUksSUFBSSxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRTtnQkFDM0MsK0NBQStDO2dCQUMvQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM1QixLQUFLLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUN0QztpQkFBTTtnQkFDTCxlQUFlLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUMxQixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDbEQsS0FBSyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQ3RDO2dCQUNELEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2pCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDdEQsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsb0JBQW9CLENBQUMsRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQzNFO2FBQ0Y7U0FDRjtLQUNGO1NBQU0sSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLEVBQUU7UUFDbEMsS0FBSyxJQUFJLElBQUksR0FBRyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQztnQkFBRSxTQUFTO1lBQ3hDLHFDQUFxQztZQUNyQyxJQUFJLElBQUksS0FBSyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRTtnQkFDdkMsU0FBUzthQUNWO1lBQ0Qsb0RBQW9EO1lBQ3BELElBQUksSUFBSSxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO2dCQUNoQyxTQUFTLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQ3ZCLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2xCO1lBQ0QsK0NBQStDO1lBQy9DLElBQUksSUFBSSxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFO2dCQUNsQyxTQUFTLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQ3ZCLE9BQU8sU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQzdCOztnQkFFRyxLQUFLLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQztZQUNyQixJQUFJLE9BQU8sS0FBSyxLQUFLLFdBQVcsRUFBRTtnQkFDaEMsU0FBUzthQUNWOztnQkFFRyxJQUFJLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxTQUFTLENBQUM7O2dCQUUvQyxLQUFLLEdBQUcsRUFBRTs7Z0JBQ1YsZUFBZSxHQUFHLEVBQUU7O2dCQUNwQixvQkFBb0IsR0FBRyxLQUFLOztnQkFFNUIsZUFBZSxHQUFHLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDcEQsSUFBSSxlQUFlLEVBQUU7Z0JBQ25CLGVBQWUsR0FBRyxlQUFlLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO2dCQUMzQyxJQUFJLEdBQUcsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzNCO2lCQUFNLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRTtnQkFDMUIsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO2dCQUM1QixJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN2QjtZQUVELElBQUksT0FBTyxFQUFFO2dCQUNYLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxTQUFTLENBQUMsQ0FBQzthQUM5RjtpQkFBTTtnQkFFTCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFO29CQUM1QixJQUFJLE1BQU0sRUFBRTs7NEJBQ04saUJBQWlCLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUM7d0JBQ3RFLHVDQUF1Qzt3QkFDdkMsSUFBSSxpQkFBaUI7NEJBQ25CLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBQzVFLGlCQUFpQixDQUFDLElBQUksSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsRUFBRTs7Ozs7Z0NBS2xELGFBQWEsR0FBUSxFQUFFOztnQ0FDdkIsU0FBUyxHQUFHLEVBQUU7O2dDQUNkLFVBQVUsU0FBQTs7Z0NBQ1YsZ0JBQWdCLEdBQUcsRUFBRTs7Z0NBRXJCLFlBQVksR0FBRyxpQkFBaUIsQ0FBQyxJQUFJLElBQUksaUJBQWlCLENBQUMsS0FBSzs0QkFDcEUsSUFBSSxZQUFZLEVBQUU7Z0NBQ2hCLFlBQVksR0FBRyxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUM7Z0NBQ3hDLFNBQVMsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDO2dDQUM5QixJQUFJLFlBQVksQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO29DQUN0QyxnQkFBZ0I7b0NBQ2hCLFVBQVUsR0FBRyxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQztvQ0FDaEQsYUFBYSxHQUFHLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztvQ0FDeEQsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLEVBQUU7d0NBQzFDLGFBQWEsR0FBRyxRQUFRLENBQUM7cUNBQzFCO2lDQUNGO3FDQUFNO29DQUNMLGFBQWEsR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDO29DQUNwQyxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsRUFBRTt3Q0FDMUMsYUFBYSxHQUFHLFFBQVEsQ0FBQztxQ0FDMUI7b0NBQ0QsVUFBVSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7aUNBQ25GOztvQ0FFRyxXQUFXLEdBQUcsS0FBSztnQ0FDdkIsOENBQThDO2dDQUM5QyxJQUFJLGlCQUFpQixDQUFDLEtBQUssSUFBSSxpQkFBaUIsQ0FBQyxlQUFlLEtBQUssU0FBUyxFQUFFO29DQUM5RSxJQUFJLGlCQUFpQixDQUFDLEtBQUssS0FBSyxhQUFhLEVBQUU7d0NBQzdDLFdBQVcsR0FBRyxJQUFJLENBQUM7cUNBQ3BCO3lDQUFNLElBQUksaUJBQWlCLENBQUMsS0FBSyxLQUFLLFdBQVcsRUFBRTt3Q0FDbEQsV0FBVyxHQUFHLEtBQUssQ0FBQztxQ0FDckI7eUNBQU07d0NBQ0wsV0FBVyxHQUFHLE1BQU0sQ0FBQyxtQkFBbUIsS0FBSyxXQUFXLENBQUM7cUNBQzFEO2lDQUNGO2dDQUNELElBQUksV0FBVyxFQUFFO29DQUNmLGFBQWEsR0FBRyxFQUFFLENBQUM7aUNBQ3BCO2dDQUVELElBQUksVUFBVSxJQUFJLGFBQWEsRUFBRTtvQ0FDL0IsSUFBSSxTQUFTLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxFQUFFLFVBQVUsQ0FBQyxFQUFFO3dDQUN6RCxnQkFBZ0IsR0FBRyxTQUFTLEdBQUcsYUFBYSxHQUFHLElBQUksR0FBRyxVQUFVLEdBQUcsR0FBRyxDQUFDO3dDQUN2RSxXQUFXLElBQUksZ0JBQWdCLENBQUM7cUNBQ2pDO2lDQUNGOzZCQUNGOztnQ0FFRyx5QkFBeUIsU0FBQTs0QkFDN0IsSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLEVBQUU7O29DQUN2QixTQUFTLEdBQUcsVUFBVSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQzs7b0NBQy9DLFVBQVUsR0FBRyxTQUFTLENBQUMsTUFBTTs7b0NBQzdCLE9BQU8sR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQztnQ0FDNUUsVUFBVSxHQUFHLE9BQU8sQ0FBQztnQ0FDckIsSUFBSSxPQUFPLEtBQUssa0NBQWtDLElBQUksVUFBVSxLQUFLLFVBQVUsRUFBRTtvQ0FDL0UsdURBQXVEO29DQUN2RCxTQUFTLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQztpQ0FDN0M7Z0NBQ0QseUJBQXlCO29DQUN2QixJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLElBQUksaUJBQWlCLENBQUM7NkJBQ3JFO2lDQUFNO2dDQUNMLHlCQUF5QjtvQ0FDdkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsSUFBSSxpQkFBaUIsQ0FBQzs2QkFDckU7NEJBRUQsSUFBSSxpQkFBaUIsQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRTtnQ0FDekUsYUFBYSxHQUFHLFFBQVEsQ0FBQztnQ0FDekIsVUFBVSxHQUFHLEtBQUssQ0FBQzs2QkFDcEI7NEJBRUQsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLG9CQUFvQixFQUFFO2dDQUNyQyxhQUFhLEdBQUcsRUFBRSxDQUFDO2dDQUNuQixVQUFVLEdBQUcsRUFBRSxDQUFDOzZCQUNqQjs0QkFFRCxFQUFFLEdBQUcsYUFBYSxDQUFDOzRCQUVuQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0NBQ3hCLHVEQUF1RDtnQ0FDdkQsYUFBYSxHQUFHO29DQUNkLE9BQU8sRUFBRSxhQUFhO29DQUN0QixNQUFNLEVBQUUsRUFBRTtpQ0FDWCxDQUFDOzZCQUNIO2lDQUFNO2dDQUNMLDBDQUEwQztnQ0FDMUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDOzZCQUN6Qjs0QkFFRCxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxVQUFVLEVBQzdELEtBQUssRUFBRSxnQkFBZ0IsRUFBRSx5QkFBeUIsRUFBRSxTQUFTLENBQUMsQ0FBQzt5QkFDbEU7NkJBQU0sSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxRQUFRLEVBQUU7OztnQ0FFbEYsNEJBQTRCLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUMzRCxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUM3QyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDOzRCQUVqRCxlQUFlLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQzs0QkFDbEUsU0FBUyxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUNwRSxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQ2xELEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFDbkYsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQzt5QkFDakY7NkJBQU07NEJBQ0wsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dDQUN4QixJQUFJLEdBQUcsZUFBZSxHQUFHLElBQUksQ0FBQzs2QkFDL0I7NEJBRUQsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDO3lCQUN0RjtxQkFDRjt5QkFBTTt3QkFDTCxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7cUJBQ3RGO2lCQUNGO2FBQ0Y7WUFFRCxFQUFFLEdBQUcsZ0JBQWdCLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDMUIsSUFBSSxlQUFlLElBQUksQ0FBQyxTQUFTLElBQUksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRTtnQkFDakYsRUFBRSxHQUFHLFFBQVEsQ0FBQzthQUNmO2lCQUFNLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLEVBQUUsQ0FBQyxFQUFFO2dCQUN0QyxFQUFFLEdBQUcsRUFBRSxDQUFDO2FBQ1Q7O2dCQUVHLFdBQVcsR0FBRyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVc7WUFDcEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ3pCLFlBQVk7Z0JBQ1osS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsZUFBZSxJQUFJLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsV0FBVztvQkFDdEcsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUN6QyxXQUFXLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRztpQkFDMUIsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUNiO1lBRUQsSUFBSSxDQUFDLFdBQVcsRUFBRTtnQkFDaEIsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDbEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQ3pCLFVBQVU7b0JBQ1YsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsZUFBZSxJQUFJLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztpQkFDeEc7YUFDRjtTQUNGO0tBQ0Y7U0FBTSxJQUFJLEdBQUcsS0FBSyxTQUFTLEVBQUU7UUFDNUIsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7S0FDN0Q7SUFDRCxTQUFTLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDdkIsT0FBTyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ3hCLENBQUMsQ0FBQztBQUVGLElBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEdBQUcsVUFBVSxLQUFLLEVBQUUsU0FBUzs7UUFDdkQsSUFBSSxHQUFHLEVBQUU7SUFFYixJQUFJLEtBQUssS0FBSyxJQUFJLEVBQUU7UUFDbEIsS0FBSyxHQUFHLEVBQUUsQ0FBQztLQUNaOztRQUVHLE9BQU8sR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7SUFDL0MsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLFFBQVEsRUFBRTs7WUFDM0IsT0FBTyxHQUFHLE9BQU8sQ0FBQyxRQUFROztZQUUxQixNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sSUFBSSxPQUFPLENBQUMsU0FBUztRQUNoRCxxRUFBcUU7UUFDckUsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNYLE1BQU0sR0FBRyxTQUFTLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3JEO2FBQU07WUFDTCxTQUFTLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNuRDtRQUNELE9BQU8sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO0tBQ3pCO0lBR0QsSUFBSSxPQUFPLEVBQUU7UUFDWCxLQUFLLElBQUksT0FBTyxJQUFJLE9BQU8sRUFBRTtZQUMzQixxQ0FBcUM7WUFDckMsSUFBSSxPQUFPLEtBQUssVUFBVSxFQUFFOztvQkFDdEIsU0FBUyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUM7Z0JBQ2hDLElBQUksSUFBSSxhQUFhLEdBQUcsU0FBUyxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsU0FBUyxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7Z0JBQ3RFLElBQUksSUFBSSxTQUFTLEdBQUcsU0FBUyxDQUFDLE1BQU0sR0FBRyxJQUFJLEdBQUcsU0FBUyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7Z0JBRXBFLFNBQVM7YUFDVjtpQkFBTTtnQkFDTCxJQUFJLElBQUksR0FBRyxHQUFHLE9BQU8sR0FBRyxJQUFJLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQzthQUNsRTtTQUNGO0tBQ0Y7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNkLENBQUMsQ0FBQzs7Ozs7OztBQVFGLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxHQUFHLFVBQVUsSUFBSSxFQUFFLEtBQUs7SUFDbkQsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1FBQ2hELE9BQU8sSUFBSSxDQUFDO0tBQ2I7O1FBRUcsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztJQUM1QyxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRTtRQUNuQyxPQUFPLElBQUksQ0FBQztLQUNiO0lBRUQsT0FBTyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ25DLENBQUMsQ0FBQztBQUVGLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLEdBQUcsVUFBVSxnQkFBZ0IsRUFBRSxTQUFTLEVBQUUsU0FBUztJQUNyRixJQUFJLENBQUMsZ0JBQWdCLElBQUksQ0FBQyxTQUFTLEVBQUU7UUFDbkMsT0FBTyxJQUFJLENBQUM7S0FDYjtJQUVELElBQUksQ0FBQyxTQUFTLEVBQUU7UUFDZCxTQUFTLEdBQUcsRUFBRSxDQUFDO0tBQ2hCO0lBRUQsSUFBSSxTQUFTLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxFQUFFO1FBQzVDLDJDQUEyQztRQUMzQyxPQUFPLElBQUksQ0FBQztLQUNiO1NBQU07UUFDTCxTQUFTLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztLQUNsRDs7UUFFRyxLQUFLLEdBQUcsSUFBSTs7UUFDZCxDQUFDLEdBQUcsQ0FBQzs7UUFDTCxLQUFLOztRQUNMLEdBQUc7SUFFTCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksZ0JBQWdCLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRTs7WUFDcEYsS0FBSyxHQUFHLGdCQUFnQixDQUFDLFlBQVk7UUFFekMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOztnQkFDN0IsT0FBTyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFFdEIsSUFBSSxPQUFPLENBQUMsS0FBSyxLQUFLLFNBQVMsRUFBRTtnQkFDL0IsS0FBSyxHQUFHLE9BQU8sQ0FBQztnQkFDaEIsTUFBTTthQUNQO1NBQ0Y7S0FDRjs7UUFFRyxNQUFNLEdBQUcsZ0JBQWdCO0lBQzdCLElBQUksTUFBTSxDQUFDLEtBQUssS0FBSyxTQUFTLElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxTQUFTLEVBQUU7UUFDM0QsT0FBTyxNQUFNLENBQUM7S0FDZjtJQUNELElBQUksTUFBTSxDQUFDLElBQUksRUFBRTtRQUNmLEdBQUcsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLElBQUksR0FBRyxDQUFDLElBQUksS0FBSyxTQUFTLEVBQUU7WUFDMUIsT0FBTyxNQUFNLENBQUM7U0FDZjtLQUNGOztRQUVHLFVBQVU7SUFFZCw0REFBNEQ7SUFDNUQsSUFBSSxNQUFNLENBQUMsS0FBSyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFOztZQUN0QyxRQUFRLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDdkMsSUFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLFVBQVUsRUFBRTtZQUNsQyxVQUFVLEdBQUcsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUM7U0FDaEQ7YUFBTTtZQUNMLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDdEQ7O1lBQ0csT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxVQUFVLENBQUM7UUFDNUQsSUFBSSxPQUFPLEVBQUU7WUFDWCxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1NBQ2xFO0tBQ0Y7SUFFRCxJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7UUFDbkIsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNsRCxLQUFLLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDaEUsSUFBSSxLQUFLLEVBQUU7Z0JBQ1QsTUFBTTthQUNQO1lBRUQsSUFBSSxLQUFLLENBQUMsS0FBSyxFQUFFOztvQkFDWCxTQUFTLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7O29CQUNuQyxjQUFjLEdBQUcsU0FBUyxDQUFDLE1BQU0sS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU07Z0JBQzVFLFVBQVUsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7O29CQUVuRixTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQztnQkFFL0QsSUFBSSxTQUFTLEVBQUU7b0JBQ2IsS0FBSyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO29CQUVwRSxJQUFJLEtBQUssRUFBRTt3QkFDVCxLQUFLLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQzt3QkFDdEMsS0FBSyxDQUFDLEtBQUssR0FBRyxjQUFjLEdBQUcsR0FBRyxHQUFHLFNBQVMsQ0FBQzt3QkFDL0MsTUFBTTtxQkFDUDtpQkFDRjthQUNGO1NBQ0Y7S0FFRjtJQUVELElBQUksQ0FBQyxLQUFLLElBQUksTUFBTSxDQUFDLEtBQUssS0FBSyxTQUFTLEVBQUU7UUFDeEMsT0FBTyxNQUFNLENBQUM7S0FDZjtJQUVELE9BQU8sS0FBSyxDQUFDO0FBQ2YsQ0FBQyxDQUFDO0FBRUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsVUFBVSxHQUFHOztRQUMvQixJQUFJLEdBQUcsSUFBSTs7UUFDYixDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7O1FBQ3BCLEtBQUssR0FBRyxFQUFFOztRQUNWLElBQUksR0FBRyxJQUFJOztRQUNYLEtBQUssR0FBRyxJQUFJOztRQUNaLE1BQU0sR0FBRyxJQUFJOztRQUNiLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTztJQUV4QixDQUFDLENBQUMsU0FBUyxHQUFHLFVBQVUsSUFBSTs7WUFDdEIsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJOztZQUNsQixLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVU7O1lBRXZCLEdBQUcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7O1lBQzdCLElBQUk7UUFDUixJQUFJLEdBQUcsRUFBRTtZQUNQLElBQUk7Z0JBQ0YsR0FBRyxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQzthQUNqRDtZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNWLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7b0JBQ3ZCLE1BQU0sQ0FBQyxDQUFDO2lCQUNUO3FCQUFNO29CQUNMLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxPQUFPLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO2lCQUNqRDthQUNGO1NBQ0Y7YUFBTTtZQUNMLElBQUksR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQy9CLElBQUksSUFBSSxLQUFLLGFBQWEsRUFBRTtnQkFDMUIsSUFBSSxHQUFHLElBQUksa0JBQWtCLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDdEQsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNsQjtpQkFBTSxJQUFJLElBQUksS0FBSyxRQUFRLEVBQUU7Z0JBQzVCLHdGQUF3RjtnQkFDeEYsSUFBSSxHQUFHLElBQUksa0JBQWtCLENBQUMsYUFBYSxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDckQsS0FBSyxHQUFHLElBQUksWUFBWSxDQUFDLE9BQU8sRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQzFDLE1BQU0sR0FBRyxJQUFJLGFBQWEsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUNuRCxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN2QixJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ3BCO2lCQUFNO2dCQUNMLE1BQU0sSUFBSSxLQUFLLENBQUMsNENBQTRDLENBQUMsQ0FBQzthQUMvRDtTQUNGO0lBQ0gsQ0FBQyxDQUFDO0lBRUYsQ0FBQyxDQUFDLFVBQVUsR0FBRyxVQUFVLElBQUk7O1lBQ3ZCLEdBQUcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDakMsTUFBTSxDQUFDLEdBQUcsRUFBRSx1QkFBdUIsR0FBRyxJQUFJLENBQUMsQ0FBQztRQUU1QyxHQUFHLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM5QixDQUFDLENBQUM7SUFFRixDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBRXJCLE9BQU8sSUFBSSxDQUFDO0FBQ2QsQ0FBQyxDQUFDO0FBRUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsVUFBVSxHQUFHO0lBQ3JDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksR0FBRztRQUM5QixLQUFLLEVBQUUsRUFBRTtLQUNWLENBQUM7SUFDRixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztBQUNqQixDQUFDLENBQUM7QUFFRixJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsR0FBRyxVQUFVLFFBQVE7QUFFakQsQ0FBQyxDQUFDO0FBSUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUc7O1FBQ3JCLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUs7O1FBQzlCLEdBQUcsR0FBRyxFQUFFO0lBQ1osS0FBSyxJQUFJLEtBQUssSUFBSSxLQUFLLEVBQUU7UUFDdkIsSUFBSSxLQUFLLEtBQUssRUFBRSxJQUFJLEtBQUssS0FBSyxVQUFVLEVBQUU7WUFDeEMsU0FBUztTQUNWOztZQUNHLEVBQUUsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ3JCLFFBQVEsRUFBRSxFQUFFO1lBQ1YsS0FBSyxnQ0FBZ0MsQ0FBQyxDQUFDLGFBQWE7WUFDcEQsS0FBSyxrQ0FBa0MsQ0FBQyxDQUFDLE9BQU87WUFDaEQsS0FBSyx1Q0FBdUMsQ0FBQyxDQUFDLFdBQVc7WUFDekQsS0FBSyx5Q0FBeUMsQ0FBQyxDQUFDLGFBQWE7WUFDN0QsS0FBSywyQ0FBMkMsQ0FBQyxDQUFDLFVBQVU7WUFDNUQsS0FBSyxrQ0FBa0MsRUFBRSxNQUFNO2dCQUM3QyxTQUFTO1NBQ1o7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxFQUFFO1lBQzlDLFNBQVM7U0FDVjtRQUNELElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLG9CQUFvQixDQUFDLEVBQUU7WUFDckMsU0FBUztTQUNWO1FBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsRUFBRTtZQUN6QyxTQUFTO1NBQ1Y7UUFDRCxHQUFHLElBQUksU0FBUyxHQUFHLEtBQUssR0FBRyxJQUFJLEdBQUcsRUFBRSxHQUFHLEdBQUcsQ0FBQztLQUM1QztJQUNELE9BQU8sR0FBRyxDQUFDO0FBQ2IsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkYsU0FBUyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsT0FBTzs7UUFDbkMsU0FBUzs7UUFDWCxVQUFVO0lBRVosdUNBQXVDO0lBQ3ZDLHdCQUF3QjtJQUN4QixrQkFBa0I7SUFDbEIsSUFBSTtJQUVKLFVBQVUsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDO0lBRWhDLElBQUksU0FBUyxHQUFHLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRTtRQUMvQixvREFBb0Q7UUFDcEQsT0FBTyxTQUFTLENBQUM7S0FDbEI7SUFFRCxPQUFPLFNBQVMsQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUM7QUFDakMsQ0FBQzs7Ozs7O0FBRUQsTUFBTSxVQUFnQixTQUFTLENBQUMsR0FBRyxFQUFFLE9BQU87Ozs7Ozs7Ozs7O29CQU90QyxVQUFVLEdBQUcsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO29CQUNyQyxlQUFlLEdBQUcsT0FBTyxDQUFDLFlBQVk7b0JBQ3RDLGVBQWUsR0FBRyxPQUFPLENBQUMsWUFBWTtvQkFFMUMsWUFBWTtvQkFDWiwrQkFBK0I7b0JBQy9CLHVDQUF1QztvQkFDdkMsNERBQTREO29CQUM1RCxvQkFBb0I7b0JBQ3BCLDBCQUEwQjtvQkFDMUIsV0FBVztvQkFDWCxnQkFBZ0I7b0JBQ2hCLHNEQUFzRDtvQkFDdEQscUNBQXFDO29CQUNyQyx5Q0FBeUM7b0JBQ3pDLG1DQUFtQztvQkFDbkMsV0FBVztvQkFDWCxXQUFXO29CQUNYLElBQUk7b0JBQ0osU0FBUztvQkFDVCxtQ0FBbUM7b0JBQ25DLG9FQUFvRTtvQkFDcEUsc0ZBQXNGO29CQUN0RixpQkFBaUI7b0JBQ2pCLHVCQUF1QjtvQkFDdkIsNERBQTREO29CQUM1RCxtREFBbUQ7b0JBQ25ELGtDQUFrQztvQkFDbEMsc0NBQXNDO29CQUN0QyxnQ0FBZ0M7b0JBQ2hDLGVBQWU7b0JBQ2YsNElBQTRJO29CQUM1SSxRQUFRO29CQUNSLDBDQUEwQztvQkFDMUMsSUFBSTtvQkFDSixlQUFlO29CQUVmLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxDQUFDLENBQUM7b0JBQzlCLFVBQVUsR0FBZSxPQUFPLENBQUMsVUFBVTtvQkFDakMscUJBQU0sVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQTs7b0JBQXpFLE9BQU8sR0FBRyxTQUErRDtvQkFDL0QscUJBQU0sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPOztnQ0FDbEMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsT0FBTyxDQUFDOzRCQUM1QyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDOzRCQUN2QixJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQzs0QkFDN0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDOUIsQ0FBQyxDQUFDLEVBQUE7O29CQUxJLE9BQU8sR0FBRyxTQUtkO29CQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFBO29CQUM1QixzQkFBTyxPQUFPLEVBQUM7Ozs7Q0FDaEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogQ29weXJpZ2h0IChjKSAyMDExIFZpbmF5IFB1bGltIDx2aW5heUBtaWxld2lzZS5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqXG4gKi9cbi8qanNoaW50IHByb3RvOnRydWUqL1xuXG5cInVzZSBzdHJpY3RcIjtcblxuaW1wb3J0ICogYXMgc2F4IGZyb20gJ3NheCc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgTmFtZXNwYWNlQ29udGV4dCB9IMKgZnJvbSAnLi9uc2NvbnRleHQnO1xuXG5pbXBvcnQgKiBhcyB1cmwgZnJvbSAndXJsJztcbmltcG9ydCB7IG9rIGFzIGFzc2VydCB9IGZyb20gJ2Fzc2VydCc7XG4vLyBpbXBvcnQgc3RyaXBCb20gZnJvbSAnc3RyaXAtYm9tJztcblxuY29uc3Qgc3RyaXBCb20gPSAoeDogc3RyaW5nKTogc3RyaW5nID0+IHtcbiAgLy8gQ2F0Y2hlcyBFRkJCQkYgKFVURi04IEJPTSkgYmVjYXVzZSB0aGUgYnVmZmVyLXRvLXN0cmluZ1xuICAvLyBjb252ZXJzaW9uIHRyYW5zbGF0ZXMgaXQgdG8gRkVGRiAoVVRGLTE2IEJPTSlcbiAgaWYgKHguY2hhckNvZGVBdCgwKSA9PT0gMHhGRUZGKSB7XG4gICAgcmV0dXJuIHguc2xpY2UoMSk7XG4gIH1cblxuICByZXR1cm4geDtcbn1cblxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0ICogYXMgdXRpbHMgZnJvbSAnLi91dGlscyc7XG5cblxubGV0IFROU19QUkVGSVggPSB1dGlscy5UTlNfUFJFRklYO1xubGV0IGZpbmRQcmVmaXggPSB1dGlscy5maW5kUHJlZml4O1xuXG5sZXQgUHJpbWl0aXZlcyA9IHtcbiAgc3RyaW5nOiAxLFxuICBib29sZWFuOiAxLFxuICBkZWNpbWFsOiAxLFxuICBmbG9hdDogMSxcbiAgZG91YmxlOiAxLFxuICBhbnlUeXBlOiAxLFxuICBieXRlOiAxLFxuICBpbnQ6IDEsXG4gIGxvbmc6IDEsXG4gIHNob3J0OiAxLFxuICBuZWdhdGl2ZUludGVnZXI6IDEsXG4gIG5vbk5lZ2F0aXZlSW50ZWdlcjogMSxcbiAgcG9zaXRpdmVJbnRlZ2VyOiAxLFxuICBub25Qb3NpdGl2ZUludGVnZXI6IDEsXG4gIHVuc2lnbmVkQnl0ZTogMSxcbiAgdW5zaWduZWRJbnQ6IDEsXG4gIHVuc2lnbmVkTG9uZzogMSxcbiAgdW5zaWduZWRTaG9ydDogMSxcbiAgZHVyYXRpb246IDAsXG4gIGRhdGVUaW1lOiAwLFxuICB0aW1lOiAwLFxuICBkYXRlOiAwLFxuICBnWWVhck1vbnRoOiAwLFxuICBnWWVhcjogMCxcbiAgZ01vbnRoRGF5OiAwLFxuICBnRGF5OiAwLFxuICBnTW9udGg6IDAsXG4gIGhleEJpbmFyeTogMCxcbiAgYmFzZTY0QmluYXJ5OiAwLFxuICBhbnlVUkk6IDAsXG4gIFFOYW1lOiAwLFxuICBOT1RBVElPTjogMFxufTtcblxuZnVuY3Rpb24gc3BsaXRRTmFtZShuc05hbWUpIHtcbiAgbGV0IGkgPSB0eXBlb2YgbnNOYW1lID09PSAnc3RyaW5nJyA/IG5zTmFtZS5pbmRleE9mKCc6JykgOiAtMTtcbiAgcmV0dXJuIGkgPCAwID8geyBwcmVmaXg6IFROU19QUkVGSVgsIG5hbWU6IG5zTmFtZSB9IDpcbiAgICB7IHByZWZpeDogbnNOYW1lLnN1YnN0cmluZygwLCBpKSwgbmFtZTogbnNOYW1lLnN1YnN0cmluZyhpICsgMSkgfTtcbn1cblxuZnVuY3Rpb24geG1sRXNjYXBlKG9iaikge1xuICBpZiAodHlwZW9mIChvYmopID09PSAnc3RyaW5nJykge1xuICAgIGlmIChvYmouc3Vic3RyKDAsIDkpID09PSAnPCFbQ0RBVEFbJyAmJiBvYmouc3Vic3RyKC0zKSA9PT0gXCJdXT5cIikge1xuICAgICAgcmV0dXJuIG9iajtcbiAgICB9XG4gICAgcmV0dXJuIG9ialxuICAgICAgLnJlcGxhY2UoLyYvZywgJyZhbXA7JylcbiAgICAgIC5yZXBsYWNlKC88L2csICcmbHQ7JylcbiAgICAgIC5yZXBsYWNlKC8+L2csICcmZ3Q7JylcbiAgICAgIC5yZXBsYWNlKC9cIi9nLCAnJnF1b3Q7JylcbiAgICAgIC5yZXBsYWNlKC8nL2csICcmYXBvczsnKTtcbiAgfVxuXG4gIHJldHVybiBvYmo7XG59XG5cbmxldCB0cmltTGVmdCA9IC9eW1xcc1xceEEwXSsvO1xubGV0IHRyaW1SaWdodCA9IC9bXFxzXFx4QTBdKyQvO1xuXG5mdW5jdGlvbiB0cmltKHRleHQpIHtcbiAgcmV0dXJuIHRleHQucmVwbGFjZSh0cmltTGVmdCwgJycpLnJlcGxhY2UodHJpbVJpZ2h0LCAnJyk7XG59XG5cbmZ1bmN0aW9uIGRlZXBNZXJnZShkZXN0aW5hdGlvbiwgc291cmNlKSB7XG4gIHJldHVybiBfLm1lcmdlV2l0aChkZXN0aW5hdGlvbiB8fCB7fSwgc291cmNlLCBmdW5jdGlvbiAoYSwgYikge1xuICAgIHJldHVybiBfLmlzQXJyYXkoYSkgPyBhLmNvbmNhdChiKSA6IHVuZGVmaW5lZDtcbiAgfSk7XG59XG5cbmxldCBFbGVtZW50OiBhbnkgPSBmdW5jdGlvbiAobnNOYW1lLCBhdHRycywgb3B0aW9ucykge1xuICBsZXQgcGFydHMgPSBzcGxpdFFOYW1lKG5zTmFtZSk7XG5cbiAgdGhpcy5uc05hbWUgPSBuc05hbWU7XG4gIHRoaXMucHJlZml4ID0gcGFydHMucHJlZml4O1xuICB0aGlzLm5hbWUgPSBwYXJ0cy5uYW1lO1xuICB0aGlzLmNoaWxkcmVuID0gW107XG4gIHRoaXMueG1sbnMgPSB7fTtcblxuICB0aGlzLl9pbml0aWFsaXplT3B0aW9ucyhvcHRpb25zKTtcblxuICBmb3IgKGxldCBrZXkgaW4gYXR0cnMpIHtcbiAgICBsZXQgbWF0Y2ggPSAvXnhtbG5zOj8oLiopJC8uZXhlYyhrZXkpO1xuICAgIGlmIChtYXRjaCkge1xuICAgICAgdGhpcy54bWxuc1ttYXRjaFsxXSA/IG1hdGNoWzFdIDogVE5TX1BSRUZJWF0gPSBhdHRyc1trZXldO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIGlmIChrZXkgPT09ICd2YWx1ZScpIHtcbiAgICAgICAgdGhpc1t0aGlzLnZhbHVlS2V5XSA9IGF0dHJzW2tleV07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzWyckJyArIGtleV0gPSBhdHRyc1trZXldO1xuICAgICAgfVxuICAgIH1cbiAgfVxuICBpZiAodGhpcy4kdGFyZ2V0TmFtZXNwYWNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAvLyBBZGQgdGFyZ2V0TmFtZXNwYWNlIHRvIHRoZSBtYXBwaW5nXG4gICAgdGhpcy54bWxuc1tUTlNfUFJFRklYXSA9IHRoaXMuJHRhcmdldE5hbWVzcGFjZTtcbiAgfVxufTtcblxuRWxlbWVudC5wcm90b3R5cGUuX2luaXRpYWxpemVPcHRpb25zID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgaWYgKG9wdGlvbnMpIHtcbiAgICB0aGlzLnZhbHVlS2V5ID0gb3B0aW9ucy52YWx1ZUtleSB8fCAnJHZhbHVlJztcbiAgICB0aGlzLnhtbEtleSA9IG9wdGlvbnMueG1sS2V5IHx8ICckeG1sJztcbiAgICB0aGlzLmlnbm9yZWROYW1lc3BhY2VzID0gb3B0aW9ucy5pZ25vcmVkTmFtZXNwYWNlcyB8fCBbXTtcbiAgfSBlbHNlIHtcbiAgICB0aGlzLnZhbHVlS2V5ID0gJyR2YWx1ZSc7XG4gICAgdGhpcy54bWxLZXkgPSAnJHhtbCc7XG4gICAgdGhpcy5pZ25vcmVkTmFtZXNwYWNlcyA9IFtdO1xuICB9XG59O1xuXG5FbGVtZW50LnByb3RvdHlwZS5kZWxldGVGaXhlZEF0dHJzID0gZnVuY3Rpb24gKCkge1xuICB0aGlzLmNoaWxkcmVuICYmIHRoaXMuY2hpbGRyZW4ubGVuZ3RoID09PSAwICYmIGRlbGV0ZSB0aGlzLmNoaWxkcmVuO1xuICB0aGlzLnhtbG5zICYmIE9iamVjdC5rZXlzKHRoaXMueG1sbnMpLmxlbmd0aCA9PT0gMCAmJiBkZWxldGUgdGhpcy54bWxucztcbiAgZGVsZXRlIHRoaXMubnNOYW1lO1xuICBkZWxldGUgdGhpcy5wcmVmaXg7XG4gIGRlbGV0ZSB0aGlzLm5hbWU7XG59O1xuXG5FbGVtZW50LnByb3RvdHlwZS5hbGxvd2VkQ2hpbGRyZW4gPSBbXTtcblxuRWxlbWVudC5wcm90b3R5cGUuc3RhcnRFbGVtZW50ID0gZnVuY3Rpb24gKHN0YWNrLCBuc05hbWUsIGF0dHJzLCBvcHRpb25zKSB7XG4gIGlmICghdGhpcy5hbGxvd2VkQ2hpbGRyZW4pIHtcbiAgICByZXR1cm47XG4gIH1cblxuICBsZXQgQ2hpbGRDbGFzcyA9IHRoaXMuYWxsb3dlZENoaWxkcmVuW3NwbGl0UU5hbWUobnNOYW1lKS5uYW1lXSxcbiAgICBlbGVtZW50ID0gbnVsbDtcblxuICBpZiAoQ2hpbGRDbGFzcykge1xuICAgIHN0YWNrLnB1c2gobmV3IENoaWxkQ2xhc3MobnNOYW1lLCBhdHRycywgb3B0aW9ucykpO1xuICB9XG4gIGVsc2Uge1xuICAgIHRoaXMudW5leHBlY3RlZChuc05hbWUpO1xuICB9XG5cbn07XG5cbkVsZW1lbnQucHJvdG90eXBlLmVuZEVsZW1lbnQgPSBmdW5jdGlvbiAoc3RhY2ssIG5zTmFtZSkge1xuICBpZiAodGhpcy5uc05hbWUgPT09IG5zTmFtZSkge1xuICAgIGlmIChzdGFjay5sZW5ndGggPCAyKVxuICAgICAgcmV0dXJuO1xuICAgIGxldCBwYXJlbnQgPSBzdGFja1tzdGFjay5sZW5ndGggLSAyXTtcbiAgICBpZiAodGhpcyAhPT0gc3RhY2tbMF0pIHtcbiAgICAgIF8uZGVmYXVsdHNEZWVwKHN0YWNrWzBdLnhtbG5zLCB0aGlzLnhtbG5zKTtcbiAgICAgIC8vIGRlbGV0ZSB0aGlzLnhtbG5zO1xuICAgICAgcGFyZW50LmNoaWxkcmVuLnB1c2godGhpcyk7XG4gICAgICBwYXJlbnQuYWRkQ2hpbGQodGhpcyk7XG4gICAgfVxuICAgIHN0YWNrLnBvcCgpO1xuICB9XG59O1xuXG5FbGVtZW50LnByb3RvdHlwZS5hZGRDaGlsZCA9IGZ1bmN0aW9uIChjaGlsZCkge1xuICByZXR1cm47XG59O1xuXG5FbGVtZW50LnByb3RvdHlwZS51bmV4cGVjdGVkID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgdGhyb3cgbmV3IEVycm9yKCdGb3VuZCB1bmV4cGVjdGVkIGVsZW1lbnQgKCcgKyBuYW1lICsgJykgaW5zaWRlICcgKyB0aGlzLm5zTmFtZSk7XG59O1xuXG5FbGVtZW50LnByb3RvdHlwZS5kZXNjcmlwdGlvbiA9IGZ1bmN0aW9uIChkZWZpbml0aW9ucykge1xuICByZXR1cm4gdGhpcy4kbmFtZSB8fCB0aGlzLm5hbWU7XG59O1xuXG5FbGVtZW50LnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24gKCkge1xufTtcblxuRWxlbWVudC5jcmVhdGVTdWJDbGFzcyA9IGZ1bmN0aW9uICgpIHtcbiAgbGV0IHJvb3QgPSB0aGlzO1xuICBsZXQgc3ViRWxlbWVudCA9IGZ1bmN0aW9uICgpIHtcbiAgICByb290LmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgdGhpcy5pbml0KCk7XG4gIH07XG4gIC8vIGluaGVyaXRzKHN1YkVsZW1lbnQsIHJvb3QpO1xuICBzdWJFbGVtZW50LnByb3RvdHlwZS5fX3Byb3RvX18gPSByb290LnByb3RvdHlwZTtcbiAgcmV0dXJuIHN1YkVsZW1lbnQ7XG59O1xuXG5cbmxldCBFbGVtZW50RWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBBbnlFbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xubGV0IElucHV0RWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBPdXRwdXRFbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xubGV0IFNpbXBsZVR5cGVFbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xubGV0IFJlc3RyaWN0aW9uRWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBFeHRlbnNpb25FbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xubGV0IENob2ljZUVsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgRW51bWVyYXRpb25FbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xubGV0IENvbXBsZXhUeXBlRWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBDb21wbGV4Q29udGVudEVsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgU2ltcGxlQ29udGVudEVsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgU2VxdWVuY2VFbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xubGV0IEFsbEVsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgTWVzc2FnZUVsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5sZXQgRG9jdW1lbnRhdGlvbkVsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5cbmxldCBTY2hlbWFFbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xubGV0IFR5cGVzRWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBPcGVyYXRpb25FbGVtZW50ID0gRWxlbWVudC5jcmVhdGVTdWJDbGFzcygpO1xubGV0IFBvcnRUeXBlRWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBCaW5kaW5nRWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBQb3J0RWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBTZXJ2aWNlRWxlbWVudCA9IEVsZW1lbnQuY3JlYXRlU3ViQ2xhc3MoKTtcbmxldCBEZWZpbml0aW9uc0VsZW1lbnQgPSBFbGVtZW50LmNyZWF0ZVN1YkNsYXNzKCk7XG5cbmxldCBFbGVtZW50VHlwZU1hcCA9IHtcbiAgdHlwZXM6IFtUeXBlc0VsZW1lbnQsICdzY2hlbWEgZG9jdW1lbnRhdGlvbiddLFxuICBzY2hlbWE6IFtTY2hlbWFFbGVtZW50LCAnZWxlbWVudCBjb21wbGV4VHlwZSBzaW1wbGVUeXBlIGluY2x1ZGUgaW1wb3J0J10sXG4gIGVsZW1lbnQ6IFtFbGVtZW50RWxlbWVudCwgJ2Fubm90YXRpb24gY29tcGxleFR5cGUnXSxcbiAgYW55OiBbQW55RWxlbWVudCwgJyddLFxuICBzaW1wbGVUeXBlOiBbU2ltcGxlVHlwZUVsZW1lbnQsICdyZXN0cmljdGlvbiddLFxuICByZXN0cmljdGlvbjogW1Jlc3RyaWN0aW9uRWxlbWVudCwgJ2VudW1lcmF0aW9uIGFsbCBjaG9pY2Ugc2VxdWVuY2UnXSxcbiAgZXh0ZW5zaW9uOiBbRXh0ZW5zaW9uRWxlbWVudCwgJ2FsbCBzZXF1ZW5jZSBjaG9pY2UnXSxcbiAgY2hvaWNlOiBbQ2hvaWNlRWxlbWVudCwgJ2VsZW1lbnQgc2VxdWVuY2UgY2hvaWNlIGFueSddLFxuICAvLyBncm91cDogW0dyb3VwRWxlbWVudCwgJ2VsZW1lbnQgZ3JvdXAnXSxcbiAgZW51bWVyYXRpb246IFtFbnVtZXJhdGlvbkVsZW1lbnQsICcnXSxcbiAgY29tcGxleFR5cGU6IFtDb21wbGV4VHlwZUVsZW1lbnQsICdhbm5vdGF0aW9uIHNlcXVlbmNlIGFsbCBjb21wbGV4Q29udGVudCBzaW1wbGVDb250ZW50IGNob2ljZSddLFxuICBjb21wbGV4Q29udGVudDogW0NvbXBsZXhDb250ZW50RWxlbWVudCwgJ2V4dGVuc2lvbiddLFxuICBzaW1wbGVDb250ZW50OiBbU2ltcGxlQ29udGVudEVsZW1lbnQsICdleHRlbnNpb24nXSxcbiAgc2VxdWVuY2U6IFtTZXF1ZW5jZUVsZW1lbnQsICdlbGVtZW50IHNlcXVlbmNlIGNob2ljZSBhbnknXSxcbiAgYWxsOiBbQWxsRWxlbWVudCwgJ2VsZW1lbnQgY2hvaWNlJ10sXG5cbiAgc2VydmljZTogW1NlcnZpY2VFbGVtZW50LCAncG9ydCBkb2N1bWVudGF0aW9uJ10sXG4gIHBvcnQ6IFtQb3J0RWxlbWVudCwgJ2FkZHJlc3MgZG9jdW1lbnRhdGlvbiddLFxuICBiaW5kaW5nOiBbQmluZGluZ0VsZW1lbnQsICdfYmluZGluZyBTZWN1cml0eVNwZWMgb3BlcmF0aW9uIGRvY3VtZW50YXRpb24nXSxcbiAgcG9ydFR5cGU6IFtQb3J0VHlwZUVsZW1lbnQsICdvcGVyYXRpb24gZG9jdW1lbnRhdGlvbiddLFxuICBtZXNzYWdlOiBbTWVzc2FnZUVsZW1lbnQsICdwYXJ0IGRvY3VtZW50YXRpb24nXSxcbiAgb3BlcmF0aW9uOiBbT3BlcmF0aW9uRWxlbWVudCwgJ2RvY3VtZW50YXRpb24gaW5wdXQgb3V0cHV0IGZhdWx0IF9vcGVyYXRpb24nXSxcbiAgaW5wdXQ6IFtJbnB1dEVsZW1lbnQsICdib2R5IFNlY3VyaXR5U3BlY1JlZiBkb2N1bWVudGF0aW9uIGhlYWRlciddLFxuICBvdXRwdXQ6IFtPdXRwdXRFbGVtZW50LCAnYm9keSBTZWN1cml0eVNwZWNSZWYgZG9jdW1lbnRhdGlvbiBoZWFkZXInXSxcbiAgZmF1bHQ6IFtFbGVtZW50LCAnX2ZhdWx0IGRvY3VtZW50YXRpb24nXSxcbiAgZGVmaW5pdGlvbnM6IFtEZWZpbml0aW9uc0VsZW1lbnQsICd0eXBlcyBtZXNzYWdlIHBvcnRUeXBlIGJpbmRpbmcgc2VydmljZSBpbXBvcnQgZG9jdW1lbnRhdGlvbiddLFxuICBkb2N1bWVudGF0aW9uOiBbRG9jdW1lbnRhdGlvbkVsZW1lbnQsICcnXVxufTtcblxuZnVuY3Rpb24gbWFwRWxlbWVudFR5cGVzKHR5cGVzKSB7XG4gIGxldCBydG4gPSB7fTtcbiAgdHlwZXMgPSB0eXBlcy5zcGxpdCgnICcpO1xuICB0eXBlcy5mb3JFYWNoKGZ1bmN0aW9uICh0eXBlKSB7XG4gICAgcnRuW3R5cGUucmVwbGFjZSgvXl8vLCAnJyldID0gKEVsZW1lbnRUeXBlTWFwW3R5cGVdIHx8IFtFbGVtZW50XSlbMF07XG4gIH0pO1xuICByZXR1cm4gcnRuO1xufVxuXG5mb3IgKGxldCBuIGluIEVsZW1lbnRUeXBlTWFwKSB7XG4gIGxldCB2ID0gRWxlbWVudFR5cGVNYXBbbl07XG4gIHZbMF0ucHJvdG90eXBlLmFsbG93ZWRDaGlsZHJlbiA9IG1hcEVsZW1lbnRUeXBlcyh2WzFdKTtcbn1cblxuTWVzc2FnZUVsZW1lbnQucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiAoKSB7XG4gIHRoaXMuZWxlbWVudCA9IG51bGw7XG4gIHRoaXMucGFydHMgPSBudWxsO1xufTtcblxuU2NoZW1hRWxlbWVudC5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcbiAgdGhpcy5jb21wbGV4VHlwZXMgPSB7fTtcbiAgdGhpcy50eXBlcyA9IHt9O1xuICB0aGlzLmVsZW1lbnRzID0ge307XG4gIHRoaXMuaW5jbHVkZXMgPSBbXTtcbn07XG5cblR5cGVzRWxlbWVudC5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcbiAgdGhpcy5zY2hlbWFzID0ge307XG59O1xuXG5PcGVyYXRpb25FbGVtZW50LnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24gKCkge1xuICB0aGlzLmlucHV0ID0gbnVsbDtcbiAgdGhpcy5vdXRwdXQgPSBudWxsO1xuICB0aGlzLmlucHV0U29hcCA9IG51bGw7XG4gIHRoaXMub3V0cHV0U29hcCA9IG51bGw7XG4gIHRoaXMuc3R5bGUgPSAnJztcbiAgdGhpcy5zb2FwQWN0aW9uID0gJyc7XG59O1xuXG5Qb3J0VHlwZUVsZW1lbnQucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiAoKSB7XG4gIHRoaXMubWV0aG9kcyA9IHt9O1xufTtcblxuQmluZGluZ0VsZW1lbnQucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiAoKSB7XG4gIHRoaXMudHJhbnNwb3J0ID0gJyc7XG4gIHRoaXMuc3R5bGUgPSAnJztcbiAgdGhpcy5tZXRob2RzID0ge307XG59O1xuXG5Qb3J0RWxlbWVudC5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcbiAgdGhpcy5sb2NhdGlvbiA9IG51bGw7XG59O1xuXG5TZXJ2aWNlRWxlbWVudC5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcbiAgdGhpcy5wb3J0cyA9IHt9O1xufTtcblxuRGVmaW5pdGlvbnNFbGVtZW50LnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24gKCkge1xuICBpZiAodGhpcy5uYW1lICE9PSAnZGVmaW5pdGlvbnMnKSB0aGlzLnVuZXhwZWN0ZWQodGhpcy5uc05hbWUpO1xuICB0aGlzLm1lc3NhZ2VzID0ge307XG4gIHRoaXMucG9ydFR5cGVzID0ge307XG4gIHRoaXMuYmluZGluZ3MgPSB7fTtcbiAgdGhpcy5zZXJ2aWNlcyA9IHt9O1xuICB0aGlzLnNjaGVtYXMgPSB7fTtcbn07XG5cbkRvY3VtZW50YXRpb25FbGVtZW50LnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24gKCkge1xufTtcblxuU2NoZW1hRWxlbWVudC5wcm90b3R5cGUubWVyZ2UgPSBmdW5jdGlvbiAoc291cmNlKSB7XG4gIGFzc2VydChzb3VyY2UgaW5zdGFuY2VvZiBTY2hlbWFFbGVtZW50KTtcbiAgaWYgKHRoaXMuJHRhcmdldE5hbWVzcGFjZSA9PT0gc291cmNlLiR0YXJnZXROYW1lc3BhY2UpIHtcbiAgICBfLm1lcmdlKHRoaXMuY29tcGxleFR5cGVzLCBzb3VyY2UuY29tcGxleFR5cGVzKTtcbiAgICBfLm1lcmdlKHRoaXMudHlwZXMsIHNvdXJjZS50eXBlcyk7XG4gICAgXy5tZXJnZSh0aGlzLmVsZW1lbnRzLCBzb3VyY2UuZWxlbWVudHMpO1xuICAgIF8ubWVyZ2UodGhpcy54bWxucywgc291cmNlLnhtbG5zKTtcbiAgfVxuICByZXR1cm4gdGhpcztcbn07XG5cblxuU2NoZW1hRWxlbWVudC5wcm90b3R5cGUuYWRkQ2hpbGQgPSBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgaWYgKGNoaWxkLiRuYW1lIGluIFByaW1pdGl2ZXMpXG4gICAgcmV0dXJuO1xuICBpZiAoY2hpbGQubmFtZSA9PT0gJ2luY2x1ZGUnIHx8IGNoaWxkLm5hbWUgPT09ICdpbXBvcnQnKSB7XG4gICAgbGV0IGxvY2F0aW9uID0gY2hpbGQuJHNjaGVtYUxvY2F0aW9uIHx8IGNoaWxkLiRsb2NhdGlvbjtcbiAgICBpZiAobG9jYXRpb24pIHtcbiAgICAgIHRoaXMuaW5jbHVkZXMucHVzaCh7XG4gICAgICAgIG5hbWVzcGFjZTogY2hpbGQuJG5hbWVzcGFjZSB8fCBjaGlsZC4kdGFyZ2V0TmFtZXNwYWNlIHx8IHRoaXMuJHRhcmdldE5hbWVzcGFjZSxcbiAgICAgICAgbG9jYXRpb246IGxvY2F0aW9uXG4gICAgICB9KTtcbiAgICB9XG4gIH1cbiAgZWxzZSBpZiAoY2hpbGQubmFtZSA9PT0gJ2NvbXBsZXhUeXBlJykge1xuICAgIHRoaXMuY29tcGxleFR5cGVzW2NoaWxkLiRuYW1lXSA9IGNoaWxkO1xuICB9XG4gIGVsc2UgaWYgKGNoaWxkLm5hbWUgPT09ICdlbGVtZW50Jykge1xuICAgIHRoaXMuZWxlbWVudHNbY2hpbGQuJG5hbWVdID0gY2hpbGQ7XG4gIH1cbiAgZWxzZSBpZiAoY2hpbGQuJG5hbWUpIHtcbiAgICB0aGlzLnR5cGVzW2NoaWxkLiRuYW1lXSA9IGNoaWxkO1xuICB9XG4gIHRoaXMuY2hpbGRyZW4ucG9wKCk7XG4gIC8vIGNoaWxkLmRlbGV0ZUZpeGVkQXR0cnMoKTtcbn07XG4vL2ZpeCMzMjVcblR5cGVzRWxlbWVudC5wcm90b3R5cGUuYWRkQ2hpbGQgPSBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgYXNzZXJ0KGNoaWxkIGluc3RhbmNlb2YgU2NoZW1hRWxlbWVudCk7XG5cbiAgbGV0IHRhcmdldE5hbWVzcGFjZSA9IGNoaWxkLiR0YXJnZXROYW1lc3BhY2U7XG5cbiAgaWYgKCF0aGlzLnNjaGVtYXMuaGFzT3duUHJvcGVydHkodGFyZ2V0TmFtZXNwYWNlKSkge1xuICAgIHRoaXMuc2NoZW1hc1t0YXJnZXROYW1lc3BhY2VdID0gY2hpbGQ7XG4gIH0gZWxzZSB7XG4gICAgY29uc29sZS5lcnJvcignVGFyZ2V0LU5hbWVzcGFjZSBcIicgKyB0YXJnZXROYW1lc3BhY2UgKyAnXCIgYWxyZWFkeSBpbiB1c2UgYnkgYW5vdGhlciBTY2hlbWEhJyk7XG4gIH1cbn07XG5cbklucHV0RWxlbWVudC5wcm90b3R5cGUuYWRkQ2hpbGQgPSBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgaWYgKGNoaWxkLm5hbWUgPT09ICdib2R5Jykge1xuICAgIHRoaXMudXNlID0gY2hpbGQuJHVzZTtcbiAgICBpZiAodGhpcy51c2UgPT09ICdlbmNvZGVkJykge1xuICAgICAgdGhpcy5lbmNvZGluZ1N0eWxlID0gY2hpbGQuJGVuY29kaW5nU3R5bGU7XG4gICAgfVxuICAgIHRoaXMuY2hpbGRyZW4ucG9wKCk7XG4gIH1cbn07XG5cbk91dHB1dEVsZW1lbnQucHJvdG90eXBlLmFkZENoaWxkID0gZnVuY3Rpb24gKGNoaWxkKSB7XG4gIGlmIChjaGlsZC5uYW1lID09PSAnYm9keScpIHtcbiAgICB0aGlzLnVzZSA9IGNoaWxkLiR1c2U7XG4gICAgaWYgKHRoaXMudXNlID09PSAnZW5jb2RlZCcpIHtcbiAgICAgIHRoaXMuZW5jb2RpbmdTdHlsZSA9IGNoaWxkLiRlbmNvZGluZ1N0eWxlO1xuICAgIH1cbiAgICB0aGlzLmNoaWxkcmVuLnBvcCgpO1xuICB9XG59O1xuXG5PcGVyYXRpb25FbGVtZW50LnByb3RvdHlwZS5hZGRDaGlsZCA9IGZ1bmN0aW9uIChjaGlsZCkge1xuICBpZiAoY2hpbGQubmFtZSA9PT0gJ29wZXJhdGlvbicpIHtcbiAgICB0aGlzLnNvYXBBY3Rpb24gPSBjaGlsZC4kc29hcEFjdGlvbiB8fCAnJztcbiAgICB0aGlzLnN0eWxlID0gY2hpbGQuJHN0eWxlIHx8ICcnO1xuICAgIHRoaXMuY2hpbGRyZW4ucG9wKCk7XG4gIH1cbn07XG5cbkJpbmRpbmdFbGVtZW50LnByb3RvdHlwZS5hZGRDaGlsZCA9IGZ1bmN0aW9uIChjaGlsZCkge1xuICBpZiAoY2hpbGQubmFtZSA9PT0gJ2JpbmRpbmcnKSB7XG4gICAgdGhpcy50cmFuc3BvcnQgPSBjaGlsZC4kdHJhbnNwb3J0O1xuICAgIHRoaXMuc3R5bGUgPSBjaGlsZC4kc3R5bGU7XG4gICAgdGhpcy5jaGlsZHJlbi5wb3AoKTtcbiAgfVxufTtcblxuUG9ydEVsZW1lbnQucHJvdG90eXBlLmFkZENoaWxkID0gZnVuY3Rpb24gKGNoaWxkKSB7XG4gIGlmIChjaGlsZC5uYW1lID09PSAnYWRkcmVzcycgJiYgdHlwZW9mIChjaGlsZC4kbG9jYXRpb24pICE9PSAndW5kZWZpbmVkJykge1xuICAgIHRoaXMubG9jYXRpb24gPSBjaGlsZC4kbG9jYXRpb247XG4gIH1cbn07XG5cbkRlZmluaXRpb25zRWxlbWVudC5wcm90b3R5cGUuYWRkQ2hpbGQgPSBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgbGV0IHNlbGYgPSB0aGlzO1xuICBpZiAoY2hpbGQgaW5zdGFuY2VvZiBUeXBlc0VsZW1lbnQpIHtcbiAgICAvLyBNZXJnZSB0eXBlcy5zY2hlbWFzIGludG8gZGVmaW5pdGlvbnMuc2NoZW1hc1xuICAgIF8ubWVyZ2Uoc2VsZi5zY2hlbWFzLCBjaGlsZC5zY2hlbWFzKTtcbiAgfVxuICBlbHNlIGlmIChjaGlsZCBpbnN0YW5jZW9mIE1lc3NhZ2VFbGVtZW50KSB7XG4gICAgc2VsZi5tZXNzYWdlc1tjaGlsZC4kbmFtZV0gPSBjaGlsZDtcbiAgfVxuICBlbHNlIGlmIChjaGlsZC5uYW1lID09PSAnaW1wb3J0Jykge1xuICAgIHNlbGYuc2NoZW1hc1tjaGlsZC4kbmFtZXNwYWNlXSA9IG5ldyBTY2hlbWFFbGVtZW50KGNoaWxkLiRuYW1lc3BhY2UsIHt9KTtcbiAgICBzZWxmLnNjaGVtYXNbY2hpbGQuJG5hbWVzcGFjZV0uYWRkQ2hpbGQoY2hpbGQpO1xuICB9XG4gIGVsc2UgaWYgKGNoaWxkIGluc3RhbmNlb2YgUG9ydFR5cGVFbGVtZW50KSB7XG4gICAgc2VsZi5wb3J0VHlwZXNbY2hpbGQuJG5hbWVdID0gY2hpbGQ7XG4gIH1cbiAgZWxzZSBpZiAoY2hpbGQgaW5zdGFuY2VvZiBCaW5kaW5nRWxlbWVudCkge1xuICAgIGlmIChjaGlsZC50cmFuc3BvcnQgPT09ICdodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy9zb2FwL2h0dHAnIHx8XG4gICAgICBjaGlsZC50cmFuc3BvcnQgPT09ICdodHRwOi8vd3d3LnczLm9yZy8yMDAzLzA1L3NvYXAvYmluZGluZ3MvSFRUUC8nKVxuICAgICAgc2VsZi5iaW5kaW5nc1tjaGlsZC4kbmFtZV0gPSBjaGlsZDtcbiAgfVxuICBlbHNlIGlmIChjaGlsZCBpbnN0YW5jZW9mIFNlcnZpY2VFbGVtZW50KSB7XG4gICAgc2VsZi5zZXJ2aWNlc1tjaGlsZC4kbmFtZV0gPSBjaGlsZDtcbiAgfVxuICBlbHNlIGlmIChjaGlsZCBpbnN0YW5jZW9mIERvY3VtZW50YXRpb25FbGVtZW50KSB7XG4gIH1cbiAgdGhpcy5jaGlsZHJlbi5wb3AoKTtcbn07XG5cbk1lc3NhZ2VFbGVtZW50LnByb3RvdHlwZS5wb3N0UHJvY2VzcyA9IGZ1bmN0aW9uIChkZWZpbml0aW9ucykge1xuICBsZXQgcGFydCA9IG51bGw7XG4gIGxldCBjaGlsZCA9IHVuZGVmaW5lZDtcbiAgbGV0IGNoaWxkcmVuID0gdGhpcy5jaGlsZHJlbiB8fCBbXTtcbiAgbGV0IG5zID0gdW5kZWZpbmVkO1xuICBsZXQgbnNOYW1lID0gdW5kZWZpbmVkO1xuICBsZXQgaSA9IHVuZGVmaW5lZDtcbiAgbGV0IHR5cGUgPSB1bmRlZmluZWQ7XG5cbiAgZm9yIChpIGluIGNoaWxkcmVuKSB7XG4gICAgaWYgKChjaGlsZCA9IGNoaWxkcmVuW2ldKS5uYW1lID09PSAncGFydCcpIHtcbiAgICAgIHBhcnQgPSBjaGlsZDtcbiAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuXG4gIGlmICghcGFydCkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIGlmIChwYXJ0LiRlbGVtZW50KSB7XG4gICAgbGV0IGxvb2t1cFR5cGVzID0gW10sXG4gICAgICBlbGVtZW50Q2hpbGRyZW47XG5cbiAgICBkZWxldGUgdGhpcy5wYXJ0cztcblxuICAgIG5zTmFtZSA9IHNwbGl0UU5hbWUocGFydC4kZWxlbWVudCk7XG4gICAgbnMgPSBuc05hbWUucHJlZml4O1xuICAgIGxldCBzY2hlbWEgPSBkZWZpbml0aW9ucy5zY2hlbWFzW2RlZmluaXRpb25zLnhtbG5zW25zXV07XG4gICAgdGhpcy5lbGVtZW50ID0gc2NoZW1hLmVsZW1lbnRzW25zTmFtZS5uYW1lXTtcbiAgICBpZiAoIXRoaXMuZWxlbWVudCkge1xuICAgICAgLy8gZGVidWcobnNOYW1lLm5hbWUgKyBcIiBpcyBub3QgcHJlc2VudCBpbiB3c2RsIGFuZCBjYW5ub3QgYmUgcHJvY2Vzc2VkIGNvcnJlY3RseS5cIik7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMuZWxlbWVudC50YXJnZXROU0FsaWFzID0gbnM7XG4gICAgdGhpcy5lbGVtZW50LnRhcmdldE5hbWVzcGFjZSA9IGRlZmluaXRpb25zLnhtbG5zW25zXTtcblxuICAgIC8vIHNldCB0aGUgb3B0aW9uYWwgJGxvb2t1cFR5cGUgdG8gYmUgdXNlZCB3aXRoaW4gYGNsaWVudCNfaW52b2tlKClgIHdoZW5cbiAgICAvLyBjYWxsaW5nIGB3c2RsI29iamVjdFRvRG9jdW1lbnRYTUwoKVxuICAgIHRoaXMuZWxlbWVudC4kbG9va3VwVHlwZSA9IHBhcnQuJGVsZW1lbnQ7XG5cbiAgICBlbGVtZW50Q2hpbGRyZW4gPSB0aGlzLmVsZW1lbnQuY2hpbGRyZW47XG5cbiAgICAvLyBnZXQgYWxsIG5lc3RlZCBsb29rdXAgdHlwZXMgKG9ubHkgY29tcGxleCB0eXBlcyBhcmUgZm9sbG93ZWQpXG4gICAgaWYgKGVsZW1lbnRDaGlsZHJlbi5sZW5ndGggPiAwKSB7XG4gICAgICBmb3IgKGkgPSAwOyBpIDwgZWxlbWVudENoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGxvb2t1cFR5cGVzLnB1c2godGhpcy5fZ2V0TmVzdGVkTG9va3VwVHlwZVN0cmluZyhlbGVtZW50Q2hpbGRyZW5baV0pKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBpZiBuZXN0ZWQgbG9va3VwIHR5cGVzIHdoZXJlIGZvdW5kLCBwcmVwYXJlIHRoZW0gZm9yIGZ1cnRlciB1c2FnZVxuICAgIGlmIChsb29rdXBUeXBlcy5sZW5ndGggPiAwKSB7XG4gICAgICBsb29rdXBUeXBlcyA9IGxvb2t1cFR5cGVzLlxuICAgICAgICBqb2luKCdfJykuXG4gICAgICAgIHNwbGl0KCdfJykuXG4gICAgICAgIGZpbHRlcihmdW5jdGlvbiByZW1vdmVFbXB0eUxvb2t1cFR5cGVzKHR5cGUpIHtcbiAgICAgICAgICByZXR1cm4gdHlwZSAhPT0gJ14nO1xuICAgICAgICB9KTtcblxuICAgICAgbGV0IHNjaGVtYVhtbG5zID0gZGVmaW5pdGlvbnMuc2NoZW1hc1t0aGlzLmVsZW1lbnQudGFyZ2V0TmFtZXNwYWNlXS54bWxucztcblxuICAgICAgZm9yIChpID0gMDsgaSA8IGxvb2t1cFR5cGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGxvb2t1cFR5cGVzW2ldID0gdGhpcy5fY3JlYXRlTG9va3VwVHlwZU9iamVjdChsb29rdXBUeXBlc1tpXSwgc2NoZW1hWG1sbnMpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMuZWxlbWVudC4kbG9va3VwVHlwZXMgPSBsb29rdXBUeXBlcztcblxuICAgIGlmICh0aGlzLmVsZW1lbnQuJHR5cGUpIHtcbiAgICAgIHR5cGUgPSBzcGxpdFFOYW1lKHRoaXMuZWxlbWVudC4kdHlwZSk7XG4gICAgICBsZXQgdHlwZU5zID0gc2NoZW1hLnhtbG5zICYmIHNjaGVtYS54bWxuc1t0eXBlLnByZWZpeF0gfHwgZGVmaW5pdGlvbnMueG1sbnNbdHlwZS5wcmVmaXhdO1xuXG4gICAgICBpZiAodHlwZU5zKSB7XG4gICAgICAgIGlmICh0eXBlLm5hbWUgaW4gUHJpbWl0aXZlcykge1xuICAgICAgICAgIC8vIHRoaXMuZWxlbWVudCA9IHRoaXMuZWxlbWVudC4kdHlwZTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAvLyBmaXJzdCBjaGVjayBsb2NhbCBtYXBwaW5nIG9mIG5zIGFsaWFzIHRvIG5hbWVzcGFjZVxuICAgICAgICAgIHNjaGVtYSA9IGRlZmluaXRpb25zLnNjaGVtYXNbdHlwZU5zXTtcbiAgICAgICAgICBsZXQgY3R5cGUgPSBzY2hlbWEuY29tcGxleFR5cGVzW3R5cGUubmFtZV0gfHwgc2NoZW1hLnR5cGVzW3R5cGUubmFtZV0gfHwgc2NoZW1hLmVsZW1lbnRzW3R5cGUubmFtZV07XG5cblxuICAgICAgICAgIGlmIChjdHlwZSkge1xuICAgICAgICAgICAgdGhpcy5wYXJ0cyA9IGN0eXBlLmRlc2NyaXB0aW9uKGRlZmluaXRpb25zLCBzY2hlbWEueG1sbnMpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIGxldCBtZXRob2QgPSB0aGlzLmVsZW1lbnQuZGVzY3JpcHRpb24oZGVmaW5pdGlvbnMsIHNjaGVtYS54bWxucyk7XG4gICAgICB0aGlzLnBhcnRzID0gbWV0aG9kW25zTmFtZS5uYW1lXTtcbiAgICB9XG5cblxuICAgIHRoaXMuY2hpbGRyZW4uc3BsaWNlKDAsIDEpO1xuICB9IGVsc2Uge1xuICAgIC8vIHJwYyBlbmNvZGluZ1xuICAgIHRoaXMucGFydHMgPSB7fTtcbiAgICBkZWxldGUgdGhpcy5lbGVtZW50O1xuICAgIGZvciAoaSA9IDA7IHBhcnQgPSB0aGlzLmNoaWxkcmVuW2ldOyBpKyspIHtcbiAgICAgIGlmIChwYXJ0Lm5hbWUgPT09ICdkb2N1bWVudGF0aW9uJykge1xuICAgICAgICAvLyA8d3NkbDpkb2N1bWVudGF0aW9uIGNhbiBiZSBwcmVzZW50IHVuZGVyIDx3c2RsOm1lc3NhZ2U+XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuICAgICAgYXNzZXJ0KHBhcnQubmFtZSA9PT0gJ3BhcnQnLCAnRXhwZWN0ZWQgcGFydCBlbGVtZW50Jyk7XG4gICAgICBuc05hbWUgPSBzcGxpdFFOYW1lKHBhcnQuJHR5cGUpO1xuICAgICAgbnMgPSBkZWZpbml0aW9ucy54bWxuc1tuc05hbWUucHJlZml4XTtcbiAgICAgIHR5cGUgPSBuc05hbWUubmFtZTtcbiAgICAgIGxldCBzY2hlbWFEZWZpbml0aW9uID0gZGVmaW5pdGlvbnMuc2NoZW1hc1tuc107XG4gICAgICBpZiAodHlwZW9mIHNjaGVtYURlZmluaXRpb24gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHRoaXMucGFydHNbcGFydC4kbmFtZV0gPSBkZWZpbml0aW9ucy5zY2hlbWFzW25zXS50eXBlc1t0eXBlXSB8fCBkZWZpbml0aW9ucy5zY2hlbWFzW25zXS5jb21wbGV4VHlwZXNbdHlwZV07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnBhcnRzW3BhcnQuJG5hbWVdID0gcGFydC4kdHlwZTtcbiAgICAgIH1cblxuICAgICAgaWYgKHR5cGVvZiB0aGlzLnBhcnRzW3BhcnQuJG5hbWVdID09PSAnb2JqZWN0Jykge1xuICAgICAgICB0aGlzLnBhcnRzW3BhcnQuJG5hbWVdLnByZWZpeCA9IG5zTmFtZS5wcmVmaXg7XG4gICAgICAgIHRoaXMucGFydHNbcGFydC4kbmFtZV0ueG1sbnMgPSBucztcbiAgICAgIH1cblxuICAgICAgdGhpcy5jaGlsZHJlbi5zcGxpY2UoaS0tLCAxKTtcbiAgICB9XG4gIH1cbiAgdGhpcy5kZWxldGVGaXhlZEF0dHJzKCk7XG59O1xuXG4vKipcbiAqIFRha2VzIGEgZ2l2ZW4gbmFtZXNwYWNlZCBTdHJpbmcoZm9yIGV4YW1wbGU6ICdhbGlhczpwcm9wZXJ0eScpIGFuZCBjcmVhdGVzIGEgbG9va3VwVHlwZVxuICogb2JqZWN0IGZvciBmdXJ0aGVyIHVzZSBpbiBhcyBmaXJzdCAobG9va3VwKSBgcGFyYW1ldGVyVHlwZU9iamAgd2l0aGluIHRoZSBgb2JqZWN0VG9YTUxgXG4gKiBtZXRob2QgYW5kIHByb3ZpZGVzIGFuIGVudHJ5IHBvaW50IGZvciB0aGUgYWxyZWFkeSBleGlzdGluZyBjb2RlIGluIGBmaW5kQ2hpbGRTY2hlbWFPYmplY3RgLlxuICpcbiAqIEBtZXRob2QgX2NyZWF0ZUxvb2t1cFR5cGVPYmplY3RcbiAqIEBwYXJhbSB7U3RyaW5nfSAgICAgICAgICAgIG5zU3RyaW5nICAgICAgICAgIFRoZSBOUyBTdHJpbmcgKGZvciBleGFtcGxlIFwiYWxpYXM6dHlwZVwiKS5cbiAqIEBwYXJhbSB7T2JqZWN0fSAgICAgICAgICAgIHhtbG5zICAgICAgIFRoZSBmdWxseSBwYXJzZWQgYHdzZGxgIGRlZmluaXRpb25zIG9iamVjdCAoaW5jbHVkaW5nIGFsbCBzY2hlbWFzKS5cbiAqIEByZXR1cm5zIHtPYmplY3R9XG4gKiBAcHJpdmF0ZVxuICovXG5NZXNzYWdlRWxlbWVudC5wcm90b3R5cGUuX2NyZWF0ZUxvb2t1cFR5cGVPYmplY3QgPSBmdW5jdGlvbiAobnNTdHJpbmcsIHhtbG5zKSB7XG4gIGxldCBzcGxpdHRlZE5TU3RyaW5nID0gc3BsaXRRTmFtZShuc1N0cmluZyksXG4gICAgbnNBbGlhcyA9IHNwbGl0dGVkTlNTdHJpbmcucHJlZml4LFxuICAgIHNwbGl0dGVkTmFtZSA9IHNwbGl0dGVkTlNTdHJpbmcubmFtZS5zcGxpdCgnIycpLFxuICAgIHR5cGUgPSBzcGxpdHRlZE5hbWVbMF0sXG4gICAgbmFtZSA9IHNwbGl0dGVkTmFtZVsxXSxcbiAgICBsb29rdXBUeXBlT2JqOiBhbnkgPSB7fTtcblxuICBsb29rdXBUeXBlT2JqLiRuYW1lc3BhY2UgPSB4bWxuc1tuc0FsaWFzXTtcbiAgbG9va3VwVHlwZU9iai4kdHlwZSA9IG5zQWxpYXMgKyAnOicgKyB0eXBlO1xuICBsb29rdXBUeXBlT2JqLiRuYW1lID0gbmFtZTtcblxuICByZXR1cm4gbG9va3VwVHlwZU9iajtcbn07XG5cbi8qKlxuICogSXRlcmF0ZXMgdGhyb3VnaCB0aGUgZWxlbWVudCBhbmQgZXZlcnkgbmVzdGVkIGNoaWxkIHRvIGZpbmQgYW55IGRlZmluZWQgYCR0eXBlYFxuICogcHJvcGVydHkgYW5kIHJldHVybnMgaXQgaW4gYSB1bmRlcnNjb3JlICgnXycpIHNlcGFyYXRlZCBTdHJpbmcgKHVzaW5nICdeJyBhcyBkZWZhdWx0XG4gKiB2YWx1ZSBpZiBubyBgJHR5cGVgIHByb3BlcnR5IHdhcyBmb3VuZCkuXG4gKlxuICogQG1ldGhvZCBfZ2V0TmVzdGVkTG9va3VwVHlwZVN0cmluZ1xuICogQHBhcmFtIHtPYmplY3R9ICAgICAgICAgICAgZWxlbWVudCAgICAgICAgIFRoZSBlbGVtZW50IHdoaWNoIChwcm9iYWJseSkgY29udGFpbnMgbmVzdGVkIGAkdHlwZWAgdmFsdWVzLlxuICogQHJldHVybnMge1N0cmluZ31cbiAqIEBwcml2YXRlXG4gKi9cbk1lc3NhZ2VFbGVtZW50LnByb3RvdHlwZS5fZ2V0TmVzdGVkTG9va3VwVHlwZVN0cmluZyA9IGZ1bmN0aW9uIChlbGVtZW50KSB7XG4gIGxldCByZXNvbHZlZFR5cGUgPSAnXicsXG4gICAgZXhjbHVkZWQgPSB0aGlzLmlnbm9yZWROYW1lc3BhY2VzLmNvbmNhdCgneHMnKTsgLy8gZG8gbm90IHByb2Nlc3MgJHR5cGUgdmFsdWVzIHdpY2ggc3RhcnQgd2l0aFxuXG4gIGlmIChlbGVtZW50Lmhhc093blByb3BlcnR5KCckdHlwZScpICYmIHR5cGVvZiBlbGVtZW50LiR0eXBlID09PSAnc3RyaW5nJykge1xuICAgIGlmIChleGNsdWRlZC5pbmRleE9mKGVsZW1lbnQuJHR5cGUuc3BsaXQoJzonKVswXSkgPT09IC0xKSB7XG4gICAgICByZXNvbHZlZFR5cGUgKz0gKCdfJyArIGVsZW1lbnQuJHR5cGUgKyAnIycgKyBlbGVtZW50LiRuYW1lKTtcbiAgICB9XG4gIH1cblxuICBpZiAoZWxlbWVudC5jaGlsZHJlbi5sZW5ndGggPiAwKSB7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuXG4gICAgZWxlbWVudC5jaGlsZHJlbi5mb3JFYWNoKGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICAgbGV0IHJlc29sdmVkQ2hpbGRUeXBlID0gc2VsZi5fZ2V0TmVzdGVkTG9va3VwVHlwZVN0cmluZyhjaGlsZCkucmVwbGFjZSgvXFxeXy8sICcnKTtcblxuICAgICAgaWYgKHJlc29sdmVkQ2hpbGRUeXBlICYmIHR5cGVvZiByZXNvbHZlZENoaWxkVHlwZSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgcmVzb2x2ZWRUeXBlICs9ICgnXycgKyByZXNvbHZlZENoaWxkVHlwZSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICByZXR1cm4gcmVzb2x2ZWRUeXBlO1xufTtcblxuT3BlcmF0aW9uRWxlbWVudC5wcm90b3R5cGUucG9zdFByb2Nlc3MgPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMsIHRhZykge1xuICBsZXQgY2hpbGRyZW4gPSB0aGlzLmNoaWxkcmVuO1xuICBmb3IgKGxldCBpID0gMCwgY2hpbGQ7IGNoaWxkID0gY2hpbGRyZW5baV07IGkrKykge1xuICAgIGlmIChjaGlsZC5uYW1lICE9PSAnaW5wdXQnICYmIGNoaWxkLm5hbWUgIT09ICdvdXRwdXQnKVxuICAgICAgY29udGludWU7XG4gICAgaWYgKHRhZyA9PT0gJ2JpbmRpbmcnKSB7XG4gICAgICB0aGlzW2NoaWxkLm5hbWVdID0gY2hpbGQ7XG4gICAgICBjaGlsZHJlbi5zcGxpY2UoaS0tLCAxKTtcbiAgICAgIGNvbnRpbnVlO1xuICAgIH1cbiAgICBsZXQgbWVzc2FnZU5hbWUgPSBzcGxpdFFOYW1lKGNoaWxkLiRtZXNzYWdlKS5uYW1lO1xuICAgIGxldCBtZXNzYWdlID0gZGVmaW5pdGlvbnMubWVzc2FnZXNbbWVzc2FnZU5hbWVdO1xuICAgIG1lc3NhZ2UucG9zdFByb2Nlc3MoZGVmaW5pdGlvbnMpO1xuICAgIGlmIChtZXNzYWdlLmVsZW1lbnQpIHtcbiAgICAgIGRlZmluaXRpb25zLm1lc3NhZ2VzW21lc3NhZ2UuZWxlbWVudC4kbmFtZV0gPSBtZXNzYWdlO1xuICAgICAgdGhpc1tjaGlsZC5uYW1lXSA9IG1lc3NhZ2UuZWxlbWVudDtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICB0aGlzW2NoaWxkLm5hbWVdID0gbWVzc2FnZTtcbiAgICB9XG4gICAgY2hpbGRyZW4uc3BsaWNlKGktLSwgMSk7XG4gIH1cbiAgdGhpcy5kZWxldGVGaXhlZEF0dHJzKCk7XG59O1xuXG5Qb3J0VHlwZUVsZW1lbnQucHJvdG90eXBlLnBvc3RQcm9jZXNzID0gZnVuY3Rpb24gKGRlZmluaXRpb25zKSB7XG4gIGxldCBjaGlsZHJlbiA9IHRoaXMuY2hpbGRyZW47XG4gIGlmICh0eXBlb2YgY2hpbGRyZW4gPT09ICd1bmRlZmluZWQnKVxuICAgIHJldHVybjtcbiAgZm9yIChsZXQgaSA9IDAsIGNoaWxkOyBjaGlsZCA9IGNoaWxkcmVuW2ldOyBpKyspIHtcbiAgICBpZiAoY2hpbGQubmFtZSAhPT0gJ29wZXJhdGlvbicpXG4gICAgICBjb250aW51ZTtcbiAgICBjaGlsZC5wb3N0UHJvY2VzcyhkZWZpbml0aW9ucywgJ3BvcnRUeXBlJyk7XG4gICAgdGhpcy5tZXRob2RzW2NoaWxkLiRuYW1lXSA9IGNoaWxkO1xuICAgIGNoaWxkcmVuLnNwbGljZShpLS0sIDEpO1xuICB9XG4gIGRlbGV0ZSB0aGlzLiRuYW1lO1xuICB0aGlzLmRlbGV0ZUZpeGVkQXR0cnMoKTtcbn07XG5cbkJpbmRpbmdFbGVtZW50LnByb3RvdHlwZS5wb3N0UHJvY2VzcyA9IGZ1bmN0aW9uIChkZWZpbml0aW9ucykge1xuICBsZXQgdHlwZSA9IHNwbGl0UU5hbWUodGhpcy4kdHlwZSkubmFtZSxcbiAgICBwb3J0VHlwZSA9IGRlZmluaXRpb25zLnBvcnRUeXBlc1t0eXBlXSxcbiAgICBzdHlsZSA9IHRoaXMuc3R5bGUsXG4gICAgY2hpbGRyZW4gPSB0aGlzLmNoaWxkcmVuO1xuICBpZiAocG9ydFR5cGUpIHtcbiAgICBwb3J0VHlwZS5wb3N0UHJvY2VzcyhkZWZpbml0aW9ucyk7XG4gICAgdGhpcy5tZXRob2RzID0gcG9ydFR5cGUubWV0aG9kcztcblxuICAgIGZvciAobGV0IGkgPSAwLCBjaGlsZDsgY2hpbGQgPSBjaGlsZHJlbltpXTsgaSsrKSB7XG4gICAgICBpZiAoY2hpbGQubmFtZSAhPT0gJ29wZXJhdGlvbicpXG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgY2hpbGQucG9zdFByb2Nlc3MoZGVmaW5pdGlvbnMsICdiaW5kaW5nJyk7XG4gICAgICBjaGlsZHJlbi5zcGxpY2UoaS0tLCAxKTtcbiAgICAgIGNoaWxkLnN0eWxlIHx8IChjaGlsZC5zdHlsZSA9IHN0eWxlKTtcbiAgICAgIGxldCBtZXRob2QgPSB0aGlzLm1ldGhvZHNbY2hpbGQuJG5hbWVdO1xuXG4gICAgICBpZiAobWV0aG9kKSB7XG4gICAgICAgIG1ldGhvZC5zdHlsZSA9IGNoaWxkLnN0eWxlO1xuICAgICAgICBtZXRob2Quc29hcEFjdGlvbiA9IGNoaWxkLnNvYXBBY3Rpb247XG4gICAgICAgIG1ldGhvZC5pbnB1dFNvYXAgPSBjaGlsZC5pbnB1dCB8fCBudWxsO1xuICAgICAgICBtZXRob2Qub3V0cHV0U29hcCA9IGNoaWxkLm91dHB1dCB8fCBudWxsO1xuICAgICAgICBtZXRob2QuaW5wdXRTb2FwICYmIG1ldGhvZC5pbnB1dFNvYXAuZGVsZXRlRml4ZWRBdHRycygpO1xuICAgICAgICBtZXRob2Qub3V0cHV0U29hcCAmJiBtZXRob2Qub3V0cHV0U29hcC5kZWxldGVGaXhlZEF0dHJzKCk7XG4gICAgICB9XG4gICAgfVxuICB9XG4gIGRlbGV0ZSB0aGlzLiRuYW1lO1xuICBkZWxldGUgdGhpcy4kdHlwZTtcbiAgdGhpcy5kZWxldGVGaXhlZEF0dHJzKCk7XG59O1xuXG5TZXJ2aWNlRWxlbWVudC5wcm90b3R5cGUucG9zdFByb2Nlc3MgPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMpIHtcbiAgbGV0IGNoaWxkcmVuID0gdGhpcy5jaGlsZHJlbixcbiAgICBiaW5kaW5ncyA9IGRlZmluaXRpb25zLmJpbmRpbmdzO1xuICBpZiAoY2hpbGRyZW4gJiYgY2hpbGRyZW4ubGVuZ3RoID4gMCkge1xuICAgIGZvciAobGV0IGkgPSAwLCBjaGlsZDsgY2hpbGQgPSBjaGlsZHJlbltpXTsgaSsrKSB7XG4gICAgICBpZiAoY2hpbGQubmFtZSAhPT0gJ3BvcnQnKVxuICAgICAgICBjb250aW51ZTtcbiAgICAgIGxldCBiaW5kaW5nTmFtZSA9IHNwbGl0UU5hbWUoY2hpbGQuJGJpbmRpbmcpLm5hbWU7XG4gICAgICBsZXQgYmluZGluZyA9IGJpbmRpbmdzW2JpbmRpbmdOYW1lXTtcbiAgICAgIGlmIChiaW5kaW5nKSB7XG4gICAgICAgIGJpbmRpbmcucG9zdFByb2Nlc3MoZGVmaW5pdGlvbnMpO1xuICAgICAgICB0aGlzLnBvcnRzW2NoaWxkLiRuYW1lXSA9IHtcbiAgICAgICAgICBsb2NhdGlvbjogY2hpbGQubG9jYXRpb24sXG4gICAgICAgICAgYmluZGluZzogYmluZGluZ1xuICAgICAgICB9O1xuICAgICAgICBjaGlsZHJlbi5zcGxpY2UoaS0tLCAxKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgZGVsZXRlIHRoaXMuJG5hbWU7XG4gIHRoaXMuZGVsZXRlRml4ZWRBdHRycygpO1xufTtcblxuXG5TaW1wbGVUeXBlRWxlbWVudC5wcm90b3R5cGUuZGVzY3JpcHRpb24gPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMpIHtcbiAgbGV0IGNoaWxkcmVuID0gdGhpcy5jaGlsZHJlbjtcbiAgZm9yIChsZXQgaSA9IDAsIGNoaWxkOyBjaGlsZCA9IGNoaWxkcmVuW2ldOyBpKyspIHtcbiAgICBpZiAoY2hpbGQgaW5zdGFuY2VvZiBSZXN0cmljdGlvbkVsZW1lbnQpXG4gICAgICByZXR1cm4gdGhpcy4kbmFtZSArIFwifFwiICsgY2hpbGQuZGVzY3JpcHRpb24oKTtcbiAgfVxuICByZXR1cm4ge307XG59O1xuXG5SZXN0cmljdGlvbkVsZW1lbnQucHJvdG90eXBlLmRlc2NyaXB0aW9uID0gZnVuY3Rpb24gKGRlZmluaXRpb25zLCB4bWxucykge1xuICBsZXQgY2hpbGRyZW4gPSB0aGlzLmNoaWxkcmVuO1xuICBsZXQgZGVzYztcbiAgZm9yIChsZXQgaSA9IDAsIGNoaWxkOyBjaGlsZCA9IGNoaWxkcmVuW2ldOyBpKyspIHtcbiAgICBpZiAoY2hpbGQgaW5zdGFuY2VvZiBTZXF1ZW5jZUVsZW1lbnQgfHxcbiAgICAgIGNoaWxkIGluc3RhbmNlb2YgQ2hvaWNlRWxlbWVudCkge1xuICAgICAgZGVzYyA9IGNoaWxkLmRlc2NyaXB0aW9uKGRlZmluaXRpb25zLCB4bWxucyk7XG4gICAgICBicmVhaztcbiAgICB9XG4gIH1cbiAgaWYgKGRlc2MgJiYgdGhpcy4kYmFzZSkge1xuICAgIGxldCB0eXBlID0gc3BsaXRRTmFtZSh0aGlzLiRiYXNlKSxcbiAgICAgIHR5cGVOYW1lID0gdHlwZS5uYW1lLFxuICAgICAgbnMgPSB4bWxucyAmJiB4bWxuc1t0eXBlLnByZWZpeF0gfHwgZGVmaW5pdGlvbnMueG1sbnNbdHlwZS5wcmVmaXhdLFxuICAgICAgc2NoZW1hID0gZGVmaW5pdGlvbnMuc2NoZW1hc1tuc10sXG4gICAgICB0eXBlRWxlbWVudCA9IHNjaGVtYSAmJiAoc2NoZW1hLmNvbXBsZXhUeXBlc1t0eXBlTmFtZV0gfHwgc2NoZW1hLnR5cGVzW3R5cGVOYW1lXSB8fCBzY2hlbWEuZWxlbWVudHNbdHlwZU5hbWVdKTtcblxuICAgIGRlc2MuZ2V0QmFzZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiB0eXBlRWxlbWVudC5kZXNjcmlwdGlvbihkZWZpbml0aW9ucywgc2NoZW1hLnhtbG5zKTtcbiAgICB9O1xuICAgIHJldHVybiBkZXNjO1xuICB9XG5cbiAgLy8gdGhlbiBzaW1wbGUgZWxlbWVudFxuICBsZXQgYmFzZSA9IHRoaXMuJGJhc2UgPyB0aGlzLiRiYXNlICsgXCJ8XCIgOiBcIlwiO1xuICByZXR1cm4gYmFzZSArIHRoaXMuY2hpbGRyZW4ubWFwKGZ1bmN0aW9uIChjaGlsZCkge1xuICAgIHJldHVybiBjaGlsZC5kZXNjcmlwdGlvbigpO1xuICB9KS5qb2luKFwiLFwiKTtcbn07XG5cbkV4dGVuc2lvbkVsZW1lbnQucHJvdG90eXBlLmRlc2NyaXB0aW9uID0gZnVuY3Rpb24gKGRlZmluaXRpb25zLCB4bWxucykge1xuICBsZXQgY2hpbGRyZW4gPSB0aGlzLmNoaWxkcmVuO1xuICBsZXQgZGVzYyA9IHt9O1xuICBmb3IgKGxldCBpID0gMCwgY2hpbGQ7IGNoaWxkID0gY2hpbGRyZW5baV07IGkrKykge1xuICAgIGlmIChjaGlsZCBpbnN0YW5jZW9mIFNlcXVlbmNlRWxlbWVudCB8fFxuICAgICAgY2hpbGQgaW5zdGFuY2VvZiBDaG9pY2VFbGVtZW50KSB7XG4gICAgICBkZXNjID0gY2hpbGQuZGVzY3JpcHRpb24oZGVmaW5pdGlvbnMsIHhtbG5zKTtcbiAgICB9XG4gIH1cbiAgaWYgKHRoaXMuJGJhc2UpIHtcbiAgICBsZXQgdHlwZSA9IHNwbGl0UU5hbWUodGhpcy4kYmFzZSksXG4gICAgICB0eXBlTmFtZSA9IHR5cGUubmFtZSxcbiAgICAgIG5zID0geG1sbnMgJiYgeG1sbnNbdHlwZS5wcmVmaXhdIHx8IGRlZmluaXRpb25zLnhtbG5zW3R5cGUucHJlZml4XSxcbiAgICAgIHNjaGVtYSA9IGRlZmluaXRpb25zLnNjaGVtYXNbbnNdO1xuXG4gICAgaWYgKHR5cGVOYW1lIGluIFByaW1pdGl2ZXMpIHtcbiAgICAgIHJldHVybiB0aGlzLiRiYXNlO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIGxldCB0eXBlRWxlbWVudCA9IHNjaGVtYSAmJiAoc2NoZW1hLmNvbXBsZXhUeXBlc1t0eXBlTmFtZV0gfHxcbiAgICAgICAgc2NoZW1hLnR5cGVzW3R5cGVOYW1lXSB8fCBzY2hlbWEuZWxlbWVudHNbdHlwZU5hbWVdKTtcblxuICAgICAgaWYgKHR5cGVFbGVtZW50KSB7XG4gICAgICAgIGxldCBiYXNlID0gdHlwZUVsZW1lbnQuZGVzY3JpcHRpb24oZGVmaW5pdGlvbnMsIHNjaGVtYS54bWxucyk7XG4gICAgICAgIGRlc2MgPSBfLmRlZmF1bHRzRGVlcChiYXNlLCBkZXNjKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgcmV0dXJuIGRlc2M7XG59O1xuXG5FbnVtZXJhdGlvbkVsZW1lbnQucHJvdG90eXBlLmRlc2NyaXB0aW9uID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gdGhpc1t0aGlzLnZhbHVlS2V5XTtcbn07XG5cbkNvbXBsZXhUeXBlRWxlbWVudC5wcm90b3R5cGUuZGVzY3JpcHRpb24gPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMsIHhtbG5zKSB7XG4gIGxldCBjaGlsZHJlbiA9IHRoaXMuY2hpbGRyZW4gfHwgW107XG4gIGZvciAobGV0IGkgPSAwLCBjaGlsZDsgY2hpbGQgPSBjaGlsZHJlbltpXTsgaSsrKSB7XG4gICAgaWYgKGNoaWxkIGluc3RhbmNlb2YgQ2hvaWNlRWxlbWVudCB8fFxuICAgICAgY2hpbGQgaW5zdGFuY2VvZiBTZXF1ZW5jZUVsZW1lbnQgfHxcbiAgICAgIGNoaWxkIGluc3RhbmNlb2YgQWxsRWxlbWVudCB8fFxuICAgICAgY2hpbGQgaW5zdGFuY2VvZiBTaW1wbGVDb250ZW50RWxlbWVudCB8fFxuICAgICAgY2hpbGQgaW5zdGFuY2VvZiBDb21wbGV4Q29udGVudEVsZW1lbnQpIHtcblxuICAgICAgcmV0dXJuIGNoaWxkLmRlc2NyaXB0aW9uKGRlZmluaXRpb25zLCB4bWxucyk7XG4gICAgfVxuICB9XG4gIHJldHVybiB7fTtcbn07XG5cbkNvbXBsZXhDb250ZW50RWxlbWVudC5wcm90b3R5cGUuZGVzY3JpcHRpb24gPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMsIHhtbG5zKSB7XG4gIGxldCBjaGlsZHJlbiA9IHRoaXMuY2hpbGRyZW47XG4gIGZvciAobGV0IGkgPSAwLCBjaGlsZDsgY2hpbGQgPSBjaGlsZHJlbltpXTsgaSsrKSB7XG4gICAgaWYgKGNoaWxkIGluc3RhbmNlb2YgRXh0ZW5zaW9uRWxlbWVudCkge1xuICAgICAgcmV0dXJuIGNoaWxkLmRlc2NyaXB0aW9uKGRlZmluaXRpb25zLCB4bWxucyk7XG4gICAgfVxuICB9XG4gIHJldHVybiB7fTtcbn07XG5cblNpbXBsZUNvbnRlbnRFbGVtZW50LnByb3RvdHlwZS5kZXNjcmlwdGlvbiA9IGZ1bmN0aW9uIChkZWZpbml0aW9ucywgeG1sbnMpIHtcbiAgbGV0IGNoaWxkcmVuID0gdGhpcy5jaGlsZHJlbjtcbiAgZm9yIChsZXQgaSA9IDAsIGNoaWxkOyBjaGlsZCA9IGNoaWxkcmVuW2ldOyBpKyspIHtcbiAgICBpZiAoY2hpbGQgaW5zdGFuY2VvZiBFeHRlbnNpb25FbGVtZW50KSB7XG4gICAgICByZXR1cm4gY2hpbGQuZGVzY3JpcHRpb24oZGVmaW5pdGlvbnMsIHhtbG5zKTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHt9O1xufTtcblxuRWxlbWVudEVsZW1lbnQucHJvdG90eXBlLmRlc2NyaXB0aW9uID0gZnVuY3Rpb24gKGRlZmluaXRpb25zLCB4bWxucykge1xuICBsZXQgZWxlbWVudCA9IHt9LFxuICAgIG5hbWUgPSB0aGlzLiRuYW1lO1xuICBsZXQgaXNNYW55ID0gIXRoaXMuJG1heE9jY3VycyA/IGZhbHNlIDogKGlzTmFOKHRoaXMuJG1heE9jY3VycykgPyAodGhpcy4kbWF4T2NjdXJzID09PSAndW5ib3VuZGVkJykgOiAodGhpcy4kbWF4T2NjdXJzID4gMSkpO1xuICBpZiAodGhpcy4kbWluT2NjdXJzICE9PSB0aGlzLiRtYXhPY2N1cnMgJiYgaXNNYW55KSB7XG4gICAgbmFtZSArPSAnW10nO1xuICB9XG5cbiAgaWYgKHhtbG5zICYmIHhtbG5zW1ROU19QUkVGSVhdKSB7XG4gICAgdGhpcy4kdGFyZ2V0TmFtZXNwYWNlID0geG1sbnNbVE5TX1BSRUZJWF07XG4gIH1cbiAgbGV0IHR5cGUgPSB0aGlzLiR0eXBlIHx8IHRoaXMuJHJlZjtcbiAgaWYgKHR5cGUpIHtcbiAgICB0eXBlID0gc3BsaXRRTmFtZSh0eXBlKTtcbiAgICBsZXQgdHlwZU5hbWUgPSB0eXBlLm5hbWUsXG4gICAgICBucyA9IHhtbG5zICYmIHhtbG5zW3R5cGUucHJlZml4XSB8fCBkZWZpbml0aW9ucy54bWxuc1t0eXBlLnByZWZpeF0sXG4gICAgICBzY2hlbWEgPSBkZWZpbml0aW9ucy5zY2hlbWFzW25zXSxcbiAgICAgIHR5cGVFbGVtZW50ID0gc2NoZW1hICYmICh0aGlzLiR0eXBlID8gc2NoZW1hLmNvbXBsZXhUeXBlc1t0eXBlTmFtZV0gfHwgc2NoZW1hLnR5cGVzW3R5cGVOYW1lXSA6IHNjaGVtYS5lbGVtZW50c1t0eXBlTmFtZV0pO1xuXG4gICAgaWYgKG5zICYmIGRlZmluaXRpb25zLnNjaGVtYXNbbnNdKSB7XG4gICAgICB4bWxucyA9IGRlZmluaXRpb25zLnNjaGVtYXNbbnNdLnhtbG5zO1xuICAgIH1cblxuICAgIGlmICh0eXBlRWxlbWVudCAmJiAhKHR5cGVOYW1lIGluIFByaW1pdGl2ZXMpKSB7XG5cbiAgICAgIGlmICghKHR5cGVOYW1lIGluIGRlZmluaXRpb25zLmRlc2NyaXB0aW9ucy50eXBlcykpIHtcblxuICAgICAgICBsZXQgZWxlbTogYW55ID0ge307XG4gICAgICAgIGRlZmluaXRpb25zLmRlc2NyaXB0aW9ucy50eXBlc1t0eXBlTmFtZV0gPSBlbGVtO1xuICAgICAgICBsZXQgZGVzY3JpcHRpb24gPSB0eXBlRWxlbWVudC5kZXNjcmlwdGlvbihkZWZpbml0aW9ucywgeG1sbnMpO1xuICAgICAgICBpZiAodHlwZW9mIGRlc2NyaXB0aW9uID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGVsZW0gPSBkZXNjcmlwdGlvbjtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICBPYmplY3Qua2V5cyhkZXNjcmlwdGlvbikuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgICAgICBlbGVtW2tleV0gPSBkZXNjcmlwdGlvbltrZXldO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuJHJlZikge1xuICAgICAgICAgIGVsZW1lbnQgPSBlbGVtO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGVsZW1lbnRbbmFtZV0gPSBlbGVtO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHR5cGVvZiBlbGVtID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgIGVsZW0udGFyZ2V0TlNBbGlhcyA9IHR5cGUucHJlZml4O1xuICAgICAgICAgIGVsZW0udGFyZ2V0TmFtZXNwYWNlID0gbnM7XG4gICAgICAgIH1cblxuICAgICAgICBkZWZpbml0aW9ucy5kZXNjcmlwdGlvbnMudHlwZXNbdHlwZU5hbWVdID0gZWxlbTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBpZiAodGhpcy4kcmVmKSB7XG4gICAgICAgICAgZWxlbWVudCA9IGRlZmluaXRpb25zLmRlc2NyaXB0aW9ucy50eXBlc1t0eXBlTmFtZV07XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgZWxlbWVudFtuYW1lXSA9IGRlZmluaXRpb25zLmRlc2NyaXB0aW9ucy50eXBlc1t0eXBlTmFtZV07XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIGVsZW1lbnRbbmFtZV0gPSB0aGlzLiR0eXBlO1xuICAgIH1cbiAgfVxuICBlbHNlIHtcbiAgICBsZXQgY2hpbGRyZW4gPSB0aGlzLmNoaWxkcmVuO1xuICAgIGVsZW1lbnRbbmFtZV0gPSB7fTtcbiAgICBmb3IgKGxldCBpID0gMCwgY2hpbGQ7IGNoaWxkID0gY2hpbGRyZW5baV07IGkrKykge1xuICAgICAgaWYgKGNoaWxkIGluc3RhbmNlb2YgQ29tcGxleFR5cGVFbGVtZW50KSB7XG4gICAgICAgIGVsZW1lbnRbbmFtZV0gPSBjaGlsZC5kZXNjcmlwdGlvbihkZWZpbml0aW9ucywgeG1sbnMpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuICByZXR1cm4gZWxlbWVudDtcbn07XG5cbkFsbEVsZW1lbnQucHJvdG90eXBlLmRlc2NyaXB0aW9uID1cbiAgU2VxdWVuY2VFbGVtZW50LnByb3RvdHlwZS5kZXNjcmlwdGlvbiA9IGZ1bmN0aW9uIChkZWZpbml0aW9ucywgeG1sbnMpIHtcbiAgICBsZXQgY2hpbGRyZW4gPSB0aGlzLmNoaWxkcmVuO1xuICAgIGxldCBzZXF1ZW5jZSA9IHt9O1xuICAgIGZvciAobGV0IGkgPSAwLCBjaGlsZDsgY2hpbGQgPSBjaGlsZHJlbltpXTsgaSsrKSB7XG4gICAgICBpZiAoY2hpbGQgaW5zdGFuY2VvZiBBbnlFbGVtZW50KSB7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuICAgICAgbGV0IGRlc2NyaXB0aW9uID0gY2hpbGQuZGVzY3JpcHRpb24oZGVmaW5pdGlvbnMsIHhtbG5zKTtcbiAgICAgIGZvciAobGV0IGtleSBpbiBkZXNjcmlwdGlvbikge1xuICAgICAgICBzZXF1ZW5jZVtrZXldID0gZGVzY3JpcHRpb25ba2V5XTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHNlcXVlbmNlO1xuICB9O1xuXG5DaG9pY2VFbGVtZW50LnByb3RvdHlwZS5kZXNjcmlwdGlvbiA9IGZ1bmN0aW9uIChkZWZpbml0aW9ucywgeG1sbnMpIHtcbiAgbGV0IGNoaWxkcmVuID0gdGhpcy5jaGlsZHJlbjtcbiAgbGV0IGNob2ljZSA9IHt9O1xuICBmb3IgKGxldCBpID0gMCwgY2hpbGQ7IGNoaWxkID0gY2hpbGRyZW5baV07IGkrKykge1xuICAgIGxldCBkZXNjcmlwdGlvbiA9IGNoaWxkLmRlc2NyaXB0aW9uKGRlZmluaXRpb25zLCB4bWxucyk7XG4gICAgZm9yIChsZXQga2V5IGluIGRlc2NyaXB0aW9uKSB7XG4gICAgICBjaG9pY2Vba2V5XSA9IGRlc2NyaXB0aW9uW2tleV07XG4gICAgfVxuICB9XG4gIHJldHVybiBjaG9pY2U7XG59O1xuXG5NZXNzYWdlRWxlbWVudC5wcm90b3R5cGUuZGVzY3JpcHRpb24gPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMpIHtcbiAgaWYgKHRoaXMuZWxlbWVudCkge1xuICAgIHJldHVybiB0aGlzLmVsZW1lbnQgJiYgdGhpcy5lbGVtZW50LmRlc2NyaXB0aW9uKGRlZmluaXRpb25zKTtcbiAgfVxuICBsZXQgZGVzYyA9IHt9O1xuICBkZXNjW3RoaXMuJG5hbWVdID0gdGhpcy5wYXJ0cztcbiAgcmV0dXJuIGRlc2M7XG59O1xuXG5Qb3J0VHlwZUVsZW1lbnQucHJvdG90eXBlLmRlc2NyaXB0aW9uID0gZnVuY3Rpb24gKGRlZmluaXRpb25zKSB7XG4gIGxldCBtZXRob2RzID0ge307XG4gIGZvciAobGV0IG5hbWUgaW4gdGhpcy5tZXRob2RzKSB7XG4gICAgbGV0IG1ldGhvZCA9IHRoaXMubWV0aG9kc1tuYW1lXTtcbiAgICBtZXRob2RzW25hbWVdID0gbWV0aG9kLmRlc2NyaXB0aW9uKGRlZmluaXRpb25zKTtcbiAgfVxuICByZXR1cm4gbWV0aG9kcztcbn07XG5cbk9wZXJhdGlvbkVsZW1lbnQucHJvdG90eXBlLmRlc2NyaXB0aW9uID0gZnVuY3Rpb24gKGRlZmluaXRpb25zKSB7XG4gIGxldCBpbnB1dERlc2MgPSB0aGlzLmlucHV0ID8gdGhpcy5pbnB1dC5kZXNjcmlwdGlvbihkZWZpbml0aW9ucykgOiBudWxsO1xuICBsZXQgb3V0cHV0RGVzYyA9IHRoaXMub3V0cHV0ID8gdGhpcy5vdXRwdXQuZGVzY3JpcHRpb24oZGVmaW5pdGlvbnMpIDogbnVsbDtcbiAgcmV0dXJuIHtcbiAgICBpbnB1dDogaW5wdXREZXNjICYmIGlucHV0RGVzY1tPYmplY3Qua2V5cyhpbnB1dERlc2MpWzBdXSxcbiAgICBvdXRwdXQ6IG91dHB1dERlc2MgJiYgb3V0cHV0RGVzY1tPYmplY3Qua2V5cyhvdXRwdXREZXNjKVswXV1cbiAgfTtcbn07XG5cbkJpbmRpbmdFbGVtZW50LnByb3RvdHlwZS5kZXNjcmlwdGlvbiA9IGZ1bmN0aW9uIChkZWZpbml0aW9ucykge1xuICBsZXQgbWV0aG9kcyA9IHt9O1xuICBmb3IgKGxldCBuYW1lIGluIHRoaXMubWV0aG9kcykge1xuICAgIGxldCBtZXRob2QgPSB0aGlzLm1ldGhvZHNbbmFtZV07XG4gICAgbWV0aG9kc1tuYW1lXSA9IG1ldGhvZC5kZXNjcmlwdGlvbihkZWZpbml0aW9ucyk7XG4gIH1cbiAgcmV0dXJuIG1ldGhvZHM7XG59O1xuXG5TZXJ2aWNlRWxlbWVudC5wcm90b3R5cGUuZGVzY3JpcHRpb24gPSBmdW5jdGlvbiAoZGVmaW5pdGlvbnMpIHtcbiAgbGV0IHBvcnRzID0ge307XG4gIGZvciAobGV0IG5hbWUgaW4gdGhpcy5wb3J0cykge1xuICAgIGxldCBwb3J0ID0gdGhpcy5wb3J0c1tuYW1lXTtcbiAgICBwb3J0c1tuYW1lXSA9IHBvcnQuYmluZGluZy5kZXNjcmlwdGlvbihkZWZpbml0aW9ucyk7XG4gIH1cbiAgcmV0dXJuIHBvcnRzO1xufTtcblxuZXhwb3J0IGxldCBXU0RMID0gZnVuY3Rpb24gKGRlZmluaXRpb24sIHVyaSwgb3B0aW9ucykge1xuICBsZXQgc2VsZiA9IHRoaXMsXG4gICAgZnJvbUZ1bmM7XG5cbiAgdGhpcy51cmkgPSB1cmk7XG4gIHRoaXMuY2FsbGJhY2sgPSBmdW5jdGlvbiAoKSB7XG4gIH07XG4gIHRoaXMuX2luY2x1ZGVzV3NkbCA9IFtdO1xuXG4gIC8vIGluaXRpYWxpemUgV1NETCBjYWNoZVxuICB0aGlzLldTRExfQ0FDSEUgPSAob3B0aW9ucyB8fCB7fSkuV1NETF9DQUNIRSB8fCB7fTtcblxuICB0aGlzLl9pbml0aWFsaXplT3B0aW9ucyhvcHRpb25zKTtcblxuICBpZiAodHlwZW9mIGRlZmluaXRpb24gPT09ICdzdHJpbmcnKSB7XG4gICAgZGVmaW5pdGlvbiA9IHN0cmlwQm9tKGRlZmluaXRpb24pO1xuICAgIGZyb21GdW5jID0gdGhpcy5fZnJvbVhNTDtcbiAgfVxuICBlbHNlIGlmICh0eXBlb2YgZGVmaW5pdGlvbiA9PT0gJ29iamVjdCcpIHtcbiAgICBmcm9tRnVuYyA9IHRoaXMuX2Zyb21TZXJ2aWNlcztcbiAgfVxuICBlbHNlIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ1dTREwgbGV0cnVjdG9yIHRha2VzIGVpdGhlciBhbiBYTUwgc3RyaW5nIG9yIHNlcnZpY2UgZGVmaW5pdGlvbicpO1xuICB9XG5cbiAgUHJvbWlzZS5yZXNvbHZlKHRydWUpLnRoZW4oKCkgPT4ge1xuICAgIHRyeSB7XG4gICAgICBmcm9tRnVuYy5jYWxsKHNlbGYsIGRlZmluaXRpb24pO1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIHJldHVybiBzZWxmLmNhbGxiYWNrKGUubWVzc2FnZSk7XG4gICAgfVxuXG4gICAgc2VsZi5wcm9jZXNzSW5jbHVkZXMoKS50aGVuKCgpID0+IHtcbiAgICAgIHNlbGYuZGVmaW5pdGlvbnMuZGVsZXRlRml4ZWRBdHRycygpO1xuICAgICAgbGV0IHNlcnZpY2VzID0gc2VsZi5zZXJ2aWNlcyA9IHNlbGYuZGVmaW5pdGlvbnMuc2VydmljZXM7XG4gICAgICBpZiAoc2VydmljZXMpIHtcbiAgICAgICAgZm9yIChjb25zdCBuYW1lIGluIHNlcnZpY2VzKSB7XG4gICAgICAgICAgc2VydmljZXNbbmFtZV0ucG9zdFByb2Nlc3Moc2VsZi5kZWZpbml0aW9ucyk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGxldCBjb21wbGV4VHlwZXMgPSBzZWxmLmRlZmluaXRpb25zLmNvbXBsZXhUeXBlcztcbiAgICAgIGlmIChjb21wbGV4VHlwZXMpIHtcbiAgICAgICAgZm9yIChjb25zdCBuYW1lIGluIGNvbXBsZXhUeXBlcykge1xuICAgICAgICAgIGNvbXBsZXhUeXBlc1tuYW1lXS5kZWxldGVGaXhlZEF0dHJzKCk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLy8gZm9yIGRvY3VtZW50IHN0eWxlLCBmb3IgZXZlcnkgYmluZGluZywgcHJlcGFyZSBpbnB1dCBtZXNzYWdlIGVsZW1lbnQgbmFtZSB0byAobWV0aG9kTmFtZSwgb3V0cHV0IG1lc3NhZ2UgZWxlbWVudCBuYW1lKSBtYXBwaW5nXG4gICAgICBsZXQgYmluZGluZ3MgPSBzZWxmLmRlZmluaXRpb25zLmJpbmRpbmdzO1xuICAgICAgZm9yIChsZXQgYmluZGluZ05hbWUgaW4gYmluZGluZ3MpIHtcbiAgICAgICAgbGV0IGJpbmRpbmcgPSBiaW5kaW5nc1tiaW5kaW5nTmFtZV07XG4gICAgICAgIGlmICh0eXBlb2YgYmluZGluZy5zdHlsZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICBiaW5kaW5nLnN0eWxlID0gJ2RvY3VtZW50JztcbiAgICAgICAgfVxuICAgICAgICBpZiAoYmluZGluZy5zdHlsZSAhPT0gJ2RvY3VtZW50JylcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgbGV0IG1ldGhvZHMgPSBiaW5kaW5nLm1ldGhvZHM7XG4gICAgICAgIGxldCB0b3BFbHMgPSBiaW5kaW5nLnRvcEVsZW1lbnRzID0ge307XG4gICAgICAgIGZvciAobGV0IG1ldGhvZE5hbWUgaW4gbWV0aG9kcykge1xuICAgICAgICAgIGlmIChtZXRob2RzW21ldGhvZE5hbWVdLmlucHV0KSB7XG4gICAgICAgICAgICBsZXQgaW5wdXROYW1lID0gbWV0aG9kc1ttZXRob2ROYW1lXS5pbnB1dC4kbmFtZTtcbiAgICAgICAgICAgIGxldCBvdXRwdXROYW1lID0gXCJcIjtcbiAgICAgICAgICAgIGlmIChtZXRob2RzW21ldGhvZE5hbWVdLm91dHB1dClcbiAgICAgICAgICAgICAgb3V0cHV0TmFtZSA9IG1ldGhvZHNbbWV0aG9kTmFtZV0ub3V0cHV0LiRuYW1lO1xuICAgICAgICAgICAgdG9wRWxzW2lucHV0TmFtZV0gPSB7IFwibWV0aG9kTmFtZVwiOiBtZXRob2ROYW1lLCBcIm91dHB1dE5hbWVcIjogb3V0cHV0TmFtZSB9O1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAvLyBwcmVwYXJlIHNvYXAgZW52ZWxvcGUgeG1sbnMgZGVmaW5pdGlvbiBzdHJpbmdcbiAgICAgIHNlbGYueG1sbnNJbkVudmVsb3BlID0gc2VsZi5feG1sbnNNYXAoKTtcbiAgICAgIHNlbGYuY2FsbGJhY2sobnVsbCwgc2VsZik7XG4gICAgfSkuY2F0Y2goZXJyID0+IHNlbGYuY2FsbGJhY2soZXJyKSk7XG5cbiAgfSk7XG5cbiAgLy8gcHJvY2Vzcy5uZXh0VGljayhmdW5jdGlvbigpIHtcbiAgLy8gICB0cnkge1xuICAvLyAgICAgZnJvbUZ1bmMuY2FsbChzZWxmLCBkZWZpbml0aW9uKTtcbiAgLy8gICB9IGNhdGNoIChlKSB7XG4gIC8vICAgICByZXR1cm4gc2VsZi5jYWxsYmFjayhlLm1lc3NhZ2UpO1xuICAvLyAgIH1cblxuICAvLyAgIHNlbGYucHJvY2Vzc0luY2x1ZGVzKGZ1bmN0aW9uKGVycikge1xuICAvLyAgICAgbGV0IG5hbWU7XG4gIC8vICAgICBpZiAoZXJyKSB7XG4gIC8vICAgICAgIHJldHVybiBzZWxmLmNhbGxiYWNrKGVycik7XG4gIC8vICAgICB9XG5cbiAgLy8gICAgIHNlbGYuZGVmaW5pdGlvbnMuZGVsZXRlRml4ZWRBdHRycygpO1xuICAvLyAgICAgbGV0IHNlcnZpY2VzID0gc2VsZi5zZXJ2aWNlcyA9IHNlbGYuZGVmaW5pdGlvbnMuc2VydmljZXM7XG4gIC8vICAgICBpZiAoc2VydmljZXMpIHtcbiAgLy8gICAgICAgZm9yIChuYW1lIGluIHNlcnZpY2VzKSB7XG4gIC8vICAgICAgICAgc2VydmljZXNbbmFtZV0ucG9zdFByb2Nlc3Moc2VsZi5kZWZpbml0aW9ucyk7XG4gIC8vICAgICAgIH1cbiAgLy8gICAgIH1cbiAgLy8gICAgIGxldCBjb21wbGV4VHlwZXMgPSBzZWxmLmRlZmluaXRpb25zLmNvbXBsZXhUeXBlcztcbiAgLy8gICAgIGlmIChjb21wbGV4VHlwZXMpIHtcbiAgLy8gICAgICAgZm9yIChuYW1lIGluIGNvbXBsZXhUeXBlcykge1xuICAvLyAgICAgICAgIGNvbXBsZXhUeXBlc1tuYW1lXS5kZWxldGVGaXhlZEF0dHJzKCk7XG4gIC8vICAgICAgIH1cbiAgLy8gICAgIH1cblxuICAvLyAgICAgLy8gZm9yIGRvY3VtZW50IHN0eWxlLCBmb3IgZXZlcnkgYmluZGluZywgcHJlcGFyZSBpbnB1dCBtZXNzYWdlIGVsZW1lbnQgbmFtZSB0byAobWV0aG9kTmFtZSwgb3V0cHV0IG1lc3NhZ2UgZWxlbWVudCBuYW1lKSBtYXBwaW5nXG4gIC8vICAgICBsZXQgYmluZGluZ3MgPSBzZWxmLmRlZmluaXRpb25zLmJpbmRpbmdzO1xuICAvLyAgICAgZm9yIChsZXQgYmluZGluZ05hbWUgaW4gYmluZGluZ3MpIHtcbiAgLy8gICAgICAgbGV0IGJpbmRpbmcgPSBiaW5kaW5nc1tiaW5kaW5nTmFtZV07XG4gIC8vICAgICAgIGlmICh0eXBlb2YgYmluZGluZy5zdHlsZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgLy8gICAgICAgICBiaW5kaW5nLnN0eWxlID0gJ2RvY3VtZW50JztcbiAgLy8gICAgICAgfVxuICAvLyAgICAgICBpZiAoYmluZGluZy5zdHlsZSAhPT0gJ2RvY3VtZW50JylcbiAgLy8gICAgICAgICBjb250aW51ZTtcbiAgLy8gICAgICAgbGV0IG1ldGhvZHMgPSBiaW5kaW5nLm1ldGhvZHM7XG4gIC8vICAgICAgIGxldCB0b3BFbHMgPSBiaW5kaW5nLnRvcEVsZW1lbnRzID0ge307XG4gIC8vICAgICAgIGZvciAobGV0IG1ldGhvZE5hbWUgaW4gbWV0aG9kcykge1xuICAvLyAgICAgICAgIGlmIChtZXRob2RzW21ldGhvZE5hbWVdLmlucHV0KSB7XG4gIC8vICAgICAgICAgICBsZXQgaW5wdXROYW1lID0gbWV0aG9kc1ttZXRob2ROYW1lXS5pbnB1dC4kbmFtZTtcbiAgLy8gICAgICAgICAgIGxldCBvdXRwdXROYW1lPVwiXCI7XG4gIC8vICAgICAgICAgICBpZihtZXRob2RzW21ldGhvZE5hbWVdLm91dHB1dCApXG4gIC8vICAgICAgICAgICAgIG91dHB1dE5hbWUgPSBtZXRob2RzW21ldGhvZE5hbWVdLm91dHB1dC4kbmFtZTtcbiAgLy8gICAgICAgICAgIHRvcEVsc1tpbnB1dE5hbWVdID0ge1wibWV0aG9kTmFtZVwiOiBtZXRob2ROYW1lLCBcIm91dHB1dE5hbWVcIjogb3V0cHV0TmFtZX07XG4gIC8vICAgICAgICAgfVxuICAvLyAgICAgICB9XG4gIC8vICAgICB9XG5cbiAgLy8gICAgIC8vIHByZXBhcmUgc29hcCBlbnZlbG9wZSB4bWxucyBkZWZpbml0aW9uIHN0cmluZ1xuICAvLyAgICAgc2VsZi54bWxuc0luRW52ZWxvcGUgPSBzZWxmLl94bWxuc01hcCgpO1xuXG4gIC8vICAgICBzZWxmLmNhbGxiYWNrKGVyciwgc2VsZik7XG4gIC8vICAgfSk7XG5cbiAgLy8gfSk7XG59O1xuXG5XU0RMLnByb3RvdHlwZS5pZ25vcmVkTmFtZXNwYWNlcyA9IFsndG5zJywgJ3RhcmdldE5hbWVzcGFjZScsICd0eXBlZE5hbWVzcGFjZSddO1xuXG5XU0RMLnByb3RvdHlwZS5pZ25vcmVCYXNlTmFtZVNwYWNlcyA9IGZhbHNlO1xuXG5XU0RMLnByb3RvdHlwZS52YWx1ZUtleSA9ICckdmFsdWUnO1xuV1NETC5wcm90b3R5cGUueG1sS2V5ID0gJyR4bWwnO1xuXG5XU0RMLnByb3RvdHlwZS5faW5pdGlhbGl6ZU9wdGlvbnMgPSBmdW5jdGlvbiAob3B0aW9ucykge1xuICB0aGlzLl9vcmlnaW5hbElnbm9yZWROYW1lc3BhY2VzID0gKG9wdGlvbnMgfHwge30pLmlnbm9yZWROYW1lc3BhY2VzO1xuICB0aGlzLm9wdGlvbnMgPSB7fTtcblxuICBsZXQgaWdub3JlZE5hbWVzcGFjZXMgPSBvcHRpb25zID8gb3B0aW9ucy5pZ25vcmVkTmFtZXNwYWNlcyA6IG51bGw7XG5cbiAgaWYgKGlnbm9yZWROYW1lc3BhY2VzICYmXG4gICAgKEFycmF5LmlzQXJyYXkoaWdub3JlZE5hbWVzcGFjZXMubmFtZXNwYWNlcykgfHwgdHlwZW9mIGlnbm9yZWROYW1lc3BhY2VzLm5hbWVzcGFjZXMgPT09ICdzdHJpbmcnKSkge1xuICAgIGlmIChpZ25vcmVkTmFtZXNwYWNlcy5vdmVycmlkZSkge1xuICAgICAgdGhpcy5vcHRpb25zLmlnbm9yZWROYW1lc3BhY2VzID0gaWdub3JlZE5hbWVzcGFjZXMubmFtZXNwYWNlcztcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5vcHRpb25zLmlnbm9yZWROYW1lc3BhY2VzID0gdGhpcy5pZ25vcmVkTmFtZXNwYWNlcy5jb25jYXQoaWdub3JlZE5hbWVzcGFjZXMubmFtZXNwYWNlcyk7XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIHRoaXMub3B0aW9ucy5pZ25vcmVkTmFtZXNwYWNlcyA9IHRoaXMuaWdub3JlZE5hbWVzcGFjZXM7XG4gIH1cblxuICB0aGlzLm9wdGlvbnMudmFsdWVLZXkgPSBvcHRpb25zLnZhbHVlS2V5IHx8IHRoaXMudmFsdWVLZXk7XG4gIHRoaXMub3B0aW9ucy54bWxLZXkgPSBvcHRpb25zLnhtbEtleSB8fCB0aGlzLnhtbEtleTtcbiAgaWYgKG9wdGlvbnMuZXNjYXBlWE1MICE9PSB1bmRlZmluZWQpIHtcbiAgICB0aGlzLm9wdGlvbnMuZXNjYXBlWE1MID0gb3B0aW9ucy5lc2NhcGVYTUw7XG4gIH0gZWxzZSB7XG4gICAgdGhpcy5vcHRpb25zLmVzY2FwZVhNTCA9IHRydWU7XG4gIH1cbiAgaWYgKG9wdGlvbnMucmV0dXJuRmF1bHQgIT09IHVuZGVmaW5lZCkge1xuICAgIHRoaXMub3B0aW9ucy5yZXR1cm5GYXVsdCA9IG9wdGlvbnMucmV0dXJuRmF1bHQ7XG4gIH0gZWxzZSB7XG4gICAgdGhpcy5vcHRpb25zLnJldHVybkZhdWx0ID0gZmFsc2U7XG4gIH1cbiAgdGhpcy5vcHRpb25zLmhhbmRsZU5pbEFzTnVsbCA9ICEhb3B0aW9ucy5oYW5kbGVOaWxBc051bGw7XG5cbiAgaWYgKG9wdGlvbnMubmFtZXNwYWNlQXJyYXlFbGVtZW50cyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgdGhpcy5vcHRpb25zLm5hbWVzcGFjZUFycmF5RWxlbWVudHMgPSBvcHRpb25zLm5hbWVzcGFjZUFycmF5RWxlbWVudHM7XG4gIH0gZWxzZSB7XG4gICAgdGhpcy5vcHRpb25zLm5hbWVzcGFjZUFycmF5RWxlbWVudHMgPSB0cnVlO1xuICB9XG5cbiAgLy8gQWxsb3cgYW55IHJlcXVlc3QgaGVhZGVycyB0byBrZWVwIHBhc3NpbmcgdGhyb3VnaFxuICB0aGlzLm9wdGlvbnMud3NkbF9oZWFkZXJzID0gb3B0aW9ucy53c2RsX2hlYWRlcnM7XG4gIHRoaXMub3B0aW9ucy53c2RsX29wdGlvbnMgPSBvcHRpb25zLndzZGxfb3B0aW9ucztcbiAgaWYgKG9wdGlvbnMuaHR0cENsaWVudCkge1xuICAgIHRoaXMub3B0aW9ucy5odHRwQ2xpZW50ID0gb3B0aW9ucy5odHRwQ2xpZW50O1xuICB9XG5cbiAgLy8gVGhlIHN1cHBsaWVkIHJlcXVlc3Qtb2JqZWN0IHNob3VsZCBiZSBwYXNzZWQgdGhyb3VnaFxuICBpZiAob3B0aW9ucy5yZXF1ZXN0KSB7XG4gICAgdGhpcy5vcHRpb25zLnJlcXVlc3QgPSBvcHRpb25zLnJlcXVlc3Q7XG4gIH1cblxuICBsZXQgaWdub3JlQmFzZU5hbWVTcGFjZXMgPSBvcHRpb25zID8gb3B0aW9ucy5pZ25vcmVCYXNlTmFtZVNwYWNlcyA6IG51bGw7XG4gIGlmIChpZ25vcmVCYXNlTmFtZVNwYWNlcyAhPT0gbnVsbCAmJiB0eXBlb2YgaWdub3JlQmFzZU5hbWVTcGFjZXMgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgdGhpcy5vcHRpb25zLmlnbm9yZUJhc2VOYW1lU3BhY2VzID0gaWdub3JlQmFzZU5hbWVTcGFjZXM7XG4gIH0gZWxzZSB7XG4gICAgdGhpcy5vcHRpb25zLmlnbm9yZUJhc2VOYW1lU3BhY2VzID0gdGhpcy5pZ25vcmVCYXNlTmFtZVNwYWNlcztcbiAgfVxuXG4gIC8vIFdvcmtzIG9ubHkgaW4gY2xpZW50XG4gIHRoaXMub3B0aW9ucy5mb3JjZVNvYXAxMkhlYWRlcnMgPSBvcHRpb25zLmZvcmNlU29hcDEySGVhZGVycztcbiAgdGhpcy5vcHRpb25zLmN1c3RvbURlc2VyaWFsaXplciA9IG9wdGlvbnMuY3VzdG9tRGVzZXJpYWxpemVyO1xuXG4gIGlmIChvcHRpb25zLm92ZXJyaWRlUm9vdEVsZW1lbnQgIT09IHVuZGVmaW5lZCkge1xuICAgIHRoaXMub3B0aW9ucy5vdmVycmlkZVJvb3RFbGVtZW50ID0gb3B0aW9ucy5vdmVycmlkZVJvb3RFbGVtZW50O1xuICB9XG5cbiAgdGhpcy5vcHRpb25zLnVzZUVtcHR5VGFnID0gISFvcHRpb25zLnVzZUVtcHR5VGFnO1xufTtcblxuV1NETC5wcm90b3R5cGUub25SZWFkeSA9IGZ1bmN0aW9uIChjYWxsYmFjaykge1xuICBpZiAoY2FsbGJhY2spXG4gICAgdGhpcy5jYWxsYmFjayA9IGNhbGxiYWNrO1xufTtcblxuV1NETC5wcm90b3R5cGUuX3Byb2Nlc3NOZXh0SW5jbHVkZSA9IGFzeW5jIGZ1bmN0aW9uIChpbmNsdWRlcykge1xuICBsZXQgc2VsZiA9IHRoaXMsXG4gICAgaW5jbHVkZSA9IGluY2x1ZGVzLnNoaWZ0KCksXG4gICAgb3B0aW9ucztcblxuICBpZiAoIWluY2x1ZGUpXG4gICAgcmV0dXJuOyAvLyBjYWxsYmFjaygpO1xuXG4gIGxldCBpbmNsdWRlUGF0aDtcbiAgaWYgKCEvXmh0dHBzPzovLnRlc3Qoc2VsZi51cmkpICYmICEvXmh0dHBzPzovLnRlc3QoaW5jbHVkZS5sb2NhdGlvbikpIHtcbiAgICAvLyBpbmNsdWRlUGF0aCA9IHBhdGgucmVzb2x2ZShwYXRoLmRpcm5hbWUoc2VsZi51cmkpLCBpbmNsdWRlLmxvY2F0aW9uKTtcbiAgfSBlbHNlIHtcbiAgICBpbmNsdWRlUGF0aCA9IHVybC5yZXNvbHZlKHNlbGYudXJpIHx8ICcnLCBpbmNsdWRlLmxvY2F0aW9uKTtcbiAgfVxuXG4gIG9wdGlvbnMgPSBfLmFzc2lnbih7fSwgdGhpcy5vcHRpb25zKTtcbiAgLy8gZm9sbG93IHN1cHBsaWVkIGlnbm9yZWROYW1lc3BhY2VzIG9wdGlvblxuICBvcHRpb25zLmlnbm9yZWROYW1lc3BhY2VzID0gdGhpcy5fb3JpZ2luYWxJZ25vcmVkTmFtZXNwYWNlcyB8fCB0aGlzLm9wdGlvbnMuaWdub3JlZE5hbWVzcGFjZXM7XG4gIG9wdGlvbnMuV1NETF9DQUNIRSA9IHRoaXMuV1NETF9DQUNIRTtcblxuICBjb25zdCB3c2RsID0gYXdhaXQgb3Blbl93c2RsX3JlY3Vyc2l2ZShpbmNsdWRlUGF0aCwgb3B0aW9ucylcbiAgc2VsZi5faW5jbHVkZXNXc2RsLnB1c2god3NkbCk7XG5cbiAgaWYgKHdzZGwuZGVmaW5pdGlvbnMgaW5zdGFuY2VvZiBEZWZpbml0aW9uc0VsZW1lbnQpIHtcbiAgICBfLm1lcmdlV2l0aChzZWxmLmRlZmluaXRpb25zLCB3c2RsLmRlZmluaXRpb25zLCBmdW5jdGlvbiAoYSwgYikge1xuICAgICAgcmV0dXJuIChhIGluc3RhbmNlb2YgU2NoZW1hRWxlbWVudCkgPyBhLm1lcmdlKGIpIDogdW5kZWZpbmVkO1xuICAgIH0pO1xuICB9IGVsc2Uge1xuICAgIHNlbGYuZGVmaW5pdGlvbnMuc2NoZW1hc1tpbmNsdWRlLm5hbWVzcGFjZSB8fCB3c2RsLmRlZmluaXRpb25zLiR0YXJnZXROYW1lc3BhY2VdID0gZGVlcE1lcmdlKHNlbGYuZGVmaW5pdGlvbnMuc2NoZW1hc1tpbmNsdWRlLm5hbWVzcGFjZSB8fCB3c2RsLmRlZmluaXRpb25zLiR0YXJnZXROYW1lc3BhY2VdLCB3c2RsLmRlZmluaXRpb25zKTtcbiAgfVxuXG4gIHJldHVybiBzZWxmLl9wcm9jZXNzTmV4dEluY2x1ZGUoaW5jbHVkZXMpO1xuXG4gIC8vIG9wZW5fd3NkbF9yZWN1cnNpdmUoaW5jbHVkZVBhdGgsIG9wdGlvbnMsIGZ1bmN0aW9uKGVyciwgd3NkbCkge1xuICAvLyAgIGlmIChlcnIpIHtcbiAgLy8gICAgIHJldHVybiBjYWxsYmFjayhlcnIpO1xuICAvLyAgIH1cblxuICAvLyAgIHNlbGYuX2luY2x1ZGVzV3NkbC5wdXNoKHdzZGwpO1xuXG4gIC8vICAgaWYgKHdzZGwuZGVmaW5pdGlvbnMgaW5zdGFuY2VvZiBEZWZpbml0aW9uc0VsZW1lbnQpIHtcbiAgLy8gICAgIF8ubWVyZ2VXaXRoKHNlbGYuZGVmaW5pdGlvbnMsIHdzZGwuZGVmaW5pdGlvbnMsIGZ1bmN0aW9uKGEsYikge1xuICAvLyAgICAgICByZXR1cm4gKGEgaW5zdGFuY2VvZiBTY2hlbWFFbGVtZW50KSA/IGEubWVyZ2UoYikgOiB1bmRlZmluZWQ7XG4gIC8vICAgICB9KTtcbiAgLy8gICB9IGVsc2Uge1xuICAvLyAgICAgc2VsZi5kZWZpbml0aW9ucy5zY2hlbWFzW2luY2x1ZGUubmFtZXNwYWNlIHx8IHdzZGwuZGVmaW5pdGlvbnMuJHRhcmdldE5hbWVzcGFjZV0gPSBkZWVwTWVyZ2Uoc2VsZi5kZWZpbml0aW9ucy5zY2hlbWFzW2luY2x1ZGUubmFtZXNwYWNlIHx8IHdzZGwuZGVmaW5pdGlvbnMuJHRhcmdldE5hbWVzcGFjZV0sIHdzZGwuZGVmaW5pdGlvbnMpO1xuICAvLyAgIH1cbiAgLy8gICBzZWxmLl9wcm9jZXNzTmV4dEluY2x1ZGUoaW5jbHVkZXMsIGZ1bmN0aW9uKGVycikge1xuICAvLyAgICAgY2FsbGJhY2soZXJyKTtcbiAgLy8gICB9KTtcbiAgLy8gfSk7XG59O1xuXG5XU0RMLnByb3RvdHlwZS5wcm9jZXNzSW5jbHVkZXMgPSBhc3luYyBmdW5jdGlvbiAoKSB7XG4gIGxldCBzY2hlbWFzID0gdGhpcy5kZWZpbml0aW9ucy5zY2hlbWFzLFxuICAgIGluY2x1ZGVzID0gW107XG5cbiAgZm9yIChsZXQgbnMgaW4gc2NoZW1hcykge1xuICAgIGxldCBzY2hlbWEgPSBzY2hlbWFzW25zXTtcbiAgICBpbmNsdWRlcyA9IGluY2x1ZGVzLmNvbmNhdChzY2hlbWEuaW5jbHVkZXMgfHwgW10pO1xuICB9XG5cbiAgcmV0dXJuIHRoaXMuX3Byb2Nlc3NOZXh0SW5jbHVkZShpbmNsdWRlcyk7XG59O1xuXG5XU0RMLnByb3RvdHlwZS5kZXNjcmliZVNlcnZpY2VzID0gZnVuY3Rpb24gKCkge1xuICBsZXQgc2VydmljZXMgPSB7fTtcbiAgZm9yIChsZXQgbmFtZSBpbiB0aGlzLnNlcnZpY2VzKSB7XG4gICAgbGV0IHNlcnZpY2UgPSB0aGlzLnNlcnZpY2VzW25hbWVdO1xuICAgIHNlcnZpY2VzW25hbWVdID0gc2VydmljZS5kZXNjcmlwdGlvbih0aGlzLmRlZmluaXRpb25zKTtcbiAgfVxuICByZXR1cm4gc2VydmljZXM7XG59O1xuXG5XU0RMLnByb3RvdHlwZS50b1hNTCA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMueG1sIHx8ICcnO1xufTtcblxuV1NETC5wcm90b3R5cGUueG1sVG9PYmplY3QgPSBmdW5jdGlvbiAoeG1sLCBjYWxsYmFjaykge1xuICBsZXQgc2VsZiA9IHRoaXM7XG4gIGxldCBwID0gdHlwZW9mIGNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8ge30gOiBzYXgucGFyc2VyKHRydWUpO1xuICBsZXQgb2JqZWN0TmFtZSA9IG51bGw7XG4gIGxldCByb290OiBhbnkgPSB7fTtcbiAgbGV0IHNjaGVtYSA9IHtcbiAgICBFbnZlbG9wZToge1xuICAgICAgSGVhZGVyOiB7XG4gICAgICAgIFNlY3VyaXR5OiB7XG4gICAgICAgICAgVXNlcm5hbWVUb2tlbjoge1xuICAgICAgICAgICAgVXNlcm5hbWU6ICdzdHJpbmcnLFxuICAgICAgICAgICAgUGFzc3dvcmQ6ICdzdHJpbmcnXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgQm9keToge1xuICAgICAgICBGYXVsdDoge1xuICAgICAgICAgIGZhdWx0Y29kZTogJ3N0cmluZycsXG4gICAgICAgICAgZmF1bHRzdHJpbmc6ICdzdHJpbmcnLFxuICAgICAgICAgIGRldGFpbDogJ3N0cmluZydcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfTtcbiAgbGV0IHN0YWNrOiBhbnlbXSA9IFt7IG5hbWU6IG51bGwsIG9iamVjdDogcm9vdCwgc2NoZW1hOiBzY2hlbWEgfV07XG4gIGxldCB4bWxuczogYW55ID0ge307XG5cbiAgbGV0IHJlZnMgPSB7fSwgaWQ7IC8vIHtpZDp7aHJlZnM6W10sb2JqOn0sIC4uLn1cblxuICBwLm9ub3BlbnRhZyA9IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgbGV0IG5zTmFtZSA9IG5vZGUubmFtZTtcbiAgICBsZXQgYXR0cnM6IGFueSA9IG5vZGUuYXR0cmlidXRlcztcbiAgICBsZXQgbmFtZSA9IHNwbGl0UU5hbWUobnNOYW1lKS5uYW1lLFxuICAgICAgYXR0cmlidXRlTmFtZSxcbiAgICAgIHRvcCA9IHN0YWNrW3N0YWNrLmxlbmd0aCAtIDFdLFxuICAgICAgdG9wU2NoZW1hID0gdG9wLnNjaGVtYSxcbiAgICAgIGVsZW1lbnRBdHRyaWJ1dGVzID0ge30sXG4gICAgICBoYXNOb25YbWxuc0F0dHJpYnV0ZSA9IGZhbHNlLFxuICAgICAgaGFzTmlsQXR0cmlidXRlID0gZmFsc2UsXG4gICAgICBvYmogPSB7fTtcbiAgICBsZXQgb3JpZ2luYWxOYW1lID0gbmFtZTtcblxuICAgIGlmICghb2JqZWN0TmFtZSAmJiB0b3AubmFtZSA9PT0gJ0JvZHknICYmIG5hbWUgIT09ICdGYXVsdCcpIHtcbiAgICAgIGxldCBtZXNzYWdlID0gc2VsZi5kZWZpbml0aW9ucy5tZXNzYWdlc1tuYW1lXTtcbiAgICAgIC8vIFN1cHBvcnQgUlBDL2xpdGVyYWwgbWVzc2FnZXMgd2hlcmUgcmVzcG9uc2UgYm9keSBjb250YWlucyBvbmUgZWxlbWVudCBuYW1lZFxuICAgICAgLy8gYWZ0ZXIgdGhlIG9wZXJhdGlvbiArICdSZXNwb25zZScuIFNlZSBodHRwOi8vd3d3LnczLm9yZy9UUi93c2RsI19uYW1lc1xuICAgICAgaWYgKCFtZXNzYWdlKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgLy8gRGV0ZXJtaW5lIGlmIHRoaXMgaXMgcmVxdWVzdCBvciByZXNwb25zZVxuICAgICAgICAgIGxldCBpc0lucHV0ID0gZmFsc2U7XG4gICAgICAgICAgbGV0IGlzT3V0cHV0ID0gZmFsc2U7XG4gICAgICAgICAgaWYgKCgvUmVzcG9uc2UkLykudGVzdChuYW1lKSkge1xuICAgICAgICAgICAgaXNPdXRwdXQgPSB0cnVlO1xuICAgICAgICAgICAgbmFtZSA9IG5hbWUucmVwbGFjZSgvUmVzcG9uc2UkLywgJycpO1xuICAgICAgICAgIH0gZWxzZSBpZiAoKC9SZXF1ZXN0JC8pLnRlc3QobmFtZSkpIHtcbiAgICAgICAgICAgIGlzSW5wdXQgPSB0cnVlO1xuICAgICAgICAgICAgbmFtZSA9IG5hbWUucmVwbGFjZSgvUmVxdWVzdCQvLCAnJyk7XG4gICAgICAgICAgfSBlbHNlIGlmICgoL1NvbGljaXQkLykudGVzdChuYW1lKSkge1xuICAgICAgICAgICAgaXNJbnB1dCA9IHRydWU7XG4gICAgICAgICAgICBuYW1lID0gbmFtZS5yZXBsYWNlKC9Tb2xpY2l0JC8sICcnKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLy8gTG9vayB1cCB0aGUgYXBwcm9wcmlhdGUgbWVzc2FnZSBhcyBnaXZlbiBpbiB0aGUgcG9ydFR5cGUncyBvcGVyYXRpb25zXG4gICAgICAgICAgbGV0IHBvcnRUeXBlcyA9IHNlbGYuZGVmaW5pdGlvbnMucG9ydFR5cGVzO1xuICAgICAgICAgIGxldCBwb3J0VHlwZU5hbWVzID0gT2JqZWN0LmtleXMocG9ydFR5cGVzKTtcbiAgICAgICAgICAvLyBDdXJyZW50bHkgdGhpcyBzdXBwb3J0cyBvbmx5IG9uZSBwb3J0VHlwZSBkZWZpbml0aW9uLlxuICAgICAgICAgIGxldCBwb3J0VHlwZSA9IHBvcnRUeXBlc1twb3J0VHlwZU5hbWVzWzBdXTtcbiAgICAgICAgICBpZiAoaXNJbnB1dCkge1xuICAgICAgICAgICAgbmFtZSA9IHBvcnRUeXBlLm1ldGhvZHNbbmFtZV0uaW5wdXQuJG5hbWU7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIG5hbWUgPSBwb3J0VHlwZS5tZXRob2RzW25hbWVdLm91dHB1dC4kbmFtZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgbWVzc2FnZSA9IHNlbGYuZGVmaW5pdGlvbnMubWVzc2FnZXNbbmFtZV07XG4gICAgICAgICAgLy8gJ2NhY2hlJyB0aGlzIGFsaWFzIHRvIHNwZWVkIGZ1dHVyZSBsb29rdXBzXG4gICAgICAgICAgc2VsZi5kZWZpbml0aW9ucy5tZXNzYWdlc1tvcmlnaW5hbE5hbWVdID0gc2VsZi5kZWZpbml0aW9ucy5tZXNzYWdlc1tuYW1lXTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgIGlmIChzZWxmLm9wdGlvbnMucmV0dXJuRmF1bHQpIHtcbiAgICAgICAgICAgIHAub25lcnJvcihlKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgdG9wU2NoZW1hID0gbWVzc2FnZS5kZXNjcmlwdGlvbihzZWxmLmRlZmluaXRpb25zKTtcbiAgICAgIG9iamVjdE5hbWUgPSBvcmlnaW5hbE5hbWU7XG4gICAgfVxuXG4gICAgaWYgKGF0dHJzLmhyZWYpIHtcbiAgICAgIGlkID0gYXR0cnMuaHJlZi5zdWJzdHIoMSk7XG4gICAgICBpZiAoIXJlZnNbaWRdKSB7XG4gICAgICAgIHJlZnNbaWRdID0geyBocmVmczogW10sIG9iajogbnVsbCB9O1xuICAgICAgfVxuICAgICAgcmVmc1tpZF0uaHJlZnMucHVzaCh7IHBhcjogdG9wLm9iamVjdCwga2V5OiBuYW1lLCBvYmo6IG9iaiB9KTtcbiAgICB9XG4gICAgaWYgKGlkID0gYXR0cnMuaWQpIHtcbiAgICAgIGlmICghcmVmc1tpZF0pIHtcbiAgICAgICAgcmVmc1tpZF0gPSB7IGhyZWZzOiBbXSwgb2JqOiBudWxsIH07XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy9IYW5kbGUgZWxlbWVudCBhdHRyaWJ1dGVzXG4gICAgZm9yIChhdHRyaWJ1dGVOYW1lIGluIGF0dHJzKSB7XG4gICAgICBpZiAoL154bWxuczp8XnhtbG5zJC8udGVzdChhdHRyaWJ1dGVOYW1lKSkge1xuICAgICAgICB4bWxuc1tzcGxpdFFOYW1lKGF0dHJpYnV0ZU5hbWUpLm5hbWVdID0gYXR0cnNbYXR0cmlidXRlTmFtZV07XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuICAgICAgaGFzTm9uWG1sbnNBdHRyaWJ1dGUgPSB0cnVlO1xuICAgICAgZWxlbWVudEF0dHJpYnV0ZXNbYXR0cmlidXRlTmFtZV0gPSBhdHRyc1thdHRyaWJ1dGVOYW1lXTtcbiAgICB9XG5cbiAgICBmb3IgKGF0dHJpYnV0ZU5hbWUgaW4gZWxlbWVudEF0dHJpYnV0ZXMpIHtcbiAgICAgIGxldCByZXMgPSBzcGxpdFFOYW1lKGF0dHJpYnV0ZU5hbWUpO1xuICAgICAgaWYgKHJlcy5uYW1lID09PSAnbmlsJyAmJiB4bWxuc1tyZXMucHJlZml4XSA9PT0gJ2h0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlJyAmJiBlbGVtZW50QXR0cmlidXRlc1thdHRyaWJ1dGVOYW1lXSAmJlxuICAgICAgICAoZWxlbWVudEF0dHJpYnV0ZXNbYXR0cmlidXRlTmFtZV0udG9Mb3dlckNhc2UoKSA9PT0gJ3RydWUnIHx8IGVsZW1lbnRBdHRyaWJ1dGVzW2F0dHJpYnV0ZU5hbWVdID09PSAnMScpXG4gICAgICApIHtcbiAgICAgICAgaGFzTmlsQXR0cmlidXRlID0gdHJ1ZTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKGhhc05vblhtbG5zQXR0cmlidXRlKSB7XG4gICAgICBvYmpbc2VsZi5vcHRpb25zLmF0dHJpYnV0ZXNLZXldID0gZWxlbWVudEF0dHJpYnV0ZXM7XG4gICAgfVxuXG4gICAgLy8gUGljayB1cCB0aGUgc2NoZW1hIGZvciB0aGUgdHlwZSBzcGVjaWZpZWQgaW4gZWxlbWVudCdzIHhzaTp0eXBlIGF0dHJpYnV0ZS5cbiAgICBsZXQgeHNpVHlwZVNjaGVtYTtcbiAgICBsZXQgeHNpVHlwZSA9IGVsZW1lbnRBdHRyaWJ1dGVzWyd4c2k6dHlwZSddO1xuICAgIGlmICh4c2lUeXBlKSB7XG4gICAgICBsZXQgdHlwZSA9IHNwbGl0UU5hbWUoeHNpVHlwZSk7XG4gICAgICBsZXQgdHlwZVVSSTtcbiAgICAgIGlmICh0eXBlLnByZWZpeCA9PT0gVE5TX1BSRUZJWCkge1xuICAgICAgICAvLyBJbiBjYXNlIG9mIHhzaTp0eXBlID0gXCJNeVR5cGVcIlxuICAgICAgICB0eXBlVVJJID0geG1sbnNbdHlwZS5wcmVmaXhdIHx8IHhtbG5zLnhtbG5zO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdHlwZVVSSSA9IHhtbG5zW3R5cGUucHJlZml4XTtcbiAgICAgIH1cbiAgICAgIGxldCB0eXBlRGVmID0gc2VsZi5maW5kU2NoZW1hT2JqZWN0KHR5cGVVUkksIHR5cGUubmFtZSk7XG4gICAgICBpZiAodHlwZURlZikge1xuICAgICAgICB4c2lUeXBlU2NoZW1hID0gdHlwZURlZi5kZXNjcmlwdGlvbihzZWxmLmRlZmluaXRpb25zKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAodG9wU2NoZW1hICYmIHRvcFNjaGVtYVtuYW1lICsgJ1tdJ10pIHtcbiAgICAgIG5hbWUgPSBuYW1lICsgJ1tdJztcbiAgICB9XG4gICAgc3RhY2sucHVzaCh7XG4gICAgICBuYW1lOiBvcmlnaW5hbE5hbWUsXG4gICAgICBvYmplY3Q6IG9iaixcbiAgICAgIHNjaGVtYTogKHhzaVR5cGVTY2hlbWEgfHwgKHRvcFNjaGVtYSAmJiB0b3BTY2hlbWFbbmFtZV0pKSxcbiAgICAgIGlkOiBhdHRycy5pZCxcbiAgICAgIG5pbDogaGFzTmlsQXR0cmlidXRlXG4gICAgfSk7XG4gIH07XG5cbiAgcC5vbmNsb3NldGFnID0gZnVuY3Rpb24gKG5zTmFtZSkge1xuICAgIGxldCBjdXI6IGFueSA9IHN0YWNrLnBvcCgpLFxuICAgICAgb2JqID0gY3VyLm9iamVjdCxcbiAgICAgIHRvcCA9IHN0YWNrW3N0YWNrLmxlbmd0aCAtIDFdLFxuICAgICAgdG9wT2JqZWN0ID0gdG9wLm9iamVjdCxcbiAgICAgIHRvcFNjaGVtYSA9IHRvcC5zY2hlbWEsXG4gICAgICBuYW1lID0gc3BsaXRRTmFtZShuc05hbWUpLm5hbWU7XG5cbiAgICBpZiAodHlwZW9mIGN1ci5zY2hlbWEgPT09ICdzdHJpbmcnICYmIChjdXIuc2NoZW1hID09PSAnc3RyaW5nJyB8fCAoPHN0cmluZz5jdXIuc2NoZW1hKS5zcGxpdCgnOicpWzFdID09PSAnc3RyaW5nJykpIHtcbiAgICAgIGlmICh0eXBlb2Ygb2JqID09PSAnb2JqZWN0JyAmJiBPYmplY3Qua2V5cyhvYmopLmxlbmd0aCA9PT0gMCkgb2JqID0gY3VyLm9iamVjdCA9ICcnO1xuICAgIH1cblxuICAgIGlmIChjdXIubmlsID09PSB0cnVlKSB7XG4gICAgICBpZiAoc2VsZi5vcHRpb25zLmhhbmRsZU5pbEFzTnVsbCkge1xuICAgICAgICBvYmogPSBudWxsO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChfLmlzUGxhaW5PYmplY3Qob2JqKSAmJiAhT2JqZWN0LmtleXMob2JqKS5sZW5ndGgpIHtcbiAgICAgIG9iaiA9IG51bGw7XG4gICAgfVxuXG4gICAgaWYgKHRvcFNjaGVtYSAmJiB0b3BTY2hlbWFbbmFtZSArICdbXSddKSB7XG4gICAgICBpZiAoIXRvcE9iamVjdFtuYW1lXSkge1xuICAgICAgICB0b3BPYmplY3RbbmFtZV0gPSBbXTtcbiAgICAgIH1cbiAgICAgIHRvcE9iamVjdFtuYW1lXS5wdXNoKG9iaik7XG4gICAgfSBlbHNlIGlmIChuYW1lIGluIHRvcE9iamVjdCkge1xuICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHRvcE9iamVjdFtuYW1lXSkpIHtcbiAgICAgICAgdG9wT2JqZWN0W25hbWVdID0gW3RvcE9iamVjdFtuYW1lXV07XG4gICAgICB9XG4gICAgICB0b3BPYmplY3RbbmFtZV0ucHVzaChvYmopO1xuICAgIH0gZWxzZSB7XG4gICAgICB0b3BPYmplY3RbbmFtZV0gPSBvYmo7XG4gICAgfVxuXG4gICAgaWYgKGN1ci5pZCkge1xuICAgICAgcmVmc1tjdXIuaWRdLm9iaiA9IG9iajtcbiAgICB9XG4gIH07XG5cbiAgcC5vbmNkYXRhID0gZnVuY3Rpb24gKHRleHQpIHtcbiAgICBsZXQgb3JpZ2luYWxUZXh0ID0gdGV4dDtcbiAgICB0ZXh0ID0gdHJpbSh0ZXh0KTtcbiAgICBpZiAoIXRleHQubGVuZ3RoKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKC88XFw/eG1sW1xcc1xcU10rXFw/Pi8udGVzdCh0ZXh0KSkge1xuICAgICAgbGV0IHRvcCA9IHN0YWNrW3N0YWNrLmxlbmd0aCAtIDFdO1xuICAgICAgbGV0IHZhbHVlID0gc2VsZi54bWxUb09iamVjdCh0ZXh0KTtcbiAgICAgIGlmICh0b3Aub2JqZWN0W3NlbGYub3B0aW9ucy5hdHRyaWJ1dGVzS2V5XSkge1xuICAgICAgICB0b3Aub2JqZWN0W3NlbGYub3B0aW9ucy52YWx1ZUtleV0gPSB2YWx1ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRvcC5vYmplY3QgPSB2YWx1ZTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgcC5vbnRleHQob3JpZ2luYWxUZXh0KTtcbiAgICB9XG4gIH07XG5cbiAgcC5vbmVycm9yID0gZnVuY3Rpb24gKGUpIHtcbiAgICBwLnJlc3VtZSgpO1xuICAgIHRocm93IHtcbiAgICAgIEZhdWx0OiB7XG4gICAgICAgIGZhdWx0Y29kZTogNTAwLFxuICAgICAgICBmYXVsdHN0cmluZzogJ0ludmFsaWQgWE1MJyxcbiAgICAgICAgZGV0YWlsOiBuZXcgRXJyb3IoZSkubWVzc2FnZSxcbiAgICAgICAgc3RhdHVzQ29kZTogNTAwXG4gICAgICB9XG4gICAgfTtcbiAgfTtcblxuICBwLm9udGV4dCA9IGZ1bmN0aW9uICh0ZXh0KSB7XG4gICAgbGV0IG9yaWdpbmFsVGV4dCA9IHRleHQ7XG4gICAgdGV4dCA9IHRyaW0odGV4dCk7XG4gICAgaWYgKCF0ZXh0Lmxlbmd0aCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGxldCB0b3AgPSBzdGFja1tzdGFjay5sZW5ndGggLSAxXTtcbiAgICBsZXQgbmFtZSA9IHNwbGl0UU5hbWUodG9wLnNjaGVtYSkubmFtZSxcbiAgICAgIHZhbHVlO1xuICAgIGlmIChzZWxmLm9wdGlvbnMgJiYgc2VsZi5vcHRpb25zLmN1c3RvbURlc2VyaWFsaXplciAmJiBzZWxmLm9wdGlvbnMuY3VzdG9tRGVzZXJpYWxpemVyW25hbWVdKSB7XG4gICAgICB2YWx1ZSA9IHNlbGYub3B0aW9ucy5jdXN0b21EZXNlcmlhbGl6ZXJbbmFtZV0odGV4dCwgdG9wKTtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICBpZiAobmFtZSA9PT0gJ2ludCcgfHwgbmFtZSA9PT0gJ2ludGVnZXInKSB7XG4gICAgICAgIHZhbHVlID0gcGFyc2VJbnQodGV4dCwgMTApO1xuICAgICAgfSBlbHNlIGlmIChuYW1lID09PSAnYm9vbCcgfHwgbmFtZSA9PT0gJ2Jvb2xlYW4nKSB7XG4gICAgICAgIHZhbHVlID0gdGV4dC50b0xvd2VyQ2FzZSgpID09PSAndHJ1ZScgfHwgdGV4dCA9PT0gJzEnO1xuICAgICAgfSBlbHNlIGlmIChuYW1lID09PSAnZGF0ZVRpbWUnIHx8IG5hbWUgPT09ICdkYXRlJykge1xuICAgICAgICB2YWx1ZSA9IG5ldyBEYXRlKHRleHQpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKHNlbGYub3B0aW9ucy5wcmVzZXJ2ZVdoaXRlc3BhY2UpIHtcbiAgICAgICAgICB0ZXh0ID0gb3JpZ2luYWxUZXh0O1xuICAgICAgICB9XG4gICAgICAgIC8vIGhhbmRsZSBzdHJpbmcgb3Igb3RoZXIgdHlwZXNcbiAgICAgICAgaWYgKHR5cGVvZiB0b3Aub2JqZWN0ICE9PSAnc3RyaW5nJykge1xuICAgICAgICAgIHZhbHVlID0gdGV4dDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB2YWx1ZSA9IHRvcC5vYmplY3QgKyB0ZXh0O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHRvcC5vYmplY3Rbc2VsZi5vcHRpb25zLmF0dHJpYnV0ZXNLZXldKSB7XG4gICAgICB0b3Aub2JqZWN0W3NlbGYub3B0aW9ucy52YWx1ZUtleV0gPSB2YWx1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgdG9wLm9iamVjdCA9IHZhbHVlO1xuICAgIH1cbiAgfTtcblxuICBpZiAodHlwZW9mIGNhbGxiYWNrID09PSAnZnVuY3Rpb24nKSB7XG4gICAgLy8gd2UgYmUgc3RyZWFtaW5nXG4gICAgbGV0IHNheFN0cmVhbSA9IHNheC5jcmVhdGVTdHJlYW0odHJ1ZSk7XG4gICAgc2F4U3RyZWFtLm9uKCdvcGVudGFnJywgcC5vbm9wZW50YWcpO1xuICAgIHNheFN0cmVhbS5vbignY2xvc2V0YWcnLCBwLm9uY2xvc2V0YWcpO1xuICAgIHNheFN0cmVhbS5vbignY2RhdGEnLCBwLm9uY2RhdGEpO1xuICAgIHNheFN0cmVhbS5vbigndGV4dCcsIHAub250ZXh0KTtcbiAgICB4bWwucGlwZShzYXhTdHJlYW0pXG4gICAgICAub24oJ2Vycm9yJywgZnVuY3Rpb24gKGVycikge1xuICAgICAgICBjYWxsYmFjayhlcnIpO1xuICAgICAgfSlcbiAgICAgIC5vbignZW5kJywgZnVuY3Rpb24gKCkge1xuICAgICAgICBsZXQgcjtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICByID0gZmluaXNoKCk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICByZXR1cm4gY2FsbGJhY2soZSk7XG4gICAgICAgIH1cbiAgICAgICAgY2FsbGJhY2sobnVsbCwgcik7XG4gICAgICB9KTtcbiAgICByZXR1cm47XG4gIH1cbiAgcC53cml0ZSh4bWwpLmNsb3NlKCk7XG5cbiAgcmV0dXJuIGZpbmlzaCgpO1xuXG4gIGZ1bmN0aW9uIGZpbmlzaCgpIHtcbiAgICAvLyBNdWx0aVJlZiBzdXBwb3J0OiBtZXJnZSBvYmplY3RzIGluc3RlYWQgb2YgcmVwbGFjaW5nXG4gICAgZm9yIChsZXQgbiBpbiByZWZzKSB7XG4gICAgICBsZXQgcmVmID0gcmVmc1tuXTtcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcmVmLmhyZWZzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIF8uYXNzaWduKHJlZi5ocmVmc1tpXS5vYmosIHJlZi5vYmopO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChyb290LkVudmVsb3BlKSB7XG4gICAgICBsZXQgYm9keSA9IHJvb3QuRW52ZWxvcGUuQm9keTtcbiAgICAgIGlmIChib2R5ICYmIGJvZHkuRmF1bHQpIHtcbiAgICAgICAgbGV0IGNvZGUgPSBib2R5LkZhdWx0LmZhdWx0Y29kZSAmJiBib2R5LkZhdWx0LmZhdWx0Y29kZS4kdmFsdWU7XG4gICAgICAgIGxldCBzdHJpbmcgPSBib2R5LkZhdWx0LmZhdWx0c3RyaW5nICYmIGJvZHkuRmF1bHQuZmF1bHRzdHJpbmcuJHZhbHVlO1xuICAgICAgICBsZXQgZGV0YWlsID0gYm9keS5GYXVsdC5kZXRhaWwgJiYgYm9keS5GYXVsdC5kZXRhaWwuJHZhbHVlO1xuXG4gICAgICAgIGNvZGUgPSBjb2RlIHx8IGJvZHkuRmF1bHQuZmF1bHRjb2RlO1xuICAgICAgICBzdHJpbmcgPSBzdHJpbmcgfHwgYm9keS5GYXVsdC5mYXVsdHN0cmluZztcbiAgICAgICAgZGV0YWlsID0gZGV0YWlsIHx8IGJvZHkuRmF1bHQuZGV0YWlsO1xuXG4gICAgICAgIGxldCBlcnJvcjogYW55ID0gbmV3IEVycm9yKGNvZGUgKyAnOiAnICsgc3RyaW5nICsgKGRldGFpbCA/ICc6ICcgKyBkZXRhaWwgOiAnJykpO1xuXG4gICAgICAgIGVycm9yLnJvb3QgPSByb290O1xuICAgICAgICB0aHJvdyBlcnJvcjtcbiAgICAgIH1cbiAgICAgIHJldHVybiByb290LkVudmVsb3BlO1xuICAgIH1cbiAgICByZXR1cm4gcm9vdDtcbiAgfVxufTtcblxuLyoqXG4gKiBMb29rIHVwIGEgWFNEIHR5cGUgb3IgZWxlbWVudCBieSBuYW1lc3BhY2UgVVJJIGFuZCBuYW1lXG4gKiBAcGFyYW0ge1N0cmluZ30gbnNVUkkgTmFtZXNwYWNlIFVSSVxuICogQHBhcmFtIHtTdHJpbmd9IHFuYW1lIExvY2FsIG9yIHF1YWxpZmllZCBuYW1lXG4gKiBAcmV0dXJucyB7Kn0gVGhlIFhTRCB0eXBlL2VsZW1lbnQgZGVmaW5pdGlvblxuICovXG5XU0RMLnByb3RvdHlwZS5maW5kU2NoZW1hT2JqZWN0ID0gZnVuY3Rpb24gKG5zVVJJLCBxbmFtZSkge1xuICBpZiAoIW5zVVJJIHx8ICFxbmFtZSkge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgbGV0IGRlZiA9IG51bGw7XG5cbiAgaWYgKHRoaXMuZGVmaW5pdGlvbnMuc2NoZW1hcykge1xuICAgIGxldCBzY2hlbWEgPSB0aGlzLmRlZmluaXRpb25zLnNjaGVtYXNbbnNVUkldO1xuICAgIGlmIChzY2hlbWEpIHtcbiAgICAgIGlmIChxbmFtZS5pbmRleE9mKCc6JykgIT09IC0xKSB7XG4gICAgICAgIHFuYW1lID0gcW5hbWUuc3Vic3RyaW5nKHFuYW1lLmluZGV4T2YoJzonKSArIDEsIHFuYW1lLmxlbmd0aCk7XG4gICAgICB9XG5cbiAgICAgIC8vIGlmIHRoZSBjbGllbnQgcGFzc2VkIGFuIGlucHV0IGVsZW1lbnQgd2hpY2ggaGFzIGEgYCRsb29rdXBUeXBlYCBwcm9wZXJ0eSBpbnN0ZWFkIG9mIGAkdHlwZWBcbiAgICAgIC8vIHRoZSBgZGVmYCBpcyBmb3VuZCBpbiBgc2NoZW1hLmVsZW1lbnRzYC5cbiAgICAgIGRlZiA9IHNjaGVtYS5jb21wbGV4VHlwZXNbcW5hbWVdIHx8IHNjaGVtYS50eXBlc1txbmFtZV0gfHwgc2NoZW1hLmVsZW1lbnRzW3FuYW1lXTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZGVmO1xufTtcblxuLyoqXG4gKiBDcmVhdGUgZG9jdW1lbnQgc3R5bGUgeG1sIHN0cmluZyBmcm9tIHRoZSBwYXJhbWV0ZXJzXG4gKiBAcGFyYW0ge1N0cmluZ30gbmFtZVxuICogQHBhcmFtIHsqfSBwYXJhbXNcbiAqIEBwYXJhbSB7U3RyaW5nfSBuc1ByZWZpeFxuICogQHBhcmFtIHtTdHJpbmd9IG5zVVJJXG4gKiBAcGFyYW0ge1N0cmluZ30gdHlwZVxuICovXG5XU0RMLnByb3RvdHlwZS5vYmplY3RUb0RvY3VtZW50WE1MID0gZnVuY3Rpb24gKG5hbWUsIHBhcmFtcywgbnNQcmVmaXgsIG5zVVJJLCB0eXBlKSB7XG4gIC8vSWYgdXNlciBzdXBwbGllcyBYTUwgYWxyZWFkeSwganVzdCB1c2UgdGhhdC4gIFhNTCBEZWNsYXJhdGlvbiBzaG91bGQgbm90IGJlIHByZXNlbnQuXG4gIGlmIChwYXJhbXMgJiYgcGFyYW1zLl94bWwpIHtcbiAgICByZXR1cm4gcGFyYW1zLl94bWw7XG4gIH1cbiAgbGV0IGFyZ3MgPSB7fTtcbiAgYXJnc1tuYW1lXSA9IHBhcmFtcztcbiAgbGV0IHBhcmFtZXRlclR5cGVPYmogPSB0eXBlID8gdGhpcy5maW5kU2NoZW1hT2JqZWN0KG5zVVJJLCB0eXBlKSA6IG51bGw7XG4gIHJldHVybiB0aGlzLm9iamVjdFRvWE1MKGFyZ3MsIG51bGwsIG5zUHJlZml4LCBuc1VSSSwgdHJ1ZSwgbnVsbCwgcGFyYW1ldGVyVHlwZU9iaik7XG59O1xuXG4vKipcbiAqIENyZWF0ZSBSUEMgc3R5bGUgeG1sIHN0cmluZyBmcm9tIHRoZSBwYXJhbWV0ZXJzXG4gKiBAcGFyYW0ge1N0cmluZ30gbmFtZVxuICogQHBhcmFtIHsqfSBwYXJhbXNcbiAqIEBwYXJhbSB7U3RyaW5nfSBuc1ByZWZpeFxuICogQHBhcmFtIHtTdHJpbmd9IG5zVVJJXG4gKiBAcmV0dXJucyB7c3RyaW5nfVxuICovXG5XU0RMLnByb3RvdHlwZS5vYmplY3RUb1JwY1hNTCA9IGZ1bmN0aW9uIChuYW1lLCBwYXJhbXMsIG5zUHJlZml4LCBuc1VSSSwgaXNQYXJ0cykge1xuICBsZXQgcGFydHMgPSBbXTtcbiAgbGV0IGRlZnMgPSB0aGlzLmRlZmluaXRpb25zO1xuICBsZXQgbnNBdHRyTmFtZSA9ICdfeG1sbnMnO1xuXG4gIG5zUHJlZml4ID0gbnNQcmVmaXggfHwgZmluZFByZWZpeChkZWZzLnhtbG5zLCBuc1VSSSk7XG5cbiAgbnNVUkkgPSBuc1VSSSB8fCBkZWZzLnhtbG5zW25zUHJlZml4XTtcbiAgbnNQcmVmaXggPSBuc1ByZWZpeCA9PT0gVE5TX1BSRUZJWCA/ICcnIDogKG5zUHJlZml4ICsgJzonKTtcblxuICBwYXJ0cy5wdXNoKFsnPCcsIG5zUHJlZml4LCBuYW1lLCAnPiddLmpvaW4oJycpKTtcblxuICBmb3IgKGxldCBrZXkgaW4gcGFyYW1zKSB7XG4gICAgaWYgKCFwYXJhbXMuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgY29udGludWU7XG4gICAgfVxuICAgIGlmIChrZXkgIT09IG5zQXR0ck5hbWUpIHtcbiAgICAgIGxldCB2YWx1ZSA9IHBhcmFtc1trZXldO1xuICAgICAgbGV0IHByZWZpeGVkS2V5ID0gKGlzUGFydHMgPyAnJyA6IG5zUHJlZml4KSArIGtleTtcbiAgICAgIGxldCBhdHRyaWJ1dGVzID0gW107XG4gICAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZS5oYXNPd25Qcm9wZXJ0eSh0aGlzLm9wdGlvbnMuYXR0cmlidXRlc0tleSkpIHtcbiAgICAgICAgbGV0IGF0dHJzID0gdmFsdWVbdGhpcy5vcHRpb25zLmF0dHJpYnV0ZXNLZXldO1xuICAgICAgICBmb3IgKGxldCBuIGluIGF0dHJzKSB7XG4gICAgICAgICAgYXR0cmlidXRlcy5wdXNoKCcgJyArIG4gKyAnPScgKyAnXCInICsgYXR0cnNbbl0gKyAnXCInKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcGFydHMucHVzaChbJzwnLCBwcmVmaXhlZEtleV0uY29uY2F0KGF0dHJpYnV0ZXMpLmNvbmNhdCgnPicpLmpvaW4oJycpKTtcbiAgICAgIHBhcnRzLnB1c2goKHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcpID8gdGhpcy5vYmplY3RUb1hNTCh2YWx1ZSwga2V5LCBuc1ByZWZpeCwgbnNVUkkpIDogeG1sRXNjYXBlKHZhbHVlKSk7XG4gICAgICBwYXJ0cy5wdXNoKFsnPC8nLCBwcmVmaXhlZEtleSwgJz4nXS5qb2luKCcnKSk7XG4gICAgfVxuICB9XG4gIHBhcnRzLnB1c2goWyc8LycsIG5zUHJlZml4LCBuYW1lLCAnPiddLmpvaW4oJycpKTtcbiAgcmV0dXJuIHBhcnRzLmpvaW4oJycpO1xufTtcblxuXG5mdW5jdGlvbiBhcHBlbmRDb2xvbihucykge1xuICByZXR1cm4gKG5zICYmIG5zLmNoYXJBdChucy5sZW5ndGggLSAxKSAhPT0gJzonKSA/IG5zICsgJzonIDogbnM7XG59XG5cbmZ1bmN0aW9uIG5vQ29sb25OYW1lU3BhY2UobnMpIHtcbiAgcmV0dXJuIChucyAmJiBucy5jaGFyQXQobnMubGVuZ3RoIC0gMSkgPT09ICc6JykgPyBucy5zdWJzdHJpbmcoMCwgbnMubGVuZ3RoIC0gMSkgOiBucztcbn1cblxuV1NETC5wcm90b3R5cGUuaXNJZ25vcmVkTmFtZVNwYWNlID0gZnVuY3Rpb24gKG5zKSB7XG4gIHJldHVybiB0aGlzLm9wdGlvbnMuaWdub3JlZE5hbWVzcGFjZXMuaW5kZXhPZihucykgPiAtMTtcbn07XG5cbldTREwucHJvdG90eXBlLmZpbHRlck91dElnbm9yZWROYW1lU3BhY2UgPSBmdW5jdGlvbiAobnMpIHtcbiAgbGV0IG5hbWVzcGFjZSA9IG5vQ29sb25OYW1lU3BhY2UobnMpO1xuICByZXR1cm4gdGhpcy5pc0lnbm9yZWROYW1lU3BhY2UobmFtZXNwYWNlKSA/ICcnIDogbmFtZXNwYWNlO1xufTtcblxuXG5cbi8qKlxuICogQ29udmVydCBhbiBvYmplY3QgdG8gWE1MLiAgVGhpcyBpcyBhIHJlY3Vyc2l2ZSBtZXRob2QgYXMgaXQgY2FsbHMgaXRzZWxmLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmogdGhlIG9iamVjdCB0byBjb252ZXJ0LlxuICogQHBhcmFtIHtTdHJpbmd9IG5hbWUgdGhlIG5hbWUgb2YgdGhlIGVsZW1lbnQgKGlmIHRoZSBvYmplY3QgYmVpbmcgdHJhdmVyc2VkIGlzXG4gKiBhbiBlbGVtZW50KS5cbiAqIEBwYXJhbSB7U3RyaW5nfSBuc1ByZWZpeCB0aGUgbmFtZXNwYWNlIHByZWZpeCBvZiB0aGUgb2JqZWN0IEkuRS4geHNkLlxuICogQHBhcmFtIHtTdHJpbmd9IG5zVVJJIHRoZSBmdWxsIG5hbWVzcGFjZSBvZiB0aGUgb2JqZWN0IEkuRS4gaHR0cDovL3czLm9yZy9zY2hlbWEuXG4gKiBAcGFyYW0ge0Jvb2xlYW59IGlzRmlyc3Qgd2hldGhlciBvciBub3QgdGhpcyBpcyB0aGUgZmlyc3QgaXRlbSBiZWluZyB0cmF2ZXJzZWQuXG4gKiBAcGFyYW0gez99IHhtbG5zQXR0clxuICogQHBhcmFtIHs/fSBwYXJhbWV0ZXJUeXBlT2JqZWN0XG4gKiBAcGFyYW0ge05hbWVzcGFjZUNvbnRleHR9IG5zQ29udGV4dCBOYW1lc3BhY2UgY29udGV4dFxuICovXG5XU0RMLnByb3RvdHlwZS5vYmplY3RUb1hNTCA9IGZ1bmN0aW9uIChvYmosIG5hbWUsIG5zUHJlZml4LCBuc1VSSSwgaXNGaXJzdCwgeG1sbnNBdHRyLCBzY2hlbWFPYmplY3QsIG5zQ29udGV4dCkge1xuICBsZXQgc2VsZiA9IHRoaXM7XG4gIGxldCBzY2hlbWEgPSB0aGlzLmRlZmluaXRpb25zLnNjaGVtYXNbbnNVUkldO1xuXG4gIGxldCBwYXJlbnROc1ByZWZpeCA9IG5zUHJlZml4ID8gbnNQcmVmaXgucGFyZW50IDogdW5kZWZpbmVkO1xuICBpZiAodHlwZW9mIHBhcmVudE5zUHJlZml4ICE9PSAndW5kZWZpbmVkJykge1xuICAgIC8vd2UgZ290IHRoZSBwYXJlbnROc1ByZWZpeCBmb3Igb3VyIGFycmF5LiBzZXR0aW5nIHRoZSBuYW1lc3BhY2UtbGV0aWFibGUgYmFjayB0byB0aGUgY3VycmVudCBuYW1lc3BhY2Ugc3RyaW5nXG4gICAgbnNQcmVmaXggPSBuc1ByZWZpeC5jdXJyZW50O1xuICB9XG5cbiAgcGFyZW50TnNQcmVmaXggPSBub0NvbG9uTmFtZVNwYWNlKHBhcmVudE5zUHJlZml4KTtcbiAgaWYgKHRoaXMuaXNJZ25vcmVkTmFtZVNwYWNlKHBhcmVudE5zUHJlZml4KSkge1xuICAgIHBhcmVudE5zUHJlZml4ID0gJyc7XG4gIH1cblxuICBsZXQgc29hcEhlYWRlciA9ICFzY2hlbWE7XG4gIGxldCBxdWFsaWZpZWQgPSBzY2hlbWEgJiYgc2NoZW1hLiRlbGVtZW50Rm9ybURlZmF1bHQgPT09ICdxdWFsaWZpZWQnO1xuICBsZXQgcGFydHMgPSBbXTtcbiAgbGV0IHByZWZpeE5hbWVzcGFjZSA9IChuc1ByZWZpeCB8fCBxdWFsaWZpZWQpICYmIG5zUHJlZml4ICE9PSBUTlNfUFJFRklYO1xuXG4gIGxldCB4bWxuc0F0dHJpYiA9ICcnO1xuICBpZiAobnNVUkkgJiYgaXNGaXJzdCkge1xuICAgIGlmIChzZWxmLm9wdGlvbnMub3ZlcnJpZGVSb290RWxlbWVudCAmJiBzZWxmLm9wdGlvbnMub3ZlcnJpZGVSb290RWxlbWVudC54bWxuc0F0dHJpYnV0ZXMpIHtcbiAgICAgIHNlbGYub3B0aW9ucy5vdmVycmlkZVJvb3RFbGVtZW50LnhtbG5zQXR0cmlidXRlcy5mb3JFYWNoKGZ1bmN0aW9uIChhdHRyaWJ1dGUpIHtcbiAgICAgICAgeG1sbnNBdHRyaWIgKz0gJyAnICsgYXR0cmlidXRlLm5hbWUgKyAnPVwiJyArIGF0dHJpYnV0ZS52YWx1ZSArICdcIic7XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHByZWZpeE5hbWVzcGFjZSAmJiAhdGhpcy5pc0lnbm9yZWROYW1lU3BhY2UobnNQcmVmaXgpKSB7XG4gICAgICAgIC8vIHJlc29sdmUgdGhlIHByZWZpeCBuYW1lc3BhY2VcbiAgICAgICAgeG1sbnNBdHRyaWIgKz0gJyB4bWxuczonICsgbnNQcmVmaXggKyAnPVwiJyArIG5zVVJJICsgJ1wiJztcbiAgICAgIH1cbiAgICAgIC8vIG9ubHkgYWRkIGRlZmF1bHQgbmFtZXNwYWNlIGlmIHRoZSBzY2hlbWEgZWxlbWVudEZvcm1EZWZhdWx0IGlzIHF1YWxpZmllZFxuICAgICAgaWYgKHF1YWxpZmllZCB8fCBzb2FwSGVhZGVyKSB4bWxuc0F0dHJpYiArPSAnIHhtbG5zPVwiJyArIG5zVVJJICsgJ1wiJztcbiAgICB9XG4gIH1cblxuICBpZiAoIW5zQ29udGV4dCkge1xuICAgIG5zQ29udGV4dCA9IG5ldyBOYW1lc3BhY2VDb250ZXh0KCk7XG4gICAgbnNDb250ZXh0LmRlY2xhcmVOYW1lc3BhY2UobnNQcmVmaXgsIG5zVVJJKTtcbiAgfSBlbHNlIHtcbiAgICBuc0NvbnRleHQucHVzaENvbnRleHQoKTtcbiAgfVxuXG4gIC8vIGV4cGxpY2l0bHkgdXNlIHhtbG5zIGF0dHJpYnV0ZSBpZiBhdmFpbGFibGVcbiAgaWYgKHhtbG5zQXR0ciAmJiAhKHNlbGYub3B0aW9ucy5vdmVycmlkZVJvb3RFbGVtZW50ICYmIHNlbGYub3B0aW9ucy5vdmVycmlkZVJvb3RFbGVtZW50LnhtbG5zQXR0cmlidXRlcykpIHtcbiAgICB4bWxuc0F0dHJpYiA9IHhtbG5zQXR0cjtcbiAgfVxuXG4gIGxldCBucyA9ICcnO1xuXG4gIGlmIChzZWxmLm9wdGlvbnMub3ZlcnJpZGVSb290RWxlbWVudCAmJiBpc0ZpcnN0KSB7XG4gICAgbnMgPSBzZWxmLm9wdGlvbnMub3ZlcnJpZGVSb290RWxlbWVudC5uYW1lc3BhY2U7XG4gIH0gZWxzZSBpZiAocHJlZml4TmFtZXNwYWNlICYmIChxdWFsaWZpZWQgfHwgaXNGaXJzdCB8fCBzb2FwSGVhZGVyKSAmJiAhdGhpcy5pc0lnbm9yZWROYW1lU3BhY2UobnNQcmVmaXgpKSB7XG4gICAgbnMgPSBuc1ByZWZpeDtcbiAgfVxuXG4gIGxldCBpLCBuO1xuICAvLyBzdGFydCBidWlsZGluZyBvdXQgWE1MIHN0cmluZy5cbiAgaWYgKEFycmF5LmlzQXJyYXkob2JqKSkge1xuICAgIGZvciAoaSA9IDAsIG4gPSBvYmoubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XG4gICAgICBsZXQgaXRlbSA9IG9ialtpXTtcbiAgICAgIGxldCBhcnJheUF0dHIgPSBzZWxmLnByb2Nlc3NBdHRyaWJ1dGVzKGl0ZW0sIG5zQ29udGV4dCksXG4gICAgICAgIGNvcnJlY3RPdXRlck5zUHJlZml4ID0gcGFyZW50TnNQcmVmaXggfHwgbnM7IC8vdXNpbmcgdGhlIHBhcmVudCBuYW1lc3BhY2UgcHJlZml4IGlmIGdpdmVuXG5cbiAgICAgIGxldCBib2R5ID0gc2VsZi5vYmplY3RUb1hNTChpdGVtLCBuYW1lLCBuc1ByZWZpeCwgbnNVUkksIGZhbHNlLCBudWxsLCBzY2hlbWFPYmplY3QsIG5zQ29udGV4dCk7XG5cbiAgICAgIGxldCBvcGVuaW5nVGFnUGFydHMgPSBbJzwnLCBhcHBlbmRDb2xvbihjb3JyZWN0T3V0ZXJOc1ByZWZpeCksIG5hbWUsIGFycmF5QXR0ciwgeG1sbnNBdHRyaWJdO1xuXG4gICAgICBpZiAoYm9keSA9PT0gJycgJiYgc2VsZi5vcHRpb25zLnVzZUVtcHR5VGFnKSB7XG4gICAgICAgIC8vIFVzZSBlbXB0eSAoc2VsZi1jbG9zaW5nKSB0YWdzIGlmIG5vIGNvbnRlbnRzXG4gICAgICAgIG9wZW5pbmdUYWdQYXJ0cy5wdXNoKCcgLz4nKTtcbiAgICAgICAgcGFydHMucHVzaChvcGVuaW5nVGFnUGFydHMuam9pbignJykpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgb3BlbmluZ1RhZ1BhcnRzLnB1c2goJz4nKTtcbiAgICAgICAgaWYgKHNlbGYub3B0aW9ucy5uYW1lc3BhY2VBcnJheUVsZW1lbnRzIHx8IGkgPT09IDApIHtcbiAgICAgICAgICBwYXJ0cy5wdXNoKG9wZW5pbmdUYWdQYXJ0cy5qb2luKCcnKSk7XG4gICAgICAgIH1cbiAgICAgICAgcGFydHMucHVzaChib2R5KTtcbiAgICAgICAgaWYgKHNlbGYub3B0aW9ucy5uYW1lc3BhY2VBcnJheUVsZW1lbnRzIHx8IGkgPT09IG4gLSAxKSB7XG4gICAgICAgICAgcGFydHMucHVzaChbJzwvJywgYXBwZW5kQ29sb24oY29ycmVjdE91dGVyTnNQcmVmaXgpLCBuYW1lLCAnPiddLmpvaW4oJycpKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSBlbHNlIGlmICh0eXBlb2Ygb2JqID09PSAnb2JqZWN0Jykge1xuICAgIGZvciAobmFtZSBpbiBvYmopIHtcbiAgICAgIGlmICghb2JqLmhhc093blByb3BlcnR5KG5hbWUpKSBjb250aW51ZTtcbiAgICAgIC8vZG9uJ3QgcHJvY2VzcyBhdHRyaWJ1dGVzIGFzIGVsZW1lbnRcbiAgICAgIGlmIChuYW1lID09PSBzZWxmLm9wdGlvbnMuYXR0cmlidXRlc0tleSkge1xuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cbiAgICAgIC8vSXRzIHRoZSB2YWx1ZSBvZiBhIHhtbCBvYmplY3QuIFJldHVybiBpdCBkaXJlY3RseS5cbiAgICAgIGlmIChuYW1lID09PSBzZWxmLm9wdGlvbnMueG1sS2V5KSB7XG4gICAgICAgIG5zQ29udGV4dC5wb3BDb250ZXh0KCk7XG4gICAgICAgIHJldHVybiBvYmpbbmFtZV07XG4gICAgICB9XG4gICAgICAvL0l0cyB0aGUgdmFsdWUgb2YgYW4gaXRlbS4gUmV0dXJuIGl0IGRpcmVjdGx5LlxuICAgICAgaWYgKG5hbWUgPT09IHNlbGYub3B0aW9ucy52YWx1ZUtleSkge1xuICAgICAgICBuc0NvbnRleHQucG9wQ29udGV4dCgpO1xuICAgICAgICByZXR1cm4geG1sRXNjYXBlKG9ialtuYW1lXSk7XG4gICAgICB9XG5cbiAgICAgIGxldCBjaGlsZCA9IG9ialtuYW1lXTtcbiAgICAgIGlmICh0eXBlb2YgY2hpbGQgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuXG4gICAgICBsZXQgYXR0ciA9IHNlbGYucHJvY2Vzc0F0dHJpYnV0ZXMoY2hpbGQsIG5zQ29udGV4dCk7XG5cbiAgICAgIGxldCB2YWx1ZSA9ICcnO1xuICAgICAgbGV0IG5vblN1Yk5hbWVTcGFjZSA9ICcnO1xuICAgICAgbGV0IGVtcHR5Tm9uU3ViTmFtZVNwYWNlID0gZmFsc2U7XG5cbiAgICAgIGxldCBuYW1lV2l0aE5zUmVnZXggPSAvXihbXjpdKyk6KFteOl0rKSQvLmV4ZWMobmFtZSk7XG4gICAgICBpZiAobmFtZVdpdGhOc1JlZ2V4KSB7XG4gICAgICAgIG5vblN1Yk5hbWVTcGFjZSA9IG5hbWVXaXRoTnNSZWdleFsxXSArICc6JztcbiAgICAgICAgbmFtZSA9IG5hbWVXaXRoTnNSZWdleFsyXTtcbiAgICAgIH0gZWxzZSBpZiAobmFtZVswXSA9PT0gJzonKSB7XG4gICAgICAgIGVtcHR5Tm9uU3ViTmFtZVNwYWNlID0gdHJ1ZTtcbiAgICAgICAgbmFtZSA9IG5hbWUuc3Vic3RyKDEpO1xuICAgICAgfVxuXG4gICAgICBpZiAoaXNGaXJzdCkge1xuICAgICAgICB2YWx1ZSA9IHNlbGYub2JqZWN0VG9YTUwoY2hpbGQsIG5hbWUsIG5zUHJlZml4LCBuc1VSSSwgZmFsc2UsIG51bGwsIHNjaGVtYU9iamVjdCwgbnNDb250ZXh0KTtcbiAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgaWYgKHNlbGYuZGVmaW5pdGlvbnMuc2NoZW1hcykge1xuICAgICAgICAgIGlmIChzY2hlbWEpIHtcbiAgICAgICAgICAgIGxldCBjaGlsZFNjaGVtYU9iamVjdCA9IHNlbGYuZmluZENoaWxkU2NoZW1hT2JqZWN0KHNjaGVtYU9iamVjdCwgbmFtZSk7XG4gICAgICAgICAgICAvL2ZpbmQgc3ViIG5hbWVzcGFjZSBpZiBub3QgYSBwcmltaXRpdmVcbiAgICAgICAgICAgIGlmIChjaGlsZFNjaGVtYU9iamVjdCAmJlxuICAgICAgICAgICAgICAoKGNoaWxkU2NoZW1hT2JqZWN0LiR0eXBlICYmIChjaGlsZFNjaGVtYU9iamVjdC4kdHlwZS5pbmRleE9mKCd4c2Q6JykgPT09IC0xKSkgfHxcbiAgICAgICAgICAgICAgICBjaGlsZFNjaGVtYU9iamVjdC4kcmVmIHx8IGNoaWxkU2NoZW1hT2JqZWN0LiRuYW1lKSkge1xuICAgICAgICAgICAgICAvKmlmIHRoZSBiYXNlIG5hbWUgc3BhY2Ugb2YgdGhlIGNoaWxkcmVuIGlzIG5vdCBpbiB0aGUgaW5nb3JlZFNjaGVtYU5hbXNwYWNlcyB3ZSB1c2UgaXQuXG4gICAgICAgICAgICAgICBUaGlzIGlzIGJlY2F1c2UgaW4gc29tZSBzZXJ2aWNlcyB0aGUgY2hpbGQgbm9kZXMgZG8gbm90IG5lZWQgdGhlIGJhc2VOYW1lU3BhY2UuXG4gICAgICAgICAgICAgICAqL1xuXG4gICAgICAgICAgICAgIGxldCBjaGlsZE5zUHJlZml4OiBhbnkgPSAnJztcbiAgICAgICAgICAgICAgbGV0IGNoaWxkTmFtZSA9ICcnO1xuICAgICAgICAgICAgICBsZXQgY2hpbGROc1VSSTtcbiAgICAgICAgICAgICAgbGV0IGNoaWxkWG1sbnNBdHRyaWIgPSAnJztcblxuICAgICAgICAgICAgICBsZXQgZWxlbWVudFFOYW1lID0gY2hpbGRTY2hlbWFPYmplY3QuJHJlZiB8fCBjaGlsZFNjaGVtYU9iamVjdC4kbmFtZTtcbiAgICAgICAgICAgICAgaWYgKGVsZW1lbnRRTmFtZSkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnRRTmFtZSA9IHNwbGl0UU5hbWUoZWxlbWVudFFOYW1lKTtcbiAgICAgICAgICAgICAgICBjaGlsZE5hbWUgPSBlbGVtZW50UU5hbWUubmFtZTtcbiAgICAgICAgICAgICAgICBpZiAoZWxlbWVudFFOYW1lLnByZWZpeCA9PT0gVE5TX1BSRUZJWCkge1xuICAgICAgICAgICAgICAgICAgLy8gTG9jYWwgZWxlbWVudFxuICAgICAgICAgICAgICAgICAgY2hpbGROc1VSSSA9IGNoaWxkU2NoZW1hT2JqZWN0LiR0YXJnZXROYW1lc3BhY2U7XG4gICAgICAgICAgICAgICAgICBjaGlsZE5zUHJlZml4ID0gbnNDb250ZXh0LnJlZ2lzdGVyTmFtZXNwYWNlKGNoaWxkTnNVUkkpO1xuICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuaXNJZ25vcmVkTmFtZVNwYWNlKGNoaWxkTnNQcmVmaXgpKSB7XG4gICAgICAgICAgICAgICAgICAgIGNoaWxkTnNQcmVmaXggPSBuc1ByZWZpeDtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgY2hpbGROc1ByZWZpeCA9IGVsZW1lbnRRTmFtZS5wcmVmaXg7XG4gICAgICAgICAgICAgICAgICBpZiAodGhpcy5pc0lnbm9yZWROYW1lU3BhY2UoY2hpbGROc1ByZWZpeCkpIHtcbiAgICAgICAgICAgICAgICAgICAgY2hpbGROc1ByZWZpeCA9IG5zUHJlZml4O1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgY2hpbGROc1VSSSA9IHNjaGVtYS54bWxuc1tjaGlsZE5zUHJlZml4XSB8fCBzZWxmLmRlZmluaXRpb25zLnhtbG5zW2NoaWxkTnNQcmVmaXhdO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGxldCB1bnF1YWxpZmllZCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIC8vIENoZWNrIHF1YWxpZmljYXRpb24gZm9ybSBmb3IgbG9jYWwgZWxlbWVudHNcbiAgICAgICAgICAgICAgICBpZiAoY2hpbGRTY2hlbWFPYmplY3QuJG5hbWUgJiYgY2hpbGRTY2hlbWFPYmplY3QudGFyZ2V0TmFtZXNwYWNlID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgIGlmIChjaGlsZFNjaGVtYU9iamVjdC4kZm9ybSA9PT0gJ3VucXVhbGlmaWVkJykge1xuICAgICAgICAgICAgICAgICAgICB1bnF1YWxpZmllZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGNoaWxkU2NoZW1hT2JqZWN0LiRmb3JtID09PSAncXVhbGlmaWVkJykge1xuICAgICAgICAgICAgICAgICAgICB1bnF1YWxpZmllZCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdW5xdWFsaWZpZWQgPSBzY2hlbWEuJGVsZW1lbnRGb3JtRGVmYXVsdCAhPT0gJ3F1YWxpZmllZCc7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmICh1bnF1YWxpZmllZCkge1xuICAgICAgICAgICAgICAgICAgY2hpbGROc1ByZWZpeCA9ICcnO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmIChjaGlsZE5zVVJJICYmIGNoaWxkTnNQcmVmaXgpIHtcbiAgICAgICAgICAgICAgICAgIGlmIChuc0NvbnRleHQuZGVjbGFyZU5hbWVzcGFjZShjaGlsZE5zUHJlZml4LCBjaGlsZE5zVVJJKSkge1xuICAgICAgICAgICAgICAgICAgICBjaGlsZFhtbG5zQXR0cmliID0gJyB4bWxuczonICsgY2hpbGROc1ByZWZpeCArICc9XCInICsgY2hpbGROc1VSSSArICdcIic7XG4gICAgICAgICAgICAgICAgICAgIHhtbG5zQXR0cmliICs9IGNoaWxkWG1sbnNBdHRyaWI7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgbGV0IHJlc29sdmVkQ2hpbGRTY2hlbWFPYmplY3Q7XG4gICAgICAgICAgICAgIGlmIChjaGlsZFNjaGVtYU9iamVjdC4kdHlwZSkge1xuICAgICAgICAgICAgICAgIGxldCB0eXBlUU5hbWUgPSBzcGxpdFFOYW1lKGNoaWxkU2NoZW1hT2JqZWN0LiR0eXBlKTtcbiAgICAgICAgICAgICAgICBsZXQgdHlwZVByZWZpeCA9IHR5cGVRTmFtZS5wcmVmaXg7XG4gICAgICAgICAgICAgICAgbGV0IHR5cGVVUkkgPSBzY2hlbWEueG1sbnNbdHlwZVByZWZpeF0gfHwgc2VsZi5kZWZpbml0aW9ucy54bWxuc1t0eXBlUHJlZml4XTtcbiAgICAgICAgICAgICAgICBjaGlsZE5zVVJJID0gdHlwZVVSSTtcbiAgICAgICAgICAgICAgICBpZiAodHlwZVVSSSAhPT0gJ2h0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hJyAmJiB0eXBlUHJlZml4ICE9PSBUTlNfUFJFRklYKSB7XG4gICAgICAgICAgICAgICAgICAvLyBBZGQgdGhlIHByZWZpeC9uYW1lc3BhY2UgbWFwcGluZywgYnV0IG5vdCBkZWNsYXJlIGl0XG4gICAgICAgICAgICAgICAgICBuc0NvbnRleHQuYWRkTmFtZXNwYWNlKHR5cGVQcmVmaXgsIHR5cGVVUkkpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXNvbHZlZENoaWxkU2NoZW1hT2JqZWN0ID1cbiAgICAgICAgICAgICAgICAgIHNlbGYuZmluZFNjaGVtYVR5cGUodHlwZVFOYW1lLm5hbWUsIHR5cGVVUkkpIHx8IGNoaWxkU2NoZW1hT2JqZWN0O1xuICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHJlc29sdmVkQ2hpbGRTY2hlbWFPYmplY3QgPVxuICAgICAgICAgICAgICAgICAgc2VsZi5maW5kU2NoZW1hT2JqZWN0KGNoaWxkTnNVUkksIGNoaWxkTmFtZSkgfHwgY2hpbGRTY2hlbWFPYmplY3Q7XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICBpZiAoY2hpbGRTY2hlbWFPYmplY3QuJGJhc2VOYW1lU3BhY2UgJiYgdGhpcy5vcHRpb25zLmlnbm9yZUJhc2VOYW1lU3BhY2VzKSB7XG4gICAgICAgICAgICAgICAgY2hpbGROc1ByZWZpeCA9IG5zUHJlZml4O1xuICAgICAgICAgICAgICAgIGNoaWxkTnNVUkkgPSBuc1VSSTtcbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuaWdub3JlQmFzZU5hbWVTcGFjZXMpIHtcbiAgICAgICAgICAgICAgICBjaGlsZE5zUHJlZml4ID0gJyc7XG4gICAgICAgICAgICAgICAgY2hpbGROc1VSSSA9ICcnO1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgbnMgPSBjaGlsZE5zUHJlZml4O1xuXG4gICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KGNoaWxkKSkge1xuICAgICAgICAgICAgICAgIC8vZm9yIGFycmF5cywgd2UgbmVlZCB0byByZW1lbWJlciB0aGUgY3VycmVudCBuYW1lc3BhY2VcbiAgICAgICAgICAgICAgICBjaGlsZE5zUHJlZml4ID0ge1xuICAgICAgICAgICAgICAgICAgY3VycmVudDogY2hpbGROc1ByZWZpeCxcbiAgICAgICAgICAgICAgICAgIHBhcmVudDogbnNcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIC8vcGFyZW50IChhcnJheSkgYWxyZWFkeSBnb3QgdGhlIG5hbWVzcGFjZVxuICAgICAgICAgICAgICAgIGNoaWxkWG1sbnNBdHRyaWIgPSBudWxsO1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgdmFsdWUgPSBzZWxmLm9iamVjdFRvWE1MKGNoaWxkLCBuYW1lLCBjaGlsZE5zUHJlZml4LCBjaGlsZE5zVVJJLFxuICAgICAgICAgICAgICAgIGZhbHNlLCBjaGlsZFhtbG5zQXR0cmliLCByZXNvbHZlZENoaWxkU2NoZW1hT2JqZWN0LCBuc0NvbnRleHQpO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChvYmpbc2VsZi5vcHRpb25zLmF0dHJpYnV0ZXNLZXldICYmIG9ialtzZWxmLm9wdGlvbnMuYXR0cmlidXRlc0tleV0ueHNpX3R5cGUpIHtcbiAgICAgICAgICAgICAgLy9pZiBwYXJlbnQgb2JqZWN0IGhhcyBjb21wbGV4IHR5cGUgZGVmaW5lZCBhbmQgY2hpbGQgbm90IGZvdW5kIGluIHBhcmVudFxuICAgICAgICAgICAgICBsZXQgY29tcGxldGVDaGlsZFBhcmFtVHlwZU9iamVjdCA9IHNlbGYuZmluZENoaWxkU2NoZW1hT2JqZWN0KFxuICAgICAgICAgICAgICAgIG9ialtzZWxmLm9wdGlvbnMuYXR0cmlidXRlc0tleV0ueHNpX3R5cGUudHlwZSxcbiAgICAgICAgICAgICAgICBvYmpbc2VsZi5vcHRpb25zLmF0dHJpYnV0ZXNLZXldLnhzaV90eXBlLnhtbG5zKTtcblxuICAgICAgICAgICAgICBub25TdWJOYW1lU3BhY2UgPSBvYmpbc2VsZi5vcHRpb25zLmF0dHJpYnV0ZXNLZXldLnhzaV90eXBlLnByZWZpeDtcbiAgICAgICAgICAgICAgbnNDb250ZXh0LmFkZE5hbWVzcGFjZShvYmpbc2VsZi5vcHRpb25zLmF0dHJpYnV0ZXNLZXldLnhzaV90eXBlLnByZWZpeCxcbiAgICAgICAgICAgICAgICBvYmpbc2VsZi5vcHRpb25zLmF0dHJpYnV0ZXNLZXldLnhzaV90eXBlLnhtbG5zKTtcbiAgICAgICAgICAgICAgdmFsdWUgPSBzZWxmLm9iamVjdFRvWE1MKGNoaWxkLCBuYW1lLCBvYmpbc2VsZi5vcHRpb25zLmF0dHJpYnV0ZXNLZXldLnhzaV90eXBlLnByZWZpeCxcbiAgICAgICAgICAgICAgICBvYmpbc2VsZi5vcHRpb25zLmF0dHJpYnV0ZXNLZXldLnhzaV90eXBlLnhtbG5zLCBmYWxzZSwgbnVsbCwgbnVsbCwgbnNDb250ZXh0KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KGNoaWxkKSkge1xuICAgICAgICAgICAgICAgIG5hbWUgPSBub25TdWJOYW1lU3BhY2UgKyBuYW1lO1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgdmFsdWUgPSBzZWxmLm9iamVjdFRvWE1MKGNoaWxkLCBuYW1lLCBuc1ByZWZpeCwgbnNVUkksIGZhbHNlLCBudWxsLCBudWxsLCBuc0NvbnRleHQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB2YWx1ZSA9IHNlbGYub2JqZWN0VG9YTUwoY2hpbGQsIG5hbWUsIG5zUHJlZml4LCBuc1VSSSwgZmFsc2UsIG51bGwsIG51bGwsIG5zQ29udGV4dCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIG5zID0gbm9Db2xvbk5hbWVTcGFjZShucyk7XG4gICAgICBpZiAocHJlZml4TmFtZXNwYWNlICYmICFxdWFsaWZpZWQgJiYgaXNGaXJzdCAmJiAhc2VsZi5vcHRpb25zLm92ZXJyaWRlUm9vdEVsZW1lbnQpIHtcbiAgICAgICAgbnMgPSBuc1ByZWZpeDtcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5pc0lnbm9yZWROYW1lU3BhY2UobnMpKSB7XG4gICAgICAgIG5zID0gJyc7XG4gICAgICB9XG5cbiAgICAgIGxldCB1c2VFbXB0eVRhZyA9ICF2YWx1ZSAmJiBzZWxmLm9wdGlvbnMudXNlRW1wdHlUYWc7XG4gICAgICBpZiAoIUFycmF5LmlzQXJyYXkoY2hpbGQpKSB7XG4gICAgICAgIC8vIHN0YXJ0IHRhZ1xuICAgICAgICBwYXJ0cy5wdXNoKFsnPCcsIGVtcHR5Tm9uU3ViTmFtZVNwYWNlID8gJycgOiBhcHBlbmRDb2xvbihub25TdWJOYW1lU3BhY2UgfHwgbnMpLCBuYW1lLCBhdHRyLCB4bWxuc0F0dHJpYixcbiAgICAgICAgICAoY2hpbGQgPT09IG51bGwgPyAnIHhzaTpuaWw9XCJ0cnVlXCInIDogJycpLFxuICAgICAgICAgIHVzZUVtcHR5VGFnID8gJyAvPicgOiAnPidcbiAgICAgICAgXS5qb2luKCcnKSk7XG4gICAgICB9XG5cbiAgICAgIGlmICghdXNlRW1wdHlUYWcpIHtcbiAgICAgICAgcGFydHMucHVzaCh2YWx1ZSk7XG4gICAgICAgIGlmICghQXJyYXkuaXNBcnJheShjaGlsZCkpIHtcbiAgICAgICAgICAvLyBlbmQgdGFnXG4gICAgICAgICAgcGFydHMucHVzaChbJzwvJywgZW1wdHlOb25TdWJOYW1lU3BhY2UgPyAnJyA6IGFwcGVuZENvbG9uKG5vblN1Yk5hbWVTcGFjZSB8fCBucyksIG5hbWUsICc+J10uam9pbignJykpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9IGVsc2UgaWYgKG9iaiAhPT0gdW5kZWZpbmVkKSB7XG4gICAgcGFydHMucHVzaCgoc2VsZi5vcHRpb25zLmVzY2FwZVhNTCkgPyB4bWxFc2NhcGUob2JqKSA6IG9iaik7XG4gIH1cbiAgbnNDb250ZXh0LnBvcENvbnRleHQoKTtcbiAgcmV0dXJuIHBhcnRzLmpvaW4oJycpO1xufTtcblxuV1NETC5wcm90b3R5cGUucHJvY2Vzc0F0dHJpYnV0ZXMgPSBmdW5jdGlvbiAoY2hpbGQsIG5zQ29udGV4dCkge1xuICBsZXQgYXR0ciA9ICcnO1xuXG4gIGlmIChjaGlsZCA9PT0gbnVsbCkge1xuICAgIGNoaWxkID0gW107XG4gIH1cblxuICBsZXQgYXR0ck9iaiA9IGNoaWxkW3RoaXMub3B0aW9ucy5hdHRyaWJ1dGVzS2V5XTtcbiAgaWYgKGF0dHJPYmogJiYgYXR0ck9iai54c2lfdHlwZSkge1xuICAgIGxldCB4c2lUeXBlID0gYXR0ck9iai54c2lfdHlwZTtcblxuICAgIGxldCBwcmVmaXggPSB4c2lUeXBlLnByZWZpeCB8fCB4c2lUeXBlLm5hbWVzcGFjZTtcbiAgICAvLyBHZW5lcmF0ZSBhIG5ldyBuYW1lc3BhY2UgZm9yIGNvbXBsZXggZXh0ZW5zaW9uIGlmIG9uZSBub3QgcHJvdmlkZWRcbiAgICBpZiAoIXByZWZpeCkge1xuICAgICAgcHJlZml4ID0gbnNDb250ZXh0LnJlZ2lzdGVyTmFtZXNwYWNlKHhzaVR5cGUueG1sbnMpO1xuICAgIH0gZWxzZSB7XG4gICAgICBuc0NvbnRleHQuZGVjbGFyZU5hbWVzcGFjZShwcmVmaXgsIHhzaVR5cGUueG1sbnMpO1xuICAgIH1cbiAgICB4c2lUeXBlLnByZWZpeCA9IHByZWZpeDtcbiAgfVxuXG5cbiAgaWYgKGF0dHJPYmopIHtcbiAgICBmb3IgKGxldCBhdHRyS2V5IGluIGF0dHJPYmopIHtcbiAgICAgIC8vaGFuZGxlIGNvbXBsZXggZXh0ZW5zaW9uIHNlcGFyYXRlbHlcbiAgICAgIGlmIChhdHRyS2V5ID09PSAneHNpX3R5cGUnKSB7XG4gICAgICAgIGxldCBhdHRyVmFsdWUgPSBhdHRyT2JqW2F0dHJLZXldO1xuICAgICAgICBhdHRyICs9ICcgeHNpOnR5cGU9XCInICsgYXR0clZhbHVlLnByZWZpeCArICc6JyArIGF0dHJWYWx1ZS50eXBlICsgJ1wiJztcbiAgICAgICAgYXR0ciArPSAnIHhtbG5zOicgKyBhdHRyVmFsdWUucHJlZml4ICsgJz1cIicgKyBhdHRyVmFsdWUueG1sbnMgKyAnXCInO1xuXG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgYXR0ciArPSAnICcgKyBhdHRyS2V5ICsgJz1cIicgKyB4bWxFc2NhcGUoYXR0ck9ialthdHRyS2V5XSkgKyAnXCInO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiBhdHRyO1xufTtcblxuLyoqXG4gKiBMb29rIHVwIGEgc2NoZW1hIHR5cGUgZGVmaW5pdGlvblxuICogQHBhcmFtIG5hbWVcbiAqIEBwYXJhbSBuc1VSSVxuICogQHJldHVybnMgeyp9XG4gKi9cbldTREwucHJvdG90eXBlLmZpbmRTY2hlbWFUeXBlID0gZnVuY3Rpb24gKG5hbWUsIG5zVVJJKSB7XG4gIGlmICghdGhpcy5kZWZpbml0aW9ucy5zY2hlbWFzIHx8ICFuYW1lIHx8ICFuc1VSSSkge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgbGV0IHNjaGVtYSA9IHRoaXMuZGVmaW5pdGlvbnMuc2NoZW1hc1tuc1VSSV07XG4gIGlmICghc2NoZW1hIHx8ICFzY2hlbWEuY29tcGxleFR5cGVzKSB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICByZXR1cm4gc2NoZW1hLmNvbXBsZXhUeXBlc1tuYW1lXTtcbn07XG5cbldTREwucHJvdG90eXBlLmZpbmRDaGlsZFNjaGVtYU9iamVjdCA9IGZ1bmN0aW9uIChwYXJhbWV0ZXJUeXBlT2JqLCBjaGlsZE5hbWUsIGJhY2t0cmFjZSkge1xuICBpZiAoIXBhcmFtZXRlclR5cGVPYmogfHwgIWNoaWxkTmFtZSkge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgaWYgKCFiYWNrdHJhY2UpIHtcbiAgICBiYWNrdHJhY2UgPSBbXTtcbiAgfVxuXG4gIGlmIChiYWNrdHJhY2UuaW5kZXhPZihwYXJhbWV0ZXJUeXBlT2JqKSA+PSAwKSB7XG4gICAgLy8gV2UndmUgcmVjdXJzZWQgYmFjayB0byBvdXJzZWx2ZXM7IGJyZWFrLlxuICAgIHJldHVybiBudWxsO1xuICB9IGVsc2Uge1xuICAgIGJhY2t0cmFjZSA9IGJhY2t0cmFjZS5jb25jYXQoW3BhcmFtZXRlclR5cGVPYmpdKTtcbiAgfVxuXG4gIGxldCBmb3VuZCA9IG51bGwsXG4gICAgaSA9IDAsXG4gICAgY2hpbGQsXG4gICAgcmVmO1xuXG4gIGlmIChBcnJheS5pc0FycmF5KHBhcmFtZXRlclR5cGVPYmouJGxvb2t1cFR5cGVzKSAmJiBwYXJhbWV0ZXJUeXBlT2JqLiRsb29rdXBUeXBlcy5sZW5ndGgpIHtcbiAgICBsZXQgdHlwZXMgPSBwYXJhbWV0ZXJUeXBlT2JqLiRsb29rdXBUeXBlcztcblxuICAgIGZvciAoaSA9IDA7IGkgPCB0eXBlcy5sZW5ndGg7IGkrKykge1xuICAgICAgbGV0IHR5cGVPYmogPSB0eXBlc1tpXTtcblxuICAgICAgaWYgKHR5cGVPYmouJG5hbWUgPT09IGNoaWxkTmFtZSkge1xuICAgICAgICBmb3VuZCA9IHR5cGVPYmo7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGxldCBvYmplY3QgPSBwYXJhbWV0ZXJUeXBlT2JqO1xuICBpZiAob2JqZWN0LiRuYW1lID09PSBjaGlsZE5hbWUgJiYgb2JqZWN0Lm5hbWUgPT09ICdlbGVtZW50Jykge1xuICAgIHJldHVybiBvYmplY3Q7XG4gIH1cbiAgaWYgKG9iamVjdC4kcmVmKSB7XG4gICAgcmVmID0gc3BsaXRRTmFtZShvYmplY3QuJHJlZik7XG4gICAgaWYgKHJlZi5uYW1lID09PSBjaGlsZE5hbWUpIHtcbiAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgfVxuICB9XG5cbiAgbGV0IGNoaWxkTnNVUkk7XG5cbiAgLy8gd2FudCB0byBhdm9pZCB1bmVjZXNzYXJ5IHJlY3Vyc2lvbiB0byBpbXByb3ZlIHBlcmZvcm1hbmNlXG4gIGlmIChvYmplY3QuJHR5cGUgJiYgYmFja3RyYWNlLmxlbmd0aCA9PT0gMSkge1xuICAgIGxldCB0eXBlSW5mbyA9IHNwbGl0UU5hbWUob2JqZWN0LiR0eXBlKTtcbiAgICBpZiAodHlwZUluZm8ucHJlZml4ID09PSBUTlNfUFJFRklYKSB7XG4gICAgICBjaGlsZE5zVVJJID0gcGFyYW1ldGVyVHlwZU9iai4kdGFyZ2V0TmFtZXNwYWNlO1xuICAgIH0gZWxzZSB7XG4gICAgICBjaGlsZE5zVVJJID0gdGhpcy5kZWZpbml0aW9ucy54bWxuc1t0eXBlSW5mby5wcmVmaXhdO1xuICAgIH1cbiAgICBsZXQgdHlwZURlZiA9IHRoaXMuZmluZFNjaGVtYVR5cGUodHlwZUluZm8ubmFtZSwgY2hpbGROc1VSSSk7XG4gICAgaWYgKHR5cGVEZWYpIHtcbiAgICAgIHJldHVybiB0aGlzLmZpbmRDaGlsZFNjaGVtYU9iamVjdCh0eXBlRGVmLCBjaGlsZE5hbWUsIGJhY2t0cmFjZSk7XG4gICAgfVxuICB9XG5cbiAgaWYgKG9iamVjdC5jaGlsZHJlbikge1xuICAgIGZvciAoaSA9IDAsIGNoaWxkOyBjaGlsZCA9IG9iamVjdC5jaGlsZHJlbltpXTsgaSsrKSB7XG4gICAgICBmb3VuZCA9IHRoaXMuZmluZENoaWxkU2NoZW1hT2JqZWN0KGNoaWxkLCBjaGlsZE5hbWUsIGJhY2t0cmFjZSk7XG4gICAgICBpZiAoZm91bmQpIHtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG5cbiAgICAgIGlmIChjaGlsZC4kYmFzZSkge1xuICAgICAgICBsZXQgYmFzZVFOYW1lID0gc3BsaXRRTmFtZShjaGlsZC4kYmFzZSk7XG4gICAgICAgIGxldCBjaGlsZE5hbWVTcGFjZSA9IGJhc2VRTmFtZS5wcmVmaXggPT09IFROU19QUkVGSVggPyAnJyA6IGJhc2VRTmFtZS5wcmVmaXg7XG4gICAgICAgIGNoaWxkTnNVUkkgPSBjaGlsZC54bWxuc1tiYXNlUU5hbWUucHJlZml4XSB8fCB0aGlzLmRlZmluaXRpb25zLnhtbG5zW2Jhc2VRTmFtZS5wcmVmaXhdO1xuXG4gICAgICAgIGxldCBmb3VuZEJhc2UgPSB0aGlzLmZpbmRTY2hlbWFUeXBlKGJhc2VRTmFtZS5uYW1lLCBjaGlsZE5zVVJJKTtcblxuICAgICAgICBpZiAoZm91bmRCYXNlKSB7XG4gICAgICAgICAgZm91bmQgPSB0aGlzLmZpbmRDaGlsZFNjaGVtYU9iamVjdChmb3VuZEJhc2UsIGNoaWxkTmFtZSwgYmFja3RyYWNlKTtcblxuICAgICAgICAgIGlmIChmb3VuZCkge1xuICAgICAgICAgICAgZm91bmQuJGJhc2VOYW1lU3BhY2UgPSBjaGlsZE5hbWVTcGFjZTtcbiAgICAgICAgICAgIGZvdW5kLiR0eXBlID0gY2hpbGROYW1lU3BhY2UgKyAnOicgKyBjaGlsZE5hbWU7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgfVxuXG4gIGlmICghZm91bmQgJiYgb2JqZWN0LiRuYW1lID09PSBjaGlsZE5hbWUpIHtcbiAgICByZXR1cm4gb2JqZWN0O1xuICB9XG5cbiAgcmV0dXJuIGZvdW5kO1xufTtcblxuV1NETC5wcm90b3R5cGUuX3BhcnNlID0gZnVuY3Rpb24gKHhtbCkge1xuICBsZXQgc2VsZiA9IHRoaXMsXG4gICAgcCA9IHNheC5wYXJzZXIodHJ1ZSksXG4gICAgc3RhY2sgPSBbXSxcbiAgICByb290ID0gbnVsbCxcbiAgICB0eXBlcyA9IG51bGwsXG4gICAgc2NoZW1hID0gbnVsbCxcbiAgICBvcHRpb25zID0gc2VsZi5vcHRpb25zO1xuXG4gIHAub25vcGVudGFnID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICBsZXQgbnNOYW1lID0gbm9kZS5uYW1lO1xuICAgIGxldCBhdHRycyA9IG5vZGUuYXR0cmlidXRlcztcblxuICAgIGxldCB0b3AgPSBzdGFja1tzdGFjay5sZW5ndGggLSAxXTtcbiAgICBsZXQgbmFtZTtcbiAgICBpZiAodG9wKSB7XG4gICAgICB0cnkge1xuICAgICAgICB0b3Auc3RhcnRFbGVtZW50KHN0YWNrLCBuc05hbWUsIGF0dHJzLCBvcHRpb25zKTtcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgaWYgKHNlbGYub3B0aW9ucy5zdHJpY3QpIHtcbiAgICAgICAgICB0aHJvdyBlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHN0YWNrLnB1c2gobmV3IEVsZW1lbnQobnNOYW1lLCBhdHRycywgb3B0aW9ucykpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIG5hbWUgPSBzcGxpdFFOYW1lKG5zTmFtZSkubmFtZTtcbiAgICAgIGlmIChuYW1lID09PSAnZGVmaW5pdGlvbnMnKSB7XG4gICAgICAgIHJvb3QgPSBuZXcgRGVmaW5pdGlvbnNFbGVtZW50KG5zTmFtZSwgYXR0cnMsIG9wdGlvbnMpO1xuICAgICAgICBzdGFjay5wdXNoKHJvb3QpO1xuICAgICAgfSBlbHNlIGlmIChuYW1lID09PSAnc2NoZW1hJykge1xuICAgICAgICAvLyBTaGltIGEgc3RydWN0dXJlIGluIGhlcmUgdG8gYWxsb3cgdGhlIHByb3BlciBvYmplY3RzIHRvIGJlIGNyZWF0ZWQgd2hlbiBtZXJnaW5nIGJhY2suXG4gICAgICAgIHJvb3QgPSBuZXcgRGVmaW5pdGlvbnNFbGVtZW50KCdkZWZpbml0aW9ucycsIHt9LCB7fSk7XG4gICAgICAgIHR5cGVzID0gbmV3IFR5cGVzRWxlbWVudCgndHlwZXMnLCB7fSwge30pO1xuICAgICAgICBzY2hlbWEgPSBuZXcgU2NoZW1hRWxlbWVudChuc05hbWUsIGF0dHJzLCBvcHRpb25zKTtcbiAgICAgICAgdHlwZXMuYWRkQ2hpbGQoc2NoZW1hKTtcbiAgICAgICAgcm9vdC5hZGRDaGlsZCh0eXBlcyk7XG4gICAgICAgIHN0YWNrLnB1c2goc2NoZW1hKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVW5leHBlY3RlZCByb290IGVsZW1lbnQgb2YgV1NETCBvciBpbmNsdWRlJyk7XG4gICAgICB9XG4gICAgfVxuICB9O1xuXG4gIHAub25jbG9zZXRhZyA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgbGV0IHRvcCA9IHN0YWNrW3N0YWNrLmxlbmd0aCAtIDFdO1xuICAgIGFzc2VydCh0b3AsICdVbm1hdGNoZWQgY2xvc2UgdGFnOiAnICsgbmFtZSk7XG5cbiAgICB0b3AuZW5kRWxlbWVudChzdGFjaywgbmFtZSk7XG4gIH07XG5cbiAgcC53cml0ZSh4bWwpLmNsb3NlKCk7XG5cbiAgcmV0dXJuIHJvb3Q7XG59O1xuXG5XU0RMLnByb3RvdHlwZS5fZnJvbVhNTCA9IGZ1bmN0aW9uICh4bWwpIHtcbiAgdGhpcy5kZWZpbml0aW9ucyA9IHRoaXMuX3BhcnNlKHhtbCk7XG4gIHRoaXMuZGVmaW5pdGlvbnMuZGVzY3JpcHRpb25zID0ge1xuICAgIHR5cGVzOiB7fVxuICB9O1xuICB0aGlzLnhtbCA9IHhtbDtcbn07XG5cbldTREwucHJvdG90eXBlLl9mcm9tU2VydmljZXMgPSBmdW5jdGlvbiAoc2VydmljZXMpIHtcblxufTtcblxuXG5cbldTREwucHJvdG90eXBlLl94bWxuc01hcCA9IGZ1bmN0aW9uICgpIHtcbiAgbGV0IHhtbG5zID0gdGhpcy5kZWZpbml0aW9ucy54bWxucztcbiAgbGV0IHN0ciA9ICcnO1xuICBmb3IgKGxldCBhbGlhcyBpbiB4bWxucykge1xuICAgIGlmIChhbGlhcyA9PT0gJycgfHwgYWxpYXMgPT09IFROU19QUkVGSVgpIHtcbiAgICAgIGNvbnRpbnVlO1xuICAgIH1cbiAgICBsZXQgbnMgPSB4bWxuc1thbGlhc107XG4gICAgc3dpdGNoIChucykge1xuICAgICAgY2FzZSBcImh0dHA6Ly94bWwuYXBhY2hlLm9yZy94bWwtc29hcFwiOiAvLyBhcGFjaGVzb2FwXG4gICAgICBjYXNlIFwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3NkbC9cIjogLy8gd3NkbFxuICAgICAgY2FzZSBcImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzZGwvc29hcC9cIjogLy8gd3NkbHNvYXBcbiAgICAgIGNhc2UgXCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93c2RsL3NvYXAxMi9cIjogLy8gd3NkbHNvYXAxMlxuICAgICAgY2FzZSBcImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3NvYXAvZW5jb2RpbmcvXCI6IC8vIHNvYXBlbmNcbiAgICAgIGNhc2UgXCJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYVwiOiAvLyB4c2RcbiAgICAgICAgY29udGludWU7XG4gICAgfVxuICAgIGlmICh+bnMuaW5kZXhPZignaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvJykpIHtcbiAgICAgIGNvbnRpbnVlO1xuICAgIH1cbiAgICBpZiAofm5zLmluZGV4T2YoJ2h0dHA6Ly93d3cudzMub3JnLycpKSB7XG4gICAgICBjb250aW51ZTtcbiAgICB9XG4gICAgaWYgKH5ucy5pbmRleE9mKCdodHRwOi8veG1sLmFwYWNoZS5vcmcvJykpIHtcbiAgICAgIGNvbnRpbnVlO1xuICAgIH1cbiAgICBzdHIgKz0gJyB4bWxuczonICsgYWxpYXMgKyAnPVwiJyArIG5zICsgJ1wiJztcbiAgfVxuICByZXR1cm4gc3RyO1xufTtcblxuLypcbiAqIEhhdmUgYW5vdGhlciBmdW5jdGlvbiB0byBsb2FkIHByZXZpb3VzIFdTRExzIGFzIHdlXG4gKiBkb24ndCB3YW50IHRoaXMgdG8gYmUgaW52b2tlZCBleHRlcm5hbGx5IChleHBlY3QgZm9yIHRlc3RzKVxuICogVGhpcyB3aWxsIGF0dGVtcHQgdG8gZml4IGNpcmN1bGFyIGRlcGVuZGVuY2llcyB3aXRoIFhTRCBmaWxlcyxcbiAqIEdpdmVuXG4gKiAtIGZpbGUud3NkbFxuICogICAtIHhzOmltcG9ydCBuYW1lc3BhY2U9XCJBXCIgc2NoZW1hTG9jYXRpb246IEEueHNkXG4gKiAtIEEueHNkXG4gKiAgIC0geHM6aW1wb3J0IG5hbWVzcGFjZT1cIkJcIiBzY2hlbWFMb2NhdGlvbjogQi54c2RcbiAqIC0gQi54c2RcbiAqICAgLSB4czppbXBvcnQgbmFtZXNwYWNlPVwiQVwiIHNjaGVtYUxvY2F0aW9uOiBBLnhzZFxuICogZmlsZS53c2RsIHdpbGwgc3RhcnQgbG9hZGluZywgaW1wb3J0IEEsIHRoZW4gQSB3aWxsIGltcG9ydCBCLCB3aGljaCB3aWxsIHRoZW4gaW1wb3J0IEFcbiAqIEJlY2F1c2UgQSBoYXMgYWxyZWFkeSBzdGFydGVkIHRvIGxvYWQgcHJldmlvdXNseSBpdCB3aWxsIGJlIHJldHVybmVkIHJpZ2h0IGF3YXkgYW5kXG4gKiBoYXZlIGFuIGludGVybmFsIGNpcmN1bGFyIHJlZmVyZW5jZVxuICogQiB3b3VsZCB0aGVuIGNvbXBsZXRlIGxvYWRpbmcsIHRoZW4gQSwgdGhlbiBmaWxlLndzZGxcbiAqIEJ5IHRoZSB0aW1lIGZpbGUgQSBzdGFydHMgcHJvY2Vzc2luZyBpdHMgaW5jbHVkZXMgaXRzIGRlZmluaXRpb25zIHdpbGwgYmUgYWxyZWFkeSBsb2FkZWQsXG4gKiB0aGlzIGlzIHRoZSBvbmx5IHRoaW5nIHRoYXQgQiB3aWxsIGRlcGVuZCBvbiB3aGVuIFwib3BlbmluZ1wiIEFcbiAqL1xuZnVuY3Rpb24gb3Blbl93c2RsX3JlY3Vyc2l2ZSh1cmksIG9wdGlvbnMpOiBQcm9taXNlPGFueT4ge1xuICBsZXQgZnJvbUNhY2hlLFxuICAgIFdTRExfQ0FDSEU7XG5cbiAgLy8gaWYgKHR5cGVvZiBvcHRpb25zID09PSAnZnVuY3Rpb24nKSB7XG4gIC8vICAgY2FsbGJhY2sgPSBvcHRpb25zO1xuICAvLyAgIG9wdGlvbnMgPSB7fTtcbiAgLy8gfVxuXG4gIFdTRExfQ0FDSEUgPSBvcHRpb25zLldTRExfQ0FDSEU7XG5cbiAgaWYgKGZyb21DYWNoZSA9IFdTRExfQ0FDSEVbdXJpXSkge1xuICAgIC8vIHJldHVybiBjYWxsYmFjay5jYWxsKGZyb21DYWNoZSwgbnVsbCwgZnJvbUNhY2hlKTtcbiAgICByZXR1cm4gZnJvbUNhY2hlO1xuICB9XG5cbiAgcmV0dXJuIG9wZW5fd3NkbCh1cmksIG9wdGlvbnMpO1xufVxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gb3Blbl93c2RsKHVyaSwgb3B0aW9ucyk6IFByb21pc2U8YW55PiB7XG4gIC8vIGlmICh0eXBlb2Ygb3B0aW9ucyA9PT0gJ2Z1bmN0aW9uJykge1xuICAvLyAgIGNhbGxiYWNrID0gb3B0aW9ucztcbiAgLy8gICBvcHRpb25zID0ge307XG4gIC8vIH1cblxuICAvLyBpbml0aWFsaXplIGNhY2hlIHdoZW4gY2FsbGluZyBvcGVuX3dzZGwgZGlyZWN0bHlcbiAgbGV0IFdTRExfQ0FDSEUgPSBvcHRpb25zLldTRExfQ0FDSEUgfHwge307XG4gIGxldCByZXF1ZXN0X2hlYWRlcnMgPSBvcHRpb25zLndzZGxfaGVhZGVycztcbiAgbGV0IHJlcXVlc3Rfb3B0aW9ucyA9IG9wdGlvbnMud3NkbF9vcHRpb25zO1xuXG4gIC8vIGxldCB3c2RsO1xuICAvLyBpZiAoIS9eaHR0cHM/Oi8udGVzdCh1cmkpKSB7XG4gIC8vICAgLy8gZGVidWcoJ1JlYWRpbmcgZmlsZTogJXMnLCB1cmkpO1xuICAvLyAgIC8vIGZzLnJlYWRGaWxlKHVyaSwgJ3V0ZjgnLCBmdW5jdGlvbihlcnIsIGRlZmluaXRpb24pIHtcbiAgLy8gICAvLyAgIGlmIChlcnIpIHtcbiAgLy8gICAvLyAgICAgY2FsbGJhY2soZXJyKTtcbiAgLy8gICAvLyAgIH1cbiAgLy8gICAvLyAgIGVsc2Uge1xuICAvLyAgIC8vICAgICB3c2RsID0gbmV3IFdTREwoZGVmaW5pdGlvbiwgdXJpLCBvcHRpb25zKTtcbiAgLy8gICAvLyAgICAgV1NETF9DQUNIRVsgdXJpIF0gPSB3c2RsO1xuICAvLyAgIC8vICAgICB3c2RsLldTRExfQ0FDSEUgPSBXU0RMX0NBQ0hFO1xuICAvLyAgIC8vICAgICB3c2RsLm9uUmVhZHkoY2FsbGJhY2spO1xuICAvLyAgIC8vICAgfVxuICAvLyAgIC8vIH0pO1xuICAvLyB9XG4gIC8vIGVsc2Uge1xuICAvLyAgIGRlYnVnKCdSZWFkaW5nIHVybDogJXMnLCB1cmkpO1xuICAvLyAgIGxldCBodHRwQ2xpZW50ID0gb3B0aW9ucy5odHRwQ2xpZW50IHx8IG5ldyBIdHRwQ2xpZW50KG9wdGlvbnMpO1xuICAvLyAgIGh0dHBDbGllbnQucmVxdWVzdCh1cmksIG51bGwgLyogb3B0aW9ucyAqLywgZnVuY3Rpb24oZXJyLCByZXNwb25zZSwgZGVmaW5pdGlvbikge1xuICAvLyAgICAgaWYgKGVycikge1xuICAvLyAgICAgICBjYWxsYmFjayhlcnIpO1xuICAvLyAgICAgfSBlbHNlIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5zdGF0dXNDb2RlID09PSAyMDApIHtcbiAgLy8gICAgICAgd3NkbCA9IG5ldyBXU0RMKGRlZmluaXRpb24sIHVyaSwgb3B0aW9ucyk7XG4gIC8vICAgICAgIFdTRExfQ0FDSEVbIHVyaSBdID0gd3NkbDtcbiAgLy8gICAgICAgd3NkbC5XU0RMX0NBQ0hFID0gV1NETF9DQUNIRTtcbiAgLy8gICAgICAgd3NkbC5vblJlYWR5KGNhbGxiYWNrKTtcbiAgLy8gICAgIH0gZWxzZSB7XG4gIC8vICAgICAgIGNhbGxiYWNrKG5ldyBFcnJvcignSW52YWxpZCBXU0RMIFVSTDogJyArIHVyaSArIFwiXFxuXFxuXFxyIENvZGU6IFwiICsgcmVzcG9uc2Uuc3RhdHVzQ29kZSArIFwiXFxuXFxuXFxyIFJlc3BvbnNlIEJvZHk6IFwiICsgcmVzcG9uc2UuYm9keSkpO1xuICAvLyAgICAgfVxuICAvLyAgIH0sIHJlcXVlc3RfaGVhZGVycywgcmVxdWVzdF9vcHRpb25zKTtcbiAgLy8gfVxuICAvLyByZXR1cm4gd3NkbDtcblxuICBjb25zb2xlLmxvZygnUmVhZGluZyB1cmw6ICVzJywgdXJpKTtcbiAgY29uc3QgaHR0cENsaWVudDogSHR0cENsaWVudCA9IG9wdGlvbnMuaHR0cENsaWVudDtcbiAgY29uc3Qgd3NkbERlZiA9IGF3YWl0IGh0dHBDbGllbnQuZ2V0KHVyaSwgeyByZXNwb25zZVR5cGU6ICd0ZXh0JyB9KS50b1Byb21pc2UoKTtcbiAgY29uc3Qgd3NkbE9iaiA9IGF3YWl0IG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiB7XG4gICAgY29uc3Qgd3NkbCA9IG5ldyBXU0RMKHdzZGxEZWYsIHVyaSwgb3B0aW9ucyk7XG4gICAgV1NETF9DQUNIRVt1cmldID0gd3NkbDtcbiAgICB3c2RsLldTRExfQ0FDSEUgPSBXU0RMX0NBQ0hFO1xuICAgIHdzZGwub25SZWFkeShyZXNvbHZlKHdzZGwpKTtcbiAgfSk7XG4gIGNvbnNvbGUubG9nKFwid3NkbFwiLCB3c2RsT2JqKVxuICByZXR1cm4gd3NkbE9iajtcbn1cbiJdfQ==
